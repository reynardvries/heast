# TU Delft Hybrid Electric Aircraft Sizing Tool (HEAST) 
# Release v1.0
#
# Code available at: data.4TU.nl (doi: 10.4121/494f6cd8-cd4a-4d1a-8d02-8340a6da2b5b)



##########################
## Description 
##########################

The Hybrid Electric Aircraft Sizing Tool (HEAST) was developed at Delft University of Technology between 2017 and 2023 and is intended to be used to perform the conceptual design of aircraft with (hybrid-) electric propulsion systems. These drivetrains open the space to new propulsor layouts commonly know as "distributed propulsion", where the aerodynamic interaction between propulsors and airframe can be significant. More specifically, the tool focuses on the preliminary sizing part of the conceptual design phase, where the designer translates the top-level aircraft requirements and design choices into a first layout and estimate of weights, wing area, and installed power. The tool is therefore a sizing tool and *not* an analysis tool: it cannot assess the performance of a predefined geometry/weight/etc.

The theory behind this tool can be found in Ref. [1]. In essence, it takes the preliminary sizing methods of traditional well-known aircraft design handbooks (Refs. [2-4]) and expands them to account for (1) the hybrid-electric powertrain and (2) the aero-propulsive interaction effects. Additional information can be found in Ref. [5]. The tool has Class-I weight and aerodynamic models which have been cross-validated in Ref. [6]. An example where this tool is integrated in a broader design tool and evaluated for different configurations can be found in Ref. [7]. Class-II weight and aerodynamic methods have also been implemented. However, these are only applicable to CS-25/Part25 tube-and-wing aircraft with propeller propulsion, and have not been extensively tested or validated. Therefore, any Class-II analysis should be performed with additional caution.

The tool is implemented in MATLAB and is versatile because it allows the user to size aircraft with a wide array of powertrain types (conventional, serial, parallel, electric, turboelectric,...), propulsor layouts (leading-edge propellers, over-the-wing propellers,...), and can easily be adapted to account for different performance requirements. However, it requires a significant amount of input and is not coded efficiently, containing numerous (sub-) functions with many if-checks and iteration loops. It is therefore only recommended for users who have a detailed understanding of aircraft conceptual design *and* are versatile in the MATLAB language. Improvements in code hygiene are recommended.



##########################
## History
##########################

- v1.0 (12 July 2024): first public release of the code (TUD internal version ID: v171).



##########################
## Authors 
##########################

This tool has been developed at Delft University of Technology by Reynard de Vries, with support from Maurice Hoogreef, Malcom Brown, and Roelof Vos.



##########################
## Requirements  
##########################

The tool requires MathWorks MATLAB software, version R2018b or later. The tool can be run directly in the MATLAB command window by executing "main.m". It makes use of two MATLAB toolboxes:

- Statistics & machine learning toolbox (for the "nanmean" function)
- Aerospace toolbox (for the "atmosisa" function)

Note that the tool can also run without these additional toolboxes if the two functions above are implemented directly in the code, which is relatively straightforward.



##########################
## Structure
##########################

This section describes the files that constitute the HEAST tool. Most as MATLAB functions or MATLAB scripts (e.g. top-level scripts or things that have been bundled into a separate script file to make things more readable). In the subfolders there are also some data files. In the following list, indents indicate the file is a sub-function of the next level higher.

## 0. MAIN TOP-LEVEL SCRIPTS
    Main.m (this one executes the actual tool)
    MainLoop_Sweep3D_v2.m (this can be used to execute the tool in a parameter sweep)
    	MainCalledInLoop.m

## 1. SIZING FOR POWER (WING/POWER LOADING DIAGRAMS)
    AssignAeroProperties.m
    CheckInput.m
    ComputeReferencePowerLoading.m (for Class-I weights only; also calls WP_WS_diagram.m)
    WP_WS_diagram.m (based on theory of Ref. [1])
    	ComputeDesignPoint.m
    	ConstraintAEOClimbGradient.m
    	ConstraintAEOClimbRate.m
    	ConstraintCruisSpeed.m
    	ConstraintLandingStallSpeed.m
    	ConstraintOEIClimbGradient.m
    	ConstraintOEIClimbRate.m
    	ConstraintTakeOffDistance_v3.m (based on TOP method)
    	ConstraintTakeOffDistance_v5.m (based on Torenbeek 2013 method)
		    ComputeThrustLoading_vKnown.m (used in several constraint functions)
		    ComputeThrustLoading_vMargin.m  (used in several constraint functions)
		    SizePowertrain.m (used in several constraint functions)

## 2. SIZING FOR ENERGY (MISSION ANALYSIS) AND WEIGHT ESTIMATION
    MissionAnalysis.m
    	MissionAnalysisClimb.m
    	MissionAnalysisCruise.m
    	MissionAnalysisDescent.m
    	MissionAnalysisLoiter.m
    	ComputeWeights_v2.m
		    WeightEstimationClass1.m (based on weight breakdown of Ref. [1])
		    WeightEstimationClass2_v2.m (based on weight breakdown of Ref. [2])
    			WeightEstimationClass2_airframeStruct.m
			    WeightEstimationClass2_operItems.m
			    WeightEstimationClass2_propGroup.mn
			    WeightEstimationClass2_servAndEquip.m

## 3. DETERMINE GEOMETRY (FOR CLASS-II ANALYSES ONLY) 
    CallConfigurationAndGeometry.m
    	ConfigurationAndGeometry_v2.m
		    CreateFuselageGeometry.m
		    CreateMLGNacelle.m
		    CreatePropulsionGroup.m
		    CreateTailGroup.m
    			CreateLiftingSurfaceGeometry.m (used in several create-geometry functions)
			    CreateLiftingSurfaceGeometryMirrored.m (used in several create-geometry functions)
			    CreateBezierCurve.m (used in several create-geometry functions)
		    PlotAircraftGeometry.m

## 4. DETERMINE CLASS-II DRAG POLAR (FOR CLASS-II ANALYSES ONLY) 
    ComputeClassIIAero.m (based on drag buildup method of Ref. [4])


## 5. PLOTTING & POST-PROCESSING
    CreateLandingDragRequirements_v2.m
    CreateLiftDragPolarMap.m
    CreatePowerControlEnvelope.m
    	ComputePowerControlLimits.m
    PlotMissionAnalysis.m
    PlotOverallEfficiencies.m
    PlotPowertrain.m

## CORE SUPPORTING FUNCTIONS
    PowerTransmissionComputation_v3.m (used to calculate powertrain power flows; see Ref. [5], Appendix A)
    WingPropDeltas_v5.m (used to compute aero-propulsive interaction "deltas"; see Ref. [5], Appendix C)
    	EvaluateSurrogateModel.m (used in some SM-based aero-propulsive models; see Ref. [5], Appendix F.1)

## FOLDERS:
Input files 
	InputConventionalAircraft_OnlyClassI.m
	InputConventionalAircraft.m
	InputConventionalAircraft_ClassII.m
	InputHybridAircraft.m
	InputHybridAircraft_ClassII.m
Surrogate models and data 
	Airfoil_NACA0012.txt
	Airfoil_NACA43015.txt
	D2W_LLM.m
	LLM_SurrModel_4thOrder_52k.mat (used for Ref. [7])
	SurrogateModelOTWDP_2kpts_poly3.mat (used for Ref. [5], Ch. 8)



##########################
## Input files
##########################

Three sets of input files are included as example. The input files contain a long list of variables and therefore I recommend the user to carefully familiarize with one example before going to the next one, in the order as they are listed here. So, first size a conventional aircraft with Class-I methods, then size a conventional aircraft with Class-II methods, and only after that try to size hybrid/electric aircraft and/or aircraft with aero-propulsive interaction effects included. Note that the example aircraft represent feasible designs but not necessarily good designs.


EXAMPLE 1:InputConventionalAircraft_OnlyClassI.m: 
Contains input to size a regional twin-turboprop aircraft with Class-I weight and aerodynamic methods. Assumes constant propeller efficiencies.

EXAMPLE 2: InputConventionalAircraft.m and InputConventionalAircraft_ClassII.m:
Contains input to size the same regional twin-turboprop aircraft but in this case using Class-II weight and aerodynamic models. Estimates (variable) propeller efficiencies.

EXAMPLE 3: InputHybridAircraft.m and InputHybridAircraft_ClassII.m:
Contains input to size a regional hybrid-electric aircraft for the same mission requirements as the conventional one above. This example features a parallel hybrid architecture, where each of the six leading-edge propellers is powered by a gas turbine and batteries. The batteries are used to provide additional power during take-off/climb and for electric taxiing. The "blown lift" effect of the six leading-edge distributed propellers is incorporated with the 'LEDP' aero-propulsive model.  



##########################
## Input parameters
##########################

The most important functions have a description of what the input/output parameters mean. The output of the tool is collected in the variables described in the introduction of "main.m". Analogously, the input files contain descriptions of what each variable represents. Nevertheless, there are some input parameters that are important to understand to be able to run the code correctly, which are listed below:

- a.ref.(...): this is used to estimate the empty-weight excl. powertraind and wing (OEM') in the class-I weight method. Here you must assume properties of a "reference" aircraft that is similar in configuration and TLARs to the one that you are trying to design, if it were designed as a conventional turboprop or turbofan aircraft.

- p.config: this selects the type of powertrain architecture of the aircraft

- p.AeroPropModel: this selects the type of aero-propulsive model used. To neglect A-P effects, use 'none'. Of the models (incorporated in WingPropDeltas_v5.m), only 'LEDP' has been documented and validated (see Ref. [1]) in way that makes it easily usable. I recommend NOT USING any other A-P model since they have several underlying assumptions and limitations, and without understanding those, they can easily lead to erroneous results. Additional information on the 'OTW' model for over-the-wing distributed propellers can be found in Ref. [5], Ch 7, Ch. 8, Appendix C, and Appendix F.1. In any case, I recommend users to develop their own A-P models based on aerodynamic data/models and incorporating them in WingPropDeltas_v5.m. Note that the function is called many times in each iteration, so the model must be fast to evaluate.

- p.DP: this parameters determines whether the "distributed propulsion" system -- i.e. the system to which the aero-propulsive model is applied -- is the primary (= 1) or secondary (= 2) powertrain branch. See Ref. [1] for definitions. Throughout the rest of the code, parameters with a subscript "_conv" generally require to parameters of the non-DP system. If one selects that none of the two powertrain branches is a "distributed propulsion system" (p.DP = 0), then aero-propulsive effects are neglected overall (so p.AeroPropModel is ignored). Examples:
	- A serial powertrain with p.DP = 2 means the electrically driven propulsors have A-P effects
	- A serial powertrain with p.DP = 1 is not possible since it has no mechanically-driven propellers. the "CheckInput.m" script will issue a warning
	- A serial powertrain with p.DP = 0 does not apply any A-P effects
	- A partial turboelectric powertrain with p.DP = 1 will apply the A-P effects to the gas-turbine-driven propulsors
	- A partial turboelectric powertrain with p.DP = 2 will apply the A-P effects to the electrically-driven propulsors

- p.geom.b_dp: determines the span fraction of the "distributed propulsion" system in the aero-propulsive model. I.e. it is relevant if p.DP = 1 or p.DP = 2, and a A-P model is selected in p.AeroPropModel. Note that the diameter of the propulsors is not directly specified (since aircraft dimensions are not known a prior); it is a consequence of the span fraction that the (array of) propulsors occupies, the spacing between propulsors, and the number of propulsors (see Ref. [1]). Also note that b_dp is also used to compute the propulsor size when estimating propulsive efficiency or when creating the configuration & geometry.

- p,geom.b_conv: determines the span fraction of the other, "non-DP" propulsion system.

- p.SE.bat: this is the specific energy (in S.I., so J/kg) of the battery at PACK LEVEL, at 100% SoC. It is therefore lower than the specific energy of a battery CELL. This is the energy density used in the sizing mission, so if you want to account for degradation, make sure to correct this value for that. 

- p.SP.bat: this is the specific power of the battery (in W/kg) at PACK LEVEL, which is assumed to be independent of the state of charge. 

- p.minSOC: this is the state-of-charge margin that must be kept in the battery pack for safety or life-cycle reasons. For example, if in practice the battery will only be discharged from max 90% SoC at the start of the mission to 10% SoC at the end of the mission, then p.minSOC = 0.2.

- m.(constraint).xi, m.(constraint).phi and m.(constraint).Phi: these specify the value of the power-control parameters (throttle, supplied power ratio, and shaft power ratio) at which each performance constraint is carried out. Whether phi or Phi must be specified or not ('NaN') depends on the powertrain selected (see Ref. [1]). If you specify a value for phi/Phi even though it is not applicable, 'CheckInput.m' will overwrite it so keep an eye out on the messages in the command window.

- MA_in.(mission phase).xi, MA_in.(mission phase).phi, and MA_in.(mission phase).Phi (1 x 2 arrays): these specify the power-control parameters during each mission segment, assuming linear evolution between element 1 and element 2 of the array. Depending on the type of mission segment, all parameters applicable to that powertrain or all except one must be specified (see theory in Ref. [1]). These parameters do not need to be consistent with the values selected in the constraint diagram, although you probably would want to do that if part of the "nominal" mission is evaluated as a constraint. TIP: it is sometimes challenging to select the correct throttle setting in climb (MA_in.cl.xi and MA_in.Dcl.xi). If you select a value that is too high, the gas-turbine may over-power other downstream powertrain elements (if they have not been sized accordingly) due to the power-lapse allowing the GT to have a lot of power at sea-level. If you select a value that is too low, the aircraft may not have enough power to climb and accelerate to altitude, and the mission analysis doesn't converge. Therefore, you have to carefully select representative throttle settings. The "CreatePowerControlEnvelope.m" post-processing function was created to check how close the throttle settings throughout the mission are to the limits.

- s.SelDes: specifies which design point is selected in the wing/power loading diagram. I suggest starting with "minWS" (the design point in the top right corner of the feasible design space). You can select other limit points, or manually choose a point in the space by setting s.SelDes = 'manual' and adapting s.xManual and s.yManual.

- s.MAResize: if set to 1, if during the mission analysis climb a moment is encountered where at the given throttle setting, other powertrain elements are overpowered, then this toggle will re-size those components such that they have sufficient power. This in essence moves the design point downwards in the power-loading diagram. I suggest using this only if you're sure of what is going on; it generally helps to get a converged design but may seem "hocus pocus" if the method is not understood properly.

- s.ComputeEtap: switch between assuming constant propulsive efficiencies per segment (from the p structure) and calculating propulsive efficiency using empirical relations specified in the f structure.

- s.ComputeEtaGt: idem but for the gas turbine efficiency.

- s.WeightEstimationClassII: if = 2, then a Class-II weight breakdown based on Ref. [2] is used instead of a Class-I breakdown. Note: IN THIS RELEASE v1.0, THE CLASS II WEIGHT BREAKDOWN IS ONLY IMPLEMENTED FOR REGIONAL PROPELLER AIRCRAFT-LIKE CONFIGURATIONS (i.e. "TRANSPORT TURBOPROPS"). IT DOES NOT WORK FOR GENERAL AVIATION, WIDEBODIES, ETC. Those can be implemented by implementing the equations of Ref [2] (or others) in the "WeightEstimationClass2_v2.m" (sub)function(s).

- s.AeroEstimationClassII: if = 2, then a Class-II weight breakdown is used instead of taking the constant CD0/e values from the input structure a. Note that the "Class II" method still assumes a simple parabolic drag polar, just that in this case, the CD0 is calculated as a buildup following  Ref. [4], and the Oswald factor is estimated based on the wing aspect ratio.

- s.plot(...), s.Polar.plot, s.LandingCheck.plot, s.Env.plot: toggle whether to show additional results plots (= 1) or not (= 0).

- f: this structure contains simple empirical or physics based models to calculate different parameters of the design process. Adapt the models as desired on a case-by-case basis; i.e. here in this structure the "input" is not a numerical value, but generally a (anonymous) function.

- C2.settings.FixedSpan (Class-II input file): determines whether a span constraint is imposed on the design of the aircraft. Make sure that if active, s.SelDes is set to 'manual' since the wing loading may have to be increased to ensure the wing span stays within the limit specified by C2.settings.SpanLimit. Aspect ratio is kept constant.



##########################
## License
##########################

The contents of this tool are licensed under an MIT open source software license (see LICENSE file).

Copyright notice:  
Technische Universiteit Delft hereby disclaims all copyright interest in the program “HEAST” written by R. de Vries. 
Henri Werij, Faculty of Aerospace Engineering, Technische Universiteit Delft. 

Copyright (c) 2024 by Reynard de Vries



##########################
## Acknowledgments
##########################

The vast majority of the HEAST tool development has been funded by the European Union's Horizon 2020 Clean Sky 2 Large Passenger Aircraft research program (CS2-LPA-GAM 2017-2021), under grant agreement numbers 807097 and 945583. Part of the development (particularly, the implementation of the Class-II methods) has been co-funded by Panta Holdings B.V. The author wants to thank all members from the TU Delft Faculty of Aerospace Engineering, Flight Performance and Propulsion Section, who have helped with this work. The author also thanks D. Felix Finger for contributing to the validation of parts of this work.



##########################
## References
##########################

Reference 1. R. de Vries, M.T.H. Brown, and R. Vos, "Preliminary sizing method for hybrid-electric distributed-propulsion aircraft." Journal of Aircraft 56(6), pp. 2172-2188, 2019.

Reference 2. E. Torenbeek, "Synthesis of subsonic airplane design: an introduction to the preliminary design of subsonic general aviation and transport aircraft, with emphasis on layout, aerodynamic design, propulsion and performance", Delft University Press, 1982.

Reference 3. J. Roskam, "Airplane design", DARcorporation, 1985.

Reference 4. D. Raymer, "Aircraft design: A conceptual approach", 6th Ed., American Institute of Aeronautics and Astronautics, 2012.

Reference 5. R. de Vries, "Hybrid-electric aircraft with over-the-wing distributed propulsion: Aerodynamic performance and conceptual design", Delft University of Technology PhD thesis, 2022.

Reference 6. D.F. Finger, R. de Vries, R. Vos, C. Braun, and C. Bil, "Cross-validation of hybrid-electric aircraft sizing methods", Journal of Aircraft 59(3), pp. 742-760, 2022.

Reference 7. M.F.M. Hoogreef, R. de Vries, T. Sinnige, and R. Vos., "Synthesis of aero-propulsive interaction studies applied to conceptual hybrid-electric aircraft design", AIAA SciTech 2020 Forum, Orlando, FL, USA, 6-10 January 2020. 



##########################
## Citation
##########################

If you want to cite this repository in your research paper, please use the following information:
        
de Vries, R., "Hybrid Electric Aircraft Sizing Tool (HEAST)". Version 1.0, 4TU.ResearchData, 2024, doi: 10.4121/494f6cd8-cd4a-4d1a-8d02-8340a6da2b5b



