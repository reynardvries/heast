function [geom,P] = CreateFuselageGeometry(geom_in,settings)
%%% Description
% This function computes a fuselage geometry. For meaning of input
% parameters specified in "geom.in", see example input below.
%
% Output:
%   - geom: same as "geom_in", but with main dimensions of fuselage added:
%       - NoRowsBusiness: number of rows of business-class seats
%       - NoRowsEcon: number of rows of economy-class seats
%       - l_cabin: cabin length [m]
%       - D_cabin: cabin diameter [m]
%       - l_fus: fuselage length [m]
%       - D_fus: fuselage diameter [m]
%       - l_nose: length of nosecone section [m]
%       - l_tail: length of tailcone section [m]
%       - l_cyl: length of central cylindrical section [m]
%       - x_cabin_start: x-coordinate of start of cabin [m]
%       - x_cabin_end: x-coordinate of rear of cabin [m]
%       - x_flightDeck_start: x-coordinate of start of flight deck [m]
%       - x_flightDeck_start: x-coordinate of rear of flight deck [m]
%       - S_ext: surface area [m2]
%       - S_wet: wetted area [m2]; same as S_ext but with "covered factor"
%   - P: Cell array, each entry containing the x/y/z coordinates (1st dim)
%       of the points defining a section of a surface, with sections being
%       appended along the 3rd dim.
%       - P{1}: Nosecone surface, size = [3 x nAzimuth x nPointsNose]
%       - P{2}: Cylindrical surface, size = [3 x nAzimuth x nPointsCyl]
%       - P{3}: Tailcone surface, size = [3 x nAzimuth x nPointsTail]
%       - P{4}: Cabin surface (cylinder), size = [3 x nAzimuth x 2]
%       - P{5}: Flight deck surface (conical), size = [3 x nAzimuth x 2]
%
% Note regarding coordinate system:
% x = zero at front tip of fuselage, positive downstream
% y = zero in symmetry plane, postive starboard
% z = zero along fuselage cylinder axis, positive upwards
%
%%% Reynard de Vries
%%% First created: 18-03-22
%%% Last modified: 12-04-22

% Example input
%{
clear all
close all
clc
tic

% Parameters that come from Class-1/1.5 input file
geom_in.NoPassengers = 90;              % Number of passengers [-]

% Geometrical parameters: cabin layout 
geom_in.SeatsBusiness = 10;             % Number of business-class seats [-]
geom_in.SeatsAbreast = 5;               % Number of seats abreast [-]
geom_in.SeatWidth = 18*0.0254;          % Seat width [m] (now assumed same for econ and business)
geom_in.SeatPitchEcon = 34*0.0254;      % Seat pitch economy class [m]
geom_in.SeatPitchBusiness = 38*0.0254;  % Seat pitch business class [m]
geom_in.SeatWidthArmrest = 2*0.0254;    % Width of armrest [m]
geom_in.LengthFlightDeck = 100*0.0254;  % Flight deck length [m]
geom_in.LengthLavatories = 36*0.0254;   % Length lavatory section [m]
geom_in.LengthGalley = 30*0.0254;       % Gally length [m]
geom_in.WidthAisle = 20*0.0254;         % Aisle width [m]
geom_in.WidthExitAisle = 20*0.0254;     % Width of exit aisles [m] (normally near a galley)
geom_in.NoAisles = 1;                   % Number of aisles
geom_in.NoGalleys = 1;                  % Number of galleys
geom_in.NoLavatories = 1;               % Number of lavatory sections
geom_in.NoExitAisles = 2;               % Number of exist aisles 

% Geometrical parameters: fuselage shape
geom_in.lD_nose = 2;                    % Nosecone length-over-diameter ratio [-]
geom_in.lD_tail = 4;                    % Tailcone length-over-diameter ratio [-]
geom_in.xl_nose = -0.2;                 % Axial position of start of cabin, as fraction of nosecone length [-]
                                        % e.g.: 0 = cabin starts at nose end; -0.1 = cabin starts at 90% of the nose cone 
                                        % Note that flight deck goes in front of cabin!
geom_in.xl_tail = +0.2;                 % Axial position of end of cabin, as fraction of tailcone length [-]
                                        % e.g.: 0 = cabin ends at start of tailcone; 0.1 = cabin ends at 10% of tailcone length
geom_in.sFraction = 0.1;                % Fraction of external surface area covered because it's "inside" another component
                                
% Settings
settings.nPointsNose = 30;              % Number of axial points defining nosecone section
settings.nPointsCyl = 3;                % Number of axial points defining central cylindrical section
settings.nPointsTail = 30;              % Number of axial points defining tailcone section
settings.nAzimuth = 100;                % Number of azimuthal divisions on fuselage
settings.plotOption = 1;                % Plot fuselage geometry?
%}

                                
%% Calculate fuselage dimensions (dimensions in m)

% Extract variables for shorthand notation
geomNames = fieldnames(geom_in);
for k = 1:length(geomNames)
    eval([geomNames{k} ' = geom_in.' geomNames{k} ';']);
end
settingNames = fieldnames(settings);
for k = 1:length(settingNames)
    eval([settingNames{k} ' = settings.' settingNames{k} ';']);
end


% Number of rows business/econ class
NoRowsBusiness = ceil(SeatsBusiness/SeatsAbreast);
NoRowsEcon = ceil((NoPassengers - SeatsBusiness)/SeatsAbreast);

% Cabin length
l_cabin = NoRowsBusiness*SeatPitchBusiness + ...
          NoRowsEcon*SeatPitchEcon + ...
          NoLavatories*LengthLavatories + ...
          NoGalleys*LengthGalley + ...
          NoExitAisles*WidthExitAisle;

% Cabin diameter (should be updated! non-circular cross-section,
% calculations at shoulder height)
D_cabin = SeatsAbreast*SeatWidth + ...
          (SeatsAbreast + NoAisles + 1)*SeatWidthArmrest + ...
          NoAisles*WidthAisle;

% Fuselage diameter (wall thickness should be 100-200 mm; plus some margin
% because D_cabin is evaluated at mid-height, not at shoulder height)
% From AE-1222 course slides [m]:
D_fus = 1.045*D_cabin + 0.084;
ThicknessFusWall = (D_fus - D_cabin)/2;
% NB This approach works for large transport aircraft. For small aircraft,
% thicknes should be 40-50 mm.

% Nose & tailcone lengths
l_nose = lD_nose*D_fus;
l_tail = lD_tail*D_fus;

% Fuselage length
l_fus = (1 + xl_nose)*l_nose + l_cabin + (1 - xl_tail)*l_tail;

% Length cylindrical section
l_cyl = l_fus - l_nose - l_tail;

% Start and end of cabin / cylindrical sections
x_cyl_start = l_nose;
x_cyl_end = l_nose + l_cyl;
x_cabin_start = (1 + xl_nose)*l_nose;
x_cabin_end = x_cabin_start + l_cabin;


%% Create geometry for plot: Nose cone

% Define control points for nose cross-section shape
% Note: for the port/stbd sides, we define the vertical projection of the
% widest section. Height will be defined with ellipses later
P_nose.bottom = [0      0       +0.6    +1;     % x/L_nose
                 0      0       0       0;      % y/D_nose
                 -0.2   -0.5    -0.5    -0.5];  % z/D_nose
P_nose.top =    [0      0       +0.4    +0.4    +1;     % x/L_nose
                 0      0       0       0       0;      % y/D_nose
                 -0.2   +0.1    +0.2    +0.5    +0.5];  % z/D_nose
P_nose.stbd =   [0      0       0.6     1;      % x/L_nose 
                 0      +0.4    +0.5    +0.5;   % y/D_nose
                 0      0       0       0];     % z/D_nose
             
% Create bezier curves
[A_nose.bottom] = CreateBezierCurve(P_nose.bottom,nPointsNose,0);
[A_nose.top] = CreateBezierCurve(P_nose.top,nPointsNose,0);
[A_nose.stbd] = CreateBezierCurve(P_nose.stbd,nPointsNose,0);

% Mirror left/right
A_nose.port = A_nose.stbd;
A_nose.port(2,:) = -A_nose.port(2,:);

% Create x-coordinates at which ellipses are defined; use cosine spacing
x_nose = l_nose*(1 - cos(linspace(0,1,nPointsNose)*pi/2));

% Create tangential coordinate at which ellipses (and other surfaces) are 
% defined
theta = linspace(0,2*pi,nAzimuth);

% Scale curves to [m] and interpolate to x-grid
fnames = fieldnames(A_nose);
for j = 1:length(fnames)
    xv = A_nose.(fnames{j})(1,:)*l_nose;
    yv = A_nose.(fnames{j})(2,:)*D_fus;
    zv = A_nose.(fnames{j})(3,:)*D_fus;
    B_nose.(fnames{j})(1,:) = x_nose;
    B_nose.(fnames{j})(2,:) = interp1(xv,yv,x_nose,'linear');
    B_nose.(fnames{j})(3,:) = interp1(xv,zv,x_nose,'linear');
end

% Define centerline
B_nose.centerline(1,:) = x_nose;
B_nose.centerline(2,:) = zeros(1,nPointsNose);
B_nose.centerline(3,:) = 0.5*(B_nose.top(3,:) + B_nose.bottom(3,:));

% Preallocate ellipse coordinates. Ellipses appended along third dimension
E_nose = NaN(3,nAzimuth,nPointsNose);
aa = NaN(1,nPointsNose); 
bb = NaN(1,nPointsNose);

% Loop over axial coordinates and compute ellipse
for i = 1:nPointsNose
    
    % Center of ellipse
    y0 = B_nose.centerline(2,i);
    z0 = B_nose.centerline(3,i);
    
    % Semi-width and semi-height of ellipse
    aa(i) = B_nose.stbd(2,i); % symmetrical
    bb(i) = 0.5*(B_nose.top(3,i) - B_nose.bottom(3,i));
    
    % Compute coordinates. Tangential coordinate starts at top, positive
    % clockwise viewed from behind
    E_nose(1,:,i) = x_nose(i)*ones(1,nAzimuth);
    E_nose(2,:,i) = y0 + aa(i)*cos(pi/2 - theta);
    E_nose(3,:,i) = z0 + bb(i)*sin(pi/2 - theta);
end

% Compute surface area & volume by integrating in axial direction
dS = NaN(1,nPointsNose-1);
dV = NaN(1,nPointsNose-1);
for i = 1:(nPointsNose-1)
    
    % Perimeter of section i
    pm1 = sum(((E_nose(1,1:end-1,i) - E_nose(1,2:end,i)).^2 + ...
               (E_nose(2,1:end-1,i) - E_nose(2,2:end,i)).^2 + ...
               (E_nose(3,1:end-1,i) - E_nose(3,2:end,i)).^2).^0.5);
    
    % Perimeter of section i+1
    pm2 = sum(((E_nose(1,1:end-1,i+1) - E_nose(1,2:end,i+1)).^2 + ...
               (E_nose(2,1:end-1,i+1) - E_nose(2,2:end,i+1)).^2 + ...
               (E_nose(3,1:end-1,i+1) - E_nose(3,2:end,i+1)).^2).^0.5);
    
    % Axial separation between sections i and i+1 (sections supposed to
    % parallel to X = 0 plane)
    dx = abs(E_nose(1,1,i) - E_nose(1,1,i+1));
    
    % Area of this segment
    dS(i) = dx*(pm1 + pm2)/2;
    
    % Volume of a truncated elliptical cone with different semi-axes
    % https://math.stackexchange.com/questions/3223528/volume-of-a-
    % truncated-elliptical-cone-with-2-different-elliptical-semi-axis-on-e
    a0 = aa(i); a1 = aa(i+1);
    b0 = bb(i); b1 = bb(i+1);
    dV(i) = dx*pi/6*(2*a1*b1 + 2*a0*b0 + a1*b0 + a0*b1);
end

% Total surface & volume
S_ext_nose = sum(dS);
V_ext_nose = sum(dV);


%% Create geometry for plot: Tail cone

% Define control points for nose cross-section shape
% Note: for the port/stbd sides, we define the vertical projection of the
% widest section. Height will be defined with ellipses later
P_tail.bottom = [0      0.5     +1      +1;     % x/L_tail
                 0      0       0       0;      % y/D_tail
                 -0.5   -0.5    0       +0.35]; % z/D_tail
P_tail.top =    [0      0.5     +1      +1;     % x/L_tail
                 0      0       0       0;      % y/D_tail
                 +0.5   +0.5    +0.5    +0.35]; % z/D_tail
P_tail.stbd =   [0      +0.5    1       1;      % x/L_tail
                 +0.5   +0.5    +0.2    0;      % y/D_tail
                 0      0       0       0];     % z/D_tail
             
% Create bezier curves
[A_tail.bottom] = CreateBezierCurve(P_tail.bottom,nPointsTail,0);
[A_tail.top] = CreateBezierCurve(P_tail.top,nPointsTail,0);
[A_tail.stbd] = CreateBezierCurve(P_tail.stbd,nPointsTail,0);

% Mirror left/right
A_tail.port = A_tail.stbd;
A_tail.port(2,:) = -A_tail.port(2,:);

% Create x-coordinates at which ellipses are defined; use sine spacing
x_tail = x_cyl_end + l_tail*(0 - cos(linspace(0,1,nPointsTail)*pi/2+pi/2));

% Scale curves to [m] and interpolate to x-grid and displace in axial 
% direction to end of fuselage
fnames = fieldnames(A_tail);
for j = 1:length(fnames)
    xv = A_tail.(fnames{j})(1,:)*l_tail + x_cyl_end;
    yv = A_tail.(fnames{j})(2,:)*D_fus;
    zv = A_tail.(fnames{j})(3,:)*D_fus;
    B_tail.(fnames{j})(1,:) = x_tail;
    B_tail.(fnames{j})(2,:) = interp1(xv,yv,x_tail,'linear');
    B_tail.(fnames{j})(3,:) = interp1(xv,zv,x_tail,'linear');
end

% Define centerline
B_tail.centerline(1,:) = x_tail;
B_tail.centerline(2,:) = zeros(1,nPointsTail);
B_tail.centerline(3,:) = 0.5*(B_tail.top(3,:) + B_tail.bottom(3,:));

% Compute tailcone upsweep angle for Class-II aero. Positive upwards
upsweepAngle = atand((B_tail.centerline(3,end)-B_tail.centerline(3,1))/...
                     (B_tail.centerline(1,end)-B_tail.centerline(1,1)));

% Preallocate ellipse coordinates. Ellipses appended along third dimension
E_tail = NaN(3,nAzimuth,nPointsTail);
aa = NaN(1,nPointsTail); 
bb = NaN(1,nPointsTail);

% Loop over axial coordinates and compute ellipse
for i = 1:nPointsTail
    
    % Center of ellipse
    y0 = B_tail.centerline(2,i);
    z0 = B_tail.centerline(3,i);
    
    % Semi-width and semi-height of ellipse
    aa(i) = B_tail.stbd(2,i); % symmetrical
    bb(i) = 0.5*(B_tail.top(3,i) - B_tail.bottom(3,i));
    
    % Compute coordinates. Tangential coordinate starts at top, positive
    % clockwise viewed from behind
    E_tail(1,:,i) = x_tail(i)*ones(1,nAzimuth);
    E_tail(2,:,i) = y0 + aa(i)*cos(pi/2 - theta);
    E_tail(3,:,i) = z0 + bb(i)*sin(pi/2 - theta);
end

% Compute surface area by integrating in axial direction
dS = NaN(1,nPointsTail-1);
dV = NaN(1,nPointsTail-1);
for i = 1:(nPointsTail-1)
    
    % Perimeter of section i
    pm1 = sum(((E_tail(1,1:end-1,i) - E_tail(1,2:end,i)).^2 + ...
               (E_tail(2,1:end-1,i) - E_tail(2,2:end,i)).^2 + ...
               (E_tail(3,1:end-1,i) - E_tail(3,2:end,i)).^2).^0.5);
    
    % Perimeter of section i+1
    pm2 = sum(((E_tail(1,1:end-1,i+1) - E_tail(1,2:end,i+1)).^2 + ...
               (E_tail(2,1:end-1,i+1) - E_tail(2,2:end,i+1)).^2 + ...
               (E_tail(3,1:end-1,i+1) - E_tail(3,2:end,i+1)).^2).^0.5);
    
    % Axial separation between sections i and i+1 (sections supposed to
    % parallel to X = 0 plane)
    dx = abs(E_tail(1,1,i) - E_tail(1,1,i+1));
    
    % Area of this segment
    dS(i) = dx*(pm1 + pm2)/2;
    
    % Volume of this segment
    a0 = aa(i); a1 = aa(i+1);
    b0 = bb(i); b1 = bb(i+1);
    dV(i) = dx*pi/6*(2*a1*b1 + 2*a0*b0 + a1*b0 + a0*b1);
end

% Total surface & volume
S_ext_tail = sum(dS);
V_ext_tail = sum(dV);


%% Create geometry for plot: Cylinder, cabin, and flight deck

% Define centerline with x-coordinates
x_cyl = linspace(x_cyl_start,x_cyl_end,nPointsCyl);

% Preallocate ellipse (=circle) coordinates. Circles appended along third 
% dimension
E_cyl = NaN(3,nAzimuth,nPointsCyl);

% Loop over axial coordinates and compute ellipse (centered along X-axis)
for i = 1:nPointsCyl
    E_cyl(1,:,i) = x_cyl(i)*ones(1,nAzimuth);
    E_cyl(2,:,i) = D_fus/2*cos(pi/2 - theta);
    E_cyl(3,:,i) = D_fus/2*sin(pi/2 - theta);
end

% Compute cylinder surface area by integrating in axial direction
dS = NaN(1,nPointsCyl-1);
dV = NaN(1,nPointsCyl-1);
for i = 1:(nPointsCyl-1)
    
    % Perimeter of section i
    pm1 = sum(((E_cyl(1,1:end-1,i) - E_cyl(1,2:end,i)).^2 + ...
               (E_cyl(2,1:end-1,i) - E_cyl(2,2:end,i)).^2 + ...
               (E_cyl(3,1:end-1,i) - E_cyl(3,2:end,i)).^2).^0.5);
    
    % Perimeter of section i+1
    pm2 = sum(((E_cyl(1,1:end-1,i+1) - E_cyl(1,2:end,i+1)).^2 + ...
               (E_cyl(2,1:end-1,i+1) - E_cyl(2,2:end,i+1)).^2 + ...
               (E_cyl(3,1:end-1,i+1) - E_cyl(3,2:end,i+1)).^2).^0.5);
    
    % Axial separation between sections i and i+1 (sections supposed to
    % parallel to X = 0 plane)
    dx = abs(E_cyl(1,1,i) - E_cyl(1,1,i+1));
    
    % Area of this segment
    dS(i) = dx*(pm1 + pm2)/2;
    
    % Volume of this segment (can be done analytically but using same
    % approach as for nose/tail to check implementation is correct)
    a0 = D_fus/2; a1 = D_fus/2;
    b0 = D_fus/2; b1 = D_fus/2;
    dV(i) = dx*pi/6*(2*a1*b1 + 2*a0*b0 + a1*b0 + a0*b1);
end

% Total area and volume
S_ext_cyl = sum(dS);
V_ext_cyl = sum(dV);

% Total fuselage external/wetted areas
S_ext = S_ext_cyl + S_ext_nose + S_ext_tail;
S_wet = S_ext*(1 - sFraction);

% Total fuselage volume
V_ext = V_ext_cyl + V_ext_nose + V_ext_tail;

% Create cylinder representing cabin
x_cabin = [x_cabin_start x_cabin_end];
E_cabin = NaN(3,nAzimuth,2);
for i = 1:2
    E_cabin(1,:,i) = x_cabin(i)*ones(1,nAzimuth);
    E_cabin(2,:,i) = D_cabin/2*cos(pi/2 - theta);
    E_cabin(3,:,i) = D_cabin/2*sin(pi/2 - theta);
end

% Create flight deck, using conical segment. Determine radius of front
% section through interpolation at the right x-location. Note that for the
% flight deck we assume a circular cross-section at the front as well; if
% the nose cone is highly elliptical, it may stick out on the sides or be
% to narrow.
x_flightDeck = [(x_cabin_start - LengthFlightDeck) x_cabin_start];
if x_flightDeck(1) > x_cyl_start        % If flight deck starts in cylindrical part
    D_flightDeck(1) = D_cabin;
    z_flightDeck(1) = 0;
elseif x_flightDeck(1) < 0              % If flight deck starts ahead of aircraft
    error('Flight deck starts ahead of fuselage nose')
else                                    % If flight deck start in nose cone
    h1 = interp1(B_nose.top(1,:),B_nose.top(3,:),x_flightDeck(1));
    h2 = interp1(B_nose.bottom(1,:),B_nose.bottom(3,:),x_flightDeck(1));
    z_flightDeck(1) = interp1(B_nose.centerline(1,:),B_nose.centerline(3,:),x_flightDeck(1));
    D_flightDeck(1) = h1 - h2 - 2*ThicknessFusWall;
end
if x_flightDeck(2) > x_cyl_start        % If flight deck ends in cylindrical part
    D_flightDeck(2) = D_cabin;
    z_flightDeck(2) = 0;
else                                    % If flight deck ends in nose cone
    h1 = interp1(B_nose.top(1,:),B_nose.top(3,:),x_flightDeck(2));
    h2 = interp1(B_nose.bottom(1,:),B_nose.bottom(3,:),x_flightDeck(2));
    z_flightDeck(2) = interp1(B_nose.centerline(1,:),B_nose.centerline(3,:),x_flightDeck(2));
    D_flightDeck(2) = h1 - h2 - 2*ThicknessFusWall;
end

% Create conical surface
E_flightDeck = NaN(3,nAzimuth,2);
for i = 1:2
    E_flightDeck(1,:,i) = x_flightDeck(i)*ones(1,nAzimuth);
    E_flightDeck(2,:,i) = D_flightDeck(i)/2*cos(pi/2 - theta);
    E_flightDeck(3,:,i) = D_flightDeck(i)/2*sin(pi/2 - theta) + z_flightDeck(i);
end


%% Organize output

% Store variables back into geom-structure
for k = 1:length(geomNames)
    eval(['geom.' geomNames{k} ' = ' geomNames{k} ';'])
end

% Add new output parameters
geom.NoRowsBusiness = NoRowsBusiness;
geom.NoRowsEcon = NoRowsEcon;
geom.l_cabin = l_cabin;
geom.D_cabin = D_cabin;
geom.l_fus = l_fus;
geom.D_fus = D_fus;
geom.l_nose = l_nose;
geom.l_tail = l_tail;
geom.l_cyl = l_cyl;
geom.x_cabin_start = x_cabin_start;
geom.x_cabin_end = x_cabin_end;
geom.x_flightDeck_start = x_flightDeck(1);
geom.x_flightDeck_start = x_flightDeck(2);
geom.S_ext = S_ext;
geom.S_wet = S_wet;
geom.Volume = V_ext;
geom.upsweepAngle = upsweepAngle;

% Store surfaces in P-cell-array
P{1} = E_nose;          % Nose cone, [3 x nAzimuth x nPointsNose]
P{2} = E_cyl;           % Cylindrical part, [3 x nAzimuth x nPointsCyl]
P{3} = E_tail;          % Tail cone, [3 x nAzimuth x nPointsTail]
P{4} = E_cabin;         % Cabin cylinder, [3 x nAzimuth x 2] 
P{5} = E_flightDeck;    % Flight-deck cone, [3 x nAzimuth x 2] 


%% Show in figure

% Create 3D figure
if plotOption == 1
    fig = figure;
    fig.Name = 'Fuselage geometry';
    hold on; grid on; box on;
    xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]');
    axis equal; view(3);
    
    % Nosecone bezier curves
    fnames = fieldnames(B_nose);
    for j = 1:length(fnames)
        plot3(B_nose.(fnames{j})(1,:),B_nose.(fnames{j})(2,:),B_nose.(fnames{j})(3,:),'-');
    end
    
    % For checking: Add ellipses
    for i = 1:nPointsNose
        plot3(E_nose(1,:,i),E_nose(2,:,i),E_nose(3,:,i),'color',[0.5 0.5 0.5])
    end
    
    % Nosecone surface
    xx = squeeze(E_nose(1,:,:));
    yy = squeeze(E_nose(2,:,:));
    zz = squeeze(E_nose(3,:,:));
    surf(xx,yy,zz,'facecolor',[0.5 0.5 0.5],'edgecolor','none','facealpha',0.5)
    
    % Tailcone bezier curves
    fnames = fieldnames(B_tail);
    for j = 1:length(fnames)
        hh(j) = plot3(B_tail.(fnames{j})(1,:),B_tail.(fnames{j})(2,:),...
            B_tail.(fnames{j})(3,:),'-');
    end
    
    % For checking: Add ellipses
    for i = 1:nPointsNose
        plot3(E_tail(1,:,i),E_tail(2,:,i),E_tail(3,:,i),'color',[0.5 0.5 0.5])
    end
    
    % Tailcone surface
    xx = squeeze(E_tail(1,:,:));
    yy = squeeze(E_tail(2,:,:));
    zz = squeeze(E_tail(3,:,:));
    surf(xx,yy,zz,'facecolor',[0.5 0.5 0.5],'edgecolor','none','facealpha',0.5)
    
    % Cylinder surface
    xx = squeeze(E_cyl(1,:,:));
    yy = squeeze(E_cyl(2,:,:));
    zz = squeeze(E_cyl(3,:,:));
    surf(xx,yy,zz,'facecolor',[0.5 0.5 0.5],'edgecolor','none','facealpha',0.5)
    
    % Cabin
    xx = squeeze(E_cabin(1,:,:));
    yy = squeeze(E_cabin(2,:,:));
    zz = squeeze(E_cabin(3,:,:));
    surf(xx,yy,zz,'facecolor','m','edgecolor','none','facealpha',1)
    
    % Flight deck
    xx = squeeze(E_flightDeck(1,:,:));
    yy = squeeze(E_flightDeck(2,:,:));
    zz = squeeze(E_flightDeck(3,:,:));
    surf(xx,yy,zz,'facecolor','g','edgecolor','none','facealpha',1)
end





