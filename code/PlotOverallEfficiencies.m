function [s] = PlotOverallEfficiencies(a,m,p,f,s,c,MA)

% Segment names
sNames= {'cl','cr','de','Dcl','Dcr','Dde','Loiter'};

% Create figure
fig = figure(s.figStart + size(s.figs,2));
fig.Name = 'Overall efficiencies';
s.figs(size(s.figs,2)+1) = fig;
fig.Color = [1 1 1];

% Prepare plots
subplot(2,2,1); hold on; grid on; box on;
xlabel('t [min]'); ylabel('\eta_{PT} = P_{shaft}/P_{energy}')
ylim([0 0.5])
subplot(2,2,2); hold on; grid on; box on;
xlabel('t [min]'); ylabel('L/D')
ylim([5 25])
subplot(2,2,3); hold on; grid on; box on;
xlabel('t [min]'); ylabel('\eta_p')
ylim([0.5 1])
subplot(2,2,4); hold on; grid on; box on;
xlabel('t [min]'); ylabel('overall efficiency, \eta_p\cdot\eta_{PT}\cdot(L/D)')
ylim([0 10])

% Loop over mission segments
for i = 1:length(sNames)
    
   % Extract variables and replace Nans
   P_s1 = MA.(sNames{i}).P.s1; P_s1(isnan(P_s1)) = 0;
   P_s2 = MA.(sNames{i}).P.s2; P_s2(isnan(P_s2)) = 0;
   P_f = MA.(sNames{i}).P.f; P_f(isnan(P_f)) = 0;
   P_bat = MA.(sNames{i}).P.bat; P_bat(isnan(P_bat)) = 0;
   
   % Compute quantities
   eta_PT = (P_s1 + P_s2)./(P_f + P_bat);
   etap1 = MA.(sNames{i}).aero.etap1;
   etap2 = MA.(sNames{i}).aero.etap2;
   etap1_iso = MA.(sNames{i}).aero.etap1iso;
   etap2_iso = MA.(sNames{i}).aero.etap2iso;
   etapAvg = MA.(sNames{i}).aero.etapAvg;
   LD = MA.(sNames{i}).aero.LD;
   LD_iso = MA.(sNames{i}).aero.CLiso./MA.(sNames{i}).aero.CDiso;
   eta_overall = (MA.(sNames{i}).P.p)./(P_f + P_bat).*LD;
   
   % Time array
   t = MA.(sNames{i}).t/60;
   
   % Plot powertrain effciency
   subplot(2,2,1); plot(t,eta_PT,'-b')
   
   % Plot Lift-to-drag ratio
   subplot(2,2,2); H2(1) = plot(t,LD_iso,'--b');
   subplot(2,2,2); H2(2) = plot(t,LD,'-b');
   subplot(2,2,2); H2(3) = plot(t,LD.*etapAvg,'-g');
   
   % Plot Lift-to-drag ratio
   subplot(2,2,3); H3(1) = plot(t,etap1_iso,'--b');
   subplot(2,2,3); H3(2) = plot(t,etap1,'-b');
   subplot(2,2,3); H3(3) = plot(t,etap2_iso,'--r');
   subplot(2,2,3); H3(4) = plot(t,etap2,'-r');
   subplot(2,2,3); H3(5) = plot(t,etapAvg,':g');
   
    % Plot overall efficiency
   subplot(2,2,4); plot(t,eta_overall,'-b');
   
   % Append parameters to compute mean during nominal mission
   avg.(sNames{i}).eta_PT = nanmean(eta_PT);
   avg.(sNames{i}).LD = nanmean(LD);
   avg.(sNames{i}).etapAvg = nanmean(etapAvg);
   avg.(sNames{i}).eta_overall = nanmean(eta_overall);
   avg.(sNames{i}).duration = t(end) - t(1); % Duration of segment for weighted average
end

% Add legends
subplot(2,2,2); legend(H2,'airframe only','installed','L/D*\eta_{p,avg}')
subplot(2,2,3); legend(H3,'\eta_{p1,iso}','\eta_{p1}','\eta_{p2,iso}','\eta_{p2}','\eta_{p,avg}')

% Calculate average throughout nominal mission
eta_PT_avg = (avg.cl.eta_PT*avg.cl.duration + ...
              avg.cr.eta_PT*avg.cr.duration + ...
              avg.de.eta_PT*avg.de.duration)/...
              (avg.cl.duration + avg.cr.duration + avg.de.duration);
LD_avg =     (avg.cl.LD*avg.cl.duration + ...
              avg.cr.LD*avg.cr.duration + ...
              avg.de.LD*avg.de.duration)/...
              (avg.cl.duration + avg.cr.duration + avg.de.duration);
etapAvg_avg =(avg.cl.etapAvg*avg.cl.duration + ...
              avg.cr.etapAvg*avg.cr.duration + ...
              avg.de.etapAvg*avg.de.duration)/...
              (avg.cl.duration + avg.cr.duration + avg.de.duration);
eta_overall_avg =  (avg.cl.eta_overall*avg.cl.duration + ...
                    avg.cr.eta_overall*avg.cr.duration + ...
                    avg.de.eta_overall*avg.de.duration)/...
                    (avg.cl.duration + avg.cr.duration + avg.de.duration);          

% Add titles
subplot(2,2,1); title(['Average \eta_{PT} during nominal mission = ' num2str(eta_PT_avg,'%.3f')])
subplot(2,2,2); title(['Average installed L/D during nominal mission = ' num2str(LD_avg,'%.3f')])
subplot(2,2,3); title(['Average \eta_{PT} during nominal mission = ' num2str(etapAvg_avg,'%.3f')])
subplot(2,2,4); title(['Average \eta_{overall} during nominal mission = ' num2str(eta_overall_avg,'%.3f')])












