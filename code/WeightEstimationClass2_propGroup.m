function [M] = WeightEstimationClass2_propGroup(M,TOM0,a,m,p,f,s,c,aircraft,C2)
% For input/output see WeightEstimationClass2.m.
%%% Description:
% The following elements are considered part of the propulsion group.
% DOUBLE CHECK that the decision of "has gearbox" is compatible with the
% corresponding architecture! 
%
% .----.  Pf  .----.  Pgt .----.  Ps1 .----. Pp1
% |  F |----->| GT |----->| GB |----->| P1 |----->
% '----'      '----'      '----'      '----'
%                            | 
%                            | Pgb
%                            V
%                         .----.
%                         |EM1 |
%                         '----'
%                            | 
%                            | Pe1
%                            V
%             .----. Pbat .----. Pe2  .----. Ps2  .----. Pp2
%             |BAT |----->|PMAD|----->|EM2 |----->| P2 |----->
%             '----'      '----'      '----'      '----'
%
% Components related to the non-electric primary components (GT, GB, P1):
% Reference        Turbofan (TF)        Turboprop (TP)       Full-e***
%               __________________  ___________________  __________________
% Has gearbox   yes       no        yes       no         yes       no      
%               ________  ________  _________ _________  ________  ________
% Applicable    parallel  conv*     conv      serial**   e-1       e-1
% architectures PTE                 parallel  TE**       dual-e    dual-e
%               SPPH                PTE
%                                   SPPH
% WEIGHTS       ________  ________  _________ _________  ________  ________
% GT:           W_TF      W_TF      W_TP-W_GB W_TP-W_GB  0         0
% GB:           W_GB      0         W_GB      0          W_GB      0
% P1:           0         0         W_P1      0          W_P1      W_P1
% acessories:   W_acc     W_acc     W_acc     W_acc      0         0
% oil system:   W_oil     W_oil     W_oil     W_oil      0****     0
%
% Additionally, we have the following propulsion-group weight components
% which do not depend on whether it is a TP or TF, or whether it has a
% gearbox or not, but that may or may not be present depending on the
% architecture:
%               conv  ser.  par.  TE    PTE   SPPH  e-1   e-2   dual-e
% WEIGHTS       _____ _____ _____ _____ _____ _____ _____ _____ _____
% Fuel system:  Yes   Yes   Yes   Yes   Yes   Yes   0     0     0
% P2:           0     Yes   0     Yes   Yes   Yes   0     Yes   Yes
% EM1:          0     Yes   Yes   Yes   Yes   Yes   Yes   0     Yes
% EM1 TMS:      0     Yes   Yes   Yes   Yes   Yes   Yes   0     Yes
% EM2:          0     Yes   0     Yes   Yes   Yes   0     Yes   Yes
% EM2 TMS:      0     Yes   0     Yes   Yes   Yes   0     Yes   Yes
% PMAD:         0     Yes   Yes   Yes   Yes   Yes   Yes   Yes   Yes
% PMAD TMS:     0     Yes   Yes   Yes   Yes   Yes   Yes   Yes   Yes
% Battery TMS:  0     Yes   Yes   0     0     Yes   Yes   Yes   Yes

% Notes:
% - The TF weight correlation includes core, fan, etc, and has no gearbox
% - The TP weight correlation includes gearbox, etc., but not the propeller
% * In the case of a geared turbofan, the weight of the GB is assumed to
% form part of W_GT (requires dedicated correlation!)
% ** It is assumed that serial and turboelectric configs don't need a GB,
% since it is beneficial to operate the EM1 at high RPM
% *** Full-e configs can have a gearbox between EM and propeller, depending
% on difference in optimum RPM.
% **** If electric config has a gearbox, it would also need oil cooling or
% similar, but this weight contribution is neglected here. Similarly, a TF
% with dedicated GB would need more cooling, and a TP without gearbox would
% require less cooling than estimated by the empirical correlations. These
% effects are neglected here.
%                
%%% Reynard de Vries
%%% First created: 29-03-22
%%% Last modified: 10-02-23


%% Check input compatibility 

% Extract fields from "C2" structure (for shorter writing only...)
fNames = fieldnames(C2);
for i = 1:length(fNames)
    eval([fNames{i} ' = C2.(fNames{i});'])
end

% Turbofan-based aircraft
if strcmp(a.ref.config,'TF')
    
    % Powertrains which are not supposed to have a GB
    if C2.propulsionGroup1.geom.hasGearbox == 1
        if strcmp(p.config,'conventional')
            warning(['A ' p.config ' turbofan configuration is not assumed ' ...
                     'to have a gearbox in the Class-II weight estimation. ' ...
                     'Check WeightEstimationClass2_propGroup.m.'])
        end
        
    % Powertrains which are supposed to have a GB
    else
        if strcmp(p.config,'parallel') || strcmp(p.config,'PTE') || strcmp(p.config,'SPPH')
            warning(['A ' p.config ' turbofan configuration is assumed ' ...
                     'to require a gearbox in the Class-II weight estimation. ' ...
                     'Check WeightEstimationClass2_propGroup.m.'])
        end        
    end
    
    % For architectures without P1, should use TP instead for C2 weight
    % estimation
    if strcmp(p.config,'serial') || strcmp(p.config,'turboelectric')
        warning(['For a ' p.config ' architecture, a "TP" reference should ' ...
                     'be used in the Class-II weight estimation. ' ...
                     'Check WeightEstimationClass2_propGroup.m.'])
    end
end

% Turboprop-based aircraft
if strcmp(a.ref.config,'TP')
    
    % Powertrains which are not supposed to have a GB
    if C2.propulsionGroup1.geom.hasGearbox == 1
        if strcmp(p.config,'serial') || strcmp(p.config,'turboelectric')
            warning(['A ' p.config ' turboprop configuration is not assumed ' ...
                     'to have a gearbox in the Class-II weight estimation. ' ...
                     'Check WeightEstimationClass2_propGroup.m.'])
        end
        
    % Powertrains which are supposed to have a GB
    else
        if strcmp(p.config,'parallel') || strcmp(p.config,'PTE') || ...
           strcmp(p.config,'SPPH') || strcmp(p.config,'conventional')
            warning(['A ' p.config ' turboprop configuration is assumed ' ...
                     'to require a gearbox in the Class-II weight estimation. ' ...
                     'Check WeightEstimationClass2_propGroup.m.'])
        end        
    end
end


%% Calculations

% Gas turbines
if strcmp(p.config,'e-1') || strcmp(p.config,'e-2') ...
        || strcmp(p.config,'dual-e')
    M.propGroup.GT = 0;
else
     
    % For TF-based aircraft
    if strcmp(a.ref.config,'TF') % Assuming TF limited by TO constraint!
        WS_array = linspace(s.WSmin,s.WSmax,s.n);
        v_TO = interp1(WS_array,m.TO.v,aircraft.WSdes);
        etap_TO = interp1(WS_array,p.TO.etap_conv,aircraft.WSdes);
        T_TO = aircraft.Pdes.GTM/v_TO*etap_TO;
        M.propGroup.GT = p.N1*f.W.TF(T_TO/p.N1);
    
    % For TP-based aircraft: separate gearbox wieght
    else
        
        % Gearbox weight, based on Johnson NASA report 2009, p. 158
        f_rs = 0.13;                            % Fraction of weight corresponding to rotor shaft
        N_rotor = 1;                            % Number of main rotors (helicopters...)
        n_P1 = C2.propulsionGroup1.geom.n_P1;   % Rotor rpm
        n_GT = C2.propulsionGroup1.geom.n_GT;   % Engine rpm
        P_ds = aircraft.Pdes.GTM/745.7/p.N1;    % Drive system rated power [hp] 
        M_GB = p.N1*C2.propulsionGroup1.geom.FF_GB*(1-f_rs)*0.453592*... 
               95.7634*N_rotor^0.38553*P_ds^0.78137*n_GT^0.09899/n_P1^0.80686;
       
        % Turboshaft weight
        M_turboshaft_incl_GB = p.N1*f.W.GT(aircraft.Pdes.GTM/p.N1);
        M.propGroup.GT = M_turboshaft_incl_GB - M_GB;
    end
end

% Gearbox (based on Johnson NASA report 2009, p. 158)
if C2.propulsionGroup1.geom.hasGearbox == 1 && ~strcmp(p.config,'e-2')
    f_rs = 0.13;                    % Fraction of weight corresponding to rotor shaft
    N_rotor = 1;                    % Number of main rotors (helicopters...)
    n_P1 = C2.propulsionGroup1.geom.n_P1;   % Rotor rpm
    n_GT = C2.propulsionGroup1.geom.n_GT;   % Engine rpm
    P_ds = aircraft.Pdes.GTM/745.7/p.N1; % Rated power per engine [hp]
    M.propGroup.gearbox = p.N1*C2.propulsionGroup1.geom.FF_GB*(1-f_rs)*...
        0.453592*95.7634*N_rotor^0.38553*P_ds^0.78137*n_GT^0.09899/n_P1^0.80686;
else
    M.propGroup.gearbox = 0;
end

% Acessory gears, drives, starting, induction/exhaust systems. 
% For powertrain architectures with GT only.
if strcmp(p.config,'e-1') || strcmp(p.config,'e-2') || strcmp(p.config,'dual-e')
    M.propGroup.accessories = 0;
else
    if strcmp(a.category,'LightSingleEngine') || ...    % Reciprocating
            strcmp(a.category,'LightMultiEngine') || ...
            strcmp(a.category,'TransportMultiEngine')
        M.propGroup.accessories = propulsionGroup1.geom.FF_accessories*...
            0.467*p.N1*(aircraft.Pdes.GTM/p.N1/745.7)^0.7;
        if propulsionGroup1.geom.hasSupercharger == 1
            M.propGroup.accessories = M.propGroup.accessories + ...
                propulsionGroup1.geom.FF_acessories*...
                0.435*(M.propGroup.GT)^0.943;
        end
    elseif strcmp(a.category,'TransportTurboprop')      % Turboprop (excl. prop governor, variable pitch)
        M.propGroup.accessories = propulsionGroup1.geom.FF_accessories*...
            0.181*p.N1*(aircraft.Pdes.GTM/p.N1/745.7)^0.8;
    else                                                % Turbofan
        fuelMassFlow = aircraft.Pdes.f/p.SE.f;          % [kg/s]
        M.propGroup.accessories = propulsionGroup1.geom.FF_accessories*...
            36*p.N1*(fuelMassFlow/p.N1);                % Gives low values? check for actual TF
    end
end

% Oil system and cooler (for powertrains that have a GT)
% Powertrains with GT: all except e-1, e-2, dual-e
% NOTE: no oil system applied to e-1/dual-e even if they have a gearbox!
if strcmp(p.config,'e-1') || strcmp(p.config,'e-2') || strcmp(p.config,'dual-e')
    M.propGroup.oilSystem = 0;
else
    switch a.category
        case 'TransportTurboprop'
            k_oil = 0.07;
        case 'TransportTurbofan'
            k_oil = 0.02;
        case 'BusinessJet'
            k_oil = 0.02;
        otherwise % Reciprocating
            if nacelles.geom.hasHorOppCyls == 1
                k_oil = 0.03;
            else
                k_oil = 0.08;
            end
    end
    M.propGroup.oilSystem = propulsionGroup1.geom.FF_oil*...
        k_oil*M.propGroup.GT;
end

% Fuel system
tankVolume = (M.f/p.rho_fuel)*fuelSystem.geom.tank_fraction; % [l]
if strcmp(p.config,'e-1') || strcmp(p.config,'e-2') || strcmp(p.config,'dual-e')
    M.propGroup.fuelSystem = 0;
else
    switch a.category
        case 'TransportTurboprop'
            M.propGroup.fuelSystem = fuelSystem.geom.FF_fuel*...
                (36.3*(p.N1 + fuelSystem.geom.N_ftanks - 1) + ...
                 4.366*fuelSystem.geom.N_ftanks^0.5*tankVolume^0.333);
        case 'TransportTurbofan'
            M.propGroup.fuelSystem = fuelSystem.geom.FF_fuel*...
                (36.3*(p.N1 + fuelSystem.geom.N_ftanks - 1) + ...
                 4.366*fuelSystem.geom.N_ftanks^0.5*tankVolume^0.333);
        case 'TransportMultiEngine'
            M.propGroup.fuelSystem = fuelSystem.geom.FF_fuel*...
                (0.9184*tankVolume^0.600);
        case 'LightSingleEngine'
            M.propGroup.fuelSystem = fuelSystem.geom.FF_fuel*...
                (0.3735*tankVolume^0.667);
        case 'LightMultiEngine'
            M.propGroup.fuelSystem = fuelSystem.geom.FF_fuel*...
                (0.9184*tankVolume^0.600);
        case 'JetTrainer'
            M.propGroup.fuelSystem = fuelSystem.geom.FF_fuel*...
                (36.3*(p.N1 + fuelSystem.geom.N_ftanks - 1) + ...
                 4.366*fuelSystem.geom.N_ftanks^0.5*tankVolume^0.333);
        case 'BusinessJet'
            M.propGroup.fuelSystem = fuelSystem.geom.FF_fuel*...
                (36.3*(p.N1 + fuelSystem.geom.N_ftanks - 1) + ...
                 4.366*fuelSystem.geom.N_ftanks^0.5*tankVolume^0.333);
    end
end

% Primary propulsor installation
% Using empirical correlation based on Filippone (2012), table 6.2. Same as
% Y. Teeuwen's MSc thesis. Weight includes spinner, but not sure if it
% includes governor/variable pitch mechanism?! Additional sensitivity
% required. 
if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') || ...
        strcmp(p.config,'PTE') || strcmp(p.config,'SPPH') || ...
        strcmp(p.config,'e-1') || strcmp(p.config,'dual-e') % Has P1
    
    % For TF aircraft, fan is already included in the GT (TF) weight
    % estimation
    if strcmp(a.ref.config,'TF')
        M.propGroup.propellers1 = 0;
        
    % Turboprop aircraft: propeller weight
    else
        
        % Currently only works for propellers, not ducted fans, CRORs, etc.
        if propulsionGroup1.geom.propulsorType == 1
            M.propGroup.propellers1 = propulsionGroup1.geom.FF_prop*...
                p.N1*3.646*(propulsionGroup1.geom.D * (aircraft.Pdes.s1/1000/p.N1) * ...
                C2.propulsionGroup1.geom.B^0.5)^0.3991;
            
        else
            error('Propulsor weight estimation only works for propellers at the moment')
        end
    end
else
    M.propGroup.propellers1 = 0;
end

% Secondary propulsor installation
if strcmp(p.config,'serial') || strcmp(p.config,'turboelectric') || ...
        strcmp(p.config,'PTE') || strcmp(p.config,'SPPH') || ...
        strcmp(p.config,'e-2') || strcmp(p.config,'dual-e') % Has P2
    if propulsionGroup2.geom.propulsorType == 1
        M.propGroup.propellers2 = propulsionGroup2.geom.FF_prop*...
            p.N2*3.646*(propulsionGroup2.geom.D * (aircraft.Pdes.s2/1000/p.N2) * ...
            C2.propulsionGroup1.geom.B^0.5)^0.3991;
    else
        error('Propulsor weight estimation only works for propellers at the moment')
    end
else
    M.propGroup.propellers2 = 0;
end

% Electrical machines. Use throttle-corrected (M) values. For secondary
% machines, the empirical correlation accounts for propeller RPM!
if ~isnan(aircraft.Pdes.EM1M) 
    % M.propGroup.EM1 = p.N1*f.W.EM1(aircraft.Pdes.EM1M/p.N1);  
    M.propGroup.EM1 = p.N1*f.W.EM1_ClassII(aircraft.Pdes.EM1M/p.N1,0);
else
    M.propGroup.EM1 = 0;
end
if ~isnan(aircraft.Pdes.EM2M)
    % M.propGroup.EM2 = p.N2*f.W.EM2(aircraft.Pdes.EM2M/p.N2);
    M.propGroup.EM2 = p.N2*f.W.EM2_ClassII(aircraft.Pdes.EM2M/p.N2,...
                                           C2.propulsionGroup2.geom.n);
else
    M.propGroup.EM2 = 0;
end

% PMAD: cables, switches, transformers, fuses, etc. To be improved with
% sensitivity to voltage level, cable length
if ~isnan(aircraft.Pdes.PM)
    M.propGroup.PMAD = C2.PMAD.geom.FF*aircraft.Pdes.PM/p.SP.PMAD;
else
    M.propGroup.PMAD = 0;
end
    
% EM1 thermal management system. Note: cooling system sized for "EM1M'
% condition, i.e., at throttle = 1. If over-powering is used, cooling
% system will not be able to cool that transient power peak! Adapt if we
% want to study the sensitivity of having a heavier cooling system vs.
% lighter EM weight by allowing more over-powering
if ~isnan(aircraft.Pdes.EM1M)
%     M.propGroup.coolingEM1 = C2.propulsionGroup1.geom.FF_TMS_EM1*...
%         (aircraft.Pdes.EM1M*(1-p.eta_EM1))/p.SP.TMS_EM1
    M.propGroup.coolingEM1 = C2.propulsionGroup1.geom.FF_TMS_EM1*...
        (aircraft.Pdes.EM1*(1-p.eta_EM1))/p.SP.TMS_EM1; % 10-02-23: Size based on max instantaneous!
else
    M.propGroup.coolingEM1 = 0;
end

% EM2 thermal management system. See note above for EM1.
if ~isnan(aircraft.Pdes.EM2M)
%     M.propGroup.coolingEM2 = C2.propulsionGroup2.geom.FF_TMS_EM2*...
%         (aircraft.Pdes.EM2M*(1-p.eta_EM2))/p.SP.TMS_EM2
    M.propGroup.coolingEM2 = C2.propulsionGroup2.geom.FF_TMS_EM2*...
        (aircraft.Pdes.EM2*(1-p.eta_EM2))/p.SP.TMS_EM2; % 10-02-23: Size based on max instantaneous!
else
    M.propGroup.coolingEM2 = 0;
end

% PMAD thermal management system. 
if ~isnan(aircraft.Pdes.PM)
    M.propGroup.coolingPMAD = C2.PMAD.geom.FF_TMS*(aircraft.Pdes.PM*(1-p.eta_PM))/p.SP.TMS_PM;
else
    M.propGroup.coolingPMAD = 0;
end

% Battery thermal management system. 
if ~isnan(aircraft.Pdes.bat)
    M.propGroup.coolingBatteries = C2.propulsionGroup2.geom.FF_TMS_bat*...
        (aircraft.Pdes.bat*(1-p.eta_bat))/p.SP.TMS_bat;
else
    M.propGroup.coolingBatteries = 0;
end

% Total propulsion group mass
fNames = fieldnames(M.propGroup);
M.propGroupMass = 0;
for i = 1:length(fNames)
    M.propGroupMass = M.propGroupMass + M.propGroup.(fNames{i});
end


