function [M] = WeightEstimationClass2_v2(a,m,p,f,s,c,aircraft,M,C2)
%%% Description
% This function estimates the "Class 2" weight breakdown of the aircraft
% for a given output of the WP-WS diagram and a determined battery and fuel
% energy required to complete the mission. Currently, only the Torenbeek
% method is implemented (except for the GT weight estimation, which can be
% specified externally in the Class-I input file, and EMs, which assume
% power densities).
% 
% Components included in weight breakdown**:                Object group*:
% Payload                                                   FG
% Battery                                                   FG/WG
% Fuel                                                      FG/WG
% Airframe structure
%   - Wing group                                            WG
%   - Tail group                                            TG
%   - Body group                                            FG
%   - Nose LG                                               FG
%   - Main LG                                               FG/WG
%   - Surface controls                                      WG
%   - Engine/nacelle installation                           PG***
% Propulsion group
%   - Engines (GT, including fan in case of TF/TJ's)        PG1
%   - Accessory GBs, drives, starter systems                PG1
%   - Oil system & (conventional) cooling                   PG1
%   - Fuel system                                           FG/WG
%   - Propellers                                            PG***
%   - Main gearbox                                          PG1
%   - Primary electical machines & converters (EM1)         PG1
%   - Secondary electical machines & converters (EM2)       PG2
%   - Power management & distribution (cables/switches)     FG/WG
%   - PMAD thermal management system                        FG/WG
%   - EM1 thermal management system                         PG1
%   - EM2 thermal management system                         PG2
%   - Battery thermal management system                     FG/WG
% Aiframe serivces & equipment                              (FG****)
%   - APU                                                   FG
%   - Instruments & navigation group                        FG
%   - Hydraulic/pneumatic group                             FG/WG
%   - Electrical group                                      FG/WG
%   - Furnishing group                                      FG
%   - ECS & anti-icing group                                FG
%   - Miscellaneous                                         FG
% Operational items                                         (FG****)
%   - Crew provisions                                       FG
%   - Cabin supplies                                        FG
%   - Water & toilet chemicals                              FG
%   - Safety equipment                                      FG
%   - Oil, residual fuel                                    FG/WG
%
% *Object group: WG (wing group), FG (fuselage group), TG (tail group), or
% PG (propulsion group, 1 or 2) of ConfigurationAndGeometry.m in which the
% component belongs. Note that the mass contributions of e.g. the 
% "propulsion group" here are not necessarily located in the "PG" of the 
% configuration/geometry estimation. Idem for wing/tail groups. 
% ** Ignoring water injection systems & thrust reversers.
% *** These elements exist for the primary and secondary propulsion system
% separately, depending on the powertrain architecture.
% **** For now, airframe services & equipment and operational items are
% gathered as a single object with a single CG for configuration and
% geometry. They are placed int he fuselage group, and no sub-division is
% made.
%
% Input:
%   - a,m,p,f,s,c: structures containing aircraft and program data (see
%       main input file)
%   - aircraft: structure containing:
%       - Pdes: installed power of the powerplant components on the
%           aircraft, as obtained from WPdes in the WP-WS diagram [W]
%       - TOM: initial guess of the take-off mass of the aircraft [kg]
%       - Sw: wing area [m2]
%       - Sw_ref, Pdes_ref: characteristics of reference aircraft
%       - PL: payload [kg]
%   - M: structure containing masses in [kg] of:
%           - f: fuel mass
%           - bat: installed battery mass
%           - bat_miss: battery mass required for nominal mission
%           - bat_E: battery mass required for total mission energy (incl. div.)
%           - bat_P: battery mass required to meet power requirements
%           - PL: payload 
%   - C2: structure containing all Class-II input parameters, as defined in
%       Input_ClassII.m.
%
% Output:
%   - M: updated structure, additionally containing masses in [kg] of the
%       components listed above.
%
% To-do
%   - Add CG info (weights, coords) to each "comps" field for Config & geom
%   - Add an option to switch between using actual TOW/DEW and
%       reference-aircraft values for TOW-based correlations
%
%%% Reynard de Vries
%%% First created: 28-03-22
%%% Last modified: 06-04-22


%% Converge on TOM 

% Initial guess for TOM
TOM0 = aircraft.TOM;

% Loop till TOM convergence
err = 1;
iter = 0;
while err > s.errmax
    
    % Update counter
    iter = iter + 1;

    % Airframe structure 
    [M] = WeightEstimationClass2_airframeStruct(M,TOM0,a,m,p,f,s,c,aircraft,C2);
    
    % Propulsion group 
    [M] = WeightEstimationClass2_propGroup(M,TOM0,a,m,p,f,s,c,aircraft,C2);
    
    % Services & equipment
    [M] = WeightEstimationClass2_servAndEquip(M,TOM0,a,m,p,f,s,c,aircraft,C2);
    
    % Operational items
    [M] = WeightEstimationClass2_operItems(M,TOM0,a,m,p,f,s,c,aircraft,C2);
           
    % Compute operating empty weight
    OEM = M.airframeStructMass + M.propGroupMass + ...
          M.servAndEquipMass + M.operItemsMass; 
    
    % Recompute TOM
    TOM1 = OEM + M.PL + M.bat + M.f;
    
    % Check error and update
    err = abs((TOM1-TOM0)/TOM0);
    TOM0 = TOM1;
    
    % Check max iterations
    if iter > s.itermax
        if s.printMessages == 1
            disp([s.levelString '    > TOM did not converge!'])
        end
        TOM1 = NaN;
        break
    end
end

% Add results to output structure
M.OEM = OEM;
M.TOM = TOM1;





