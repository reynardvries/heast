function [MLG] = CreateMLGNacelle(MLG,wing)
% The script creates 2 nacelles used to store the main landing gear in case
% of wing-mounted landing gear that is stored in a dedicated nacelle. For
% now rudimentary axisymmetric nacelle assumed; more refined geometry
% required.
%
%
%%% Reynard de Vries
%%% First created: 13-04-22
%%% Last modified: 13-04-22


% If has nacelle, create geometry and compute wetted area
if MLG.geom.hasNacelle == 1
    
    % Get nacelle contour. Nacelle assumed axisymmetric
    controlPoints = [0      0       +0.3    1       1;      % x/l
                     0      +0.02   +0.3    +0.02   0;      % y/D
                     0      0       0       0       0];     % z/D
    
    % Create bezier curve
    curve = CreateBezierCurve(controlPoints,MLG.settings.nPoints,0);
    
    % Azimuthal coordinate for geometry
    theta = linspace(0,2*pi,MLG.settings.nAzimuth);
    
    % Scale to nacelle size
    curve(1,:) = curve(1,:)/max(curve(1,:))*MLG.geom.l_nac;
    curve(2,:) = curve(2,:)/max(curve(2,:))*MLG.geom.R_nac;
    
    % Determine nacelle radius on x-grid with cosine spacing
    x_nac = MLG.geom.l_nac*(1 - cos(linspace(0,1,MLG.settings.nPoints)*pi))/2;
    r_nac = interp1(curve(1,:),curve(2,:),x_nac,'linear');
    
    % Create axisymmetric nacelle surface (axial stations along 3rd
    % dimension)
    coordsNac = NaN(3,length(theta),length(x_nac));
    for j = 1:length(x_nac)
        coordsNac(1,:,j) = x_nac(j)*ones(size(theta));
        coordsNac(2,:,j) = r_nac(j)*cos(theta);
        coordsNac(3,:,j) = r_nac(j)*sin(theta);
    end
    
    % Coordinates of nacelle front tip
    coordsLE = [MLG.geom.x_over_c*wing.geom.root.c;
        MLG.geom.y_over_b*wing.geom.b;
        MLG.geom.z_over_c*wing.geom.root.c];
    
    % Nacelle surface: Rotate and translate coordinates to account
    % for incidence angle and location:
    coordsNacRot(1,:,:) =   coordsLE(1) + ...
        coordsNac(1,:,:)*cosd(MLG.geom.incidence) + ...
        coordsNac(3,:,:)*sind(MLG.geom.incidence);
    coordsNacRot(2,:,:) =   coordsLE(2) + coordsNac(2,:,:);
    coordsNacRot(3,:,:) =   coordsLE(3) - ...
        coordsNac(1,:,:)*sind(MLG.geom.incidence) + ...
        coordsNac(3,:,:)*cosd(MLG.geom.incidence);
    
    % Nacelle surface area
    MLG.geom.S_ext = trapz(x_nac,2*pi*r_nac);
    MLG.geom.S_wet = MLG.geom.S_ext*(1 - MLG.geom.sFraction);
    
    % Store in output cell array; one port and one starboard
    MLG.plotting.P{1} = coordsNacRot;
    MLG.plotting.P{2} = coordsNacRot;
    MLG.plotting.P{2}(2,:,:) = -MLG.plotting.P{2}(2,:,:);
    
% If no nacelle, zero area
else
    MLG.geom.S_ext = 0;
    MLG.geom.S_wet = 0;
end


