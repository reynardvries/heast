function [WS,WP_path,WP_comp,WP_loss,TW,a,m,p] = ConstraintCruiseSpeed(a,m,p,f,s,c,con)
%% Cruise operating conditions

% No horizontal acceleration
dvdt = 0;  

% Climb rate specified: we want the aircraft to be able to reach cruise
% altitude (= top of climb), so it must also be able to climb at this
% speed. Using standard 100ft/min.
climb = [0 NaN];       

% No bank angle/load factor req./turn radius
maneuver = [0 NaN NaN];

% Freestream density, velocity and dynamic pressure
[T_inf,aa,~,m.(con).rho] = atmosisa(m.(con).h);
m.(con).v = m.(con).M*aa;
m.(con).Re = m.(con).v*m.(con).rho*a.c_ref/f.mu(T_inf);
q = 0.5*m.(con).rho*m.(con).v^2;


%% Thrust and (required) power loading 

% Intialize variables for loop
WS = linspace(s.WSmin,s.WSmax,s.n);
TW = NaN(size(WS));
WP = NaN(size(WS));
a.(con).CL = NaN(size(WS));
a.(con).dCL = NaN(size(WS));
a.(con).dCDi = NaN(size(WS));
p.(con).Tc = NaN(size(WS));

% Loop over WS values
for i = 1:length(WS)
    
    % Wing loading in flight condition
    WS_in = WS(i)*m.(con).f;
    
    % Initial guess to speed up convergence
    TW_in = q*a.(con).CD0./WS_in + WS_in/pi/a.AR/a.(con).e/q;
    
    % Compute thrust and power loading
    [WP_out,TW_out,CL,dCL,dCDi,~,Tc,chi,etap,etap_conv,detap] = ComputeThrustLoading_vKnown(...
                        con,TW_in,WS_in,climb,maneuver,dvdt,a,m,p,f,s,c);
       
    % Correct to MTOW
    TW(i) = TW_out*m.(con).f;
    WP(i) = WP_out/m.(con).f;
     
    % Save variables
    a.(con).CL(i) = CL;
    a.(con).dCL(i) = dCL;
    a.(con).dCDi(i) = dCDi;
    p.(con).Tc(i) = Tc;
    p.(con).detap(i) = detap;
    p.(con).chi(i) = chi;
    p.(con).etap(i) = etap;
    p.(con).etap_conv(i) = etap_conv;
end


%% Compute HEP component power loading

% All engines operative
if strcmp(con,'Dcr')
    OEI = 0; % Assume one range extender fails for diversion cruise constraint! (TEMP)
else
    OEI = 0;
end

% Replace NaNs with Inf's so that the code understands that WP is an input.
% Keep indices to revert changes later
indices = isnan(WP);
WP(indices) = Inf;

% Call sizing routine
[WP_path,WP_comp,WP_loss] = SizePowertrain(WP,con,OEI,m,p,f,s,c);

% Change -Inf to +Inf to avoid warnings; Inf is a possible solution for
% zero power flow. Convert back to NaNs if necessary
pathnames = fieldnames(WP_path);
for i = 1:size(pathnames,1)
    WP_path.(pathnames{i})(WP_path.(pathnames{i})==-Inf) = Inf;
    WP_path.(pathnames{i})(indices) = NaN;
end
compnames = fieldnames(WP_comp);
for i = 1:size(compnames,1)
    WP_loss.(compnames{i})(WP_loss.(compnames{i})==-Inf) = Inf;
    WP_comp.(compnames{i})(WP_comp.(compnames{i})==-Inf) = Inf;
    WP_loss.(compnames{i})(indices) = NaN;
    WP_comp.(compnames{i})(indices) = NaN;
end




