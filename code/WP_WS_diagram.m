%%% Description
%
% This routine generates the wing loading - power loading diagram for an
% aircraft featuring wing-mounted distributed propulsion (tractor, pusher
% or over-the-wing), where lift and drag depend on thrust. For more
% information see de Vries, Brown & Vos (2018).
%
% Note that in the comments, "propeller", "propulsor" and "fan" are used
% indistinctively. Furthermore, in the comments, "DP" does not mean the
% aircraft has to have an array of distributed propulsor as such. "DP"
% refers to a propulsive system which has significant aero-propulsive
% interaction effects (e.g. over-the-wing, BLI, tip-mounted,...) that have
% to be included in the design loop. These aero-propulsive interaction 
% effects are estimated in WingPropDeltas.m.
%
%%% Reynard de Vries
%%% TU Delft
%%% Date created: 21-06-17
%%% Last modified: 15-06-21


%% Necessary constraints
% Note: If the mission cruise segment is flown at constant L/D instead of
% constant altitude, then the end-of-cruise constraint is (erroneously)
% evaluated at the initial cruise altitude anyway, even though in practice
% the aircraft will slowly climb throughout the cruise.

% Only evaluate constraint is specified in the "m" structure in input file
if isfield(m,'cr')
    
    % Display progress
    if s.printMessages == 1
        disp([s.levelString '  > Cruise constraint'])
    end
    
    % Conventional: use power-control parameters (PCPs) specified in "m"
    % structure for WP-WS diagram
    if s.UseMAControls ~= 1
        [WS.cr,WP_path.cr,WP_comp.cr,WP_loss.cr,TW.cr,a,m,p] = ...
            ConstraintCruiseSpeed(a,m,p,f,s,c,'cr');
        
    % If the control values must be taken from the MA: it is assumed that the
    % components are limited either at the beginning or the end of the phase.
    % In order to be 100% sure that the limiting case is caught, actually all
    % possible permutations of the power-control parameters (PCPs) would have
    % to be tested. But that would generally lead to a conservative design.
    else
        
        % Check whether throttle is specified in MA (careful with nomenclature!)
        if sum(isnan(MA_in.cr.xi))==0
            xiStart = MA_in.cr.xi(1);
            xiEnd = MA_in.cr.xi(2);
        else
            xiStart = m.cr.xi;
            xiEnd = m.cr.xi;
        end
        
        % Check whether phi is specified in MA.
        if sum(isnan(MA_in.cr.phi))==0
            phiStart = MA_in.cr.phi(1);
            phiEnd = MA_in.cr.phi(2);
        else
            phiStart = m.cr.phi;
            phiEnd = m.cr.phi;
        end
        
        % Check whether Phi is specified in MA.
        if sum(isnan(MA_in.cr.Phi))==0
            PhiStart = MA_in.cr.Phi(1);
            PhiEnd = MA_in.cr.Phi(2);
        else
            PhiStart = m.cr.Phi;
            PhiEnd = m.cr.Phi;
        end
        
        % Evaluate constraint for start of segment
        mIn = m;
        mIn.cr.xi = xiStart;
        mIn.cr.phi = phiStart;
        mIn.cr.Phi = PhiStart;
        [~,WP_pathStart,WP_compStart,WP_lossStart,TWStart,~,~,~] = ...
            ConstraintCruiseSpeed(a,mIn,p,f,s,c,'cr');
        
        % Evaluate constraint for end of segment
        mIn.cr.xi = xiEnd;
        mIn.cr.phi = phiEnd;
        mIn.cr.Phi = PhiEnd;
        [WS.cr,WP_pathEnd,WP_compEnd,WP_lossEnd,TWEnd,a,m,p] = ...
            ConstraintCruiseSpeed(a,mIn,p,f,s,c,'cr');
        
        % Fields in WP arrays
        pathNames = fieldnames(WP_pathStart); nPaths = size(pathNames,1);
        compNames = fieldnames(WP_compStart); nComps = size(compNames,1);
        
        % Select limiting values for power paths
        for nn = 1:nPaths
            WP_pathArray = [WP_pathStart.(pathNames{nn});
                WP_pathEnd.(pathNames{nn})];
            WP_path.cr.(pathNames{nn}) = min(WP_pathArray,[],1);
        end
        
        % Select limiting values for components
        for nn = 1:nComps
            WP_compArray = [WP_compStart.(compNames{nn});
                WP_compEnd.(compNames{nn})];
            WP_comp.cr.(compNames{nn}) = min(WP_compArray,[],1);
            WP_lossArray = [WP_lossStart.(compNames{nn});
                WP_lossEnd.(compNames{nn})];
            WP_loss.cr.(compNames{nn}) = min(WP_lossArray,[],1);
        end
        
        % Select limiting TW values
        TWArray = [TWStart ; TWEnd];
        TW.cr = max(TWArray,[],1);
    end
    
% Cruise speed constraint is required for MA
else
    error('A cruise speed constraint must be specified')
end

% Powered wing stall speed
if isfield(m,'L')
    if s.printMessages == 1
        disp([s.levelString '  > Stall speed constraint'])
    end
    [WS.L,WP_path.L,WP_comp.L,WP_loss.L,TW.L,a,m,p] = ...
        ConstraintLandingStallSpeed(a,m,p,f,s,c,'L');
else
    error('A stall speed/landing constraint must be specified')
end

% Take-off distance (using TOP)
%{
if isfield(m,'TO')
    if s.printMessages == 1
        disp([s.levelString '  > Take-off distance constraint (TOP)'])
    end
    [WS.TO,WP_path.TO,WP_comp.TO,WP_loss.TO,TW.TO,a,m,p] = ...
        ConstraintTakeOffDistance_v3(a,m,p,f,s,c);
else
    error('A take-off distance constraint must be specified')
end
%}

% % Take-off distance (using Torenbeek 2013)
if isfield(m,'TO')
    if s.printMessages == 1
        disp([s.levelString '  > Take-off distance constraint (Torenbeek)'])
    end
    [WS.TO,WP_path.TO,WP_comp.TO,WP_loss.TO,TW.TO,a,m,p] = ...
        ConstraintTakeOffDistance_v5(a,m,p,f,s,c);
else
    error('A take-off distance constraint must be specified')
end


%% Optional constraints

% Landing distance
if isfield(m,'sL')
    if s.printMessages == 1
        disp([s.levelString '  > Landing distance constraint'])
    end
    [WS.sL,WP_path.sL,WP_comp.sL,WP_loss.sL,TW.sL,a,m,p] = ...
        ConstraintLandingStallSpeed(a,m,p,f,s,c,'sL');
end

% Balked landing with OEI
if isfield(m,'bL')
    if s.printMessages == 1
        disp([s.levelString '  > OEI balked landing constraint'])
    end
    [WS.bL1,WS.bL2,WP_path.bL1,WP_comp.bL1,WP_loss.bL1,WP_path.bL2,WP_comp.bL2,...
        WP_loss.bL2,TW.bL,a,m,p] = ConstraintOEIClimbGradient(a,m,p,f,s,c,'bL');
end

% OEI Ceiling
if isfield(m,'cI')
    if s.printMessages == 1
        disp([s.levelString '  > OEI ceiling constraint'])
    end
    [WS.cI1,WS.cI2,WP_path.cI1,WP_comp.cI1,WP_loss.cI1,WP_path.cI2,WP_comp.cI2,...
        WP_loss.cI2,TW.cI,a,m,p] = ConstraintOEIClimbRate(a,m,p,f,s,c,'cI');
end

% OEI Second segment climb
if isfield(m,'ss')
    if s.printMessages == 1
        disp([s.levelString '  > OEI second-segment-climb constraint'])
    end
    [WS.ss1,WS.ss2,WP_path.ss1,WP_comp.ss1,WP_loss.ss1,WP_path.ss2,WP_comp.ss2,...
        WP_loss.ss2,TW.ss,a,m,p] = ConstraintOEIClimbGradient(a,m,p,f,s,c,'ss');
end

% Balked landing, AEO
if isfield(m,'bLAEO')
    if s.printMessages == 1
        disp([s.levelString '  > AEO Balked landing constraint'])
    end
    [WS.bLAEO,WP_path.bLAEO,WP_comp.bLAEO,WP_loss.bLAEO,TW.bLAEO,a,m,p] = ...
        ConstraintAEOClimbGradient(a,m,p,f,s,c,'bLAEO');
end

% Climb gradient at take-off, AEO
if isfield(m,'ToClAEO')
    if s.printMessages == 1
        disp([s.levelString '  > AEO take-off climb gradient constraint'])
    end
    [WS.ToClAEO,WP_path.ToClAEO,WP_comp.ToClAEO,WP_loss.ToClAEO,TW.ToClAEO,a,m,p] = ...
        ConstraintAEOClimbGradient(a,m,p,f,s,c,'ToClAEO');
end

% OEI Climb gradient at take-off, LG extended
if isfield(m,'ToClEx')
    if s.printMessages == 1
        disp([s.levelString '  > OEI take-off climb gradient, LG extended'])
    end
    [WS.ToClEx1,WS.ToClEx2,WP_path.ToClEx1,WP_comp.ToClEx1,WP_loss.ToClEx1,...
        WP_path.ToClEx2,WP_comp.ToClEx2,WP_loss.ToClEx2,TW.ToClEx,a,m,p] = ...
        ConstraintOEIClimbGradient(a,m,p,f,s,c,'ToClEx');
end

% OEI Climb gradient at take-off, LG retracted
if isfield(m,'ToClRe')
    if s.printMessages == 1
        disp([s.levelString '  > OEI take-off climb gradient, LG retracted'])
    end
    [WS.ToClRe1,WS.ToClRe2,WP_path.ToClRe1,WP_comp.ToClRe1,WP_loss.ToClRe1,...
        WP_path.ToClRe2,WP_comp.ToClRe2,WP_loss.ToClRe2,TW.ToClRe,a,m,p] = ...
        ConstraintOEIClimbGradient(a,m,p,f,s,c,'ToClRe');
end

% OEI en-route OEI climb gradient
if isfield(m,'erOEI')
    if s.printMessages == 1
        disp([s.levelString '  > OEI en-route climb gradient'])
    end
    [WS.erOEI1,WS.erOEI2,WP_path.erOEI1,WP_comp.erOEI1,WP_loss.erOEI1,...
        WP_path.erOEI2,WP_comp.erOEI2,WP_loss.erOEI2,TW.erOEI,a,m,p] = ...
        ConstraintOEIClimbGradient(a,m,p,f,s,c,'erOEI');
end

% OEI Discontinued approach
if isfield(m,'da')
    if s.printMessages == 1
        disp([s.levelString '  > OEI discontinued approach'])
    end
    [WS.da1,WS.da2,WP_path.da1,WP_comp.da1,WP_loss.da1,...
        WP_path.da2,WP_comp.da2,WP_loss.da2,TW.da,a,m,p] = ...
        ConstraintOEIClimbGradient(a,m,p,f,s,c,'da');
end

% Ceiling
if isfield(m,'ce')
    if s.printMessages == 1
        disp([s.levelString '  > AEO Ceiling constraint'])
    end
    [WS.ce,WP_path.ce,WP_comp.ce,WP_loss.ce,TW.ce,a,m,p] = ...
        ConstraintAEOClimbRate(a,m,p,f,s,c,'ce');
end

% Specified performance: ROC @ SL, AEO
if isfield(m,'rocAEO')
    if s.printMessages == 1
        disp([s.levelString '  > AEO SL rate-of-climb constraint'])
    end
    [WS.rocAEO,WP_path.rocAEO,WP_comp.rocAEO,WP_loss.rocAEO,TW.rocAEO,a,m,p] = ...
        ConstraintAEOClimbRate(a,m,p,f,s,c,'rocAEO');
end

% Specified performance: ROC @ SL, OEI
if isfield(m,'rocOEI')
    if s.printMessages == 1
        disp([s.levelString '  > OEI SL rate-of-climb constraint'])
    end
    [WS.rocOEI1,WS.rocOEI2,WP_path.rocOEI1,WP_comp.rocOEI1,WP_loss.rocOEI1,...
        WP_path.rocOEI2,WP_comp.rocOEI2,WP_loss.rocOEI2,TW.rocOEI,a,m,p] = ...
        ConstraintOEIClimbRate(a,m,p,f,s,c,'rocOEI');
end

% Diversion cruise (useful for range extenders)
if isfield(m,'Dcr')
    
    % Display progress
    if s.printMessages == 1
        disp([s.levelString '  > Diversion cruise constraint'])
    end
    
    % Conventional: use power-control parameters (PCPs) specified in "m"
    % structure for WP-WS diagram
    if s.UseMAControls ~= 1
        [WS.Dcr,WP_path.Dcr,WP_comp.Dcr,WP_loss.Dcr,TW.Dcr,a,m,p] = ...
            ConstraintCruiseSpeed(a,m,p,f,s,c,'Dcr');
        
    % If the control values must be taken from the MA: it is assumed that the
    % components are limited either at the beginning or the end of the phase.
    % In order to be 100% sure that the limiting case is caught, actually all
    % possible permutations of the power-control parameters (PCPs) would have
    % to be tested. But that would generally lead to a conservative design.
    else
        
        % Check whether throttle is specified in MA (careful with nomenclature!)
        if sum(isnan(MA_in.Dcr.xi))==0
            xiStart = MA_in.Dcr.xi(1);
            xiEnd = MA_in.Dcr.xi(2);
        else
            xiStart = m.Dcr.xi;
            xiEnd = m.Dcr.xi;
        end
        
        % Check whether phi is specified in MA.
        if sum(isnan(MA_in.Dcr.phi))==0
            phiStart = MA_in.Dcr.phi(1);
            phiEnd = MA_in.Dcr.phi(2);
        else
            phiStart = m.Dcr.phi;
            phiEnd = m.Dcr.phi;
        end
        
        % Check whether Phi is specified in MA.
        if sum(isnan(MA_in.Dcr.Phi))==0
            PhiStart = MA_in.Dcr.Phi(1);
            PhiEnd = MA_in.Dcr.Phi(2);
        else
            PhiStart = m.Dcr.Phi;
            PhiEnd = m.Dcr.Phi;
        end
        
        % Evaluate constraint for start of segment
        mIn = m;
        mIn.Dcr.xi = xiStart;
        mIn.Dcr.phi = phiStart;
        mIn.Dcr.Phi = PhiStart;
        [~,WP_pathStart,WP_compStart,WP_lossStart,TWStart,~,~,~] = ...
            ConstraintCruiseSpeed(a,mIn,p,f,s,c,'Dcr');
        
        % Evaluate constraint for end of segment
        mIn.Dcr.xi = xiEnd;
        mIn.Dcr.phi = phiEnd;
        mIn.Dcr.Phi = PhiEnd;
        [WS.cr,WP_pathEnd,WP_compEnd,WP_lossEnd,TWEnd,a,m,p] = ...
            ConstraintCruiseSpeed(a,mIn,p,f,s,c,'Dcr');
        
        % Fields in WP arrays
        pathNames = fieldnames(WP_pathStart); nPaths = size(pathNames,1);
        compNames = fieldnames(WP_compStart); nComps = size(compNames,1);
        
        % Select limiting values for power paths
        for nn = 1:nPaths
            WP_pathArray = [WP_pathStart.(pathNames{nn});
                WP_pathEnd.(pathNames{nn})];
            WP_path.Dcr.(pathNames{nn}) = min(WP_pathArray,[],1);
        end
        
        % Select limiting values for components
        for nn = 1:nComps
            WP_compArray = [WP_compStart.(compNames{nn});
                WP_compEnd.(compNames{nn})];
            WP_comp.Dcr.(compNames{nn}) = min(WP_compArray,[],1);
            WP_lossArray = [WP_lossStart.(compNames{nn});
                WP_lossEnd.(compNames{nn})];
            WP_loss.Dcr.(compNames{nn}) = min(WP_lossArray,[],1);
        end
        
        % Select limiting TW values
        TWArray = [TWStart ; TWEnd];
        TW.Dcr = max(TWArray,[],1);
    end
end


%% Determine design points

% Manually establish which powers are important for the sizing process
names = fieldnames(WP_path);
for i = 1:size(names,1)
    WP_select.(names{i}).GT = WP_comp.(names{i}).GT;
    WP_select.(names{i}).GTM = WP_comp.(names{i}).GTM;
    WP_select.(names{i}).EM1 = WP_comp.(names{i}).EM1;
    WP_select.(names{i}).EM1M = WP_comp.(names{i}).EM1M;
    WP_select.(names{i}).EM2 = WP_comp.(names{i}).EM2;
    WP_select.(names{i}).EM2M = WP_comp.(names{i}).EM2M;
    WP_select.(names{i}).PM = WP_comp.(names{i}).PM;
    WP_select.(names{i}).f = WP_path.(names{i}).f;
    WP_select.(names{i}).bat = WP_path.(names{i}).bat;
    WP_select.(names{i}).s1 = WP_path.(names{i}).s1;
    WP_select.(names{i}).s2 = WP_path.(names{i}).s2;
    WP_select.(names{i}).p = WP_path.(names{i}).p;
end
clear('names','i')

% Don't show loading diagrams in Class-II convergence loop
s_in = s;
s_in.plotWPWS = 0;

% Call function to compute design points
if s.printMessages == 1
    disp([s.levelString '  > Evaluating design point'])
end
[WSdes,WPdes,TW_WSdes,AEROdes,AC,a,m,p,~] = ...
                    ComputeDesignPoint(WS,WP_select,TW,a,m,p,f,s_in,c);



