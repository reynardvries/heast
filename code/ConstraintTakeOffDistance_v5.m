function [WS,WP_path,WP_comp,WP_loss,TW,a,m,p] = ConstraintTakeOffDistance_v5(a,m,p,f,s,c)
% This function computes the TO wing/power loading using method of 
% Torenbeek, 2013, Sec. 9.4.4. This method is senstive to L/D during the
% airborne path and assumes engine failure during the take-off run


%% Compute power loading 

% Constant form Torenbeek 2013 p. 269, k_T = (V2/V_LOF)^2*F_avg/T_v2
k_T = 0.85;

% Atmospheric conditions 
[T_inf,aa,~,m.TO.rho] = atmosisa(m.TO.h);

% Initialize loop variables
WS = linspace(s.WSmin,s.WSmax,s.n); 
WP = NaN(1,s.n);                           
TW = NaN(1,s.n);    
m.TO.v = NaN(1,s.n);       
   
% Loop over all wing loading values
for i = 1:s.n   
    
    % Wing loading in flight condition
    WS0 = WS(i)*m.TO.f;
    
    % Initial guess for power loading & thrust-to-weight ratio at V2
    v0 = 1.2*(WS0/(a.TO.CLmax/1.2^2)/0.5/m.TO.rho)^0.5;
    q0 = 0.5*m.TO.rho*v0^2;
    TW0 = q0*a.TO.CD0./WS0 + WS0/pi/a.AR/a.TO.e/q0;
    
    % Rotor size for this wing loading
    D2W_TO = f.D2W(WS0,a.AR,p);           
    D2W_TO = min([D2W_TO 1e9]);
    D2W_conv = f.D2W_conv(WS0,a.AR,p);
    D2W_conv = min([D2W_conv 1e9]);
    
    % Compute aerodynamic properties and TW at V2 (assuming level flight)
    [~,TW2,CL2,dCL2,dCDi2,dCD02,v2,~,chi2,etap2,etap_conv2,detap2] = ...
        ComputeThrustLoading_vMargin('TO',TW0,WS0,[0 NaN],[0 NaN NaN],0,a,m,p,f,s,c);
    
    % Total lift coefficient at V2 (N.B. the TW value assumed here is not
    % the same as the actual TW value solved to match s_TO!)
    q2 = 0.5*m.TO.rho*v2^2;
    CL2_tot = CL2 + dCL2 + chi2*TW2*WS0/q2*sind(p.TO.Gamma);
   
    % Total drag coefficient at V2
    CD2_tot = f.CD(a.TO.CD0,CL2,a.TO.CL_minD,a.AR,a.TO.e) + dCD02 + dCDi2;
    
    % Initial guess for thrust share in inner loop = thrust share at V2.
    % Same for efficiencies
    chi0 = chi2;   
    etap = etap2;
    detap = detap2;
    etap_conv = etap_conv2;

    % For debugging
    %{
    if i == 1
    figure(100)
    subplot(1,3,1); hold off;
    plot(0,TW0,'or')
    hold on; grid on; box on;
    ylabel('TW')
    subplot(1,3,2); hold off
    plot(0,chi0,'or')
    hold on; grid on; box on;
    ylabel('chi'); 
    subplot(1,3,3); hold off
    plot(0,NaN)
    hold on; grid on; box on;
    ylabel('s [m]'); 
    end
    %}
    
    % Converge on WP to match the targer take-off distance
    iter = 0;
    err = 1;
    while err>s.errmax
        
        % Loop counter
        iter = iter+1;       
        
        % Compute thrust coefficient at V2, with current thrust share and
        % power-to-weight ratio
        if TW0==Inf
            Tc = chi0*1e9;
            Tc_conv = (1-chi0)*1e9;
        else
            Tc = chi0*TW0/p.N/m.TO.rho/v2^2/D2W_TO;
            Tc_conv = (1-chi0)*TW0/p.N_conv/m.TO.rho/v2^2/D2W_conv;
        end
        
        % Update (isolated) propulsive efficiency 
        if s.ComputeEtap == 1
            etap = f.etap(Tc);
            etap_conv = f.etap(Tc_conv);
        end
        
        % Re-compute thrust share, using updated iso-prop efficiency, but
        % with detap from previous iteration
        if strcmp(p.config,'PTE') || strcmp(p.config,'SPPH') || ...
                strcmp(p.config,'dual-e')
            if p.DP == 1
                chi1 = 1/( m.TO.Phi / (1-m.TO.Phi) ...
                    * etap_conv/(etap+detap) + 1);
            else
                chi1 = 1/((1-m.TO.Phi)/ m.TO.Phi   ...
                    * etap_conv/(etap+detap) + 1);
            end
        else
            
            % With one propulsor systen we can just take the default value
            % computed in CheckInput, since it must be zero or 1
            chi1 = p.TO.chi(end);
        end
        
        % Compute take-off distance(N.B. should actually account for engine
        % failure in airborne path; this approach is a bit more optimistic.
        % But with the (N-1)/N factor, it often gives erroneous results
        % because the denominator of the second term can become negative
        % (especially for twin-engine aircraft)).
        s_TO = WS0/(m.TO.rho*c.g*k_T*CL2_tot*TW0) + 2*m.TO.h_TR/(TW0 - CD2_tot/CL2_tot);

        % Compute error
        err = abs(s_TO - m.TO.s)/abs(m.TO.s);
                                    
        % If the error becomes a NaN, the code
        % will crash or provide an unconverged solution. Note that err will
        % never be complex because of the abs function
        if isnan(err)
            warning('Convergence error attained NaN value')
            TW1 = NaN;
            break
        end
        
        % Stop at max iterations
        if iter>=s.itermax
            TW1 = NaN;
            break
        end
               
        % Update TW guess. If s_TO is too high, higher TW values are
        % needed. If airborne distance is negative because CD/CL > T/W,
        % significantly increase TW to get past the asymptote
        SF = 0.02;
        MF = 2;
        if (CD2_tot/CL2_tot) > TW0
            TW1 = MF*TW0;
        else
            % If CD2/CL2 ~ TW0, s_TO tends to Inf -> Limit step size
            s_update = min([s_TO 2*m.TO.s]);
            TW1 = TW0 + SF*(s_update - m.TO.s)/abs(m.TO.s);
        end

        % For debugging
        %{
        if i == 1
        figure(100)
        subplot(1,3,1); plot(iter,TW1,'ob'); 
        title(['Iter = ' num2str(iter) ', err = ' num2str(err)])
        subplot(1,3,2); plot(iter,chi1,'ob')
        subplot(1,3,3); plot(iter,s_TO,'ob')
        keyboard;
        end
        %}
                
        % Update for next loop
        TW0 = TW1;
        chi0 = chi1;
    end    
    
    % Store thrust-to-weight ratio
    TW(i) = TW1*m.TO.f;
    
    % Store power loading 
    WP(i) = 1/TW(i)/v2;
    
    % Store speed, lift coefficient, and propulsive efficiency change. Use
    % transition value as the representative one of take-off (i.e. near V2)
    m.TO.v(i) = v2;
    m.TO.M(i) = v2/aa;
    m.TO.Re(i) = v2*a.c_ref*m.TO.rho/f.mu(T_inf);
    a.TO.CL(i) = CL2;
    p.TO.detap(i) = detap;
    p.TO.etap(i) = etap;
    p.TO.chi(i) = chi1;
    p.TO.Tc(i) = Tc;           
    p.TO.etap_conv(i) = etap_conv;

    % Check T/W required for this take-off distance is high enough to
    % actually climb the aircraft once it eaches V2 as well
    if TW(i) < TW2
        warning('Thrust-to-weight ratio required for take-off is not sufficient to climb after V2')
    end    
end

      
%% Compute HEP component power loading

% All engines operative
OEI = 0;

% Replace NaNs with Inf's so that the code understands that WP is an input.
% Keep indices to revert changes later
indices = isnan(WP);
WP(indices) = Inf;

% Call sizing routine
[WP_path,WP_comp,WP_loss] = SizePowertrain(WP,'TO',OEI,m,p,f,s,c);

% Change -Inf to +Inf to avoid warnings; Inf is a possible solution for
% zero power flow
pathnames = fieldnames(WP_path);
for i = 1:size(pathnames,1)
    WP_path.(pathnames{i})(WP_path.(pathnames{i})==-Inf) = Inf;
    WP_path.(pathnames{i})(indices) = NaN;
end
compnames = fieldnames(WP_comp);
for i = 1:size(compnames,1)
    WP_loss.(compnames{i})(WP_loss.(compnames{i})==-Inf) = Inf;
    WP_comp.(compnames{i})(WP_comp.(compnames{i})==-Inf) = Inf;
    WP_loss.(compnames{i})(indices) = NaN;
    WP_comp.(compnames{i})(indices) = NaN;
end












