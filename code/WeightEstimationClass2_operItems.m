function [M] = WeightEstimationClass2_operItems(M,TOM0,a,m,p,f,s,c,aircraft,C2)
% For input/output see WeightEstimationClass2.m.
%
%%% Reynard de Vries
%%% First created: 29-03-22
%%% Last modified: 06-04-22

% Extract fields from "aircraft" structure (for shorter writing only...)
fNames = fieldnames(C2);
for i = 1:length(fNames)
    eval([fNames{i} ' = C2.(fNames{i});'])
end

% Select TOM used for weight estimations
if C2.settings.useRefTOM == 1
    TOM_ref = a.ref.TOM;
else
    TOM_ref = TOM0;
end

% Crew provisions
M.operItems.CrewProv = operItems.geom.FF_crew*...
    (93*operItems.geom.NoFlightCrew + 68*operItems.geom.NoCabinCrew);

% Passenger cabin supplies
switch a.category
    case 'LightSingleEngine'    
        M.operItems.CabinSupplies = 0.453*fuselage.geom.NoPassengers;
    case 'LightMultiEngine'
        M.operItems.CabinSupplies = 0.453*fuselage.geom.NoPassengers;
    otherwise
        if operItems.geom.meals == 0 
            M.operItems.CabinSupplies = 0;
        elseif operItems.geom.meals == 1 
            M.operItems.CabinSupplies = operItems.geom.FF_cab*...
                (2.27*fuselage.geom.NoPassengers + 2.27*fuselage.geom.SeatsBusiness);
        elseif operItems.geom.meals == 2 
            M.operItems.CabinSupplies = operItems.geom.FF_cab*...
                (6.35*fuselage.geom.NoPassengers + 2.27*fuselage.geom.SeatsBusiness);
        elseif operItems.geom.meals == 3 
            M.operItems.CabinSupplies = operItems.geom.FF_cab*...
                (8.62*fuselage.geom.NoPassengers + 2.27*fuselage.geom.SeatsBusiness);
        else 
            error('Wrong meal settings specified for operational items')
        end
end

% Potable water & chemicals
switch a.category
    case 'LightSingleEngine'        % Assumed no toilets
        M.operItems.WaterAndChem = 0;
    case 'LightMultiEngine'         % Assumed no toilets
        M.operItems.WaterAndChem = 0;
    case 'JetTrainer'               % Assumed no toilets
        M.operItems.WaterAndChem = 0;
    otherwise
        if aircraft.R <= 1500*1e3   % ECTL: Short haul < 1500 km
            M.operItems.WaterAndChem = operItems.geom.FF_water*...
                0.68*fuselage.geom.NoPassengers;
        elseif aircraft.R > 1500*1e3 && aircraft.R < 4000*1e3
            M.operItems.WaterAndChem = operItems.geom.FF_water*...
                1.36*fuselage.geom.NoPassengers;
        elseif aircraft.R > 400*1e3 % ECTL: Long haul > 4000 km
            M.operItems.WaterAndChem = operItems.geom.FF_water*...
                2.95*fuselage.geom.NoPassengers;
        end
end

% Safety equipment
if operItems.geom.overwater == 1
    M.operItems.safeEquip = operItems.geom.FF_seq*...
        3.4*fuselage.geom.NoPassengers;
else
    M.operItems.safeEquip = operItems.geom.FF_seq*...
        0.907*fuselage.geom.NoPassengers;
end

% Residual fuel and oil
switch a.category
    case 'LightSingleEngine'    % Reciprocating engines consume oil
        M.operItems.resFuel = operItems.geom.FF_res*...
            0.008*TOM_ref + 0.045*M.f;
    case 'LightMultiEngine'
        M.operItems.resFuel = operItems.geom.FF_res*...
            0.008*TOM_ref + 0.045*M.f;
    case 'TransportMultiEngine'
        M.operItems.resFuel = operItems.geom.FF_res*...
            0.008*TOM_ref + 0.045*M.f;
    otherwise 
        tankVolume = (M.f/p.rho_fuel)*fuelSystem.geom.tank_fraction;
        M.operItems.resFuel = operItems.geom.FF_res*0.151*tankVolume^(2/3);
end

% Total operational-items mass
fNames = fieldnames(M.operItems);
M.operItemsMass = 0;
for i = 1:length(fNames)
    M.operItemsMass = M.operItemsMass + M.operItems.(fNames{i});
end



