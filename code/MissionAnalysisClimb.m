function [A] = MissionAnalysisClimb(a,p,f,s,c,con,aircraft,MA_in,h_target,M_target)
%% Initialize variables

% Select corresponding flight segment
A = MA_in.(con);

% Loop counter
k = 1;

% Time step
dt = s.dt.(con);

% Initial guess for delta eta_p
detap = 0;

% Aircraft characteristics
D2 = aircraft.D2;
D2_conv = aircraft.D2_conv;
Sw = aircraft.Sw;
Rc = aircraft.Rc;

% Powertrain component efficiencies
etas.GT = p.eta_GT;     % Gas turbine
etas.GB = p.eta_GB;     % Gearbox
etas.EM1 = p.eta_EM1;   % Primary electrical machine
etas.PM = p.eta_PM;     % PMAD (power management and distribution)
etas.EM2 = p.eta_EM2;   % Secondary electrical machine

% Gravity
g = c.g;

% First value of array
[~,aa,~,~] = atmosisa(A.h(1));
A.dhdt(1) = A.G(1)*A.v(1);
A.M(1) = A.v(1)/aa;


%% Loop over time steps until cruise altitude and speed are reached
[~,aa_target,~,~] = atmosisa(h_target);
while (A.h(k) < h_target - 1e-6 || A.v(k) < M_target*aa_target - 1e-9) 
    
    % Operating conditions
    h = A.h(k);
    v = A.v(k);
    [T_inf,aa,~,rho] = atmosisa(h);
    M = v/aa;
    Re = v*a.c_ref*rho/f.mu(T_inf);
    q = 0.5*rho*v^2;
    G = A.G(k);
    W = A.W(k);
    
    % Select power-control parameters
    if h >= 0 && h <= h_target
        xi = interp1([0 h_target],A.xi,h,'linear');
        phi = interp1([0 h_target],A.phi,h,'linear');
        Phi = interp1([0 h_target],A.Phi,h,'linear');
    else
        error('Altitude out of bounds')
    end
    
    % Compute available power at altitude
    % For serial/parallel/SPPH configurations, if phi = 1, then throttle
    % refers to electrical machine!
    switch p.config
        case 'conventional' 
            Pa = aircraft.Pdes.GTM.*f.Alpha(rho,M,c);
        case 'turboelectric'
            Pa = aircraft.Pdes.GTM.*f.Alpha(rho,M,c);
        case 'serial'
            if phi == 1
                Pa = aircraft.Pdes.EM2M;
            else
                Pa = aircraft.Pdes.GTM.*f.Alpha(rho,M,c);
            end
        case 'parallel'
            if phi == 1
                Pa = aircraft.Pdes.EM1M;
            else
                Pa = aircraft.Pdes.GTM.*f.Alpha(rho,M,c);
            end
        case 'PTE'
            Pa = aircraft.Pdes.GTM.*f.Alpha(rho,M,c);
        case 'SPPH'
            if phi == 1
                Pa = aircraft.Pdes.EM2M;
            else
                Pa = aircraft.Pdes.GTM.*f.Alpha(rho,M,c);
            end
        case 'e-1'
            Pa = aircraft.Pdes.EM1M;
        case 'e-2'
            Pa = aircraft.Pdes.EM2M;
        case 'dual-e'
            Pa = aircraft.Pdes.EM1M;
    end
    
    % Update gas turbine efficiency if required. If xi is NaN, use default
    % gas turbine efficiency
    if s.ComputeEtaGt == 1
        xi_In = xi;
        xi_In(isnan(xi)) = 1;
        etas.GT = p.eta_GT * f.etagt(xi_In,M);
    end
    
    % Calculate lift required and break down into delta CL and CLiso.
    % Iterate on lift and DP thrust coefficient
    if k == 1
        CL_iso0 = 0.5;
        Tc0 = 0.1;
        Tc0_conv = 0.1;
    else
        CL_iso0 = A.aero.CLiso(k-1);
        Tc0 = A.aero.Tc(k-1);
        Tc0_conv = A.aero.Tc_conv(k-1);
    end
    err = 1;
    while err > s.errmax
       
        % Estimate isolated prop efficiency based on Tc, if requested
        if s.ComputeEtap == 1 
            etap = f.etap(Tc0); 
            etap_conv = f.etap(Tc0_conv); 
        else  
            if p.DP == 2
                etap = p.(con).etap2; 
                etap_conv = p.(con).etap1;
            elseif  p.DP == 1
                etap = p.(con).etap1; 
                etap_conv = p.(con).etap2;
            else
                if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ||...
                        strcmp(p.config,'e-1')
                    etap = p.(con).etap2;
                    etap_conv = p.(con).etap1;
                elseif strcmp(p.config,'serial') || strcmp(p.config,'turboelectric') ||...
                        strcmp(p.config,'e-2')
                    etap = p.(con).etap1;
                    etap_conv = p.(con).etap2;
                else
                    etap = p.(con).etap2;
                    etap_conv = p.(con).etap1;
                end
            end
        end
        
        % Recompute thrust share
        if p.DP == 1 && ~isnan(Phi)
            chi = 1/(Phi/(1-Phi) * etap_conv/(etap+detap) + 1);
        elseif (p.DP == 2 || p.DP == 0) && ~isnan(Phi)
            chi = 1/((1-Phi)/Phi * etap_conv/(etap+detap) + 1);
        else
            chi = p.(con).chi(end);
        end
        
        % Update propulsive efficiency (use delta from previous iteration)
        if p.DP == 2
            etas.P1 = etap_conv;
            etas.P2 = etap + detap;
        elseif p.DP == 1
            etas.P1 = etap + detap;
            etas.P2 = etap_conv;
        else
            if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ||...
               strcmp(p.config,'e-1')
                etas.P1 = etap_conv;
                etas.P2 = etap;
            elseif strcmp(p.config,'serial') || strcmp(p.config,'turboelectric') ||...
                   strcmp(p.config,'e-2')
                etas.P1 = etap;
                etas.P2 = etap_conv;
            else
                etas.P1 = etap_conv;
                etas.P2 = etap;
            end
        end
        
        % Compute thrust
        [P_out,xi_out,phi_out,Phi_out,~,xi_flag] = ...
                PowerTransmissionComputation_v3(p.config,...
                                        etas,phi,Phi,xi,[],[],[],Pa);
        T = P_out.p/v;
        
        % Update thrust coefficients
        Tc1 = chi*T/p.N/rho/v^2/D2;
        Tc1_conv = (1-chi)*T/p.N_conv/rho/v^2/D2_conv;
        
        % Calculate required total lift coefficient
        CL = W/Sw/q*((1-G^2)^0.5-chi*sind(p.(con).Gamma)*T/W);
        
        % Obtain deltas
        oper.e = a.(con).e;
        oper.CD0 = a.(con).CD0;
        oper.Gamma = p.(con).Gamma;
        oper.CL = CL_iso0;
        oper.M = M;
        oper.etap = etap;
        oper.Tc = Tc1;
        oper.Tc_conv = Tc1_conv;
        oper.Rc = Rc;
        oper.Re = Re;
        [dCL,dCD0,dCDi,detap] = WingPropDeltas_v5(oper,a,p,f,s,c,0);
        
        % Check convergence of lift coefficient and propulsive efficiency
        % and update
        CL_iso = CL-dCL;
        err1 = abs((CL_iso-CL_iso0)/CL_iso0);
        if  any([(Tc1 ==0 && Tc0 == 0),(Tc1 == Inf && Tc0 == Inf),isnan(Tc1),isnan(Tc0)])
            err2 = 0;
        else
            err2 = abs((Tc1-Tc0)/Tc0);
        end
        if any([(Tc1_conv ==0 && Tc0_conv == 0),(Tc1_conv == Inf && Tc0_conv == Inf),...
                isnan(Tc1_conv),isnan(Tc0_conv)])
            err3 = 0;
        else
            err3 = abs((Tc1_conv-Tc0_conv)/Tc0_conv);
        end
        err = err1 + err2 + err3;
        CL_iso0 = CL_iso;
        Tc0 = Tc1;       
        Tc0_conv = Tc1_conv; 
        
        % If the error becomes a NaN anyway, the code
        % will crash or provide an unconverged solution. Note that err will
        % never be complex because of the abs function
        if isnan(err)
            warning('Convergence error attained NaN value')
            CL_iso = NaN;
            Tc1_conv = NaN;
            Tc1 = NaN;
            break
        end
    end
    
    % Compute drag
    CD_iso = f.CD(a.(con).CD0,CL_iso,a.(con).CL_minD,a.AR,a.(con).e);
    D = (CD_iso + dCD0 + dCDi)*q*Sw;

    % Compute excess power
    SEP = ((1-chi*(1-cosd(p.(con).Gamma)))*T - D)*v/W;
    
    %{
    % METHOD 1: PREDEFINED POWER SHARE PROFILE
    % Distinguish cases: cruise altitude reached
    if h >= h_target
        x = 0;
      
    % Cruise velocity reached
    elseif v >= M_target*f.a(h_target) && v/f.a(h) >= M_target;
        x = 1;

    % Any other point: follow x-profile    
    else
        x = interp1(h_array,x_array,h,'linear');
        if isnan(x); x = 0.5; end
    end
    %}
    
    % METHOD 2: Specified dM/dh
    % Altitude lapse of speed of sound
    dh = 0.1;
    [~,a1,~,~] = atmosisa(h);
    [~,a2,~,~] = atmosisa(h+dh);
    dadh = (a2-a1)/dh;
    
    % dM/dh gradient required to reach targets simultaneously
    dMdh = s.SFcl*(M_target-v/a1)/(h_target-h);
    
    % Corresponding X-share
    x = 1/(1 + a1*v/g*(dMdh + v/a1^2*dadh));
    
    % Limit X-share to [0,1]
    if x > 1
        x = 1;
    elseif x < 0
        x = 0;
    elseif isnan(x)
        error('Power share attained NaN value')
    end    
    
    % Calculate climb rate and horizontal acceleration
    dhdt = SEP*x;
    dVdt = SEP*(1-x)*g/v;
  
    % Cooling power required
    if s.WeightEstimationClass == 2
        P_EM1 = min([P_out.e1 P_out.gb])*(1 - p.eta_EM1);
        P_EM2 = min([P_out.e2 P_out.s2])*(1 - p.eta_EM2);
        P_PM = max([P_out.bat P_out.e1 P_out.e2])*(1 - p.eta_PM);
        P_bat = P_out.bat*(1 - p.eta_bat);
        Ps = sum([P_EM1 P_EM2 P_PM P_bat],'omitnan');
        A.P.cooling(k) = Ps*p.cooling_power;
    else
        A.P.cooling(k) = 0;
    end
    
    % Source power required for non-propulsive uses (currently assumed
    % constant and independent of flight condition). Note that this power
    % requirement is NOT included in power sizing of constraint diagram.
    if strcmp(p.config,'conventional') || strcmp(p.config,'turboelectric') || ...
       strcmp(p.config,'PTE')
        A.P.offtake(k) = MA_in.P_offtake*MA_in.N_pax/p.eta_GT;  % Extracted from shaft
        P_offtake_fuel = A.P.offtake(k);
        P_offtake_bat = 0;
        P_cooling_fuel = A.P.cooling(k);
        P_cooling_bat = 0;
    else
        A.P.offtake(k) = MA_in.P_offtake*MA_in.N_pax;           % Extracted from battery
        P_offtake_fuel = 0;
        P_offtake_bat =  A.P.offtake(k); 
        P_cooling_fuel = 0;
        P_cooling_bat = A.P.cooling(k);
    end
        
    % Deltas corresponding to this timestep. Note: Energy consumed = 
    % negative if discharging
    deltah = dhdt*dt;
    deltav = dVdt*dt;
    deltaR = v*dt*cos(G);
    deltaEbat = -(P_out.bat + P_offtake_bat + P_cooling_bat)*dt;    
    if strcmp(p.config,'e-1') || strcmp(p.config,'dual-e') ||...
            strcmp(p.config,'e-2')
        deltaEf = 0;
    else
        deltaEf = -(P_out.f + P_offtake_fuel + P_cooling_fuel)*dt;
    end
    deltaW = g*deltaEf/p.SE.f;
    if deltaW > 0; error('Fuel mass has increased'); end
    
    % Scale down last timestep proportional to the amount of range 
    % over-flown. Difference is small, but may improve smoothness of sizing
    % output if mission parameters are varied in sensitivity study
    if (A.h(k)+deltah) > h_target && ...
       (A.v(k)+deltav)/aa > M_target
        SF1 = (h_target-A.h(k))/(deltah);
        SF2 = (M_target-A.M(k))/(deltav/aa);
        SF = max([SF1 SF2]);
    else
        SF = 1;
    end
    
    % Update data for next iteration
    A.t(k+1) = A.t(k) + SF*dt;
    A.h(k+1) = min([h_target A.h(k)+SF*deltah]);
    A.W(k+1) = A.W(k) + SF*deltaW;
    [~,aa_next,~,~] = atmosisa(A.h(k+1)); 
    A.v(k+1) = min([M_target*aa_next A.v(k) + SF*deltav]);
    A.G(k+1) = dhdt/v;
    A.dVdt(k+1) = dVdt;
    A.dhdt(k+1) = dhdt;
    A.R(k+1) = A.R(k) + SF*deltaR;
    A.M(k+1) = A.v(k+1)/aa_next;
    A.Ebat(k+1) = A.Ebat(k) + SF*deltaEbat;
    A.Ef(k+1) = A.Ef(k) + SF*deltaEf;
    
    % Save aerodynamic variables
    A.aero.CLiso(k) = CL_iso;
    A.aero.CL(k) = CL_iso + dCL;
    A.aero.CDiso(k) = CD_iso;
    A.aero.CD(k) = CD_iso + dCD0 + dCDi;
    A.aero.LD(k) = A.aero.CL(k)/A.aero.CD(k);
    A.aero.Tc(k) = Tc1;
    A.aero.Tc_conv(k) = Tc1_conv;
    if p.DP == 1
        A.aero.etap1(k) = etap + detap;
        A.aero.etap2(k) = etap_conv;
        A.aero.etap1iso(k) = etap;
        A.aero.etap2iso(k) = etap_conv;
    elseif p.DP == 2
        A.aero.etap1(k) = etap_conv;
        A.aero.etap2(k) = etap + detap;
        A.aero.etap1iso(k) = etap_conv;
        A.aero.etap2iso(k) = etap;
    else
        if strcmp(p.config,'conventional') || ...
                strcmp(p.config,'parallel') ||...
                strcmp(p.config,'e-1')
            A.aero.etap1(k) = etap_conv;
            A.aero.etap2(k) = etap;
            A.aero.etap1iso(k) = etap_conv;
            A.aero.etap2iso(k) = etap;
        elseif strcmp(p.config,'serial') || ...
                strcmp(p.config,'turboelectric') ||...
                strcmp(p.config,'e-2')
            A.aero.etap1(k) = etap;
            A.aero.etap2(k) = etap_conv;
            A.aero.etap1iso(k) = etap;
            A.aero.etap2iso(k) = etap_conv;
        else
            A.aero.etap1(k) = etap_conv;
            A.aero.etap2(k) = etap;
            A.aero.etap1iso(k) = etap_conv;
            A.aero.etap2iso(k) = etap;
        end
    end
    
    % Save power-related variables
    A.P.x(k) = x;
    if strcmp(p.config,'e-1') || strcmp(p.config,'dual-e') ||...
            strcmp(p.config,'e-2')
        A.P.availableGT(k) = NaN;
    else
        A.P.availableGT(k) = Pa;
    end
    A.P.drag(k) = D*v;
    A.P.acceleration(k) = W*dVdt*v/g;
    A.P.climb(k) = W*dhdt;
    if k == 1; names = fieldnames(P_out); end
    for j = 1:size(names,1)
        A.P.(names{j})(k) = P_out.(names{j});
    end 
    A.P.etagt(k) = etas.GT;
    
    % Save power-control variables
    A.P.xi(k) = xi_out;
    A.P.xi_flag(k) = xi_flag;
    if strcmp(p.config,'serial') || strcmp(p.config,'parallel') ||...
            strcmp(p.config,'SPPH')
        A.P.phi(k) = phi_out;
    else
        A.P.phi(k) = NaN;
    end
    if strcmp(p.config,'PTE') || strcmp(p.config,'SPPH') || ...
            strcmp(p.config,'dual-e')
        A.P.Phi(k) = Phi_out;
    else
        A.P.Phi(k) = NaN;
    end
    A.P.chi(k) = chi;
    
    % Store effective propulsive efficiency
    if A.P.Phi(k) == 1|| strcmp(p.config,'turboelectric') || ...
            strcmp(p.config,'serial') || strcmp(p.config,'e-2')
        A.aero.etapAvg(k) = A.aero.etap2(k); 
    elseif A.P.Phi(k) == 0 || strcmp(p.config,'conventional') || ...
            strcmp(p.config,'parallel') || strcmp(p.config,'e-1')
        A.aero.etapAvg(k) = A.aero.etap1(k); 
    else
        A.aero.etapAvg(k) = (A.aero.etap1(k) + A.aero.etap2(k)*...
                             (A.P.Phi(k)/(1-A.P.Phi(k)))) / ...
                             (1 + (A.P.Phi(k)/(1-A.P.Phi(k)))); 
    end
    
    % Update counter, avoid excessive iterations
    k = k+1;
    if k > 5*s.itermax
        if strcmp(con,'Dcl')
            error('Could not reach diversion altitude/Mach number')
        else
            error('Could not reach cruise altitude/Mach number')
        end
    end
end

% Add last element to aerodynamic/power variables to make plotting arrays 
% same length
aeronames = fieldnames(A.aero);
for i = 1:size(aeronames,1)
    A.aero.(aeronames{i})(k) = NaN;
end
Pnames = fieldnames(A.P);
for i = 1:size(Pnames,1)
    A.P.(Pnames{i})(k) = NaN;
end
