function [M] = WeightEstimationClass2_servAndEquip(M,TOM0,a,m,p,f,s,c,aircraft,C2)
% For input/output see WeightEstimationClass2.m.
%
%%% Reynard de Vries
%%% First created: 29-03-22
%%% Last modified: 06-04-22

% Extract fields from "aircraft" structure (for shorter writing only...)
fNames = fieldnames(C2);
for i = 1:length(fNames)
    eval([fNames{i} ' = C2.(fNames{i});'])
end

% Select TOM used for weight estimations
if C2.settings.useRefTOM == 1
    TOM_ref = a.ref.TOM;
    DEM_ref = a.ref.TOM*a.ref.DEM_fraction;
    OEM_ref = a.ref.TOM*a.ref.OEM_fraction;
    PL_ref =  a.ref.TOM*a.ref.PL_fraction;
else
    TOM_ref = TOM0;
    DEM_ref = M.OEM - M.operItemsMass;
    OEM_ref = M.OEM;
    PL_ref = M.PL;
end

% APU
% Torenbeek: modern APU's up to 35% lighter; factor 2-2.5 for installation 
% penalty. Mass flow in [kg/min] dependent on cabin volume.
k_APU = 2.25*0.65; 
cabinVolume = (fuselage.geom.l_cabin * pi *(fuselage.geom.D_cabin/2)^2);
massFlowAPU = 0.4*cabinVolume; 
if servAndEquip.geom.hasAPU == 1
    M.servAndEquip.APU = servAndEquip.geom.FF_APU*k_APU*11.7*massFlowAPU^0.6;
else
    M.servAndEquip.APU = 0;
end

% Instruments and avionics
switch a.category
    case 'TransportTurboprop'  
        M.servAndEquip.InstrAndAvionics = servAndEquip.geom.FF_ieg*...
                    (54.4 + 9.1*a.ref.N + 0.006*TOM_ref);
    case 'TransportTurbofan'
        M.servAndEquip.InstrAndAvionics = servAndEquip.geom.FF_ieg*...
            0.347*(DEM_ref)^(5/9)*aircraft.R^0.25;
    case 'TransportMultiEngine'
        if servAndEquip.hasIFR == 1
            M.servAndEquip.InstrAndAvionics = servAndEquip.geom.FF_ieg*...
                (54.4 + 9.1*a.ref.N + 0.006*TOM_ref);
        else
            M.servAndEquip.InstrAndAvionics = servAndEquip.geom.FF_ieg*...
                (18.1 + 0.008*TOM_ref);
        end
    case 'LightSingleEngine'    
        M.servAndEquip.InstrAndAvionics = servAndEquip.geom.FF_ieg*...
            (10 + 3.6*servAndEquip.geom.NoPilots);
    case 'LightMultiEngine'
        if servAndEquip.hasIFR == 1
            M.servAndEquip.InstrAndAvionics = servAndEquip.geom.FF_ieg*...
                (54.4 + 9.1*a.ref.N + 0.006*TOM_ref);
        else
            M.servAndEquip.InstrAndAvionics = servAndEquip.geom.FF_ieg*...
                (18.1 + 0.008*TOM_ref);
        end
    case 'JetTrainer'           % Should this use a different correlation?
        M.servAndEquip.InstrAndAvionics = servAndEquip.geom.FF_ieg*...
            0.347*(DEM_ref)^(5/9)*aircraft.R^0.25;
    case 'BusinessJet'          % Should this use a different correlation?
        M.servAndEquip.InstrAndAvionics = servAndEquip.geom.FF_ieg*...
            0.347*(DEM_ref)^(5/9)*aircraft.R^0.25;
end

% Hydraulic, pneumatic, and electrical systems
switch a.category
    case 'TransportMultiEngine'
        M.servAndEquip.HydrPneuElec = servAndEquip.geom.FF_hydr*...
            0.277*(OEM_ref)^(4/5);
    case 'LightSingleEngine'    
        M.servAndEquip.HydrPneuElec = servAndEquip.geom.FF_hydr*...
            0.00914*(OEM_ref)^(6/5);
    case 'LightMultiEngine' 
        M.servAndEquip.HydrPneuElec = servAndEquip.geom.FF_hydr*...
            0.00914*(OEM_ref)^(6/5);
    case 'JetTrainer'     
        M.servAndEquip.HydrPneuElec = servAndEquip.geom.FF_hydr*...
            0.064*(OEM_ref);
    otherwise % Seperate Hydr/Pneu from electrical system for large aircraft
        if servAndEquip.geom.poweredControls == 0
            M.servAndEquip.hydrPneuSystems = servAndEquip.geom.FF_hydr*...
                (45 + 0.004*OEM_ref);
        elseif servAndEquip.geom.poweredControls == 1
            M.servAndEquip.hydrPneuSystems = servAndEquip.geom.FF_hydr*...
                (91 + 0.007*OEM_ref);
        elseif servAndEquip.geom.poweredControls == 2
            M.servAndEquip.hydrPneuSystems = servAndEquip.geom.FF_hydr*...
                (181 + 0.011*OEM_ref);
        elseif servAndEquip.geom.poweredControls == 3
            M.servAndEquip.hydrPneuSystems = servAndEquip.geom.FF_hydr*...
                (272 + 0.015*OEM_ref);
        end
        if servAndEquip.geom.hasDCsystem == 1
            M.servAndEquip.elecSystem = servAndEquip.geom.FF_elec*...
                (181 + 0.02*TOM_ref);
        else
            if servAndEquip.geom.elecFromAPU == 1
                P_elec = 3.64*cabinVolume^0.7;
            else
                P_elec = 0.565*cabinVolume;
            end
            M.servAndEquip.elecSystem = servAndEquip.geom.FF_elec*...
                (16.3*P_elec*(1-0.033*P_elec^0.5));
        end
end

% Furnishing (to be improved using table 8.12)
switch a.category
    case 'TransportMultiEngine'
        M.servAndEquip.furnishing = servAndEquip.geom.FF_furn* ...
            (6.3*fuselage.geom.NoPassengers + 16*cabinVolume);
    case 'LightSingleEngine'    
        M.servAndEquip.furnishing = servAndEquip.geom.FF_furn* ...
            (2.3 + 5.9*fuselage.geom.NoPassengers + 11.3*...
            ceil(fuselage.geom.NoPassengers/fuselage.geom.SeatsAbreast));
    case 'LightMultiEngine' 
        M.servAndEquip.furnishing = servAndEquip.geom.FF_furn* ...
            (6.3*fuselage.geom.NoPassengers + 16*cabinVolume);
    case 'JetTrainer'     
        M.servAndEquip.furnishing = servAndEquip.geom.FF_furn* ...
            0.065*DEM_ref;
    otherwise 
     M.servAndEquip.furnishing = servAndEquip.geom.FF_furn* ...
        0.196*(OEM_ref + PL_ref)^0.91;
end

% Airco & anti-icing
switch a.category
    case 'TransportMultiEngine' % Applying same as light multi engine, assuming unpressurized
        M.servAndEquip.AircoAntiIce = servAndEquip.geom.FF_AircoAntiIce* ...
            0.018*DEM_ref;
    case 'LightSingleEngine'    
        M.servAndEquip.AircoAntiIce = servAndEquip.geom.FF_AircoAntiIce* ...
            1.1*fuselage.geom.NoPassengers;
    case 'LightMultiEngine' 
        M.servAndEquip.AircoAntiIce = servAndEquip.geom.FF_AircoAntiIce* ...
            0.018*DEM_ref;
    case 'JetTrainer'           % Applying same as light multi engine, assuming unpressurized
        M.servAndEquip.AircoAntiIce = servAndEquip.geom.FF_AircoAntiIce* ...
            0.018*DEM_ref;
    otherwise 
        M.servAndEquip.AircoAntiIce = servAndEquip.geom.FF_AircoAntiIce* ...
            14*fuselage.geom.l_cabin^1.28;
end

% Miscellaneous
M.servAndEquip.misc = 0.01*DEM_ref; 

% Total services & equipment mass
fNames = fieldnames(M.servAndEquip);
M.servAndEquipMass = 0;
for i = 1:length(fNames)
    M.servAndEquipMass = M.servAndEquipMass + M.servAndEquip.(fNames{i});
end




