%%% Description
%
% This routine calculates the weight breakdown of the aircraft by analyzing
% a sizing mission using the wing- and power-loading values obtained from
% the WS-WP diagram. The mission is assumed to be comprised of initial 
% energy fractions for TO/taxi + climb + cruise + descent + climb + diversion
% + descent + loiter + final energy fractions for landing/taxi.
%
%%% Reynard de Vries
%%% TU Delft
%%% Date created: 29-03-17
%%% Last modified: 05-05-22


%% Initialize

% Get aerodynamic & propulsive characteristics from structure of the
% constraint selected in the input file. Cruise phase is directly taken
% from cruise constraint.
segmentNames = {'cl','de','Dcl','Dcr','Dde','Loiter'};
for i = 1:length(segmentNames)
    a.(segmentNames{i}) = a.(MA_in.GetProperties.(segmentNames{i}));
    p.(segmentNames{i}) = p.(MA_in.GetProperties.(segmentNames{i}));
end

% Powertrain components/paths to be verified (fields of WPdes.(SelDes)
% structure)
Pnames = {'GTM','EM1','EM2','bat','s1'};


%% Start loop on MTOM

% Initial guess of take-off mass [kg]. 
if electric == 0
    TOM0 = (MA_in.PL+MA_in.OEM)/(1-FF_tot0*(1+p.SE.f/p.SE.bat*DOH_tot0/(1-DOH_tot0)));
else
    TOM0 = (MA_in.PL+MA_in.OEM)/(1-BF_tot0);
end

% Initialize loop variables
count = 0;
err = 1;
conv.DOH = DOH_tot0;
if electric == 0; conv.FF = FF_tot0; else; conv.FF = BF_tot0; end
conv.TOM = TOM0;
conv.err = err;

% Initial guess for scaling fractions
RatioGTM0 = 1;
RatioEM10 = 1;
RatioEM20 = 1;
Ratiobat0 = 1;
Ratios10 = 1;

% % Figure for debugging: plot progress
% figure(102)
% % subplot(2,2,1); hold on; grid on;
% % subplot(2,2,2); hold on; grid on;
% % subplot(2,2,3); hold on; grid on;
% % subplot(2,2,4); hold on; grid on;
% hold on; grid on; view(3);
% drawnow
    
% Loop till TOM convergence
while err > s.errmax
    
    % Progress
    count = count + 1;
    if s.printMessages == 1
        disp([s.levelString '  > Iteration ' num2str(count)])
    end
    
    % Installed powers [W] 
    aircraft.Pdes = structfun(@(x) TOM0*c.g./x,WPdes.(s.SelDes),'UniformOutput',0); 
    aircraft.Pdes_ref = structfun(@(x) TOM0*c.g./x,WPdes_ref.(s.SelDes),'UniformOutput',0); 
    
    % DP Disk area of each DP propulsor (D^2) [m2]
    aircraft.D2W = f.D2W(WSdes.(s.SelDes),a.AR,p);
    aircraft.D2 = c.g*TOM0*aircraft.D2W;
    
    % DP Disk area of each non-DP propulsor (D^2) [m2]
    aircraft.D2W_conv = f.D2W_conv(WSdes.(s.SelDes),a.AR,p);
    aircraft.D2_conv = c.g*TOM0*aircraft.D2W_conv;
    
    % Wing area [m2] 
    aircraft.WSdes = WSdes.(s.SelDes);
    aircraft.Sw = c.g*TOM0/WSdes.(s.SelDes);
    aircraft.Sw_ref = c.g*TOM0/WSdes_ref.(s.SelDes);
    
    % Prop radius/wing chord ratio
    aircraft.Rc = 0.5*(aircraft.D2W*WSdes.(s.SelDes)*a.AR)^0.5;
    
    % Save aircraft weight
    aircraft.TOM = TOM0;
    
    % Save properties at take-off for conversion to thrust, used for 
    % reference TF thrust/weight estimation  
    % (etas incl. aero-propulsive effects)
    aircraft.v_TO = AEROdes.(s.SelDes).TO.v;
    aircraft.etap1 = AEROdes.(s.SelDes).TO.etap1;
    aircraft.etap2 = AEROdes.(s.SelDes).TO.etap2;
    
    % Update end-of-cruise altitudes
    if count == 1
        h_target = m.cr.h;
        h_targetDiversion = MA_in.h_div;
    else
        if s.CruiseMode == 2
            h_target = MA.cr.h(end);
            h_targetDiversion = MA.Dcr.h(end);
        else
            h_target = m.cr.h;
            h_targetDiversion = MA_in.h_div;
        end
    end    
    
    % Clear MA structure to allow new arrays of different length
    clear('MA')
          
    % Fuel mass used during nominal mission and diversion
    Mf_miss0 = TOM0*FF_miss0;
    Mf_tot0 = TOM0*FF_tot0;
    Mf_div = Mf_tot0-Mf_miss0;
    
    % Battery energy used during nominal mission and diversion (so excl.
    % SOC margin/power sizing margin)
    if electric == 0
        Ebat_tot0 = Mf_tot0*DOH_tot0/(1-DOH_tot0)*p.SE.f;
    else
        Ebat_tot0 = TOM0*BF_tot0*p.SE.bat;
    end
    
    % Total energy on board (make sure battery energy is not NaN; or it
    % will propagate into the weight estimations). Only includes energy
    % that is USED, so no extra battery energy used to maintain the SOC
    % margin or for power requirements
    if isnan(Ebat_tot0); Ebat_tot0 = 0; end
    E_tot = Ebat_tot0 + Mf_tot0*p.SE.f;
    
    % Energy consumed during taxi/TO/L
    MA.EF.E_bat_TakeOff = E_tot * MA_in.EF.TakeOff * MA_in.EF.phiTakeOff;
    MA.EF.E_bat_TaxiOut = E_tot * MA_in.EF.TaxiOut * MA_in.EF.phiTaxiOut;
    MA.EF.E_bat_Landing = E_tot * MA_in.EF.Landing * MA_in.EF.phiLanding;
    MA.EF.E_bat_TaxiIn =  E_tot * MA_in.EF.TaxiIn  * MA_in.EF.phiTaxiIn;
    MA.EF.E_f_TakeOff =   E_tot * MA_in.EF.TakeOff * (1-MA_in.EF.phiTakeOff);
    MA.EF.E_f_TaxiOut =   E_tot * MA_in.EF.TaxiOut * (1-MA_in.EF.phiTaxiOut);
    MA.EF.E_f_Landing =   E_tot * MA_in.EF.Landing * (1-MA_in.EF.phiLanding);
    MA.EF.E_f_TaxiIn =    E_tot * MA_in.EF.TaxiIn  * (1-MA_in.EF.phiTaxiIn);
    
    % Shorthand notation for input to mission segments
    EfStart = MA.EF.E_f_TakeOff + MA.EF.E_f_TaxiOut;
    WfStart = EfStart/p.SE.f*c.g;
    WfLanding = MA.EF.E_f_Landing/p.SE.f*c.g;   % Included in "mission" 
    WfTaxIn = MA.EF.E_f_TaxiIn/p.SE.f*c.g;      % Included in "div" 
    
    
    %% 1. Climb segment
    % During climb the output power is specified, while the flight path is not
    
    % Initial conditions
    MA_in.cl.t(1) = 0;                          % Start of time vector
    MA_in.cl.R(1) = 0;                          % No range flown yet
    MA_in.cl.h(1) = m.TO.h;                     % Altitude of TO runway
    MA_in.cl.W(1) = TOM0*c.g - WfStart;         % Start at TOM except fuel used for taxi/TO
    MA_in.cl.v(1) = AEROdes.(s.SelDes).TO.v;    % Take V2 speed as initial speed
    MA_in.cl.G(1) = 0.05;                       % Give generic initial climb gradient
    MA_in.cl.dVdt(1) = 0.1;                     % Give generic initial acceleration
    MA_in.cl.Ebat(1) = 0;                       % All battery energy left except taxi/TO
    MA_in.cl.Ef(1) = Mf_tot0*p.SE.f - EfStart;  % All fuel left except taxi/TO

    % Call function
    [MA.cl] = MissionAnalysisClimb(a,p,f,s,c,'cl',aircraft,MA_in,...
        m.cr.h,m.cr.M);
    
    
    %% 2. Descent segment
    % For the descent segment, the program iterates backwards, starting at
    % landing altitude, with zero fuel and zero battery energy left
    
    % Initial conditions: first index corresponds to last timestep. Output
    % arrays are flipped such that the first index corresponds to first
    % timestep.
    MA_in.de.t(1) = 0;                          % Start of time vector
    MA_in.de.R(1) = MA_in.R;                    % End of nominal mission
    MA_in.de.h(1) = m.L.h;                      % Altitude of landing runway
    MA_in.de.W(1) = TOM0*c.g - (Mf_miss0*c.g - WfLanding);%+ WfEnd;% Diversion fuel weight + taxi-in 
                                                %  (NOT landing, that's considered part of the section!)
    MA_in.de.v(1) = m.L.vs*1.3;                 % Approach speed
    MA_in.de.G(1) = 0.0;                        % Generic final climb gradient
    MA_in.de.dVdt(1) = 0.0;                     % Generic final deceleration
    MA_in.de.Ebat(1) = 0;                       % Generic value; curves will be shifted in ComputeWeights.m
    MA_in.de.Ef(1) = Mf_div*p.SE.f + MA.EF.E_f_Landing;%+ EfEnd;     % Only diversion fuel energy left + L/taxi

    % Call function
    [MA.de] = MissionAnalysisDescent(a,p,f,s,c,'de',aircraft,MA_in,...
        h_target,m.cr.M);

                         
    %% 3. Cruise segment
    % During cruise the flight path is specified, while one of the power control
    % parameters is not
    
    % Range flown during cruise segment (depends on ranges covered in climb and
    % descent phases) [m]
    R_climb = max(MA.cl.R);
    R_descent = max(MA.de.R)-min(MA.de.R);
    R_cruise = MA_in.R - R_climb - R_descent;
    if R_cruise < 0
        error(['Climb and descent require more than the '...
                'specified nominal mission range'])
    end
    
    % Initial conditions
    [~,aa,~,~] = atmosisa(m.cr.h);
    MA_in.cr.t(1) = MA.cl.t(end);
    MA_in.cr.R(1) = MA.cl.R(end);
    MA_in.cr.h(1) = m.cr.h;
    MA_in.cr.W(1) = MA.cl.W(end);
    MA_in.cr.v(1) = m.cr.M*aa;
    MA_in.cr.G(1) = 0; 
    MA_in.cr.dVdt(1) = 0;
    MA_in.cr.Ebat(1) = 0;
    MA_in.cr.Ef(1) = MA.cl.Ef(end);
    
    % Call function
    [MA.cr] = MissionAnalysisCruise(a,p,f,s,c,'cr',aircraft,...
        MA_in,R_cruise);

    % Shift time-axis of descent phase
    MA.de.t = MA.de.t + abs(min(MA.de.t)) + max(MA.cr.t);
    
          
    %% 4. Loiter Segment
    % For the loiter segment, the program iterates backwards, starting at
    % loiter altitude, with zero fuel and zero battery energy left. The 
    % loiter occurs "after" the diversion, with a discontinuity in velocity
    % and altitude. It is not placed in the middle of the nominal mission
    % descent, because then the representative nominal-mission energy
    % consumption can't be computed.
    
    % Initial conditions: first index corresponds to last timestep. Output
    % arrays are flipped such that the first index corresponds to first
    % timestep.
    MA_in.Loiter.t(1) = 0;                      % Start of time vector
    MA_in.Loiter.R(1) = MA_in.R+MA_in.R_div;    % End-of-mission
    MA_in.Loiter.h(1) = MA_in.h_Loiter;         % Altitude of loiter
    MA_in.Loiter.W(1) = (TOM0-Mf_tot0)*c.g+WfTaxIn+WfLanding;%WfEnd;% No fuel weight left 
    MA_in.Loiter.v(1) = m.L.vs*1.3;             % Diversion Mach number (initial guess)
    MA_in.Loiter.G(1) = 0.0;                    % Constant altitude
    MA_in.Loiter.dVdt(1) = 0.0;                 % Generic final deceleration
    MA_in.Loiter.Ebat(1) = 0;                   % Generic value; curves will be shifted in ComputeWeights.m
    MA_in.Loiter.Ef(1) = MA.EF.E_f_Landing + MA.EF.E_f_TaxiIn;%EfEnd;                 % Fuel left for taxi/L
    
    % Loiter time in seconds
    t_target = MA_in.t_Loiter*60;
    
    % Min and max velocity range
    [~,aa,~,~] = atmosisa(MA_in.Loiter.h(1));
    vLimits = [m.L.vs*1.3 m.cr.M*aa];
    
    % Call function, only if loiter is required
    if MA_in.t_Loiter > 0
        [MA.Loiter] = MissionAnalysisLoiter(a,p,f,s,c,'Loiter',aircraft,MA_in,t_target,vLimits);
    else
        
        % Use NaN's to fill up all fields
        MA.Loiter = MA_in.Loiter;
        MA.Loiter.dhdt = 0;
        MA.Loiter.M = MA_in.Loiter.v(1)/aa;
        MA.Loiter.limits = 0;
        pNames = fieldnames(MA.cr.P);
        for idxs = 1:length(pNames); MA.Loiter.P.(pNames{idxs}) = NaN; end
        aeroNames = fieldnames(MA.cr.aero);
        for idxs = 1:length(aeroNames); MA.Loiter.aero.(aeroNames{idxs}) = NaN; end
    end
    
    
    %% 5. Diversion climb segment
    % During climb the output power is specified, while the flight path is not
    
    % Initial conditions
    MA_in.Dcl.t(1) = max(MA.de.t);              % Start of time vector
    MA_in.Dcl.R(1) = MA_in.R;                   % Nominal mission range flown
    MA_in.Dcl.h(1) = m.L.h;                     % Altitude of L runway
    MA_in.Dcl.W(1) = MA.de.W(end);              % After nominal mission
    MA_in.Dcl.v(1) = AEROdes.(s.SelDes).TO.v;   % Balked landing speed (use 'bL' constraint if available)
    MA_in.Dcl.G(1) = 0.01;                      % Give generic initial climb gradient
    MA_in.Dcl.dVdt(1) = 0.1;                    % Give generic initial acceleration
    MA_in.Dcl.Ebat(1) = 0;                      % Generic value; curves will be shifted in ComputeWeights.m
    MA_in.Dcl.Ef(1) = Mf_div*p.SE.f + MA.EF.E_f_Landing;%EfEnd;    % Diversion fuel left + taxi/L

    % Call function, only if diversion is required
    if MA_in.R_div > 0
        [MA.Dcl] = MissionAnalysisClimb(a,p,f,s,c,'Dcl',aircraft,MA_in,...
                                        MA_in.h_div,MA_in.M_div);
                                    
    % Create 1-element arrays with initial values only if not
    else
        [~,aa,~,~] = atmosisa(MA_in.Dcl.h(1));
        MA.Dcl = MA_in.Dcl;
        MA.Dcl.dhdt = 0;
        MA.Dcl.M = MA_in.Loiter.v(1)/aa;
        MA.Dcl.limits = 0;
        pNames = fieldnames(MA.cr.P);
        for idxs = 1:length(pNames); MA.Dcl.P.(pNames{idxs}) = NaN; end
        aeroNames = fieldnames(MA.cr.aero);
        for idxs = 1:length(aeroNames); MA.Dcl.aero.(aeroNames{idxs}) = NaN; end       
    end
    
        
    %% 6. Diversion descent segment
    % For the descent segment, the program iterates backwards, starting at
    % landing altitude, with zero fuel and zero battery energy left
    
    % Initial conditions: first index corresponds to last timestep. Output
    % arrays are flipped such that the first index corresponds to first
    % timestep.
    MA_in.Dde.t(1) = 0;                         % Start of time vector
    MA_in.Dde.R(1) = MA_in.R+MA_in.R_div;       % End-of-mission
    MA_in.Dde.h(1) = m.L.h;                     % Altitude of landing runway
    MA_in.Dde.W(1) = MA.Loiter.W(1);            % Fuel weight left at landing is for loiter
    MA_in.Dde.v(1) = m.L.vs*1.3;                % Approach speed
    MA_in.Dde.G(1) = 0.0;                       % Generic final climb gradient
    MA_in.Dde.dVdt(1) = 0.0;                    % Generic final deceleration
    MA_in.Dde.Ebat(1) = 0;                      % Generic value; curves will be shifted in ComputeWeights.m
    MA_in.Dde.Ef(1) = MA.Loiter.Ef(1);          % Loiter & landing fuel left

    % Call function, only if diversion is required
    if MA_in.R_div > 0
        [MA.Dde] = MissionAnalysisDescent(a,p,f,s,c,'Dde',aircraft,MA_in,...
                                          h_targetDiversion,MA_in.M_div);

    % Create 1-element arrays with initial values only if not
    else
        [~,aa,~,~] = atmosisa(MA_in.Dde.h(1));
        MA.Dde = MA_in.Dde;
        MA.Dde.dhdt = 0;
        MA.Dde.M = MA_in.Dde.v(1)/aa;
        MA.Dde.limits = 0;
        for idxs = 1:length(pNames); MA.Dde.P.(pNames{idxs}) = NaN; end
        for idxs = 1:length(aeroNames); MA.Dde.aero.(aeroNames{idxs}) = NaN; end   
    end
    
    
    %% 7. Diversion cruise segment
    % During cruise the flight path is specified, while one of the power control
    % parameters is not
    
    % Range flown during cruise segment (depends on ranges covered in climb and
    % descent phases) [m]
    R_Dclimb = max(MA.Dcl.R)-min(MA.Dcl.R);
    R_Ddescent = max(MA.Dde.R)-min(MA.Dde.R);
    R_Dcruise = MA_in.R_div - R_Dclimb - R_Ddescent;
    if R_Dcruise < 0
        error(['Diversion climb and descent require more than the '...
                'specified diversion range'])
    end
    
    % Initial conditions
    [~,aa,~,~] = atmosisa(MA_in.h_div);
    MA_in.Dcr.t(1) = MA.Dcl.t(end);
    MA_in.Dcr.R(1) = MA.Dcl.R(end);
    MA_in.Dcr.h(1) = MA_in.h_div;
    MA_in.Dcr.W(1) = MA.Dcl.W(end);
    MA_in.Dcr.v(1) = MA_in.M_div*aa;
    MA_in.Dcr.G(1) = 0;
    MA_in.Dcr.dVdt(1) = 0;
    MA_in.Dcr.Ebat(1) = 0;
    MA_in.Dcr.Ef(1) = MA.Dcl.Ef(end);
    
    % Call function, only if diversion is required
    if MA_in.R_div > 0
        [MA.Dcr] = MissionAnalysisCruise(a,p,f,s,c,'Dcr',aircraft,...
                                         MA_in,R_Dcruise);

    % Create 1-element arrays with initial values only if not
    else
        [~,aa,~,~] = atmosisa(MA_in.Dcr.h(1));
        MA.Dcr = MA_in.Dcr;
        MA.Dcr.dhdt = 0;
        MA.Dcr.M = MA_in.Dcr.v(1)/aa;
        MA.Dcr.limits = 0;
        for idxs = 1:length(pNames); MA.Dcr.P.(pNames{idxs}) = NaN; end
        for idxs = 1:length(aeroNames); MA.Dcr.aero.(aeroNames{idxs}) = NaN; end   
    end
    
    % Shift time-axis of descent & loiter phases
    MA.Dde.t = MA.Dde.t + abs(min(MA.Dde.t)) + max(MA.Dcr.t);
    MA.Loiter.t = MA.Loiter.t + abs(min(MA.Loiter.t)) + max(MA.Dde.t);
    
    
    %% Re-size powertrain components if required
    if s.MAResize == 1
        
        % Check each constraint
        names = fieldnames(MA);
        names(contains(names,'EF')) = [];
        Ncon = size(names,1);
        
        % Check all five powertrain branches
        for j = 1:size(Pnames,2)
            switch Pnames{j}
                
                % For the gas turbine, compute how much corrected GT power
                % is required
                case 'GTM'
                    Preq = [];
                    
                    % Gather power requirements of all mission phases in
                    % one array
                    for i = 1:Ncon
                        [~,~,~,rho] = atmosisa(MA.(names{i}).h);
                        Preq = [Preq (MA.(names{i}).P.gt./...
                                f.Alpha(rho,MA.(names{i}).M,c))];
                            
                        % Note 04-03-22: The re-sized GT should not only be
                        % able to produce the required power, but to
                        % produce that power *at the throttle setting*
                        % selected for that time instance. This only
                        % applies to flight phases where throttle is an
                        % input, and not an output.
                        % However, 
                        % - If throttle is an input (e.g. climb), then you
                        %   can't exceed installed GT power
                        % - If throttle is an output (e.g. cruise), then 
                        %   you'd scale the GT to be able to produce the
                        %   throttle with xi = 1 anyway, so need to include
                        %   the factor
                    end
                    
                    % Select most critical condition
                    Preq = max(Preq,[],'omitnan');
                    
                    % If greater than installed power, scale up
                    % power-loading of all components of this branch and
                    % issue warning
                    if Preq > aircraft.Pdes.GTM
                        RatioGTM = Preq/aircraft.Pdes.GTM;
                        WPdes.(s.SelDes).GT = WPdes.(s.SelDes).GT/RatioGTM;
                        WPdes.(s.SelDes).GTM = WPdes.(s.SelDes).GTM/RatioGTM;
                        WPdes.(s.SelDes).f = WPdes.(s.SelDes).f/RatioGTM;
                        
                        % Display modification, unless within numerical
                        % error margin
                        if RatioGTM > 1+1e-10
                            if s.printMessages == 1
                                disp([s.levelString '    > Gas turbine could not '...
                                    'meet power requirements. Scaling up by a factor '...
                                    num2str(RatioGTM)])
                            end
                        end
                        
                    % If not greater than installed power, set ratio to 1 
                    % for error convergence
                    else
                        RatioGTM = 1;
                    end
                    
                % Primary electrical machine
                case 'EM1'
                    Preq = [];
                    for i = 1:Ncon
                        Preq = [Preq max([abs(MA.(names{i}).P.e1);...
                                          abs(MA.(names{i}).P.gb)],[],1)];
                    end
                    Preq = max(Preq);
                    if strcmp(p.config,'e-1') || strcmp(p.config,'dual-e')
                        Pavail = aircraft.Pdes.EM1M;
                    else
                        Pavail = aircraft.Pdes.EM1;
                    end
                    if Preq > Pavail
                        RatioEM1 = Preq/Pavail;
                        WPdes.(s.SelDes).EM1 = WPdes.(s.SelDes).EM1/RatioEM1;
                        WPdes.(s.SelDes).EM1M = WPdes.(s.SelDes).EM1M/RatioEM1;
                        if RatioEM1 > 1+1e-10
                            if s.printMessages == 1
                                disp([s.levelString '    > Primary EM could not '...
                                    'meet power requirements. Scaling up by a factor '...
                                    num2str(RatioEM1)])
                            end
                        end
                    else
                        RatioEM1 = 1;
                    end
                    
                % Secondary electrical machine
                case 'EM2'
                    Preq = [];
                    for i = 1:Ncon
                        Preq = [Preq max([abs(MA.(names{i}).P.s2);...
                                          abs(MA.(names{i}).P.e2)],[],1)];
                    end
                    Preq = max(Preq);
                    if strcmp(p.config,'e-2') 
                        Pavail = aircraft.Pdes.EM2M;
                    else
                        Pavail = aircraft.Pdes.EM2;
                    end
                    if Preq > Pavail
                        RatioEM2 = Preq/Pavail;
                        WPdes.(s.SelDes).EM2 = WPdes.(s.SelDes).EM2/RatioEM2;
                        WPdes.(s.SelDes).EM2M = WPdes.(s.SelDes).EM2M/RatioEM2;
                        WPdes.(s.SelDes).s2 = WPdes.(s.SelDes).s2/RatioEM2;
                        if RatioEM2 > 1+1e-10
                            if s.printMessages == 1
                                disp([s.levelString '    > Secondary EM could not '...
                                    'meet power requirements. Scaling up by a factor '...
                                    num2str(RatioEM2)])
                            end
                        end
                    else
                        RatioEM2 = 1;
                    end
                                        
                % Battery power
                case 'bat'
                    Preq = [];
                    for i = 1:Ncon
                        Preq = [Preq max(abs(MA.(names{i}).P.bat))];
                    end
                    Preq = max(Preq);
                    if Preq > aircraft.Pdes.bat
                        Ratiobat = Preq/aircraft.Pdes.bat;
                        WPdes.(s.SelDes).bat = WPdes.(s.SelDes).bat/Ratiobat;
                        if Ratiobat > 1+1e-10
                            if s.printMessages == 1
                                disp([s.levelString '    > Battery could not '...
                                    'meet power requirements. Scaling up by a factor '...
                                    num2str(Ratiobat)])
                            end
                        end
                    else
                        Ratiobat = 1;
                    end
                    
                % Primary shaft power
                case 's1'
                    Preq = [];
                    for i = 1:Ncon
                        Preq = [Preq max(abs(MA.(names{i}).P.s1))];
                    end
                    Preq = max(Preq);
                    if Preq > aircraft.Pdes.s1
                        Ratios1 = Preq/aircraft.Pdes.s1;
                        WPdes.(s.SelDes).s1 = WPdes.(s.SelDes).s1/Ratios1;
                        if Ratios1 > 1+1e-10
                            if s.printMessages == 1
                                disp([s.levelString '    > Primary shaft could not '...
                                    'meet power requirements. Scaling up by a factor '...
                                    num2str(Ratios1)])
                            end
                        end
                    else
                        Ratios1 = 1;
                    end
            end
        end
        
    % Set ratios to 1 for error convergence    
    else
        RatioGTM = 1;
        RatioEM1 = 1;
        RatioEM2 = 1;
        Ratiobat = 1;
        Ratios1 = 1;
    end
    
    % Compute error
    ScalingError = 0.2*(abs(RatioGTM-RatioGTM0) +    ...
                        abs(RatioEM1-RatioEM10) +    ...
                        abs(RatioEM2-RatioEM20) +    ...
                        abs(Ratiobat-Ratiobat0) +    ...
                        abs(Ratios1-Ratios10));
    
    % Update reference values for next loop
    RatioGTM0 = RatioGTM;
    RatioEM10 = RatioEM1;
    RatioEM20 = RatioEM2;
    Ratiobat0 = Ratiobat;
    Ratios10 = Ratios1;

    
    %% Evaluate convergence 

    % Add geometrical parameters to input for weight estimation
    if s.WeightEstimationClass == 2
        if p.DP == 1
            C2.propulsionGroup1.geom.D = sqrt(aircraft.D2);
            C2.propulsionGroup2.geom.D = sqrt(aircraft.D2_conv);
        elseif p.DP == 2
            C2.propulsionGroup1.geom.D = sqrt(aircraft.D2_conv);
            C2.propulsionGroup2.geom.D = sqrt(aircraft.D2);
        else
            C2.propulsionGroup1.geom.D = sqrt(aircraft.D2_conv);
            C2.propulsionGroup2.geom.D = sqrt(aircraft.D2);
        end
       
        % Compute rotational speed of secondary propellers for EM2 mass
        % estimation [Hz]. Take velocity and speed of sound in sizing
        % constraint for EM2 power.
        LimCon = AC.(s.SelDes).EM2M; % For OEI constraints, ignore 1/2 subscripts
        if length(LimCon) > 2
            if strcmp(LimCon(3),'1') || strcmp(LimCon(3),'2')
                LimCon = LimCon(1:2);
            end
        end
        v_n = AEROdes.(s.SelDes).(LimCon).v;
        [~,a_n,~,~] = atmosisa(m.(LimCon).h);
        n_n = sqrt((a_n*p.M_tip)^2 - v_n^2)/(pi*C2.propulsionGroup2.geom.D);
        C2.propulsionGroup2.geom.n = n_n*60; % [RPM]
    else
        C2 = [];
    end
    aircraft.N_pax = MA_in.N_pax;
    aircraft.PL = MA_in.PL;
    aircraft.R = MA_in.R;  
    
    % Recompute TOM 
    if iterC2 == 1 || s.WeightEstimationClass == 1
        [M,MA,DOHmiss,DOHtot] = ComputeWeights_v2(a,m,p,f,s,c,aircraft,MA,C2,[],iterC2);
    else
        [M,MA,DOHmiss,DOHtot] = ComputeWeights_v2(a,m,p,f,s,c,aircraft,MA,C2,M,iterC2);
    end
    TOM1 = M.TOM;
    Mf_tot1 = M.f;
    Mf_miss1 = M.f_miss;
    Ef_tot1 = Mf_tot1*p.SE.f;
    Ebat_tot1 = M.bat*p.SE.bat;
    
    % Update DOH and FF (swap NaN with 0 for configurations which do not
    % use batteries, to get a coherent error value). Also update nominal
    % mission DOH and FF, not to use in convergence, but to store. This
    % will allow the user to get a more accurate initial guess for the next
    % time the code is run
    if electric == 0
        if strcmp(p.config,'conventional') || ...
                strcmp(p.config,'turboelectric') || strcmp(p.config,'PTE')
            DOH_tot1 = 0;
        else
            DOH_tot1 = DOHtot;
        end
        FF_tot1 = Mf_tot1/TOM1;
        FF_miss1 = Mf_miss1/TOM1; 
    else
        DOH_tot1 = 1;
        BF_tot1 = M.bat/TOM1;
        BF_miss1 = M.bat_miss/TOM1;
    end
    
    % Calculate error: if denominator is zero, use 1 (since FF and DOH are
    % normally of order 1 anyway) to avoid a 0/0 indeterminate
    if DOH_tot0 == 0; DOH_totref = 0.1; else; DOH_totref = DOH_tot0; end
    if electric == 0
        err = 1/4*(abs((TOM1-TOM0)/TOM0) + abs((FF_tot1-FF_tot0)/...
            FF_tot0) + abs((DOH_tot1-DOH_tot0)/DOH_totref) + ScalingError);
    else
        err = 1/4*(abs((TOM1-TOM0)/TOM0) + abs((BF_tot1-BF_tot0)/...
            BF_tot0) + abs((DOH_tot1-DOH_tot0)/DOH_totref) + ScalingError);
    end
       
    % Break if not converging
    if count > s.itermax/4
        error('Mission analysis did not converge');
    end    
    
    % Save convergence of variables
    conv.DOH = [conv.DOH DOH_tot1];
    if electric == 0
        conv.FF = [conv.FF FF_tot1];
    else
        conv.FF = [conv.FF BF_tot1];
    end
    conv.TOM = [conv.TOM TOM1];
    conv.err = [conv.err err];
       
    % Update
    if ~isnan(err)
        TOM0 = TOM1;
        if electric == 0
            FF_tot0 = FF_tot1;
            FF_miss0 = FF_miss1;
            DOH_tot0 = DOH_tot1;
        else
            BF_tot0 = BF_tot1;
            BF_miss0 = BF_miss1;
        end
    else
        if electric == 0
            FF_tot0 = MA_in.FF_tot0*(1+count/s.itermax);
            FF_miss0 = MA_in.FF_miss0*(1+count/s.itermax);
            DOH_tot0 = MA_in.DOH_tot0*(1+count/s.itermax);
            TOM0 = (1+count/s.itermax)*(MA_in.PL+MA_in.OEM)/(1-FF_tot0*(1+p.SE.f/p.SE.bat*DOH_tot0/(1-DOH_tot0)));
        else
            BF_tot0 = MA_in.FF_tot0*(1+count/s.itermax);
            BF_miss0 = MA_in.FF_miss0*(1+count/s.itermax);
            TOM0 = (1+count/s.itermax)*(MA_in.PL+MA_in.OEM)/(1-BF_tot0);
        end
    end
end


%% Check power envelope

% String for warning
Pstrings = {'gas turbine','primary electrical machine',...
            'secondary electrical machine','battery','primary shaft'};

% Loop over mission segments
names = fieldnames(MA);
names(contains(names,'EF')) = [];
Ncon = size(names,1);
for i = 1:Ncon
    
    % Save an additional field in the MA.() structure which indicates
    % whether any power limit has been exceeded (for plotting)
    MA.(names{i}).limits = zeros(length(MA.(names{i}).t),1);
    
    % Evaluate limit of each component
    for j = 1:size(Pnames,2)
       
        % Generate a field in the MA.().P.Limit structure, containing an
        % array with value 1 for timesteps where the maximum allowable
        % power of the given component has be surpassed, and zeros
        % elsewhere.
        MA.(names{i}).P.limits.(Pnames{j}) = ...
                zeros(length(MA.(names{i}).t),1);
        
        % Loop over time steps of current segment
        for k = 1:length(MA.(names{i}).t)
            
            % Define limting power depending on the powertrain
            % component/path considered
            switch Pnames{j}
                
                % For the gas turbine, compute current available power
                case 'GTM'
                    [~,~,~,rho] = atmosisa(MA.(names{i}).h(k));
                    Pavail = aircraft.Pdes.GTM.*f.Alpha(rho,MA.(names{i}).M,c);
                    Preq = MA.(names{i}).P.gt(k);

                % For primary electrical machine, select sizing power
                case 'EM1'
                    if strcmp(p.config,'e-1') || strcmp(p.config,'dual-e')
                        Pavail = aircraft.Pdes.EM1M;
                    else
                        Pavail = aircraft.Pdes.EM1;
                    end
                    Preq = max(abs([MA.(names{i}).P.gb(k)...
                        MA.(names{i}).P.e1(k)]));
                    
                % For secondary electrical machine, select sizing power    
                case 'EM2'
                    if strcmp(p.config,'e-2')
                        Pavail = aircraft.Pdes.EM2M;
                    else
                        Pavail = aircraft.Pdes.EM2;
                    end
                    Preq = max(abs([MA.(names{i}).P.s2(k)...
                        MA.(names{i}).P.e2(k)]));
                    
                % Battery power
                case 'bat'
                    Pavail = aircraft.Pdes.bat;
                    Preq =abs(MA.(names{i}).P.bat(k));
                    
                % Primary shaft power    
                case 's1'
                    Pavail = aircraft.Pdes.s1;
                    Preq = abs(MA.(names{i}).P.s1(k));
            end
            
            % If limits have been surpassed, save.
            if Preq > Pavail + 1e-6
                MA.(names{i}).P.limits.(Pnames{j})(k) = 1;
            end
        end
        
        % Select mission segment string
        switch names{i}
            case 'cl'; Sstring = 'climb';
            case 'cr'; Sstring = 'cruise';
            case 'de'; Sstring = 'descent';
            case 'Dcl'; Sstring = 'diversion climb';
            case 'Dcr'; Sstring = 'diversion cruise';
            case 'Dde'; Sstring = 'diversion descent';
            case 'Loiter'; Sstring = 'loiter';
        end 
        
        % Issue warning if any power has been surpassed
        if any(MA.(names{i}).P.limits.(Pnames{j}))==1
            if s.printMessages == 1
                disp([s.levelString '  > Warning: maximum installed ' ...
                    Pstrings{j} ' continuous power has been exceeded during the '...
                    Sstring ' segment! Check power-control envelope.'])
            end
        end
        
        % If the current component power is exceeded, change corresponding
        % index in overall mission-segment limit array
        MA.(names{i}).limits(MA.(names{i}).P.limits.(Pnames{j})==1) = 1;
    end
end


%% Organize output

% Save variables to structures. Note that DOH_tot will now include any 
% possible battery energy not used during the mission, but available due to
% power requirements
if electric == 1
    MA.BF_tot = BF_tot1;
    MA.BF_miss = M.bat_miss/M.TOM;
else
    MA.FF_tot = FF_tot1;
    MA.FF_miss = M.f_miss/M.TOM;
end                                         
MA.DOH_miss = DOHmiss;
MA.conv = conv;
MA.DOH_tot = M.bat*p.SE.bat/(M.f*p.SE.f ... % NB: also includes extra 
                        + M.bat*p.SE.bat);  %   energy available if battery 
                                            %   is sized for power, not
                                            %   only the energy used during
                                            %   the nominal+diversion
                                            %   mission & reserves

% Save FOMs: payload range energy efficiency [-], also expressed in Wh per
% passenger-kilometer
aircraft.DOH = MA.DOH_tot;
if isnan(M.bat_miss)
    aircraft.PREE = (MA_in.R*MA_in.PL*c.g)/(M.f_miss*p.SE.f);
else
	aircraft.PREE = (MA_in.R*MA_in.PL*c.g)/(M.bat_miss*p.SE.bat + M.f_miss*p.SE.f);
end
aircraft.Wh_per_pkm = 1/aircraft.PREE/3600*1000*(M.PL*c.g/MA_in.N_pax);

% Include ratio of diversion energy to total mission energy (useful to
% compute equivalent e_bat in case of single-use battery as a range
% extenders)
fnames = {'cl','cr','de','Dcl','Dcr','Dde','Loiter'};
E_tot = 0;
for j = 1:length(fnames)
    if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ||...
       strcmp(p.config,'e-1') 
        P_s_tot = MA.(fnames{j}).P.s1;
    elseif strcmp(p.config,'serial') || strcmp(p.config,'turboelectric') ||...
           strcmp(p.config,'e-2') 
        P_s_tot = MA.(fnames{j}).P.s2;
    else
        P_s_tot = MA.(fnames{j}).P.s1 + MA.(fnames{j}).P.s2;
    end
    if isnan(P_s_tot(1))                % If first element is NaN
        E_tot = E_tot + sum(P_s_tot(2:end).*...
            (MA.(fnames{j}).t(2:end) - MA.(fnames{j}).t(1:end-1)));
    else                                % If last element is NaN
        E_tot = E_tot + sum(P_s_tot(1:end-1).*...
            (MA.(fnames{j}).t(2:end) - MA.(fnames{j}).t(1:end-1)));
    end
end
fnames = {'Dcl','Dcr','Dde','Loiter'};
E_div = 0;
for j = 1:length(fnames)
    if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ||...
       strcmp(p.config,'e-1') 
        P_s_tot = MA.(fnames{j}).P.s1;
    elseif strcmp(p.config,'serial') || strcmp(p.config,'turboelectric') ||...
           strcmp(p.config,'e-2') 
        P_s_tot = MA.(fnames{j}).P.s2;
    else
        P_s_tot = MA.(fnames{j}).P.s1 + MA.(fnames{j}).P.s2;
    end
    if isnan(P_s_tot(1))                % If first element is NaN
        E_div = E_div + sum(P_s_tot(2:end).*...
            (MA.(fnames{j}).t(2:end) - MA.(fnames{j}).t(1:end-1)));
    else                                % If last element is NaN
        E_div = E_div + sum(P_s_tot(1:end-1).*...
            (MA.(fnames{j}).t(2:end) - MA.(fnames{j}).t(1:end-1)));
    end
end
E_extra_tot = (MA_in.EF.TaxiOut + MA_in.EF.TakeOff + MA_in.EF.Landing + MA_in.EF.TaxiIn)*E_tot;
E_tot_incl_fractions = E_tot + E_extra_tot;
E_extra_div = MA_in.EF.TaxiIn*E_tot;
E_div_incl_fractions = E_div + E_extra_div;
aircraft.E_div_fraction = E_div_incl_fractions/E_tot_incl_fractions;

% Update fields with resulting MTOM of last iteration
aircraft.TOM = M.TOM;
aircraft.Pdes = structfun(@(x) aircraft.TOM*c.g./x,WPdes.(s.SelDes),'UniformOutput',0); 
aircraft.Pdes_ref = structfun(@(x) aircraft.TOM*c.g./x,WPdes_ref.(s.SelDes),'UniformOutput',0); 
aircraft.D2 = c.g*aircraft.TOM*aircraft.D2W;
aircraft.D2_conv = c.g*aircraft.TOM*aircraft.D2W_conv;
aircraft.Sw = c.g*aircraft.TOM/WSdes.(s.SelDes);
aircraft.Sw_ref = c.g*aircraft.TOM/WSdes_ref.(s.SelDes);

% Save wing and DP geometry
aircraft.c_mean = sqrt(aircraft.Sw/a.AR);
aircraft.b = aircraft.c_mean*a.AR;
if s.WeightEstimationClass == 2
    aircraft.b_projected = aircraft.b*cosd(C2.wing.geom.dihedral);
end
aircraft.DcRatioDP = sqrt(aircraft.D2)/aircraft.c_mean;

% Remove variables that were used for ComputeWeights
aircraft = rmfield(aircraft,'etap1');
aircraft = rmfield(aircraft,'etap2');
aircraft = rmfield(aircraft,'v_TO');






