%%% Description
% This script checks the parameters specified in Input.m. If it
% automatically corrects a parameter (e.g. a value has been specified but
% it should be a NaN) it shows a message in the command window. If it
% cannot correct a parameter (e.g. a NaN has been given, but a
% specific value is required), it returns an error. Only some of the
% powertrain-related parameters are checked. Additionally, it selects which
% of the two powertrain branches' (primary or secondary) properties should
% be applied to the DP system.
%
% 
%%% Reynard de Vries
%%% TU Delft
%%% Date created: 21-06-17
%%% Last modified: 10-04-22


%% Check powertrain input settings compatibility

% Identify constraints that are being used
phases = fieldnames(m);
np = length(phases);

% Check control laws and DP selection are correctly specified
switch p.config
    
    % 1. Conventional powertrain
    case 'conventional'
        
        % Only throttle can be defined by user
        if any(structfun(@(p) isnan(p.xi), m))
            error('Throttle must be specified for all constraints')
        end
        
        % Only primary propulsors exist, overwrite shaft power ratio 
        if any(structfun(@(p) ~isnan(p.Phi), m))
            for i = 1:np; m.(phases{i}).Phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > Phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end
        
        % No batteries exist, overwrite supplied power ratio 
        if any(structfun(@(p) ~isnan(p.phi), m))
            for i = 1:np; m.(phases{i}).phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end
        
        % If DP is used, it must be on primary powertrain
        if p.DP == 2
            p.DP = 1;
            if s.printMessages == 1
                disp([s.levelString '  > Changed DP location to primary '...
                    'powertrain (' p.config ' configuration).'])
            end
        end
        
        % At least one primary propulsor is required
        if isnan(p.N1) || p.N1<1
            error(['At least one primary propulsor is required for a '...
                   p.config ' architecture.'])
        end

% 09-09-2020     -> apply to parallel etc as well   
%         % No secondary propulsors are used
%         if ~(p.N2==0) && ~isnan(p.N2)
%             if s.printMessages == 1
%                 disp([s.levelString '  > Setting N2 to zero for ' ...
%                     p.config ' architecture.']);
%             end
%             p.N2 = 0;
%         end
        
    % 2. Turboelectric powertrain
    case 'turboelectric'
        
        % Only throttle can be defined by user
        if any(structfun(@(p) isnan(p.xi), m))
            error('Throttle must be specified for all constraints')
        end
        
        % Only secondary propulsors exist, overwrite shaft power ratio 
        if any(structfun(@(p) ~isnan(p.Phi), m))
            for i = 1:np; m.(phases{i}).Phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > Phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end
        
        % No batteries exist, overwrite supplied power ratio 
        if any(structfun(@(p) ~isnan(p.phi), m))
            for i = 1:np; m.(phases{i}).phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end
        
        % If DP is used, it must be on secondary powertrain
        if p.DP == 1
            p.DP = 2;
            if s.printMessages == 1
                disp([s.levelString '  > Changed DP location to secondary '...
                    'powertrain (' p.config ' configuration).'])
            end
        end

        % At least one secondary propulsor is required
        if isnan(p.N2) || p.N2<1
            error(['At least one secondary propulsor is required for a '...
                   p.config ' architecture.'])
        end

        % No primary propulsors are used
        if isnan(p.N1) || p.N1<1
            if s.printMessages == 1
                disp([s.levelString '  > Assuming N1 = 2 for reference GT ' ...
                    'weight estimation.']);
            end
            p.N1 = 2;
        end
        
    % 3. Serial powertrain
    case 'serial'
        
        % Throttle and supplied power ratio can be defined by user
        if any(structfun(@(p) isnan(p.xi), m))
            error('Throttle must be specified for all constraints')
        end
        if any(structfun(@(p) isnan(p.phi), m))
            error(['Supplied power ratio must be specified for all '...
                   'constraints in a ' p.config ' architecture'])
        end
        
        
        % Only secondary propulsors exist, overwrite shaft power ratio
        if any(structfun(@(p) ~isnan(p.Phi), m))
            for i = 1:np; m.(phases{i}).Phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > Phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end
        
        % If DP is used, it must be on secondary powertrain
        if p.DP == 1
            p.DP = 2;
            if s.printMessages == 1
                disp([s.levelString '  > Changed DP location to secondary '...
                    'powertrain (' p.config ' configuration).'])
            end
        end
        
        % At least one secondary propulsor is required
        if isnan(p.N2) || p.N2<1
            error(['At least one secondary propulsor is required for a '...
                   p.config ' architecture.'])
        end

        % No primary propulsors are used
        if isnan(p.N1) || p.N1<1
            if s.printMessages == 1
                disp([s.levelString '  > Assuming N1 = 2 for reference GT ' ...
                    'weight estimation.']);
            end
            p.N1 = 2;
        end
        
    % 4. Parallel powertrain
    case 'parallel'
        
        % Throttle and supplied power ratio can be defined by user
        if any(structfun(@(p) isnan(p.xi), m))
            error('Throttle must be specified for all constraints')
        end
        if any(structfun(@(p) isnan(p.phi), m))
            error(['Supplied power ratio must be specified for all '...
                   'constraints in a ' p.config ' architecture'])
        end
        
        % Only primary propulsors exist, overwrite shaft power ratio
        if any(structfun(@(p) ~isnan(p.Phi), m))
            for i = 1:np; m.(phases{i}).Phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > Phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end
        
        % If DP is used, it must be on primary powertrain
        if p.DP == 2
            p.DP = 1;
            if s.printMessages == 1
                disp([s.levelString '  > Changed DP location to primary '...
                    'powertrain (' p.config ' configuration).'])
            end
        end
        
        % At least one primary propulsor is required
        if isnan(p.N1) || p.N1<1
            error(['At least one primary propulsor is required for a '...
                   p.config ' architecture.'])
        end

        % No secondary propulsors are used
        if ~(p.N2==0) && ~isnan(p.N2)
            if s.printMessages == 1
                disp([s.levelString '  > Setting N2 to zero for ' ...
                    p.config ' architecture.']);
            end
            p.N2 = 0;
        end
           
    % 5. Partial turboelectric powertrain
    case 'PTE'
        
        % Throttle and shaft power ratio can be defined by user
        if any(structfun(@(p) isnan(p.xi), m))
            error('Throttle must be specified for all constraints')
        end
        if any(structfun(@(p) isnan(p.Phi), m))
            error(['Shaft power ratio must be specified for all '...
                   'constraints in a ' p.config ' architecture'])
        end
        
        % No batteries exist, overwrite supplied power ratio 
        if any(structfun(@(p) ~isnan(p.phi), m))
            for i = 1:np; m.(phases{i}).phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end
        
        % At least one primary propulsor is required
        if isnan(p.N1) || p.N1<1
            error(['At least one primary propulsor is required for a '...
                   p.config ' architecture.'])
        end
        
        % At least one secondary propulsor is required
        if isnan(p.N2) || p.N2<1
            error(['At least one secondary propulsor is required for a '...
                   p.config ' architecture.'])
        end
        
    % 6. S-P partial hybrid powertrain
    case 'SPPH'
        
        % Throttle, shaft power ratio and supplied powe ratio can be
        % specified by user. DP can be on both powertrains.
        if any(structfun(@(p) isnan(p.xi), m))
            error('Throttle must be specified for all constraints')
        end
        if any(structfun(@(p) isnan(p.Phi), m))
            error(['Shaft power ratio must be specified for all '...
                   'constraints in a ' p.config ' architecture'])
        end
        if any(structfun(@(p) isnan(p.phi), m))
            error(['Supplied power ratio must be specified for all '...
                   'constraints in a ' p.config ' architecture'])
        end
        
        % At least one primary propulsor is required
        if isnan(p.N1) || p.N1<1
            error(['At least one primary propulsor is required for a '...
                   p.config ' architecture.'])
        end
        
        % At least one secondary propulsor is required
        if isnan(p.N2) || p.N2<1
            error(['At least one secondary propulsor is required for a '...
                   p.config ' architecture.'])
        end
        
    % 7. Full-electric (e-1) powertrain
    case 'e-1'
        
        % Only throttle can be defined by user, which refers to
        % electromotor throttle!
        if any(structfun(@(p) isnan(p.xi), m))
            error('Throttle must be specified for all constraints')
        end
        
        % Only primary propulsors exist, overwrite shaft power ratio
        if any(structfun(@(p) ~isnan(p.Phi), m))
            for i = 1:np; m.(phases{i}).Phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > Phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end
        
        % No fuel exists, overwrite supplied power ratio 
        if any(structfun(@(p) ~isnan(p.phi), m))
            for i = 1:np; m.(phases{i}).phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end
        
        % If DP is used, it must be on primary powertrain
        if p.DP == 2
            p.DP = 1;
            if s.printMessages == 1
                disp([s.levelString '  > Changed DP location to primary '...
                    'powertrain (' p.config ' configuration).'])
            end
        end
        
        % At least one primary propulsor is required
        if isnan(p.N1) || p.N1<1
            error(['At least one primary propulsor is required for a '...
                   p.config ' architecture.'])
        end

        % No secondary propulsors are used
        if ~(p.N2==0) && ~isnan(p.N2)
            if s.printMessages == 1
                disp([s.levelString '  > Setting N2 to zero for ' ...
                    p.config ' architecture.']);
            end
            p.N2 = 0;
        end
        
        % Only two GT's supposed for reference AC weight estimation though
        if s.printMessages == 1
            disp([s.levelString '  > Assuming N1 = 2 for reference GT ' ...
                'weight estimation.']);
        end

        
    % 8. Full-electric (e-2) powertrain
    case 'e-2'
        
        % Only throttle can be defined by user, which refers to
        % electromotor throttle!
        if any(structfun(@(p) isnan(p.xi), m))
            error('Throttle must be specified for all constraints')
        end
        
        % Only secondary propulsors exist, overwrite shaft power ratio
        if any(structfun(@(p) ~isnan(p.Phi), m))
            for i = 1:np; m.(phases{i}).Phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > Phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end
        
        % No fuel exists, overwrite supplied power ratio 
        if any(structfun(@(p) ~isnan(p.phi), m))
            for i = 1:np; m.(phases{i}).phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end
        
        % If DP is used, it must be on secondary powertrain
        if p.DP == 1
            p.DP = 2;
            if s.printMessages == 1
                disp([s.levelString '  > Changed DP location to secondary '...
                    'powertrain (' p.config ' configuration).'])
            end
        end
        
        % At least one secondary propulsor is required
        if isnan(p.N2) || p.N2<1
            error(['At least one secondary propulsor is required for a '...
                   p.config ' architecture.'])
        end

        % No primary propulsors are used
        if isnan(p.N1) || p.N1<1
            p.N1 = 2;
        end
        
        % Only two GT's supposed for reference AC weight estimation anyway
        if s.printMessages == 1
            disp([s.levelString '  > Assuming N1 = 2 for reference GT ' ...
                'weight estimation.']);
        end

        
    % 9. Full-electric (dual-e) powertrain
    case 'dual-e'
        
        % Electromotor throttle and shaft power ratio can be defined by
        % user
        if any(structfun(@(p) isnan(p.xi), m))
            error('Throttle must be specified for all constraints')
        end
        if any(structfun(@(p) isnan(p.Phi), m))
            error(['Shaft power ratio must be specified for all '...
                   'constraints in a ' p.config ' architecture'])
        end
        
        % No fuel exists, overwrite supplied power ratio 
        if any(structfun(@(p) ~isnan(p.phi), m))
            for i = 1:np; m.(phases{i}).phi = NaN; end
            if s.printMessages == 1
                disp([s.levelString '  > phi is not applicable to a '...
                    p.config ' powertrain. Changed value to NaN.'])
            end
        end   
        
        % At least one primary propulsor is required
        if isnan(p.N1) || p.N1<1
            error(['At least one primary propulsor is required for a '...
                   p.config ' architecture.'])
        end
        
        % At least one secondary propulsor is required
        if isnan(p.N2) || p.N2<1
            error(['At least one secondary propulsor is required for a '...
                   p.config ' architecture.'])
        end
        
        % Only two GT's supposed for reference AC weight estimation though
        if s.printMessages == 1
            disp([s.levelString '  > Assuming N1 = 2 for reference GT ' ...
                'weight estimation.']);
        end
              
    % Give error for incorrectly specified powertrian configurations    
    otherwise
        error('Incorrect powertrain configuration specified.')
end


%% Assign parameters used for DP-evaluation to corresponding powertrain.   
% The thrust share "T" is used definitively if eta_p is used as input in
% the design process. If s.ComputeEtap = 1, then the "T" value computed
% here is used as initial guess.
%
% NOTE: This section does not work if Phi = Inf! In that case, T should be
% Inf as well.

% 09-09-2020: Remove this check so LLM can be used on conventional
% powertrain. Make sure you select p.DP correctly in input
% If DP==0, set aero-propulsive model to none, such that WingPropDeltas 
% does not affect anything.
if p.DP == 0 && ~strcmp(p.AeroPropModel,'none')
    p.AeroPropModel = 'none';
    if s.printMessages == 1
    disp([s.levelString '  > Since no DP powertrain has been specified '...
                        '(p.DP = 0), p.AeroPropModel has been set to "none" to '...
                        ' neglect the aero-propulsive interaction '...
                        'effects.'])
    end
end

% For the combined BLI & LLM model, the BLI prop must be on the secondary
% propulsor
if strcmp(p.AeroPropModel,'BLIandLLM')
    if p.DP == 1
        p.DP = 2;
        if s.printMessages == 1
            disp([s.levelString '  > For the combined BLI & LLM model, the "DP" system'...
                  ' must be on the secondary powertrain. Setting p.DP to 2.'])
        end
    end
end

% Powertrains featuring both primary and secondary propulsors   
if strcmp(p.config,'PTE') || strcmp(p.config,'SPPH') || ...
   strcmp(p.config,'dual-e')

    % If primary powertrain has DP
    if p.DP == 1                
        p.N = p.N1;             % Number of propulsors in DP system
        p.N_conv = p.N2;        % Number of propulsors of "conventional" propulsion system
                
        % Fraction of total required thrust produced by DP system, and
        % propulsive efficiency of DP propulsors 
        for i=1:np
            p.(phases{i}).chi = 1/( m.(phases{i}).Phi / (1-m.(phases {i}).Phi) ...
                * p.(phases{i}).etap2/p.(phases{i}).etap1 + 1);
        end
        
    % If secondary powertrain has DP    
    elseif p.DP == 2            
        p.N = p.N2;
        p.N_conv = p.N1;
        for i=1:np
            p.(phases{i}).chi = 1/( (1-m.(phases{i}).Phi) / m.(phases{i}).Phi ...
                * p.(phases{i}).etap1/p.(phases{i}).etap2 + 1);
        end
    
    % If none of the powertrains have DP: if eta_p must be computed, set
    % p.DP = 2 such that the conventional propulsors are on the primary
    % branch and the "DP" ones on the secondary branch, even though no A-P
    % effects are modelled (xp has already been set to Inf earlier)
    elseif p.DP == 0
        p.DP = 2;
        p.N = p.N2; 
        p.N_conv = p.N1;
        for i=1:np
            p.(phases{i}).chi = 1/( (1-m.(phases{i}).Phi) / m.(phases{i}).Phi ...
                * p.(phases{i}).etap1/p.(phases{i}).etap2 + 1);
        end
        if s.printMessages == 1
        disp([s.levelString '  > Two propulsion systems are present but no DP is used.' ...
              ' Assigning N_conv/b_conv to primary powertrain input and' ...
              ' N/b_dp to secondary powertrain input for eta_p computation.']);
        disp([s.levelString '  >     Note: Thrust vectoring will still have an effect!']);
        end
        
    % Wrong configuration specified
    else
        error('Incorrect DP-powertrain (primary/secondary/none) assigned')
    end
    
% Powertrains featuring only primary propulsors    
elseif strcmp(p.config,'conventional') || strcmp(p.config,'parallel') ||...
        strcmp(p.config,'e-1')
    p.N = p.N1;
    p.N_conv = p.N1;                % In case no DP is specified (Verify this works?)
    for i=1:np
        if p.DP == 0                % This if statement was added to remove DP thrust
            p.(phases{i}).chi = 0;    % share when no DP is used; in that case the primary 
        else                        % powertrain uses N_conv/b_conv (check?)
            p.(phases{i}).chi = 1;
        end
    end
        
% Powertrains featuring only secondary propulsors           
elseif strcmp(p.config,'turboelectric') || strcmp(p.config,'serial') || ...
        strcmp(p.config,'e-2')
    p.N = p.N2;
    p.N_conv = p.N2;
    for i=1:np
        if p.DP == 0                % Same note as above
            p.(phases{i}).chi = 0;
        else
            p.(phases{i}).chi = 1;
        end
    end
end

% Notify user that input values of eta_p will only be used as initial guess
if s.ComputeEtap == 1
    if s.printMessages == 1
        disp([s.levelString '  > Propulsive efficiency of will be computed.'...
            ' Using input values as initial guesses.'])
    end
end

% Verify that in the case of BLI, only one propulsor can be used!
if strcmp(p.AeroPropModel,'BLI') || strcmp(p.AeroPropModel,'BLIandLLM') 
    if p.N ~= 1
        error('Only one propulsor can be used for the aero-propulsive BLI model')
    end
end

% Issue warning stating that OEI constraints of powertrain
% branches with just one component instance will be neglected
if p.N1 == 1
    if s.printMessages == 1
        disp([s.levelString '  > For OEI constraints, component failure in the' ...
              ' primary powertrain branch is not considered because N1 = 1.'])
    end
end
if p.N2 == 1
    if s.printMessages == 1
        disp([s.levelString '  > For OEI constraints, component failure in the' ...
              ' secondary powertrain branch is not considered because N2 = 1.'])
    end
end

% For wing prop deltas LLM model: pre-load surrogate model coefficients to
% save time
if strcmp(p.AeroPropModel,'LLM') || strcmp(p.AeroPropModel,'BLIandLLM')
    p.geom.SM = load(p.geom.fn_SM);
end

% For wing prop deltas LLM model: pre-load surrogate model coefficients to
% save time
if strcmp(p.AeroPropModel,'OTW') 
    p.geom.SM = load(p.geom.fn_SM_OTW);
end

% Select D2W function of LLM parametrization if necessary
if strcmp(p.AeroPropModel,'LLM')
    f.D2W = f.D2W_LLM;
else
    f.D2W = f.D2W_LEDP;
end


%% Check initial conditions for MA

% Make variable names shorter (will be cleared at end of program)
FF_tot0 = MA_in.FF_tot0;
FF_miss0 = MA_in.FF_miss0;
DOH_tot0 = MA_in.DOH_tot0;
DOH_miss0 = MA_in.DOH_miss0;

% Mission segments 
names = {'cl','cr','de','Dcl','Dcr','Dde','Loiter'};
N = size(names,2);

% Check if initial guess for DOH is coherent with powertrain selected
if strcmp(p.config,'e-1') || strcmp(p.config,'e-2') ...
                          || strcmp(p.config,'dual-e')
    electric = 1;
    if s.printMessages == 1
    disp([s.levelString '  > Full-electric configuration. Using fuel mass'...
                ' fraction input as initial guess for battery mass'...
                ' fraction'])
    end
            
    % For full-electric configurations (DOH_tot = 1), ignore 
    % "fuel fraction" and converge on a battery fraction instead.
    if DOH_tot0 ~= 1
        if s.printMessages == 1
            disp([s.levelString '  > Setting DOH to 1 for '...
                'fully electric configurations'])
        end
        DOH_tot0 = 1;
        DOH_miss0 = 1;
    end
    
    % Define a "battery fraction" instead
    BF_tot0 = FF_tot0;
    BF_miss0 = FF_miss0;
    FF_tot0 = 0;
    FF_miss0 = 0;
    
%     % Make sure phi profile is always 1
%     for i = 1:size(names,1)
%         if prod(MA_in.(names{i}).phi==1)==0
%             disp([s.levelString '  > Setting phi-profile to NaN for '...
%                 'fully electric configurations (' names{i} ')'])
%             MA_in.(names{i}).phi = [NaN NaN];
%         end
%     end

% For gas-turbine only configurations    
elseif strcmp(p.config,'conventional')
    electric = 0;
    if DOH_tot0 ~= 0
        if s.printMessages == 1
            disp([s.levelString '  > Setting DOH to 0 for '...
                'conventional configurations'])
        end
        DOH_tot0 = 0;
        DOH_miss0 = 0;
    end
    
%     % Make sure phi profile is always 0 (NaN)
%     for i = 1:size(names,1)
%         if prod(MA_in.(names{i}).phi==0)==0
%             disp([s.levelString '  > Setting phi-profile to NaN for '...
%                 'conventional configurations (' names{i} ')'])
%             MA_in.(names{i}).phi = [NaN NaN];
%         end
%     end
    
% For hybrid configurations    
else
    electric = 0;
end

% Make sure that some variables are within bounds
vars = [FF_tot0 FF_miss0 DOH_tot0 DOH_miss0 p.minSOC];
if any(vars>1) || any(vars<0)
    error(['Fuel fraction, DOH, and SOC must be between 0 '...
           'and 1. Check input settings'])
end

% Make sure initial point is valid (This sanity check currently only works 
% for non-electric aircraft!)
if electric ~= 1
    if FF_tot0*(1+p.SE.f/p.SE.bat*DOH_tot0/(1-DOH_tot0)) > 1
        error(['Invalid initial guess of FF and DOH: this combination '...
            'leads to a total battery and fuel mass greater than MTOM'])
    end
end


%% MA: Check that power-control profiles are consistent with configuration

% Power control names
Pnames = {'xi','phi','Phi'};

% Mission segments where all parameters have to be specified (climb and
% descent)
segments1 = {'cl','de','Dcl','Dde'};
N1 = size(segments1,2);

% Mission segments where one DOF must remain free (cruise, loiter)
segments2 =  {'cr','Dcr','Loiter'};
N2 = size(segments2,2);

% First make sure that the control arrays have either two NaN values or no
% NaN values
for i = 1:N
    for j = 1:size(Pnames,2)
        if (isnan(MA_in.(names{i}).(Pnames{j})(1)) && ...
            ~isnan(MA_in.(names{i}).(Pnames{j})(2))) || ...
            (isnan(MA_in.(names{i}).(Pnames{j})(2)) && ...
            ~isnan(MA_in.(names{i}).(Pnames{j})(1)))
            error(['Either both or none of the fields in MA_in.' ...
                    names{i} '.' Pnames{j} ' must have a NaN value. '...
                    'Check mission analysis input profiles.'])
        end
    end
end

% Check input per powertrain configuration
switch p.config
    
    % Conventional: Throttle must be specified during climb/descent
    case 'conventional'
        for i = 1:N1
            if isnan(sum(MA_in.(segments1{i}).xi))
                error(['Throttle must be specified during '...
                     'climb/descent phases for a ' p.config ' powertrain'])
            end
        end
        
        % Throttle cannot be specified during cruise
        xiWarning = 0;
        for i = 1:N2
            if ~isnan(sum(MA_in.(segments2{i}).xi))
                xiWarning = 1;
                MA_in.(segments2{i}).xi = [NaN NaN];
            end
        end
        if xiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > Throttle cannot be specified '...
                    'during cruise/loiter phases for a ' p.config ...
                    ' powertrain. Changed value to NaN.'])
            end
        end
        
        % Ignore phi or Phi if they have been specified
        phiWarning = 0;
        PhiWarning = 0;
        for i = 1:N
            if ~isnan(sum(MA_in.(names{i}).phi))
                phiWarning = 1;
                MA_in.(names{i}).phi = [NaN NaN];
            end
            if ~isnan(sum(MA_in.(names{i}).Phi))
                PhiWarning = 1;
                MA_in.(names{i}).Phi = [NaN NaN];
            end
        end
        if phiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
        if PhiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > Phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
        
    % Turboelectric: same as conventional configuration
    case 'turboelectric'
        for i = 1:N1
            if isnan(sum(MA_in.(segments1{i}).xi))
                error(['Throttle must be specified during '...
                     'climb/descent phases for a ' p.config ' powertrain'])
            end
        end
        
        % Throttle cannot be specified during cruise
        xiWarning = 0;
        for i = 1:N2
            if ~isnan(sum(MA_in.(segments2{i}).xi))
                xiWarning = 1;
                MA_in.(segments2{i}).xi = [NaN NaN];
            end
        end
        if xiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > Throttle cannot be specified '...
                    'during cruise/loiter phases for a ' p.config ...
                    ' powertrain. Changed value to NaN.'])
            end
        end
        
        % Ignore phi or Phi if they have been specified
        phiWarning = 0;
        PhiWarning = 0;
        for i = 1:N
            if ~isnan(sum(MA_in.(names{i}).phi))
                phiWarning = 1;
                MA_in.(names{i}).phi = [NaN NaN];
            end
            if ~isnan(sum(MA_in.(names{i}).Phi))
                PhiWarning = 1;
                MA_in.(names{i}).Phi = [NaN NaN];
            end
        end
        if phiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
        if PhiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > Phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
        
    % Serial: throttle and phi must be specified during climb/descent 
    case 'serial'
        for i = 1:N1
            if isnan(sum(MA_in.(segments1{i}).xi)) || ...
                    isnan(sum(MA_in.(segments1{i}).phi))
                error(['Throttle and phi must both be specified during '...
                     'climb/descent phases for a ' p.config ' powertrain'])
            end
        end
        
        % Only one of the two can be specified during cruise
        for i = 1:N2
            if (isnan(sum(MA_in.(segments2{i}).xi)) && ...
                isnan(sum(MA_in.(segments2{i}).phi))) || ...
               (~isnan(sum(MA_in.(segments2{i}).xi)) && ...
                ~isnan(sum(MA_in.(segments2{i}).phi)))
               error(['Either throttle OR phi must be specified during '...
                     'cruise/loiter phases for a ' p.config ' powertrain'])
            end
        end
        
        % Ignore Phi if it has been specified
        PhiWarning = 0;
        for i = 1:N
            if ~isnan(sum(MA_in.(names{i}).Phi))
                PhiWarning = 1;
                MA_in.(names{i}).Phi = [NaN NaN];
            end
        end
        if PhiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > Phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
        
    % Parallel: same as serial configuration
    case 'parallel'
        for i = 1:N1
            if isnan(sum(MA_in.(segments1{i}).xi)) || ...
                    isnan(sum(MA_in.(segments1{i}).phi))
                error(['Throttle and phi must both be specified during '...
                     'climb/descent phases for a ' p.config ' powertrain'])
            end
        end
        
        % Only one of the two can be specified during cruise
        for i = 1:N2
            if (isnan(sum(MA_in.(segments2{i}).xi)) && ...
                isnan(sum(MA_in.(segments2{i}).phi))) || ...
               (~isnan(sum(MA_in.(segments2{i}).xi)) && ...
                ~isnan(sum(MA_in.(segments2{i}).phi)))
               error(['Either throttle OR phi must be specified during '...
                     'cruise/loiter phases for a ' p.config ' powertrain'])
            end
        end
        
        % Ignore Phi if it has been specified
        PhiWarning = 0;
        for i = 1:N
            if ~isnan(sum(MA_in.(names{i}).Phi))
                PhiWarning = 1;
                MA_in.(names{i}).Phi = [NaN NaN];
            end
        end
        if PhiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > Phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
        
    % PTE: throttle and Phi must be specified during climb/descent
    case 'PTE'
        for i = 1:N1
            if isnan(sum(MA_in.(segments1{i}).xi)) || ...
                    isnan(sum(MA_in.(segments1{i}).Phi))
                error(['Throttle and Phi must both be specified during '...
                    'climb/descent phases for a ' p.config ' powertrain'])
            end
        end
        
        % Only one of the two can be specified during cruise
        for i = 1:N2
            if (isnan(sum(MA_in.(segments2{i}).xi)) && ...
                    isnan(sum(MA_in.(segments2{i}).Phi))) || ...
                    (~isnan(sum(MA_in.(segments2{i}).xi)) && ...
                    ~isnan(sum(MA_in.(segments2{i}).Phi)))
                error(['Either throttle OR Phi must be specified during '...
                    'cruise/loiter phases for a ' p.config ' powertrain'])
            end
        end
        
        % Ignore phi if it has been specified
        phiWarning = 0;
        for i = 1:N
            if ~isnan(sum(MA_in.(names{i}).phi))
                phiWarning = 1;
                MA_in.(names{i}).phi = [NaN NaN];
            end
        end
        if phiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
        
    % SPPH: throttle, phi and Phi must be specified during climb/descent
    case 'SPPH'
        for i = 1:N1
            if isnan(sum(MA_in.(segments1{i}).xi)) || ...
                    isnan(sum(MA_in.(segments1{i}).phi)) || ...
                    isnan(sum(MA_in.(segments1{i}).Phi))
                error(['Throttle, phi and Phi must all be specified '...
                    'during climb/descent phases for a ' p.config ...
                    ' powertrain'])
            end
        end
        
        % Only two out of three can be specified during cruise
        for i = 1:N2
            xiStatus = isnan(sum(MA_in.(segments2{i}).xi));
            phiStatus = isnan(sum(MA_in.(segments2{i}).phi));
            PhiStatus = isnan(sum(MA_in.(segments2{i}).Phi));
            if sum([xiStatus phiStatus PhiStatus]) ~= 1
                error(['Exactly two of the three power-control '...
                       'parameters (throttle, phi and Phi) must be '...
                       'specified during cruise/loiter phases for a ' p.config ...
                       ' powertrain'])
            end
        end
        
    % e-1: same as conventional, although in this case throttle is electric
    case 'e-1'
        for i = 1:N1
            if isnan(sum(MA_in.(segments1{i}).xi))
                error(['Throttle must be specified during '...
                     'climb/descent phases for a ' p.config ' powertrain'])
            end
        end
        
        % Throttle cannot be specified during cruise
        xiWarning = 0;
        for i = 1:N2
            if ~isnan(sum(MA_in.(segments2{i}).xi))
                xiWarning = 1;
                MA_in.(segments2{i}).xi = [NaN NaN];
            end
        end
        if xiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > Throttle cannot be specified '...
                    'during cruise/loiter phases for a ' p.config ...
                    ' powertrain. Changed value to NaN.'])
            end
        end
        
        % Ignore phi or Phi if they have been specified
        phiWarning = 0;
        PhiWarning = 0;
        for i = 1:N
            if ~isnan(sum(MA_in.(names{i}).phi))
                phiWarning = 1;
                MA_in.(names{i}).phi = [NaN NaN];
            end
            if ~isnan(sum(MA_in.(names{i}).Phi))
                PhiWarning = 1;
                MA_in.(names{i}).Phi = [NaN NaN];
            end
        end
        if phiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
        if PhiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > Phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
        
    % e-2: same as e-1
    case 'e-2'
        for i = 1:N1
            if isnan(sum(MA_in.(segments1{i}).xi))
                error(['Throttle must be specified during '...
                     'climb/descent phases for a ' p.config ' powertrain'])
            end
        end
        
        % Throttle cannot be specified during cruise
        xiWarning = 0;
        for i = 1:N2
            if ~isnan(sum(MA_in.(segments2{i}).xi))
                xiWarning = 1;
                MA_in.(segments2{i}).xi = [NaN NaN];
            end
        end
        if xiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > Throttle cannot be specified '...
                    'during cruise/loiter phases for a ' p.config ...
                    ' powertrain. Changed value to NaN.'])
            end
        end
        
        % Ignore phi or Phi if they have been specified
        phiWarning = 0;
        PhiWarning = 0;
        for i = 1:N
            if ~isnan(sum(MA_in.(names{i}).phi))
                phiWarning = 1;
                MA_in.(names{i}).phi = [NaN NaN];
            end
            if ~isnan(sum(MA_in.(names{i}).Phi))
                PhiWarning = 1;
                MA_in.(names{i}).Phi = [NaN NaN];
            end
        end
        if phiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
        if PhiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > Phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
        
    % dual-e: same as PTE, but throttle refers to electrical machines
    case 'dual-e'
        for i = 1:N1
            if isnan(sum(MA_in.(segments1{i}).xi)) || ...
                    isnan(sum(MA_in.(segments1{i}).Phi))
                error(['Throttle and Phi must both be specified during '...
                    'climb/descent phases for a ' p.config ' powertrain'])
            end
        end
        
        % Only one of the two can be specified during cruise
        for i = 1:N2
            if (isnan(sum(MA_in.(segments2{i}).xi)) && ...
                    isnan(sum(MA_in.(segments2{i}).Phi))) || ...
                    (~isnan(sum(MA_in.(segments2{i}).xi)) && ...
                    ~isnan(sum(MA_in.(segments2{i}).Phi)))
                error(['Either throttle OR Phi must be specified during '...
                    'cruise/loiter phases for a ' p.config ' powertrain'])
            end
        end
        
        % Ignore phi if it has been specified
        phiWarning = 0;
        for i = 1:N
            if ~isnan(sum(MA_in.(names{i}).phi))
                phiWarning = 1;
                MA_in.(names{i}).phi = [NaN NaN];
            end
        end
        if phiWarning == 1
            if s.printMessages == 1
                disp([s.levelString '  > phi-profile is not applicable to '...
                    'the ' p.config ' powertrain''s MA. Changed value '...
                    'to NaN.'])
            end
        end
end

% Clean workspace
clear('np','i','phases','vars')
clear('N','N1','N2','phiWarning','xiWarning','PhiWarning','segments1',...
      'segments2')


%% MA: Energy fractions

% Sum of energy fractions must be correct
if (MA_in.EF.TaxiOut+MA_in.EF.TakeOff+MA_in.EF.Landing+MA_in.EF.TaxiIn)>1 || ...
    MA_in.EF.TaxiOut < 0 ||...
    MA_in.EF.TakeOff < 0 ||...
    MA_in.EF.Landing < 0 ||...
    MA_in.EF.TaxiIn < 0
    error(['The energy fractions required for taxi, take-off or landing' ...
           ' must be between 0 and 1'])
end

% The supplied power ratio must be 0, 1 or in between depending on
% powertrain configuration. If equal to zero:
if (MA_in.EF.phiTaxiOut == 0) || (MA_in.EF.phiTakeOff == 0) ||...
   (MA_in.EF.phiLanding == 0) || (MA_in.EF.phiTaxiIn == 0) 

    % Distinguish between different powertrain configs
    switch p.config
        case 'conventional'
        case 'turboelectric'
        case 'serial'
        case 'parallel'
        case 'PTE'
        case 'SPPH'
        case 'e-1'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 1 for a ' p.config ...
                    ' powertrain. Changed value to 1.'])
            end
            MA_in.EF.phiTaxiOut = 1;
            MA_in.EF.phiTakeOff = 1;
            MA_in.EF.phiLanding = 1;
            MA_in.EF.phiTaxiIn = 1;
        case 'e-2'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 1 for a ' p.config ...
                    ' powertrain. Changed value to 1.'])
            end
            MA_in.EF.phiTaxiOut = 1;
            MA_in.EF.phiTakeOff = 1;
            MA_in.EF.phiLanding = 1;
            MA_in.EF.phiTaxiIn = 1;
        case 'dual-e'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 1 for a ' p.config ...
                    ' powertrain. Changed value to 1.'])
            end
            MA_in.EF.phiTaxiOut = 1;
            MA_in.EF.phiTakeOff = 1;
            MA_in.EF.phiLanding = 1;
            MA_in.EF.phiTaxiIn = 1;
    end
end

% If equal to 1
if (MA_in.EF.phiTaxiOut == 1) || (MA_in.EF.phiTakeOff == 1) ||...
   (MA_in.EF.phiLanding == 1) || (MA_in.EF.phiTaxiIn == 1) 

    switch p.config
        case 'conventional'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 0 for a ' p.config ...
                    ' powertrain. Changed value to 0.'])
            end
            MA_in.EF.phiTaxiOut = 0;
            MA_in.EF.phiTakeOff = 0;
            MA_in.EF.phiLanding = 0;
            MA_in.EF.phiTaxiIn = 0;
        case 'turboelectric'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 0 for a ' p.config ...
                    ' powertrain. Changed value to 0.'])
            end
            MA_in.EF.phiTaxiOut = 0;
            MA_in.EF.phiTakeOff = 0;
            MA_in.EF.phiLanding = 0;
            MA_in.EF.phiTaxiIn = 0;
        case 'serial'
        case 'parallel'
        case 'PTE'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 0 for a ' p.config ...
                    ' powertrain. Changed value to 0.'])
                MA_in.EF.phiTaxiOut = 0;
                MA_in.EF.phiTakeOff = 0;
                MA_in.EF.phiLanding = 0;
                MA_in.EF.phiTaxiIn = 0;
            end
        case 'SPPH'
        case 'e-1'
        case 'e-2'
        case 'dual-e'
    end   
end

% Out of bounds    
if (MA_in.EF.phiTaxiOut > 1) || (MA_in.EF.phiTakeOff > 1) ||...
   (MA_in.EF.phiLanding > 1) || (MA_in.EF.phiTaxiIn > 1) 
       error(['The supplied power ratio mut be between 0 and 1 (inclusive) '...
              'during taxi/take-off/landing. Battery charging during these '...
              'phases is not possible with the current version of this code']);
end

% Out of bounds    
if (MA_in.EF.phiTaxiOut < 0) || (MA_in.EF.phiTakeOff < 0) ||...
   (MA_in.EF.phiLanding < 0) || (MA_in.EF.phiTaxiIn < 0) 
       error(['The supplied power ratio mut be between 0 and 1 (inclusive) '...
              'during taxi/take-off/landing. Battery charging during these '...
              'phases is not possible with the current version of this code']);
end

% If between zero and 1    
if ((MA_in.EF.phiTaxiOut > 0) && (MA_in.EF.phiTaxiOut < 1)) ||...
        ((MA_in.EF.phiTakeOff > 0) && (MA_in.EF.phiTakeOff < 1)) ||...
        ((MA_in.EF.phiLanding > 0) && (MA_in.EF.phiLanding < 1)) ||...
        ((MA_in.EF.phiTaxiIn >  0) && (MA_in.EF.phiTaxiIn < 1))
    switch p.config
        case 'conventional'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 0 for a ' p.config ...
                    ' powertrain. Changed value to 0.'])
            end
            MA_in.EF.phiTaxiOut = 0;
            MA_in.EF.phiTakeOff = 0;
            MA_in.EF.phiLanding = 0;
            MA_in.EF.phiTaxiIn = 0;
        case 'turboelectric'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 0 for a ' p.config ...
                    ' powertrain. Changed value to 0.'])
            end
            MA_in.EF.phiTaxiOut = 0;
            MA_in.EF.phiTakeOff = 0;
            MA_in.EF.phiLanding = 0;
            MA_in.EF.phiTaxiIn = 0;
        case 'serial'
        case 'parallel'
        case 'PTE'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 0 for a ' p.config ...
                    ' powertrain. Changed value to 0.'])
            end
            MA_in.EF.phiTaxiOut = 0;
            MA_in.EF.phiTakeOff = 0;
            MA_in.EF.phiLanding = 0;
            MA_in.EF.phiTaxiIn = 0;
        case 'SPPH'
        case 'e-1'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 1 for a ' p.config ...
                    ' powertrain. Changed value to 1.'])
            end
            MA_in.EF.phiTaxiOut = 1;
            MA_in.EF.phiTakeOff = 1;
            MA_in.EF.phiLanding = 1;
            MA_in.EF.phiTaxiIn = 1;
        case 'e-2'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 1 for a ' p.config ...
                    ' powertrain. Changed value to 1.'])
            end
            MA_in.EF.phiTaxiOut = 1;
            MA_in.EF.phiTakeOff = 1;
            MA_in.EF.phiLanding = 1;
            MA_in.EF.phiTaxiIn = 1;
        case 'dual-e'
            if s.printMessages == 1
                disp([s.levelString '  > The supplied power ratio during '...
                    'taxi/take-off/landing must be 1 for a ' p.config ...
                    ' powertrain. Changed value to 1.'])
            end
            MA_in.EF.phiTaxiOut = 1;
            MA_in.EF.phiTakeOff = 1;
            MA_in.EF.phiLanding = 1;
            MA_in.EF.phiTaxiIn = 1;
    end    
end

  


