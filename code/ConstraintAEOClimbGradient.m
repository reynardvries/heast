function [WS,WP_path,WP_comp,WP_loss,TW,a,m,p] = ConstraintAEOClimbGradient(a,m,p,f,s,c,con)
%% Operating conditions

% No horizontal acceleration
dvdt = 0;  

% Given climb gradient
climb = [NaN m.(con).G];       

% No bank angle/load factor req./turn radius
maneuver = [0 NaN NaN];

% Density
[T_inf,aa,~,m.(con).rho] = atmosisa(m.(con).h);


%% Compute power loading

% Initialize variables
WS = linspace(s.WSmin,s.WSmax,s.n);
TW = NaN(size(WS));
WP = NaN(size(WS));
a.(con).CL = NaN(size(WS));
a.(con).dCL = NaN(size(WS));
a.(con).dCDi = NaN(size(WS));
p.(con).Tc = NaN(size(WS));

% Loop over WS values
for i = 1:length(WS)
    
    % Wing loading in flight condition
    WS_in = WS(i)*m.(con).f;
    
    % Initial guess for flight speed
    % CS25.121d: Flight speed has to be 1.1*1.4 times V_SR, the reference stall
    % speed. The reference stall speed is taken to be equal to the stall speed
    % in landing configuration for  the balked landing, since in essence the AC
    % is in landing configuration.
    m.(con).vr = m.(con).vMargin*(WS_in*2/m.L.rho/a.(con).CLmax)^0.5;
    
    % Initial guess for dynamic pressure
    q = 0.5*m.L.rho*m.(con).vr^2;
         
    % Initial guess to speed up convergence
    TW_in = q*a.(con).CD0./WS_in + WS_in/pi/a.AR/a.cr.e/q;

    % Compute power loading and flight speed
    [WP_out,TW_out,CL,dCL,dCDi,dCD0,v,Tc,chi,etap,etap_conv,detap] = ...
        ComputeThrustLoading_vMargin(con,TW_in,WS_in,climb,...
                                     maneuver,dvdt,a,m,p,f,s,c);
    
    % Correct to MTOW
    TW(i) = TW_out*m.(con).f;
    WP(i) = WP_out/m.(con).f;
    
    % Save aerodynamic variables
    a.(con).CL(i) = CL;
    a.(con).dCL(i) = dCL;
    a.(con).dCDi(i) = dCDi;
    a.(con).dCD0(i) = dCD0;
    p.(con).Tc(i) = Tc;
    m.(con).v(i) = v;
    m.(con).M(i) = v/aa;
    m.(con).Re(i) = v*m.(con).rho*a.c_ref/f.mu(T_inf);
    p.(con).etap(i) = etap;
    p.(con).detap(i) = detap;
    p.(con).chi(i) = chi;
    p.(con).etap_conv(i) = etap_conv;
end


%% Compute HEP component power loading

% All engines operative
OEI = 0;

% Replace NaNs with Inf's so that the code understands that WP is an input.
% Keep indices to revert changes later
indices = isnan(WP);
WP(indices) = Inf;

% Call sizing routine
[WP_path,WP_comp,WP_loss] = SizePowertrain(WP,con,OEI,m,p,f,s,c);

% Change -Inf to +Inf to avoid warnings; Inf is a possible solution for
% zero power flow
pathnames = fieldnames(WP_path);
for i = 1:size(pathnames,1)
    WP_path.(pathnames{i})(WP_path.(pathnames{i})==-Inf) = Inf;
    WP_path.(pathnames{i})(indices) = NaN;
end
compnames = fieldnames(WP_comp);
for i = 1:size(compnames,1)
    WP_loss.(compnames{i})(WP_loss.(compnames{i})==-Inf) = Inf;
    WP_comp.(compnames{i})(WP_comp.(compnames{i})==-Inf) = Inf;
    WP_loss.(compnames{i})(indices) = NaN;
    WP_comp.(compnames{i})(indices) = NaN;
end




