%%% Input file example
% This file specifies the Class I input for a conventional twin-turboprop
% configuration, where a Class-II analysis is peformed (input specified in
% seperate file) and no aero-propulsive effects are considered. 


%% Aerodynamic/geometric properties (airframe only, excl. aero-propulsive)

% General
a.AR = 11;                                      % Wing aspect ratio [-]
a.Lambda = 10;                                  % Half-chord sweep angle of wing [deg]
a.TR = 0.35;                                    % Taper ratio of wing [-]
a.tc = 0.20;                                    % Thickness-to-chord ratio of root section [-]
a.nUlt = 2.5*1.5;                               % Ultimate load factor [-]
a.c_ref = 2.25;                                 % Reference chord-length used to compute Re in WingPropDeltas [m]
                                                %   Note that this value is NOT updated even when the wing size has been determined
% Zero-lift drag coefficient
a.CD0_clean = 0.022;                            % CD0 in clean configuration (i.e. flaps retracted) [-]
a.dCD0_TO = 0.005;                              % CD0 increase due to flaps in TO position [-]
a.dCD0_L = 0.055;                               % CD0 increase due to flaps in landing position [-]
a.dCD0_LG = 0.010;                              % CD0 increase due to extended landing gear[-]

% Minimum-drag lift coefficient (for two-term polar)
a.CL_minD_clean = 0.00;                         % Lift coefficient corresponding to minimum drag (CD0) in clean configuration [-]
a.dCL_minD_TO = 0;                              % Change in minimum-drag lift coefficient with TO flaps [-]
a.dCL_minD_L = 0;                               % Change in minimum-drag lift coefficient with landing flaps [-]

% Oswald factor
a.e_clean = 0.70;                               % Oswald factor in clean configuration [-]
a.de_TO = 0.1;                                  % Oswald factor increase due to flaps in TO position [-]
a.de_L = 0.15;                                  % Oswald factor increase due to flaps in landing position [-]

% Maximum lift coefficient
a.CLmax_clean = 1.2;                            % CLmax in clean configuration [-]
a.dCLmax_TO = 0.5;                              % CLmax increase due to flaps in TO position [-]
a.dCLmax_L = 1.0;                               % CLmax increase due to flaps in landing position [-]

% Reference aircraft data, for calculation of reference GT and wing mass,
% selection of TOP, etc. All other parameters are assumed to be unaltered.
a.ref.AR = 12;                                  % Wing aspect ratio of a typical reference aircraft [-]
a.ref.N = 2;                                    % Number of engines of a typical reference aircraft [-]
a.ref.config = 'TP';                            % Is reference aircraft a turboprop 'TP' or turbofan 'TF'?
a.ref.b_conv = 0.29;                            % Fraction of wing span occupied by (conventional) propulsors [-]


%% Propulsion System

% Propulsion system layout
p.config = 'conventional';                      % Powertrain architecture ('conventional', 'turboelectric', 'serial',
                                                %   'parallel', 'PTE', 'SPPH', 'e-1', 'e-2', or 'dual-e')
p.AeroPropModel = 'none';                       % Aero-propulsive interaction model: 'none','LEDP','OTW','BLI','LLM',
p.N1 = 2;                                       % Number of chains in primary powertrain [-] (NB: This value is used for estimating ref. GT weight 
                                                %   even if powertrain does not contain primary propulsors. Except electric configs, where N = 2 is used for ref.)
p.N2 = 1;                                       % Number of chains in secondary powertrain [-] (if no secondary branch present, give non-zero value to prevent NaNs)
p.DP = 0;                                       % Which powertrain branch has an effect on wing performance? (1 = primary, 2 = secondary, 0 = none)

% Geometrical parameters for aero-propulsive models
p.geom.b_dp = 0.001;                            % All: Fraction of wing span occupied by DP system [-] (i.e. if N_dp = 1, like in BLI, then this 
                                                %       equals the ratio between prop diameter and wing span!)
p.geom.b_conv = 0.28;                           % All: Fraction of wing span occupied by ALL propulsors of the non-DP propulsion system, N*D/b [-]
p.geom.dy = 0.05;                               % LEDP: Spacing between adjacent DP propulsors, as fraction of propulsor diameter [-]
p.geom.xp = -0.75;                               % LEDP/OTW: Axial position of propellers as a fraction of chord (w.r.t wing LE, negative upstream)
p.geom.ip = 0;                                  % OTW: incidence angle of propeller w.r.t. wing chord line; positive nose-up [deg]
p.geom.fn_SM_OTW = 'SurrogateModelOTWDP_2kpts_poly3.mat';  % OTW: Path & filename of .mat file containing OTW surrogate model coefficients
                                                %       obtained from CreateSurrogateModelOTWDP.m
p.geom.ARf = 10;                                % BLI: Fuselage aspect ratio L_fus/D_fus [-]
p.geom.bRatio = 0.1;                            % BLI: Ratio between fuselage diameter and wing span [-]
p.geom.L1 = 2.75;                               % BLI: Length of upstream tailcone segment, as a fraction of Dfus [-]
p.geom.L2 = 0.25;                               % BLI: Length of cylindrical part of tailcone, as a fraction of Dfus [-]
p.geom.L3 = 0.25;                               % BLI: Length of downstream tailcone segment, as a fraction of Dfus [-]
p.geom.Dcyl = 0.1;                              % BLI: Diameter of cylindrical part of tailcone, as a fraction of Dfus [-]
p.geom.Re = 1e8;                                % BLI: Fuselage length & FS velocity based Reynolds number [-] NB: treated as geometry because 
                                                %       fuselage length is not known! Select typical order of magnitude.
p.geom.fn_SM = 'LLM_SurrModel_4thOrder_52k.mat';% LLM: Path & filename of .mat file containing LLM surrogate model coefficients
                                                %       obtained from ProcessLiftingLineData.m
p.geom.J_conv = 1.0;                            % LLM: Advance ratio of non-DP propulsors [-]                  
p.geom.J_dp = 1.0;                              % LLM: Advance ratio of DP propulsors [-]
p.geom.dy_tip = 0.05;                           % LLM: dy_tip / b_free [-], where dy_tip is distance between wing tip
                                                %       and most outboard DP propsulor, and b_free is
                                                %       the avaiable space for DP propulsors
% Component properties (excl. propulsive)
p.eta_EM1 = 1.00;                               % Conversion efficiency of (electro-) generators
p.eta_EM2 = 1.00;                               % Conversion efficiency of electromotors
p.eta_PM = 1.00;                                % Conversion efficiency of PMAD
p.eta_GB = 0.95; % No GB on range extender      % Transmission efficiency of gearboxes
p.eta_GT = 0.35;                                % Conversion (thermal) efficiency of gas turbine at full throttle
p.SE.bat = 500*3600;                            % Battery specific energy [J/kg] 
p.SE.f = 42.8e6;                                % Fuel specific energy [J/kg]
p.SP.EM1 = 8e3;                                 % Primary electrical machine specific power [W/kg]
p.SP.EM2 = 8e3;                                 % Secondary electrical machine specific power [W/kg]
p.SP.bat = 3*500; % max C-rate = 3              % Battery pack specific power [W/kg]
p.minSOC = 0.1;                                 % Minimum battery SOC (maximum discharge) of batteries, as fraction of total battery capacity [-]

% Cruise
p.cr.etap1 = 0.85;                              % Primary propulsors' propulsive efficiency in cruise (of ISOLATED propulsors) [-]
p.cr.etap2 = 0.85;                              % Secondary propulsors' propulsive efficiency in cruise (of ISOLATED propulsors) [-]
p.cr.Gamma = 0;                                 % Thrust vectoring in cruise [deg]

% Landing (approach speed)
p.L.etap1 = 0.70;                               % Primary propulsors' propulsive efficiency in landing conditions (of ISOLATED propulsors) [-]
p.L.etap2 = 0.70;                               % Secondary propulsors' propulsive efficiency in landing conditions (of ISOLATED propulsors) [-]
p.L.Gamma = 0;                                  % Thrust vectoring in landing configuration [deg]

% Landing distance 
p.sL.etap1 = 0.70;                              % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.sL.etap2 = 0.70;                              % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.sL.Gamma = 0;                                 % Thrust vectoring [deg]

% Take off
p.TO.etap1 = 0.70;                              % Primary propulsors' propulsive efficiency in TO conditions (of ISOLATED propulsors) [-]
p.TO.etap2 = 0.70;                              % Secondary propulsors' propulsive efficiency in TO conditions (of ISOLATED propulsors) [-]
p.TO.Gamma = 0;                                 % Thrust vectoring in TO configuration [deg]

% AEO balked landing (CS25.119)
p.bLAEO.etap1 = 0.70;                           % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.bLAEO.etap2 = 0.70;                           % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.bLAEO.Gamma = 0;                              % Thrust vectoring [deg]

% OEI climb gradient at take-off, LG extended (CS25.121a)
p.ToClEx.etap1 = 0.70;                          % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ToClEx.etap2 = 0.70;                          % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ToClEx.Gamma = 0;                             % Thrust vectoring [deg]

% Divsersion cruise
p.Dcr.etap1 = 0.85;                              % Primary propulsors' propulsive efficiency in cruise (of ISOLATED propulsors) [-]
p.Dcr.etap2 = 0.85;                              % Secondary propulsors' propulsive efficiency in cruise (of ISOLATED propulsors) [-]
p.Dcr.Gamma = 0;                                 % Thrust vectoring in cruise [deg]

% Additional examples of performance constraints
%{
% OEI second segment climb (CS25.121b)
p.ss.etap1 = 0.70;                              % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ss.etap2 = 0.70;                              % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ss.Gamma = 0;                                 % Thrust vectoring in second-segment climb (OEI) configuration [deg]

% En-route OEI climb gradient (CS25.121c)
p.erOEI.etap1 = 0.70;                           % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.erOEI.etap2 = 0.70;                           % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.erOEI.Gamma = 0;                              % Thrust vectoring [deg]]

% OEI Balked landing (CS25.121d)
p.bL.etap1 = 0.70;                              % Primary propulsors' propulsive efficiency in balked landing conditions (of ISOLATED propulsors) [-]
p.bL.etap2 = 0.70;                              % Secondary propulsors' propulsive efficiency in balked landing conditions (of ISOLATED propulsors) [-]
p.bL.Gamma = 0;                                 % Thrust vectoring in balked landing configuration [deg]

% OEI ceiling
p.cI.etap1 = 0.85;                              % Primary propulsors' propulsive efficiency in ceiling (OEI) conditions (of ISOLATED propulsors) [-]
p.cI.etap2 = 0.85;                              % Secondary propulsors' propulsive efficiency in ceiling (OEI) conditions (of ISOLATED propulsors) [-]
p.cI.Gamma = 0;                                 % Thrust vectoring in ceiling (OEI) configuration [deg]

% AEO balked landing (CS23/type-4: ASTM F3179M-18 - 20.3)
p.bLAEO.etap1 = 0.70;                           % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.bLAEO.etap2 = 0.70;                           % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.bLAEO.Gamma = 0;                              % Thrust vectoring [deg]

% AEO climb gradient at take-off (CS23/type-4: ASTM F3179M-18 - 13.3)
p.ToClAEO.etap1 = 0.70;                         % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ToClAEO.etap2 = 0.70;                         % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ToClAEO.Gamma = 0;                            % Thrust vectoring [deg]

% OEI climb gradient at take-off, LG extended (CS23/type-4: ASTM F3179M-18 - 15.3.1)
p.ToClEx.etap1 = 0.70;                          % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ToClEx.etap2 = 0.70;                          % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ToClEx.Gamma = 0;                             % Thrust vectoring [deg]

% OEI climb gradient at take-off, LG retracted (CS23/type-4: ASTM F3179M-18 - 15.3.2)
p.ToClRe.etap1 = 0.70;                          % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ToClRe.etap2 = 0.70;                          % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ToClRe.Gamma = 0;                             % Thrust vectoring [deg]

% En-route OEI climb gradient (CS23/type-4: ASTM F3179M-18 - 15.3.3)
p.erOEI.etap1 = 0.70;                           % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.erOEI.etap2 = 0.70;                           % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.erOEI.Gamma = 0;                              % Thrust vectoring [deg]]

% Discontinued approach (CS23/type-4: ASTM F3179M-18 - 15.3.4)
p.da.etap1 = 0.70;                              % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.da.etap2 = 0.70;                              % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.da.Gamma = 0;                                 % Thrust vectoring [deg]

% AEO ceiling
p.ce.etap1 = 0.70;                              % Primary propulsors' propulsive efficiency in ceiling (OEI) conditions (of ISOLATED propulsors) [-]
p.ce.etap2 = 0.70;                              % Secondary propulsors' propulsive efficiency in ceiling (OEI) conditions (of ISOLATED propulsors) [-]
p.ce.Gamma = 0;                                 % Thrust vectoring in ceiling (OEI) configuration [deg]

% Specified performance: ROC @ SL, AEO
p.rocAEO.etap1 = 0.70;                          % Primary propulsors' propulsive efficiency in ceiling (OEI) conditions (of ISOLATED propulsors) [-]
p.rocAEO.etap2 = 0.70;                          % Secondary propulsors' propulsive efficiency in ceiling (OEI) conditions (of ISOLATED propulsors) [-]
p.rocAEO.Gamma = 0;                             % Thrust vectoring in ceiling (OEI) configuration [deg]

% Specified performance: ROC @ SL, OEI
p.rocOEI.etap1 = 0.70;                          % Primary propulsors' propulsive efficiency in ceiling (OEI) conditions (of ISOLATED propulsors) [-]
p.rocOEI.etap2 = 0.70;                          % Secondary propulsors' propulsive efficiency in ceiling (OEI) conditions (of ISOLATED propulsors) [-]
p.rocOEI.Gamma = 0;                             % Thrust vectoring in ceiling (OEI) configuration [deg]

% AEO climb gradient at take-off
p.ToClAEO.etap1 = 0.70;                         % Primary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ToClAEO.etap2 = 0.70;                         % Secondary propulsors' propulsive efficiency (of ISOLATED propulsors) [-]
p.ToClAEO.Gamma = 0;                            % Thrust vectoring [deg]
%}


%% Mission/operational requirements
% Note: the throttle, phi and Phi values used to evaluate the constraints
% should be consistent with the power-control profiles specified in the MA.
% See notes at end of script for additional info regarding selection of
% xi/phi/Phi!
%
% To deactivate constraints, remove/comment their input in this section.
% Take-off, landing and cruise constraints cannot be deactivated.

% Cruise 
m.cr.h = 20000*0.3048;                          % Initial cruise altitude [m]
m.cr.M = 0.4;                                   % Cruise Mach number [-]
m.cr.f = 0.95;                                  % Cruise weight fraction W/MTOW [-]
m.cr.xi = 0.85;                                 % Cruise throttle setting P/P_max [-] 
m.cr.phi = NaN;                                 % Cruise supplied power ratio [-]
m.cr.Phi = NaN;                                 % Cruise shaft power ratio [-]

% Take off (check in WP_WS_diagram.m which constraint formulation is used!)
m.TO.h = 0;                                     % TO altitude [m]
m.TO.h_TR = 35*0.3048;                          % Obstacle height cleared at end of TO run (35ft)
m.TO.f = 1;                                     % Take-off weight fraction W/MTOW [-]
m.TO.s = 1300;                                  % TO runway length [m]
m.TO.xi = 1.0;                                  % TO throttle setting P/P_max [-] 
m.TO.phi = NaN;                                 % TO supplied power ratio [-]
m.TO.Phi = NaN;                                 % TO shaft power ratio [-]
m.TO.vMargin = 1.20;                            % Take-off speed stall margin [-]

% Landing: approach speed
m.L.h = 0;                                      % Landing altitude [m]
m.L.f = 1.00;                                   % Landing weight fraction W/MTOW [-]
m.L.vs = 50; % Approach speed/1.23              % Stall speed requirement in landing conditions [m/s]
m.L.xi = 0.2;                                   % Landing throttle setting P/P_max [-] 
m.L.phi = NaN;                                  % Landing supplied power ratio [-]
m.L.Phi = NaN;                                  % Landing shaft power ratio [-]

% Landing distance(computing Vs based on s_L = s_TO; Roskam part I)
m.sL.h = 0;                                     % Landing altitude [m]
m.sL.f = 1.00;                                  % Landing weight fraction W/MTOW [-]
m.sL.s = 1400;                                  % Landing distance [m]
m.sL.xi = 0.2;                                  % Landing throttle setting P/P_max [-] 
m.sL.phi = NaN;                                 % Landing supplied power ratio [-]
m.sL.Phi = NaN;                                 % Landing shaft power ratio [-]
m.sL.vs = (m.sL.s/0.584)^0.5;                   % [CALCULATED] Stall speed requirement in landing conditions [m/s]

% AEO balked landing (CS25.119)
m.bLAEO.h = m.TO.h;                             % Altitude [m]
m.bLAEO.f = 0.95;                               % Weight fraction W/MTOW [-]
m.bLAEO.G = 0.032;                              % Climb gradient [-] 
m.bLAEO.vMargin = 1.23;                         % Stall margin [-] (AEO BL performed at V_ref = 1.23*Vs)
m.bLAEO.xi = 1.00;                              % Throttle setting P/P_max [-]
m.bLAEO.phi = NaN;                              % Supplied power ratio [-]
m.bLAEO.Phi = NaN;                              % Shaft power ratio [-]

% OEI take-off climb, LG extended (CS25.121a)
m.ToClEx.h = m.TO.h;                            % Altitude [m]
m.ToClEx.G = 0.0;                               % Climb gradient [-] (CS25.121a: 2-engines: 0%, 3-engines: 0.3%, 4-engines: 0.5%)
m.ToClEx.f = 1;                                 % Max landing weight (MLW) as a fraction of MTOW [-]
m.ToClEx.vMargin = 1.2;                         % Stall margin 
m.ToClEx.xi = 1;                                % Throttle setting P/P_max [-] (see note at end)
m.ToClEx.phi = NaN;                             % Supplied power ratio [-]
m.ToClEx.Phi = NaN;                             % Shaft power ratio [-]

% Diversion cruise 
m.Dcr.h = 10000*0.3048;                         % Initial cruise altitude [m]
m.Dcr.M = 0.35;                                 % Cruise Mach number [-]
m.Dcr.f = 1.0;                                  % Cruise weight fraction W/MTOW [-]
m.Dcr.xi = 1.0;                                 % Cruise throttle setting P/P_max [-] 
m.Dcr.phi = NaN;                                % Cruise supplied power ratio [-]
m.Dcr.Phi = NaN;                                % Cruise shaft power ratio [-]

% Additional examples of performance constraints
%{
% OEI second segment climb (CS25.121b)
m.ss.h = m.TO.h;                                % Altitude [m]
m.ss.G = 0.024;                                 % OEI second-segment climb gradient [-] (CS25.121b: 2-engines: 2.4%, 3-engines: 2.7%, 4-engines: 3.0%)
m.ss.f = 1;                                     % Max landing weight (MLW) as a fraction of MTOW [-]
m.ss.vMargin = 1.2;                             % Stall margin in s.s.c. conditions
m.ss.xi = 1;                                    % S.s.c. throttle setting P/P_max [-] (see note at end)
m.ss.phi = NaN;                                 % S.s.c. supplied power ratio [-]
m.ss.Phi = NaN;                                 % S.s.c. shaft power ratio [-]

% OEI end of take-off (en route) climb (CS25.121c)
m.erOEI.h = 1500*0.3048;                        % Altitude [m]
m.erOEI.G = 0.012;                              % Climb gradient [-] (CS25.121a: 2-engines: 1.2%, 3-engines: 1.5%, 4-engines: 1.7%)
m.erOEI.f = 1;                                  % Max landing weight (MLW) as a fraction of MTOW [-]
m.erOEI.vMargin = 1.18;                         % Stall margin (V_FTO = 1.18*V_SR)
m.erOEI.xi = 1;                                 % Throttle setting P/P_max [-] (see note at end)
m.erOEI.phi = NaN;                              % Supplied power ratio [-]
m.erOEI.Phi = NaN;                              % Shaft power ratio [-]

% OEI Balked landing (CS25.121d)
m.bL.h = m.TO.h;                                % Altitude [m]
m.bL.G = 0.021;                                 % OEI balked landing climb gradient [-] (CS25.121d: 2-engines: 2.1%, 3-engines: 2.4%, 4-engines: 2.7%)
m.bL.f = 1.00;                                  % Max landing weight (MLW) as a fraction of MTOW [-]
m.bL.vMargin = 1.4;                             % Stall margin in balked-landing conditions
m.bL.xi = 1;                                    % Balked landing throttle setting P/P_max [-] (see note at end)
m.bL.phi = NaN;                                 % Balked landing supplied power ratio [-]
m.bL.Phi = NaN;                                 % Balked landing shaft power ratio [-]

% OEI ceiling 
m.cI.h = 20000*0.3048;                          % OEI ceiling [m]
m.cI.f = 1.00;                                  % OEI-ceiling weight fraction W/MTOW [-]
m.cI.c = 0.508;                                 % Ceiling climb rate [m/s] (also used for cruise ceiling and cruise speed!)
m.cI.vMargin = 1.1;                             % Stall margin in OEI-ceiling conditions
m.cI.xi = 1;                                    % OEI-ceiling throttle setting P/P_max [-] (see note at end)
m.cI.phi = NaN;                                 % OEI-ceiling landing supplied power ratio [-]
m.cI.Phi = NaN;                                 % OEI-ceiling landing shaft power ratio [-]

% AEO balked landing (CS23/type-4: ASTM F3179M-18 - 20.3)
m.bLAEO.h = m.TO.h;                             % Altitude [m]
m.bLAEO.f = 1;                                  % Weight fraction W/MTOW [-]
m.bLAEO.G = 0.03;                               % Climb gradient [-] 
m.bLAEO.vMargin = 1.3;                          % Stall margin [-] (AEO BL performed at V_ref = 1.3*Vs, = approach speed)
m.bLAEO.xi = 1.00;                              % Throttle setting P/P_max [-]
m.bLAEO.phi = NaN;                              % Supplied power ratio [-]
m.bLAEO.Phi = NaN;                              % Shaft power ratio [-]

% AEO climb gradient at take-off (CS23/type-4: ASTM F3179M-18 - 13.3)
m.ToClAEO.h = m.TO.h;                           % Altitude [m]
m.ToClAEO.f = 1;                                % Weight fraction W/MTOW [-]
m.ToClAEO.G = 0.04;                             % Climb gradient [-] 
m.ToClAEO.vMargin = 1.2;                        % Stall margin [-] 
m.ToClAEO.xi = 0.90;                            % Throttle setting P/P_max [-] 
m.ToClAEO.phi = NaN;                            % Supplied power ratio [-]
m.ToClAEO.Phi = NaN;                            % Shaft power ratio [-]

% OEI climb gradient at take-off, LG extended (CS23/type-4: ASTM F3179M-18 - 15.3.1)
m.ToClEx.h = m.TO.h;                            % Altitude [m]
m.ToClEx.f = 1;                                 % Weight fraction W/MTOW [-]
m.ToClEx.G = 0.00;                              % Climb gradient [-] 
m.ToClEx.vMargin = m.TO.vMargin;                % Stall margin [-] (constraint must be performed at V2)
m.ToClEx.xi = 1.00;                             % Throttle setting P/P_max [-] 
m.ToClEx.phi = NaN;                             % Supplied power ratio [-]
m.ToClEx.Phi = NaN;                             % Shaft power ratio [-]

% OEI climb gradient at take-off, LG retracted (CS23/type-4: ASTM F3179M-18 - 15.3.2)
m.ToClRe.h = m.TO.h + 122;                      % Altitude [m]
m.ToClRe.f = 1;                                 % Weight fraction W/MTOW [-]
m.ToClRe.G = 0.02;                              % Climb gradient [-] 
m.ToClRe.vMargin = m.TO.vMargin;                % Stall margin [-] (constraint must be performed at V2)
m.ToClRe.xi = 1.00;                             % Throttle setting P/P_max [-] 
m.ToClRe.phi = NaN;                             % Supplied power ratio [-]
m.ToClRe.Phi = NaN;                             % Shaft power ratio [-]

% En-route OEI climb gradient (CS23/type-4: ASTM F3179M-18 - 15.3.3)
m.erOEI.h = m.TO.h + 457;                       % Altitude [m]
m.erOEI.f = 1;                                  % Weight fraction W/MTOW [-]
m.erOEI.G = 0.012;                              % Climb gradient [-] 
m.erOEI.vMargin = 1.2;                          % Stall margin [-] 
m.erOEI.xi = 1.00;                              % Throttle setting P/P_max [-] (max continuous power)
m.erOEI.phi = NaN;                              % Supplied power ratio [-]
m.erOEI.Phi = NaN;                              % Shaft power ratio [-]

% Discontinued approach (CS23/type-4: ASTM F3179M-18 - 15.3.4)
m.da.h = m.L.h + 122;                           % Altitude [m]
m.da.f = m.L.f;                                 % Weight fraction W/MTOW [-]
m.da.G = 0.021;                                 % Climb gradient [-] 
m.da.vMargin = m.bLAEO.vMargin;                 % Stall margin [-] 
m.da.xi = 0.90;                                 % Throttle setting P/P_max [-] (max continuous power)
m.da.phi = NaN;                                 % Supplied power ratio [-]
m.da.Phi = NaN;                                 % Shaft power ratio [-]

% Specified performace: AEO service ceiling 
m.ce.h = 8535;                                  % AEO ceiling [m]
m.ce.f = 1.00;                                  % AEO ceiling weight fraction W/MTOW [-]
m.ce.c = 0.508;                                 % Ceiling climb rate [m/s] 
m.ce.vMargin = 1.2;                             % Stall margin in AEO-ceiling conditions
m.ce.xi = 0.90;                                 % AEO-ceiling throttle setting P/P_max [-] 
m.ce.phi = NaN;                                 % AEO-ceiling supplied power ratio [-]
m.ce.Phi = NaN;                                 % AEO-ceiling shaft power ratio [-]

% Specified performance: ROC @ SL, AEO
m.rocAEO.h = 0;                                 % Altitude [m]
m.rocAEO.f = 1;                                 % AEO ceiling weight fraction W/MTOW [-]
m.rocAEO.c = 8;                                 % Ceiling climb rate [m/s] 
m.rocAEO.vMargin = 1.2;                         % Stall margin 
m.rocAEO.xi = 0.90;                             % AEO-ceiling throttle setting P/P_max [-] 
m.rocAEO.phi = NaN;                             % AEO-ceiling supplied power ratio [-]
m.rocAEO.Phi = NaN;                             % AEO-ceiling shaft power ratio [-]

% Specified performance: ROC @ SL, OEI
m.rocOEI.h = 0;                                 % Altitude [m]
m.rocOEI.f = 1;                                 % AEO ceiling weight fraction W/MTOW [-]
m.rocOEI.c = 2;                                 % Ceiling climb rate [m/s] 
m.rocOEI.vMargin = 1.2;                         % Stall margin 
m.rocOEI.xi = 1.00;                             % AEO-ceiling throttle setting P/P_max [-] 
m.rocOEI.phi = NaN;                             % AEO-ceiling supplied power ratio [-]
m.rocOEI.Phi = NaN;                             % AEO-ceiling shaft power ratio [-]

% AEO climb gradient at take-off
m.ToClAEO.h = m.TO.h;                           % Altitude [m]
m.ToClAEO.f = 1;                                % Weight fraction W/MTOW [-]
m.ToClAEO.G = 0.08;                             % Climb gradient [-] 
m.ToClAEO.vMargin = 1.2;                        % Stall margin [-] 
m.ToClAEO.xi = 1.00;                            % Throttle setting P/P_max [-] 
m.ToClAEO.phi = NaN;                            % Supplied power ratio [-]
m.ToClAEO.Phi = NaN;                            % Shaft power ratio [-]
%}


%% Mission Analysis input

% Mission characteristics
MA_in.PL = 70*100;                              % Payload [kg]
MA_in.N_pax = 70;                               % Number of passengers [-] (only used to compute Wh/pkm at the moment)
MA_in.R = 1500*1e3;                             % Range [m]
MA_in.h_Loiter = 1500*0.3048;                   % Loiter altitude [m]
MA_in.t_Loiter = 30;                            % Loiter time [min]. Set to zero to exclude loiter
MA_in.R_div = 200*1e3;                          % Diversion range [m]. Set to zero to exclude diversion
MA_in.h_div = m.Dcr.h;                          % Diversion altitude [m]
MA_in.M_div = m.Dcr.M;                          % Diversion cruise Mach number [-]
                                                % Nominal mission M and h are specified in the
                                                % "m" structure (cruise constraint)
MA_in.P_offtake = 2000;                         % Power off-take [W/pax] for cabin & other electrical systems. If aircraft has batteries, 
                                                % energy taken from bat. Else, energy taken from GT shaft.
                                                % Gnadt 2019: MEA LR aircraft <2kW/pax, future 4-5 kW/pax (excl. non-cabin power reqs?)                                                
% Initial guesses for convergence loop
MA_in.OEM = 15000;                              % Operative empty mass incl. powertrain and wing, excl. bat [kg]
MA_in.FF_tot0 = 0.06;                           % Fuel fraction (excl. batteries, incl. diversion/loiter/reserves) of aircraft [-]. 
                                                % For fully electric configs, this value specifies the battery weight fraction W_bat/TOW instead.
MA_in.FF_miss0 = 0.05;                          % Fuel fraction (excl. batteries, excl. diversion/loiter/reserves) of nominal mission [-]
                                                % For fully electric configs, this value specifies the battery weight fraction W_bat/TOW instead.
MA_in.DOH_tot0 = 0.0;                           % Degree-of-hybridization, Ebat/(Efuel + Ebat) of aircraft [-]
MA_in.DOH_miss0 = 0.0;                          % Degree-of-hybridization, Ebat/(Efuel + Ebat) of nominal mission [-]

% Mission analysis power control settings. Linear interpolation used 
% between [start of segment, end of segment]. For climb and descent, 
% interpolation is carried out versus altitude, and for cruise, versus 
% range flown. Note that xi refers to GT throttle, unless phi = 1 (which is
% automatically the case for full-e architectures).
% Climb (all parameters must be specified; SEP is computed)
MA_in.cl.xi = [0.85 0.95];
MA_in.cl.phi = [NaN NaN];
MA_in.cl.Phi = [NaN NaN];

% Cruise (level flight is specified, so one DOF must be kept free)
MA_in.cr.xi = [NaN NaN];
MA_in.cr.phi = [NaN NaN];
MA_in.cr.Phi = [NaN NaN];

% Descent (all parameters must be specified; SEP is computed)
MA_in.de.xi = [0.05 0.05];
MA_in.de.phi = [NaN NaN];
MA_in.de.Phi = [NaN NaN];

% Diversion climb (all parameters must be specified; SEP is computed)
MA_in.Dcl.xi = [0.85 0.85];
MA_in.Dcl.phi = [NaN NaN];
MA_in.Dcl.Phi = [NaN NaN];

% Diversion cruise (level flight is specified, so one DOF must be kept free)
MA_in.Dcr.xi = [NaN NaN];
MA_in.Dcr.phi = [NaN NaN];
MA_in.Dcr.Phi = [NaN NaN];

% Diversion descent (all parameters must be specified; SEP is computed)
MA_in.Dde.xi = [0.05 0.05]; 
MA_in.Dde.phi = [NaN NaN]; 
MA_in.Dde.Phi = [NaN NaN];

% Loiter (level flight is specified, so one DOF must be kept free)
MA_in.Loiter.xi = [NaN NaN];
MA_in.Loiter.phi = [NaN NaN];
MA_in.Loiter.Phi = [NaN NaN];

% Energy fractions (analogous to fuel fractions): amount of energy required
% for short mission phases, as a fraction of total on board energy. Landing
% + Taxi in assumed after diversion. Energy consumed during the missed
% approach between descent and diversion-climb is not considered.
MA_in.EF.TaxiOut = 0.01;
MA_in.EF.TakeOff = 0.005;
MA_in.EF.Landing = 0.005;
MA_in.EF.TaxiIn = 0.05;         % Include contingency fuel here, since this contribution
                                %   is not taken into account for PREE

% Supplied power ratio at which short mission segments are carried out
MA_in.EF.phiTaxiOut = 0.0; % Use zero if aircraft doesn't have batteries
MA_in.EF.phiTakeOff = 0.0;
MA_in.EF.phiLanding = 0.0;
MA_in.EF.phiTaxiIn = 0.0;

% Aerodynamic/propulsive properties: copy from constraint input. For cruise
% segment, the same properties as cruise are already selected. For
% remaining mission segments, select which constraint they should be taken
% from.
MA_in.GetProperties.cl = 'cr';
MA_in.GetProperties.de = 'cr';
MA_in.GetProperties.Dcl = 'Dcr';
MA_in.GetProperties.Dcr = 'Dcr';
MA_in.GetProperties.Dde = 'Dcr';
MA_in.GetProperties.Loiter = 'Dcr';


%% Constants
% Note: currently ISA is assumed. The atmosisa function has to be replaced
% by the atmoslapse function in order to account for environmental
% conditions.

c.g = 9.81;                                     % Gravity acceleration [m/s2]
c.rho_SL = 1.225;                               % Sea-level density [kg/m3]
c.T_SL = 288.15;                                % Sea-level temperature [K]
c.p_SL = 101325;                                % Sea-level pressure [Pa]


%% Program settings

% Sizing approach
s.SelDes = 'minWS';                             % Selected design condition ('minWS','minGT',... or 'manual')
s.UseMAControls = 0;                            % If = 1, the power-control parameters specified in the "m" structure are ignored,
                                                %   and the values obtained from the MA requirements are used instead. 
s.MAResize = 0;                                 % Re-size PT components for power requirements during MA loop? (1 = yes). 
                                                %   This makes sure every point along MA can be met with throttle <= 1. So 
                                                %   DO NOT USE with intentional "overpowering" (xi > 1) during MA
s.ComputeEtap = 1;                              % If set to 1, the prop. eff. of the DP-propulsors is computed using an anonymous function
                                                %   specified below. If not, the assumed etap values specified in the "p" structure are used.
s.ComputeEtaGt = 0;                             % If set to 1, the thermal efficiency of the gas turbine will be computed as a function of throttle
                                                %   and Mach number as specified in the f structure. This is only applied for the MA, i.e. the fuel
                                                %   power-loading diagram is NOT corrected, but directly assumes p.eta_gt instead. p.eta_gt corresponds
                                                %   to the thermal efficiency at full throttle. The efficiency may be slightly higher at part-throttle.
s.WeightEstimationClass = 2;                    % Use Class-I (1) or Class-II (= 2) weight estimation? If 2, corrersponding Input_ClassII file is required
s.AeroEstimationClass = 2;                      % Use Class-II (= 2) aero estimation? Required s.WeightEstimationClass == 2!

% Manual selection of design point
s.xManual = 1.000;                              % Selected design wing-loading as a fraction of maximum wing-loading
s.yManual = 1.000;                              % Selected design power-loading as a fraction of component max WP at the WS specified by s.xManual.

% Convergence settings                                              
s.n = 100;                                      % Number of points sampled per constraint [-]
s.TWmax = 1.0;                                  % Maximum thrust loading evaluated [-]
s.WSmax = 8000;                                 % Maxumum wing loading evaluated [N/W]
s.WSmin = 1;                                    % Minimum wing loading evaluated for landing constraint [N/W]
s.WPmax = 0.2;                                  % Maximum power loading shown in diagram [N/W]
s.itermax = 250;                                % Maximum number of iterations [-]
s.errmax = 1e-4;                                % Convergence criterion
s.NWS = 300;                                    % Number of wing loading points to sample when computing design point
s.rf = 0.6;                                     % Relaxation factor for convergence, recommended values [0.1 - 1.0]. 
                                                %   Lower RF = slower, but generally more chance of convergence
s.dt.cl = 15;                                   % Time step in MA, climb phase [s]
s.dt.cr = 40;                                   % Time step in MA, cruise phase [s]
s.dt.de = 20;                                   % Time step in MA, descent phase [s]
s.dt.Dcl = 5;                                   % Time step in MA, diversion climb phase [s]
s.dt.Dcr = 30;                                  % Time step in MA, diversion cruise phase [s]
s.dt.Dde = 15;                                  % Time step in MA, diversion descent phase [s]
s.dt.Loiter = 30;                               % Time step in MA, loiter phase [s]

% Mission analysis
s.SFcl = 1;                                     % Used for adapting climb dM/dh profile. SF < 1 = more accel at start, SF > 1 = more climb at start
s.SFde = 1;                                     % Used for adapting descent dM/dh profile. SF < 1 = more accel at start, SF > 1 = more climb at start
s.CruiseMode = 1;                               % 1 = constant altitude cruise, 2 = constant LD cruise (approximately). 
                                                %   NB: cruise constaint perfromed at inital cruise altitude!
s.LoiterVelEvals = 5;                           % Number of velocities (integer, > 3) at which each point is evaluated during loiter to determine
                                                %   the optimum L/D. A polynomial of degree n-1 is fitted to the point obtained. 

% Presentation of results           
s.printMessages = 1;                            % If 1, progress is displayed. Set to 0 to remove messages in command window.
s.levelString = [];                             % String inserted at the start of each displayed message
s.options = 0;                                  % Plot figures etc. in subroutines (careful with loops, might lead to MANY figures!)
s.figStart = 50;                                % Number of first figure generated
s.plotWPWS = 1;                                 % Plot WS-WP diagrams? 
s.plotMA = 1;                                   % Plot mission analysis graphs? 
s.plotPowertrain = 1;                           % Plot powertrain diagrams? 
s.plotOverallEfficiencies = 0;                  % Plot aero/powertrain efficiencies along mission?

% Polar characteristics with and without prop effects (if used)
s.Polar.plot = 0;                               % Plot aerodynamic polar ?
s.Polar.Tc_conv = 0.0;                          % Tc of non-DP props, at which aero-propulsive model is evaluated
s.Polar.TcInterval = [0 2];                     % Thrust coeff. interval sampled when creating aero polar
s.Polar.MInterval = [0 0.9];                    % Mach number interval sampled when creating aero polar
s.Polar.CLisoInterval = [0.3 1.8];              % Airframe lift coeff. interval sampled when creating aero polar. Avoid extremely low/high values
s.Polar.N = 20;                                 % Number of N and Tc points sampled in polar
s.Polar.N_CLiso = 100;                          % Number of CL_iso points sampled in polar

% Landing constraint check characteristics (added to constraint diagram)
s.LandingCheck.plot = 0;                        % Plot detailed landing constraint? 
s.LandingCheck.CL_map = 1.5:0.3:3.3;            % Isolated wing CL values plotted  during landing constraint check [-]
s.LandingCheck.CD0_map = [0.04:0.02:0.1 ...     % CD0 values plotted during landing constraint check [-]
                       0.15:0.05:0.4 0.5:0.1:1];

% Power-control envelope characteristics
s.Env.plot = 0;                                 % Plot power-control envelope? 
s.Env.Nphi = 50;                                % Number of phi/Phi values sampled
s.Env.Nxi = 40;                                 % Number of xi values sampled
s.Env.Nh = 30;                                  % Number of altitudes sampled
s.Env.con = 'cr';                               % Condition used for propulsive efficiency
s.Env.SPPH_phis = [0.05 0.2 NaN NaN];           % Constant phi values, for each of one an envelope is created
s.Env.SPPH_Phis = [NaN NaN 0.4 0.6];            % Constant Phi values, for each of one an envelope is created
                                                % The length of SPPH_phis same. For each element i, only ONE of the two can
                                                % be specified. The other must be NaN.
                                                
% Saving workspace
s.Save = 0;                                     % Save data?
s.SavePath = '';                                % Path of folder where results are saved
s.SaveFn = 'test';                              % Filename (will be a .mat file)


%% Functions & Dependencies

% TP take-off parameter correlation [N2/m2/W], s in [m]. Based on Raymer
% Fig. 5.4, where s = take-off distance to 50ft height
f.TOP_TP = @(s) 0.084958084*s + 6.217443298;

% TF take-off parameter correlation [N/m2], s in [m]. Based on Raymer
% Fig. 5.4
f.TOP_TF = @(s) 6.444*s + 1266.28;
        
% Normalized rotor sizing of DP system: D^2/W [m2/N] (general formulation,
% also applies to OTWDP configuration)
f.D2W_LEDP = @(WS,AR,p) (p.geom.b_dp/p.N/(1+p.geom.dy))^2*AR/WS;

% Normalized rotor sizing of DP system: D^2/W [m2/N] (LLM-specific formulation)
f.D2W_LLM = @D2W_LLM;

% Normalized rotor sizing of non-DP system: D^2/W [m2/N] (Note that b_dp
% represents the span fraction occupied by a SINGLE rotor in this case)
% f.D2W_conv = @(WS,b_conv,N,AR) (b_conv/N)^2*AR/WS;
f.D2W_conv = @(WS,AR,p) (p.geom.b_conv/p.N_conv)^2*AR/WS;

% Thrust lapse: T_max/T_max_SL [-]
f.Alpha = @(rho,M,var) (rho/var.rho_SL).^0.75;

% Thermal efficiency lapse of turbojet/turbfan engine (Based on Raymer
% Sixth Edition, Eq. 13.9, p. 479). Only used for MISSION ANALYSIS, not in
% constraint diagram! Modified to keep eta_gt constant below 15% throttle
% (~idle). 
xiMin = 0.15;   % Minimum throttle setting below which SFC is constant, as fraction of de-rated engine
fr = 1;         % Flat-rated power over de-rated power of engine. Set to 1 for engines which are not flat-rated
f.etagt = @(xi,M)  1./(0.1./xiMin + 0.24./xiMin.^0.8 + 0.66*xiMin.^0.8 + ...
                          0.1*M.*(1./xiMin-xiMin)) + ...
                      0.5*(abs(sign(xi*fr-xiMin)) + sign(xi*fr-xiMin)).* ...
                      (1./(0.1./(xi*fr) + 0.24./(xi*fr).^0.8 + 0.66*(xi*fr).^0.8 + ...
                          0.1*M.*(1./(xi*fr)-xi*fr)) - ...
                      1./(0.1./xiMin + 0.24./xiMin.^0.8 + 0.66*xiMin.^0.8 + ...
                          0.1*M.*(1./xiMin-xiMin))); clear xiMin fr
                     
% Drag model: two-term (asymmetric) parabolic polar
f.CD = @(CD0,CL_iso,CL_minD,AR,e) CD0 + (CL_iso-CL_minD).^2/(pi*AR*e);

% Weight correlations (per component instance!)
% Turboshaft weight in [kg] as a function of shaft
% power in [W], based on Roskam Part 5, Figure 6.2.
f.W.GT = @(P) 0.45359*10.^((log10(P/745.7)-0.011405)/1.1073);

% Turbofan weight [kg] versus turbofan thrust in [N], based on Roskam Part
% 5, Figure 6.1.
f.W.TF = @(T) 0.45359*220*(T/9.81/0.45359/1000).^0.9759;

% % OEM in [kg] as a function of MTOM [kg], based on ROSKAM Part 1,
% % Table 2.15 (in lb: 10.^((log10(MTOM)-AA)/BB))
% % AA = 0.3774; BB = 0.9647;   % Regional turboprop aircraft
% % AA = 0.0833; BB = 1.0383;   % Transport jets
% f.W.OEM_TF = @(MTOM) 0.45359*10.^((log10(MTOM/0.45359)-0.0833)/1.0383);
% f.W.OEM_TP = @(MTOM) 0.45359*10.^((log10(MTOM/0.45359)-0.3774)/0.9647);

% % OEM [kg] as a function of MTOM [kg], based on RAYMER p. 13
% % A = 0.96; C = -0.05; for regional TP
% % A = 1.02; C = -0.06; for commercial transport jet
% % k = 1.00 for fixed-sweep wings, k = 1.04 for variable-sweep.
f.W.OEM_TF = @(MTOM) MTOM*1.02*(MTOM/0.45359)^(-0.06)*1.00;
f.W.OEM_TP = @(MTOM) MTOM*0.96*(MTOM/0.45359)^(-0.05)*1.00;

% % OEM [kg] as a constant assumed fraction of MTOM [kg]
% f.W.OEM_TP = @(MTOM) MTOM*0.60;      

% Electrical machine weight in [kg] as a function of installed power in
% [W]. Using a constant power density for now.
f.W.EM1 = @(P) P/p.SP.EM1;
f.W.EM2 = @(P) P/p.SP.EM2;

% Propulsive efficiency of ISOLATED propulsors of DP array. Correction
% factor 0.88 based on MSc thesis of Jacopo Zamboni. 
f.etap = @(Tc) 0.88*2./(1+(1+8*Tc/pi).^0.5); 

% Anonymous function for BL profile, used for BLI model
% u/U_edge as a function of (y/d99); both evaluated in [0,1], such that 
% u/U = 0 at y/d99 = 0, and u/U = 1 at y/d99 = 1. 
% f.BLprofile = @(y) y.^(1/7);      % Typical flat plate turbulent BL profile
f.BLprofile = @(y) (0.7848*y.^4 - 2.1*y.^3 + 0.9676*y.^2 + 1.227*y + 0.1193)/0.9987; % From Biagio's experiment

% Dynamic viscosity [kg/m/s], based on Sutherland's formula (1893); using
% temperature in [K]
f.mu = @(T) 1.716e-5*(T/273.11)^1.5*(273.11+110.56)/(T + 110.56);


%% Notes
% Note regarding current nomenclature: the symbols used to designate 
% some of the variables has changed over the course of developing this
% code. This should be fixed throughout the code, but for now:
%   - Throttle is indicated with "xi". 
%   - "phi" is the supplied power ratio.
%   - "Phi" is the shaft power ratio, although this variable is represented 
%       with "Psi" in literature (see the paper of de Vries, Brown & Vos, 
%       2018)
%   - The thrust share provided by the propulsors (which is related to Phi)
%       is indicated using "Chi", which is not (only) the thrust of the 
%       aircraft, but the thrust produced by the DP propulsors divided by
%       the total thrust of the aircraft.
%
% Note regarding the "throttle":
%   - For powertrain architectures containing a gas turbine (conventional,
%       turboelectric, serial, parallel, PTE or SPPH), this refers to the
%       throttle setting of the gas turbine, P_gt/P_gt_available, where
%       P_gt_available is the maximum power available in the given flight
%       condition.
%   - For fully electric architectures:
%       - For an e-1 or dual-e architecture, throttle refers to the
%           throttle setting of the primary electrical machine, P_gb/P_EM1,
%           where P_EM1 is the installed power of the primary electrical
%           machine. Since it is assumed to present no power lapse with
%           altitude or Mach, it is equal to the available power.
%       - For an e-2 architecture, throttle refers to the throttle setting
%           of the secondary electrical machine, P_s2/P_EM2, since this
%           layout contains no primary electrical machine.
%   - For hybrid configurations that allow any value of supplied power
%       ratio (serial/parallel/SPPH), if phi = 1 in a particular part of
%       the mission analysis, then the GT throttle has no use (since it
%       would lead to an indeterminatae). So if phi = 1, throttle will be
%       applied to the ELECTRICAL MACHINE INSTEAD (EM1 for parallel, or
%       EM2 for serial/SPPH).


