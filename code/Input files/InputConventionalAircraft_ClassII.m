%%% Input file example
% This file specifies the Class II input for a conventional twin-turboprop
% configuration.


%% General input

% General settings for Class-II weight estimation
a.category = 'TransportTurboprop';              % Aircraft category determining the type of correlations applied: 
                                                %   LightSingleEngine, LightMultiEngine, TransportMultiEngine, 
                                                %   TransportTurboprop,TransportTurbofan, JetTrainer, BusinessJet
                                                %   (NB. MultiEngine = reciprocating engines) (NB2: no freighters implemented)
C2.settings.TOM_0 = 25000;                      % Initial guess for MTOM [kg]
C2.settings.xCG_over_lFus_0 = 0.40;             % Initial guess for aircraft CG location, as a fraction of fuselage length
C2.settings.useRefTOM = 0;                      % Use reference aircraft "a.ref" for TOM/OEM-based correlations? (recommended:
                                                % no (0) for conventional configs, yes (1) for e.g. full-e configs where
                                                % the TOM or OEM are much higher than the aircraft on which the empirical
                                                % correlations are based. In that case, specify a a.ref.TOM and
                                                % corresponding OEM/DEM/PL fractions of a conventional AC sized for the same mission
C2.settings.FixedSpan = 0;                      % 0 = do nothing, 1 = fix span to "SpanLimit" (equality constraint), 2 = limit span to not 
                                                % exceed "SpanLimit" (ineq. con.). Only works if "manual" WS selection enabled. If specified
                                                % span exceeds the minimum wing size (i.e., max wing loading), then max WS is selected.
C2.settings.SpanLimit = 36;                     % (In)equality constraint value for wing span [m]. Note that it refers to distance in spanwise
                                                % direction! b_spanwise_direction = b_y_direction / cos(wing_dihedral).
a.ref.TOM = 30000;                              % Take-off mass of a reference conventional aircraft sized for same mission [kg]
a.ref.PL_fraction = 0.25;                       % Ratio payload mass/TOM of reference aircraft
a.ref.OEM_fraction = 0.60;                      % Ratio operating empty mass/TOM of reference aircraft
a.ref.DEM_fraction = 0.60;                      % Ratio delivery empty mass/TOM of reference aircraft
                                                % N.B. DEM does not include operational items
% Additional propulsion-system characteristics                      
p.rho_fuel = 0.804;                             % Fuel density [kg/l]                                                 
p.SP.PMAD = 15e3;                               % Specific power [W/kg
p.SP.TMS_EM1 = 1e3;                             % TMS specific power [W/kg]: kW of heat rejected per unit mass
p.SP.TMS_EM2 = 1e3;                             % TMS specific power for EM2 [W/kh]
p.SP.TMS_PM = 1e3;                              % TMS specific power for PMAD [W/kh]
p.SP.TMS_bat = 1e3;                             % TMS specific power for battery [W/kh]
p.eta_bat = 0.96;                               % Thermal "efficiency" of battery; cooling system sized to reject P_bat*(1-eta_bat) 
                                                % watts of heat, where P_bat is the max installed battery power requirement
                                                % Note that P_bat is determined in constraint diagram; so e.g. high C-rates
                                                % during charging are not accounted for.
p.cooling_power = 0.2;                          % Watts required for cooling system, per watt of heat rejected [-]. 
                                                
% Characteristic length [m] of an electrical machine including inverter, as a function
% of EM max continuous power [W]. Assuming P = proportional to volume, and length = diameter.
f.l_EM = @(P) (0.12*P/1e6)^(1/3);             
                                                
% EM mass [kg] as a function of shaft power [W] and rotational speed [rpm]
% N.B. only applied to EM2 at the moment - EM1 depends on gearbox speed
% ratios, etc.
f.W.EM1_ClassII = @(P,n) P/p.SP.EM1;
f.W.EM2_ClassII = @(P,n) 1.2*(P/1000)./(3 + 6.5*sqrt((n/20000 + 1).^2 - 1));

% Helical blade-tip Mach number of secondary propellers in (EM2) sizing
% condition [-]
p.M_tip = 0.85;  

% General settings for Class-II aero estimation
C2.settings.CD0method = 'Raymer';               % 'Obert' (one C_f for complete aircraft, component drag based on wetted area only),
                                                % or 'Raymer' (C_f, form factor, interference factor determined per component)
a.S_wet_misc = 0.05;                            % CD0 of miscellaneous components (fairings, winglets, etc) as a fraction of
                                                % the total CD0 of the "main" components (fuselage, wing, HT, VT & nacelles), 
                                                % including leakages & protruberances drag (Raymer:  5-10% for prop aircraft, 
                                                % 2-5% for jet transports)
a.e_penalty = 0;                                % Manual modification to a.e_clean computed in Class-II aero, to account for e.g.
                                                % higher D_fus/b than typical aircraft in empirical data, or less prop disturbance 
                                                % than in empirical data. Negative = penalty.
a.cooling_drag = 0.5e-3;                        % Drag due to cooling system, per W of heat rejected [N/W]. 

% ConfigurationAndGeometry plotting options
C2.settings.plotAero = 1;                       % Plot Class-II aero results?        
C2.settings.plotAircraft = 1;                   % Plot overall aircraft geometry?
C2.settings.showCGs = 1;                        % Show CG locations?
C2.settings.useLighting = 1;                    % Plot geometry with fancy lighting?


%% Fuselage 
% Local coordinate system (same as aircraft-level coordinate system):
% x = zero at front tip of fuselage, positive downstream
% y = zero in symmetry plane, postive starboard
% z = zero along fuselage cylinder axis, positive upwards

% Geometrical parameters: cabin layout 
C2.fuselage.geom.SeatsBusiness = 8;             % Number of business-class seats [-]
C2.fuselage.geom.SeatsAbreast = 4;              % Number of seats abreast [-]
C2.fuselage.geom.SeatWidth = 18*0.0254;         % Seat width [m] (now assumed same for econ and business)
C2.fuselage.geom.SeatPitchEcon = 30*0.0254;     % Seat pitch economy class [m]
C2.fuselage.geom.SeatPitchBusiness = 38*0.0254; % Seat pitch business class [m]
C2.fuselage.geom.SeatWidthArmrest = 2*0.0254;   % Width of armrest [m]
C2.fuselage.geom.LengthFlightDeck = 100*0.0254; % Flight deck length [m]
C2.fuselage.geom.LengthLavatories = 36*0.0254;  % Length lavatory section [m]
C2.fuselage.geom.LengthGalley = 38*0.0254;      % Galley length [m]
C2.fuselage.geom.WidthAisle = 20*0.0254;        % Aisle width [m]
C2.fuselage.geom.WidthExitAisle = 0.820;        % Width of exit aisles [m] (actually 0.910 m, but compensated by foot space of first seat row)
C2.fuselage.geom.NoAisles = 1;                  % Number of aisles
C2.fuselage.geom.NoGalleys = 1;                 % Number of galleys
C2.fuselage.geom.NoLavatories = 1;              % Number of lavatory sections
C2.fuselage.geom.NoExitAisles = 2;              % Number of exist aisles 
                                            
% Geometrical parameters: fuselage shape (note: exact fuselage nose and
% tailcone contours are defined manually using Bezier curves in
% CreateFuselageGeometry.m)
C2.fuselage.geom.lD_nose = 2.5;                 % Nosecone length-over-diameter ratio [-]
C2.fuselage.geom.lD_tail = 4.5;                 % Tailcone length-over-diameter ratio [-]
C2.fuselage.geom.xl_nose = -0.3;                % Axial position of start of cabin, as fraction of nosecone length [-]
                                                % e.g.: 0 = cabin starts at nose end; -0.1 = cabin starts at 90% of the nose cone 
                                                % Note that flight deck goes in front of cabin!
C2.fuselage.geom.xl_tail = +0.4;                % Axial position of end of cabin, as fraction of tailcone length [-]
                                                % e.g.: 0 = cabin ends at start of tailcone; 0.1 = cabin ends at 10% of tailcone length
C2.fuselage.geom.sFraction = 0.0;               % Fraction of external surface area covered because it's "inside" another component

% Weights (structural weight of fuselage only! Not payload, etc)
C2.fuselage.weights.x_over_l = 0.45;            % Axial coordinate of CG, as a fraction of fuselage length [-]
C2.fuselage.weights.y_over_D = 0;               % Lateral coordiante of CG, as a fraction of fuselage diameter [-]
C2.fuselage.weights.z_over_D = 0;               % Vertical coordiante of CG, as a fraction of fuselage diameter [-]

% Fuselage body characteristics (NLG assumed in fuselage; no freighter) for Class-II weight estimation
C2.fuselage.geom.FF = 1;                        % Fudge factor [-]
C2.fuselage.geom.isPressurized = 1;             % Pressurized cabin?
C2.fuselage.geom.FusMountedEngines = 0;         % Has fuselage-mounted engines? (Link to propulsion system configuration)

% Properties for Class-II aero estimation
C2.fuselage.aero.finish = 'smoothPaint';        % 'smoothPaint','camouflagePaint','productionMetal','smoothMetal','smoothComposite'?
C2.fuselage.aero.laminarFraction = 0.05;        % Fraction of laminar flow on fuselage (Raymer: 5%, 25% = research goal, 50% w/ suction)
C2.fuselage.aero.Q = 1;                         % Interference factor (Raymer: 1.0)

% Discretization/program settings
C2.fuselage.settings.nPointsNose = 30;          % Number of axial points defining nosecone section
C2.fuselage.settings.nPointsCyl = 2;            % Number of axial points defining central cylindrical section
C2.fuselage.settings.nPointsTail = 30;          % Number of axial points defining tailcone section
C2.fuselage.settings.nAzimuth = 40;             % Number of azimuthal divisions on fuselage
C2.fuselage.settings.plotOption = 0;            % Plot figure of geometry in CreateFuselageGeometry.m? 


%% Wing settings 
% Local coordinate system:
% x = root chord leading edge, positive downstream
% y = zero in symmetry plane, postive starboard
% z = zero at root quater-chord, positive upwards
% Note that twist/incidence is applied around the quarter-chord location

% Geometrical characteristics
C2.wing.geom.dihedral = -1;                     % Dihedral angle [deg] (0 = horizontal, 90 = vertical)
C2.wing.geom.z_over_Dfus = +0.45;               % Vertical placement of wing, as a fraction of fuselage diameter [-]
C2.wing.geom.root.incidence = 0;                % Root incidence angle [deg]
C2.wing.geom.tip.tc = 0.15;                     % Tip thickness-to-chord ratio [-]
C2.wing.geom.tip.incidence = -1;                % Tip incidence angle [deg]
C2.wing.geom.xc_spar_front = 0.15;              % Axial location (as fraction of chord) of front spar [-]
C2.wing.geom.xc_spar_rear = 0.65;               % Axial location (as fraction of chord) of rear spar [-]
C2.wing.geom.d_skin = 0.05;                     % Vertical distance between wing box and outer airfoil surface,
                                                % as a fraction of max airfoil thickness at that location [-]
C2.wing.geom.sFraction = 0.05;                  % Fraction of external surface area covered because it's "inside" another component
C2.wing.geom.fn_airfoil_root = 'Airfoil_NACA43015.txt';  % Root airfoil (NEED TO ADAPT t/c!)
C2.wing.geom.fn_airfoil_tip = 'Airfoil_NACA43015.txt';   % Tip airfoil (NEED TO ADAPT t/c!)

% Parameters for Class-II weight estimation (structural wing weight only)
C2.wing.geom.FF = 1.0;                          % Fudge factor [-]
C2.wing.geom.FF_bendingRelief = 0.95;           % Weight reduction factor due to bending relief. Torenbeek:
                                                % 0.95 for 2 wing-mounted engines, 0.90 for 4 wing-mounted engines.
C2.wing.geom.hasSpoilers = 1;                   % Wing features spoilers?
C2.wing.geom.isBracedWing = 0;                  % Is it a strut/truss-braced wing?

% CG (structural weight of wing only!)
C2.wing.weights.x_over_c = 0.4;                 % Axial coordinate of CG in local ref. frame, as a fraction of root chord [-]
C2.wing.weights.y_over_b = 0;                   % Lateral coordinate of CG in local ref. frame, as a fraction of wing span [-]
C2.wing.weights.z_over_c = 0.1;                 % Vertical coordiante of CG in local ref. frame, as a fraction of root chord [-]

% Properties for Class-II aero estimation (note prop effects for %laminar)!
C2.wing.aero.finish = 'smoothPaint';            % 'smoothPaint','camouflagePaint','productionMetal','smoothMetal','smoothComposite'?
C2.wing.aero.laminarFraction = 0.05;            % Fraction of laminar flow on wing (Raymer: 10%, 50% = research goal, 80% w/ suction)
C2.wing.aero.Q = 1;                             % Interference factor (Raymer: 1.0, or 1.1-1.4 for low wing without fairing)

% Settings
C2.wing.settings.nPoints = 50;                  % Number of points used to define each side of the airfoil
C2.wing.settings.plotOption = 0;                % Show figure with geometry in CreateLiftingSurfaceGeometry?


%% Tail settings
% Local coordinate system:
% x = HT root chord leading edge, positive downstream
% y = zero in symmetry plane, postive starboard
% z = zero at HT root quater-chord, positive upwards
% Note that for e.g. a canard config, the VT is far from the HT (canard);
% in that case the location still has to be related to the origin of the HT
% Also note that in e.g. a T-tail config, the origin is not inside/on the
% fuselage

% Tail configuration/placement choices
C2.tail.config = 'conventional';                % Options: 'conventional', 'V-tail','Y-tail','Canard'
                                                % 'Conventional includes arbitrary vertical placement of HT, so e.g. in 
                                                % a T-tail config. Code currently only works for conventional!
C2.tail.SM = -0.03;                             % Tail-off static margin [-], as a fraction of c_MAC. Positive if tail-off 
                                                % aerodynamic center is downstream of aircraft CG.
C2.tail.AC_wing = 0.25;                         % Location of wing+fuselage aerodynamic center, as a fraction of c_MAC [-]
C2.tail.xHT_over_lFus = 0.92;                   % Axial location of HT root LE as a fraction of fuselage length [m]
C2.tail.zHT_over_DFus = 1.8;                    % Vertical location of HT root LE as a fraction of fuselage diameter [m]
C2.tail.zHT_over_bVT = 0.95;                    % [Conventional tail only] Vertical position of HT, as a fraction of VT 
                                                % height (= VT span). Negative values = HT under VT root. Determines VT position!
                                                % 0 = conventional tail, 1 = T-tail
C2.tail.xHT_over_cVT = 0.70;                    % [Conventional tail only] Horizontal position of HT root LE, as a fraction of VT 
                                                % root chord. Negative values = HT root LE ahead of VT root LE. Determines VT position!
C2.tail.SizingMethod = 'volume';                % 'volume' (volume coefficients) or 'x-plot' (stability/control analysis)
C2.tail.HTVolCoeff = 1.60;                      % [volume method only] HT volume coefficient, (l_HT*S_HT)/(c_MAC*S_w) [-]
C2.tail.VTVolCoeff = 0.085;                     % [volume method only] WING-SPAN AND FUSELAGE-AREA BASED VT volume coefficient, 
                                                % (l_VT*S_VT)/(l_fus*S_w) [-]. See TailVolumeCoefficient.xlsx
C2.tail.HTSizeFactor = 1.0;                     % Correction factor for HT size (due to e.g. DP), effective_area = factor*area
C2.tail.VTSizeFactor = 1.0;                     % Correction factor for VT size (due to e.g. DP), effective_area = factor*area

% Horitontal tail: geometry, settings, weights
C2.tail.HT.geom.S = 20;                         % Initial guess for HT planform area [m2]
C2.tail.HT.geom.AR = 5;                         % Aspect ratio, b/c_mean [-] 
C2.tail.HT.geom.TR = 0.5;                       % Taper ratio, c_tip/c_root [-] 
C2.tail.HT.geom.sweep_c025 = 10;                % Quarter-chord sweep angle [deg] 
C2.tail.HT.geom.dihedral = 0;                   % Dihedral angle [deg] (0 = horizontal, 90 = vertical)
C2.tail.HT.geom.root.tc = 0.12;                 % Root thickness-to-chord ratio [-] 
C2.tail.HT.geom.root.incidence = 0;             % Root incidence angle [deg]
C2.tail.HT.geom.tip.tc = 0.09;                  % Tip thickness-to-chord ratio [-]
C2.tail.HT.geom.tip.incidence = 0;              % Tip incidence angle [deg]
C2.tail.HT.geom.xc_spar_front = 0.2;            % Axial location (as fraction of chord) of front spar [-]
C2.tail.HT.geom.xc_spar_rear = 0.7;             % Axial location (as fraction of chord) of rear spar [-]
C2.tail.HT.geom.d_skin = 0.05;                  % Vertical distance between wing box and outer airfoil surface,
                                                % as a fraction of max airfoil thickness at that location [-]
C2.tail.HT.geom.sFraction = 0.05;               % Fraction of external surface area covered because it's "inside" another component
C2.tail.HT.geom.fn_airfoil_root = 'Airfoil_NACA0012.txt';   % Root airfoil
C2.tail.HT.geom.fn_airfoil_tip = 'Airfoil_NACA0012.txt';    % Tip airfoil
C2.tail.HT.settings.nPoints = 50;               % Number of points used to define each side of the airfoil
C2.tail.HT.settings.plotOption = 0;             % Show figure with geometry in CreateLiftingSurfaceGeometry?
C2.tail.HT.weights.x_over_cHT = 0.4;            % Axial coordinate of CG in local ref. frame, as a fraction of HT root chord [-]
C2.tail.HT.weights.y_over_bHT = 0;              % Lateral coordinate of CG in local ref. frame, as a fraction of HT span [-]
C2.tail.HT.weights.z_over_cHT = 0;              % Vertical coordiante of CG in local ref. frame, as a fraction of HT root chord [-]

% Vertical tail: geometry, settings, weights
C2.tail.VT.geom.S = 12;                         % Initial guess for VT planform area [m2]
C2.tail.VT.geom.AR = 1.2;                       % Aspect ratio, b/c_mean [-] 
C2.tail.VT.geom.TR = 0.7;                       % Taper ratio, c_tip/c_root [-]
C2.tail.VT.geom.sweep_c025 = 30;                % Quarter-chord sweep angle [deg] 
C2.tail.VT.geom.dihedral = 90;                  % Dihedral angle [deg] (0 = horizontal, 90 = vertical)
C2.tail.VT.geom.root.tc = 0.12;                 % Root thickness-to-chord ratio [-] 
C2.tail.VT.geom.root.incidence = 0;             % Root incidence angle [deg]
C2.tail.VT.geom.tip.tc = 0.09;                  % Tip thickness-to-chord ratio [-]
C2.tail.VT.geom.tip.incidence = 0;              % Tip incidence angle [deg]
C2.tail.VT.geom.xc_spar_front = 0.2;            % Axial location (as fraction of chord) of front spar [-]
C2.tail.VT.geom.xc_spar_rear = 0.7;             % Axial location (as fraction of chord) of rear spar [-]
C2.tail.VT.geom.d_skin = 0.05;                  % Vertical distance between wing box and outer airfoil surface,
                                                % as a fraction of max airfoil thickness at that location [-]
C2.tail.VT.geom.sFraction = 0.1;                % Fraction of external surface area covered because it's "inside" another component
C2.tail.VT.geom.fn_airfoil_root = 'Airfoil_NACA0012.txt';   % Root airfoil
C2.tail.VT.geom.fn_airfoil_tip = 'Airfoil_NACA0012.txt';    % Tip airfoil
C2.tail.VT.settings.nPoints = 50;               % Number of points used to define each side of the airfoil
C2.tail.VT.settings.plotOption = 0;             % Show figure with geometry in CreateLiftingSurfaceGeometry?
C2.tail.VT.weights.x_over_cVT = 0.6;            % Axial coordinate of CG in local ref. frame, as a fraction of VT root chord [-]
C2.tail.VT.weights.y_over_cVT = 0;              % Lateral coordinate of CG in local ref. frame, as a fraction of VT root chord [-]
C2.tail.VT.weights.z_over_bVT = 0.4;            % Vertical coordiante of CG in local ref. frame, as a fraction of VT span [-]

% Tail characteristics used in Class-II weight estimation
C2.tail.geom.FF_HT = 1.0;                       % HT fudge factor (in addition to "SizeFactor" input for geometrical sizing! [-]
C2.tail.geom.FF_VT = 1.0;                       % VT fudge factor (in addition to "SizeFactor" input for geometrical sizing! [-]
C2.tail.geom.diveSpeedRatio = 1/0.8;            % Design dive speed, as a ratio relative to cruise speed [-]
C2.tail.geom.variableHT = 1;                    % Is the HT variable incidence (1) or fixed incidence (0)?

% Properties for Class-II aero estimation
C2.tail.HT.aero.finish = 'smoothPaint';         % 'smoothPaint','camouflagePaint','productionMetal','smoothMetal','smoothComposite'?
C2.tail.HT.aero.laminarFraction = 0.10;         % Fraction of laminar flow on HT (Raymer: 10%, 50% = research goal, 80% w/ suction)
C2.tail.HT.aero.Q = 1.04;                       % Interference factor (Raymer: Conventional: 1.04-1.05; V-tail 1.03; H-tail 1.08)
C2.tail.VT.aero.finish = 'smoothPaint';         % 'smoothPaint','camouflagePaint','productionMetal','smoothMetal','smoothComposite'?
C2.tail.VT.aero.laminarFraction = 0.10;         % Fraction of laminar flow on VT (Raymer: 10%, 50% = research goal, 80% w/ suction)
C2.tail.VT.aero.Q = 1.04;                       % Interference factor (Raymer: Conventional: 1.04-1.05; V-tail 1.03; H-tail 1.08)


%% Landing gear settings
% Note that a MLG nacelle can be created which contributes to wetted area,
% but the weight of that nacelle is not inlcuded in the Class-II weight
% breakdown (assumed small compared to MLG weight). Could partially be
% accounted for in propulsionGroup1.geom.hasMLGInNacelle.

% For Class-II weight estimation (applies to both MLG and NLG)
C2.MLG.geom.isRetractable = 1;                  % Retractable landing gear?
C2.MLG.geom.isTailDragger = 0;                  % Tail-dragger (1) or tricycle (0) landing gear?

% Nose landing gear (part of fuselage group)
C2.NLG.geom.FF = 1;                             % Fudge factor [-]
C2.NLG.weights.x_over_l = 0.15;                 % Axial coordinate of CG, as a fraction of fuselage length [-]
C2.NLG.weights.y_over_D = 0;                    % Lateral coordiante of CG, as a fraction of fuselage diameter [-]
C2.NLG.weights.z_over_D = -0.4;                 % Vertical coordiante of CG, as a fraction of fuselage diameter [-]

% Main landing gear (part of fuselage or wing group)
C2.MLG.geom.FF = 1;                             % Fudge factor [-]
C2.MLG.geom.wingMounted = 1;                    % MLG mounted on wing (1) or fuselage (0)? 
C2.MLG.weights.x_over_c = 0.7;                  % Wing mounted: Axial coord. of CG in local ref. frame, as a fraction of root chord [-]
C2.MLG.weights.y_over_b = 0;                    % Wing mounted: Lateral coord. of CG in local ref. frame, as a fraction of wing span [-]
C2.MLG.weights.z_over_c = 0;                    % Wing mounted: Vertical coord. of CG in local ref. frame, as a fraction of root chord [-]
C2.MLG.weights.x_over_l = 0.55;                 % Fuselage mounted: Axial coord. of CG, as a fraction of fuselage length [-]
C2.MLG.weights.y_over_D = 0;                    % Fuselage mounted: Lateral coord. of CG, as a fraction of fuselage diameter [-]
C2.MLG.weights.z_over_D = -0.4;                 % Fuselage mounted: Vertical coord. of CG, as a fraction of fuselage diameter [-]

% If MLG is wing-mounted and is stored in a nacelle: nacelle shape
C2.MLG.geom.hasNacelle = 0;                     % Does MLG have dedicated nacelle for storage? (Wing-mounted only!)
C2.MLG.geom.R_nac = 0.6 ;                       % Maximum radius of LG nacelle [m]
C2.MLG.geom.l_nac = 4.0;                        % Length of LG nacelle [m]
C2.MLG.geom.incidence = 0;                      % Incidence angle of nacelle (positive nose-up) [deg]
C2.MLG.geom.sFraction = 0.4;                    % Fraction of nacelle surface area covered because it's "inside" another component
C2.MLG.geom.x_over_c = 0.30;                    % Axial coordinate of front of nacelle, as a fraction of wing root chord [-]
C2.MLG.geom.y_over_b = 0.15;                    % Lateral coordinate of front of nacelle, as a fraction of wing SEMI-span [-]
C2.MLG.geom.z_over_c = -0.10;                   % Vertical coordinate of front of nacelle, as a fraction of wing root chord [-]
C2.MLG.settings.nPoints = 50;                   % Number of geometry discretization points in axial direction
C2.MLG.settings.nAzimuth = 50;                  % Number of geometry discretization points in azimuthal direction

% Properties for MLG nacelle Class-II aero estimation
C2.MLG.aero.finish = 'smoothPaint';             % 'smoothPaint','camouflagePaint','productionMetal','smoothMetal','smoothComposite'?
C2.MLG.aero.laminarFraction = 0.05;             % Fraction of laminar flow on nacelle
C2.MLG.aero.Q = 1.0;                            % Interference factor (Raymer: 1.5 if directly mounted on wing or fus; 1.3 if mounted one 
                                                % diameter away, 1.0 if mounted far away, <1 if in favourable interaction region
                                                % e.g. pressure side of wing, towards rear)
                                            
                                                
%% Primary propulsion-system settings 

% General description
C2.propulsionGroup1.geom.hasNacelles = 1;       % Create nacelle objects?

% Geometry (scalars or [1 x N/2] arrays)
C2.propulsionGroup1.geom.y_over_b2 = 0.30;      % LEDP: Lateral position of prop center, as a fraction of SEMI wing span [-]
                                                %       If scalar, the value refers to the most INBOARD propulsor
C2.propulsionGroup1.geom.z_over_c = -0.10;      % LEDP: Vertical position of prop center w.r.t. wing LE, as a fraction of local wing chord [-]
C2.propulsionGroup1.geom.incidence = 0;         % Propeller incidence angle, negative upwards [deg]. Rotated aroud center of prop disk.
C2.propulsionGroup1.geom.D_mot = 1.0;           % Motor diameter [m] (TO BE COMPUTED)
C2.propulsionGroup1.geom.l_mot = 2.5;           % Motor length [m] (TO BE COMPUTED)
C2.propulsionGroup1.geom.xmot_over_Dprop = 0.05;% Axial position of front of motor, as a fraction of prop diameter [-]. Positive downstream
C2.propulsionGroup1.geom.Dnac_over_Dmot = 1.3;  % Nacelle maximum diameter over motor diameter [-]
C2.propulsionGroup1.geom.lnac_over_Dnac = 5;    % Nacelle slenderness ratio [-]
C2.propulsionGroup1.geom.xnac_over_lnac = -0.15;% Axial position of front of nacelle (spinner), as a fraction of nacelle length, 
                                                % relative to prop disk center [-]. Positive downstream
C2.propulsionGroup1.geom.sFraction = 0.3;       % Fraction of nacelle surface area covered because it's "inside" another component
C2.propulsionGroup1.geom.dy = 1;                % LEDP: Spacing between adjacent DP propulsors, as fraction of propulsor
                                                % diameter [-]. Only used if y_over_b is scalar (CAN BE FROM CLASS-I INPUT)
C2.propulsionGroup1.geom.x_over_c = p.geom.xp;  % LEDP: Axial position of prop center w.r.t. wing LE, as a fraction of local wing chord [-]
                                                %       Negative upstream (CAN BE FROM CLASS-I INPUT)
C2.propulsionGroup1.geom.x_over_l = 0.97;       % fus: axial position of fuselage propulsor, as fraction of fuselage length [-] 
C2.propulsionGroup1.geom.z_over_D = 0.30;       % fus: vertical position of fuselage propulsor, as fraction of fuselage length [-]  
                                                %   (should become interpolated on tail centerline)
C2.propulsionGroup1.geom.type = 'LEDP';         % Type of prop system: 'none', 'fus', or LEDP (to be completed).
                                                % If 'none', no objects are created. If 'fus', components assumed inside fuselage
                                                % LEDP = wing-mounted (make sure p.AeroPropModel is compatible)

% CG of primary propulsion system, in case of 'fus'. Note that in that case 
% the complete primary system is represented by a single point, i.e. no
% copies of sub-component instances are created. [To be improved]
C2.propulsionGroup1.weights.x_over_l = 0.5;     % Axial coordinate of CG, as a fraction of fuselage length [-]
C2.propulsionGroup1.weights.y_over_D = 0.0;     % Lateral coordinate of CG, as a fraction of fuselage diameter [-]
C2.propulsionGroup1.weights.z_over_D = 0.0;     % Vertical coordinate of CG, as a fraction of fuselage diameter [-]

% Fuel system settings
% This component has to be modified to allow split between fuselage and
% wing groups. Also, split into separate tanks? (cell array) to check 
% placement along wing, similar to LEDP propulsion group. 
C2.fuelSystem.geom.hasFuel = 1;                 % 0 = no fuel tanks
                                                % 1 = fuel in wing
                                                % 2 = fuel in fuselage 
                                                % 3 = fuel split between wing and fuselage [NOT IMPLEMENTED YET]
C2.fuelSystem.geom.FF_fuel = 1.0;               % Fudge factor for fuel system [-]
C2.fuelSystem.geom.N_ftanks = 3;                % Number of fuel tanks [-] (should be >= N_engines)
C2.fuelSystem.geom.tank_fraction = 1.1;         % Ratio between tank volume and volume occupied by fuel when full                                            
C2.fuelSystem.weights.x_over_c = 0.40;          % For wing-mounted only: Axial coordinate of CG, as a fraction of wing root chord [-]
C2.fuelSystem.weights.y_over_b = 0;             % For wing-mounted only: Lateral coordinate of CG, as a fraction of wing span [-]
C2.fuelSystem.weights.z_over_c = 0.00;          % For wing-mounted only: Vertical coordinate of CG, as a fraction of wing root chord [-]
C2.fuelSystem.weights.x_over_l = 0.80;          % For fuselage-mounted only: Axial coordinate of CG, as a fraction of fuselage length [-]
C2.fuelSystem.weights.y_over_D = 0;             % For fuselage-mounted only: Lateral coordinate of CG, as a fraction of fuselage diameter [-]
C2.fuelSystem.weights.z_over_D = 0.25;          % For fuselage-mounted only: Vertical coordinate of CG, as a fraction of fuselage diameter [-]

% For Class-II weight estimation 
% Accessory gears, drives, starting, induction/exhaust systems for GT
C2.propulsionGroup1.geom.FF_accessories = 1.0;  % Fudge factor for GT accessories [-]
C2.propulsionGroup1.geom.hasSupercharger = 0;   % Does engine have supercharger? (reciprocating engines only)

% Oil system & cooler 
% (only for powertrain architectures that have a GT; GB cooling loop for 
% e-2/dual-e architectures not considered here)
C2.propulsionGroup1.geom.FF_oil = 1.0;          % Fudge factor for GT oil system [-]

% Propeller (does not include governor?)
C2.propulsionGroup1.geom.propulsorType = 1;     % Does primary system have propellers (1), fans (2), or other?
C2.propulsionGroup1.geom.FF_prop = 1.0;         % Fudge factor for propeller weight, primary prop. system [-]                                                
C2.propulsionGroup1.geom.B = 6;                 % Number of blades [-]

% Gearbox
C2.propulsionGroup1.geom.FF_GB = 1.0;           % Fudge factor for gearbox weight [-]
C2.propulsionGroup1.geom.hasGearbox = 1;        % Does powertrain have a gearbox? (independently of whether 
                                                %   "GB" is part of the schematic)
C2.propulsionGroup1.geom.n_P1 = 1800;           % Revolutions per minute of primary propulsors [rpm]
C2.propulsionGroup1.geom.n_GT = 10*1800;        % Revolultions per minute of gas turbine [rpm]

% Thermal management systems
C2.propulsionGroup1.geom.FF_TMS_EM1 = 0.0;      % Fudge factor TMS of EM1

% Nacelles & engine installation (pylons)
C2.propulsionGroup1.geom.FF_nac = 1;            % Fudge factor for nacelles and pylons [-]
C2.propulsionGroup1.geom.hasThrustReverser = 0; % Does the (TF) engine have a thrust reverser?
C2.propulsionGroup1.geom.hasMLGInNacelle = 0;   % Does the MLG fold into the engine nacelle?
C2.propulsionGroup1.geom.hasHorOppCyls = 0;     % For reciprocating engines only: has horizontally-opposing cylinders?

% Properties for nacelle Class-II aero estimation
C2.propulsionGroup1.aero.finish = 'smoothPaint';% 'smoothPaint','camouflagePaint','productionMetal','smoothMetal','smoothComposite'?
C2.propulsionGroup1.aero.laminarFraction = 0.05;% Fraction of laminar flow on nacelle
C2.propulsionGroup1.aero.Q = 1.5;               % Interference factor (Raymer: 1.5 if directly mounted on wing or fus; 1.3 if mounted one 
                                                % diameter away, 1.0 if mounted far away, <1 if in favourable interaction region
                                                % e.g. pressure side of wing, towards rear)
% Settings
C2.propulsionGroup1.settings.nPoints = 50;      % Number of axial points used to define nacelle contour
C2.propulsionGroup1.settings.nAzimuth = 50;     % Number of azimuthal points used to define contours
C2.propulsionGroup1.settings.plotOption = 0;    % Show figure with geometry in CreatePropulsionGroup?


%% Secondary propulsion-system settings 

% General description
C2.propulsionGroup2.geom.hasNacelles = 0;       % Create nacelle objects?

% Geometry (scalars or [1 x N/2] arrays)
C2.propulsionGroup2.geom.y_over_b2 = 0.25;      % LEDP: Lateral position of prop center, as a fraction of SEMI wing span [-]
                                                %       If scalar, the value refers to the most INBOARD propulsor
C2.propulsionGroup2.geom.z_over_c = -0.1;       % LEDP: Vertical position of prop center w.r.t. wing LE, as a fraction of local wing chord [-]
C2.propulsionGroup2.geom.incidence = -5;        % Propeller incidence angle, negative upwards [deg]. Rotated aroud center of prop disk.
C2.propulsionGroup2.geom.xmot_over_Dprop = 0.1; % Axial position of front of motor, as a fraction of prop diameter [-]. Positive downstream
C2.propulsionGroup2.geom.Dnac_over_Dmot = 1.3;  % Nacelle maximum diameter over motor diameter [-]
C2.propulsionGroup2.geom.lnac_over_Dnac = 4;    % Nacelle slenderness ratio [-]
C2.propulsionGroup2.geom.xnac_over_lnac = -0.2; % Axial position of front of nacelle (spinner), as a fraction of nacelle length, 
                                                % relative to prop disk center [-]. Positive downstream
C2.propulsionGroup2.geom.sFraction = 0.2;       % Fraction of nacelle surface area covered because it's "inside" another component
C2.propulsionGroup2.geom.dy = p.geom.dy;        % LEDP: Spacing between adjacent DP propulsors, as fraction of propulsor
                                                % diameter [-]. Only used if y_over_b is scalar. (CAN BE FROM CLASS-I INPUT)
C2.propulsionGroup2.geom.x_over_c = p.geom.xp;  % LEDP: Axial position of prop center w.r.t. wing LE, as a fraction of local wing chord [-]
                                                %       Negative upstream (CAN BE FROM CLASS-I INPUT)
C2.propulsionGroup2.geom.type = 'none';         % Type of prop system: 'none', 'fus', or LEDP (to be completed).
                                                % If 'none', no objects are created. If 'fus', components assumed inside fuselage
                                                % LEDP = wing-mounted (make sure p.AeroPropModel is compatible)                   

% CG of primary propulsion system, in case of 'fus'. Note that in that case 
% the complete primary system is represented by a single point, i.e. no
% copies of sub-component instances are created. [To be improved]
C2.propulsionGroup2.weights.x_over_l = 0.0;     % Axial coordinate of CG, as a fraction of fuselage length [-]
C2.propulsionGroup2.weights.y_over_D = 0.0;     % Lateral coordinate of CG, as a fraction of fuselage diameter [-]
C2.propulsionGroup2.weights.z_over_D = 0.0;     % Vertical coordinate of CG, as a fraction of fuselage diameter [-]

% Class-II weight estimation
% Propeller (does not include governor?)
C2.propulsionGroup2.geom.propulsorType = 1;     % Does secondary system have propellers (1), fans (2), or other?
C2.propulsionGroup2.geom.FF_prop = 1.0;         % Fudge factor for propeller weight, primary prop. system [-]
C2.propulsionGroup2.geom.B = 6;                 % Number of blades [-]

% Thermal management systems
C2.propulsionGroup2.geom.FF_TMS_EM2 = 0.0;      % Fudge factor TMS of EM2
C2.propulsionGroup2.geom.FF_TMS_bat = 0.0;      % Fudge factor TMS of battery

% PMAD system: cables, switches, transformers (included in PG2 later on),
% including CG location (assumed part of fuselage group)
C2.PMAD.geom.FF = 0.0;                          % Fudge factor for PMAD weight
C2.PMAD.geom.FF_TMS = 0.0;                      % Fudge factor TMS of PMAD
C2.PMAD.weights.x_over_l = 0.50;                % Axial coordinate of CG, as a fraction of fuselage length [-]
C2.PMAD.weights.y_over_D = 0;                   % Lateral coordinate of CG, as a fraction of fuselage diameter [-]
C2.PMAD.weights.z_over_D = 0.00;                % Vertical coordinate of CG, as a fraction of fuselage diameter [-]

% Nacelles & engine installation (pylons)
C2.propulsionGroup2.geom.FF_nac = 1;            % Fudge factor for nacelles and pylons [-]

% Properties for nacelle Class-II aero estimation
C2.propulsionGroup2.aero.finish = 'smoothPaint';% 'smoothPaint','camouflagePaint','productionMetal','smoothMetal','smoothComposite'?
C2.propulsionGroup2.aero.laminarFraction = 0.10;% Fraction of laminar flow on nacelle
C2.propulsionGroup2.aero.Q = 1.5;               % Interference factor (Raymer: 1.5 if directly mounted on wing or fus; 1.3 if mounted one 
                                                % diameter away, 1.0 if mounted far away, <1 if in favourable interaction region
                                                % e.g. pressure side of wing, towards rear)
% Settings
C2.propulsionGroup2.settings.nPoints = 50;      % Number of axial points used to define nacelle contour
C2.propulsionGroup2.settings.nAzimuth = 50;     % Number of azimuthal points used to define nacelle contours
C2.propulsionGroup2.settings.plotOption = 0;    % Show figure with geometry in CreatePropulsionGroup?


%% Payload settings 

% Weights (CG to be computed in loading diagram)
C2.payload.weights.x_over_l = 0.50;             % Axial coordinate of CG, as a fraction of fuselage length [-]
C2.payload.weights.y_over_D = 0;                % Lateral coordiante of CG, as a fraction of fuselage diameter [-]
C2.payload.weights.z_over_D = 0;                % Vertical coordiante of CG, as a fraction of fuselage diameter [-]


%% Battery settings 
% This component has to be modified to allow split between fuselage and
% wing groups. Also, split into packs (cell array) to check placement along
% wing, similar to LEDP propulsion group. 

% Battery pack options
C2.battery.geom.hasBattery = 0;                 % 0 = no battery (check compatibility with p.config!)
                                                % 1 = battery in wing
                                                % 2 = battery in fuselage 
                                                % 3 = battery split between wing and fuselage [NOT IMPLEMENTED YET]
% Weights 
C2.battery.weights.x_over_c = 0.40;             % Wing-mounted: Axial coordinate of CG, as a fraction of wing root chord [-]
C2.battery.weights.y_over_b = 0;                % Wing-mounted: Lateral coordinate of CG, as a fraction of wing span [-]
C2.battery.weights.z_over_c = 0.10;             % Wing-mounted: Vertical coordinate of CG, as a fraction of wing root chord [-]
C2.battery.weights.x_over_l = 0.50;             % For fuselage-mounted only: Axial coordinate of CG, as a fraction of fuselage length [-]
C2.battery.weights.y_over_D = 0;                % For fuselage-mounted only: Lateral coordinate of CG, as a fraction of fuselage diameter [-]
C2.battery.weights.z_over_D = -0.4;             % For fuselage-mounted only: Vertical coordinate of CG, as a fraction of fuselage diameter [-]


%% Control-surface group settings

% For Class-II weight estimation
C2.surfaceControls.geom.FF = 1;                 % Fudge factor [-]
C2.surfaceControls.geom.hasPoweredControls = 1; % Manual (0) or powered (1) control actuators?
C2.surfaceControls.geom.hasLEDevices = 0;       % Does aircraft have LE slats or LE flaps?
C2.surfaceControls.geom.hasSpoilers = 0;        % Does aircraft have spoilers?


%% Services & Equipment 
% No distinction is made among the sub-components of this group in
% Configuration&Geometry; the CG of the group as a whole must be assumed,
% and is expressed relative to the fuselage dimensions (i.e. it is a child
% object of the fuselage group in Config & Geom.)

% APU
C2.servAndEquip.geom.FF_APU = 1.0;              % Fudge factor [-]
C2.servAndEquip.geom.hasAPU = 1;                % Does aircraft have an APU?

% Instruments & avionics
C2.servAndEquip.geom.FF_ieg = 1.0;              % Fudge factor [-]
C2.servAndEquip.geom.NoPilots = 2;              % Number of pilots 
C2.servAndEquip.geom.hasIFR = 1;                % VFR (0) or IFR (1) ?

% Hydraulics, pneumatics, & electrical
C2.servAndEquip.geom.FF_hydr = 1.0;             % Fudge factor hydraulic & pneumatic system [-]
C2.servAndEquip.geom.FF_elec = 1.0;             % Fudge factor electrical system [-]
C2.servAndEquip.geom.poweredControls = 2;       % 0 = unpowered, 1 = powered controls,
                                                % 2 = duplex powered, 3 = triplex powered
C2.servAndEquip.geom.hasDCsystem = 1;           % For large aircraft: DC (1) or AC (0) electrical system?
C2.servAndEquip.geom.elecFromAPU = 0;           % Does the APU provide power to the electrical system?

% Furnishing
C2.servAndEquip.geom.FF_furn = 1.0;             % Fudge factor [-]

% Airco and Anti-icing systems (note: transports assumed pressurized)
C2.servAndEquip.geom.FF_AircoAntiIce = 0.85;    % Fudge factor [-]

% CG location (assumed part of fuselage group)
C2.servAndEquip.weights.x_over_l = 0.50;        % Axial coordinate of CG, as a fraction of fuselage length [-]
C2.servAndEquip.weights.y_over_D = 0;           % Lateral coordinate of CG, as a fraction of fuselage diameter [-]
C2.servAndEquip.weights.z_over_D = 0.00;        % Vertical coordinate of CG, as a fraction of fuselage diameter [-]


%% Operating items
% No distinction is made among the sub-components of this group in
% Configuration&Geometry; the CG of the group as a whole must be assumed,
% and is expressed relative to the fuselage dimensions (i.e. it is a child
% object of the fuselage group in Config & Geom.)

% Crew provisions
C2.operItems.geom.FF_crew = 1.0;                % Fudge factor [-]
C2.operItems.geom.NoFlightCrew = 2;             % Number of flight-deck crew (pilots)
C2.operItems.geom.NoCabinCrew = 2;              % Number of cabin attendants

% Passenger cabin supplies
C2.operItems.geom.FF_cab = 1.0;                 % Fudge factor [-]
C2.operItems.geom.meals = 1;                    % Passenger meals: 0 = none, 1 = snacks only,
                                                % 2 = short-range meal, 3 = long-range meal(s)

% Water and toilet chemicals
C2.operItems.geom.FF_water = 1.0;               % Fudge factor [-]

% Safety equipment
C2.operItems.geom.FF_seq = 1.0;                 % Fudge factor [-]
C2.operItems.geom.overwater = 1;                % Long overwater missions expected (1) or only over land (0)?

% Residual fuel and oil
C2.operItems.geom.FF_res = 1.0;                 % Fudge factor [-]

% CG location (assumed part of fuselage group)
C2.operItems.weights.x_over_l = 0.40;           % Axial coordinate of CG, as a fraction of fuselage length [-]
C2.operItems.weights.y_over_D = 0;              % Lateral coordinate of CG, as a fraction of fuselage diameter [-]
C2.operItems.weights.z_over_D = 0.00;           % Vertical coordinate of CG, as a fraction of fuselage diameter [-]


%% Other parameters that have already been specified in Class-I input

% Take directly from Class-I input
C2.payload.weights.m = MA_in.PL;                % Payload mass [kg] 
C2.fuselage.geom.NoPassengers = MA_in.N_pax;    % Number of passengers [-] 
C2.wing.geom.AR = a.AR;                         % Aspect ratio, b/c_mean [-]
C2.wing.geom.TR = a.TR;                         % Taper ratio, c_tip/c_root [-]
C2.wing.geom.root.tc = a.tc;                    % Root thickness-to-chord ratio [-]
C2.propulsionGroup1.geom.N = p.N1;              % Number of primary propulsors
C2.propulsionGroup2.geom.N = p.N2;              % Number of secondary propulsors
C2.settings.itermax = s.itermax;                % Maximum number of iterations [-]
C2.settings.errmax = s.errmax;                  % Convergence criterion

% Compute quarter-chord weep angle [deg] based on half-chord sweep angle
% (used for weight estimation) specified in Class-I input
C2.wing.geom.sweep_c025 = atand(tand(a.Lambda) + 1/a.AR*(1-a.TR)/(1+a.TR));

% Compute tail half-chord sweep angle. For HT and VT, quarter-chord is
% specified above, while for the wing, half-chord sweep is used as input
% because it was already required in the Class-1.5 input.
C2.tail.HT.geom.sweep_c050 = atand(tand(C2.tail.HT.geom.sweep_c025) - ...
    1/C2.tail.HT.geom.AR*(1-C2.tail.HT.geom.TR)/(1+C2.tail.HT.geom.TR));
C2.tail.VT.geom.sweep_c050 = atand(tand(C2.tail.VT.geom.sweep_c025) - ...
    1/2/C2.tail.VT.geom.AR*(1-C2.tail.VT.geom.TR)/(1+C2.tail.VT.geom.TR));

