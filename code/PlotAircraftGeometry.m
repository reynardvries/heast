function [fig] = PlotAircraftGeometry(aircraft,tail,propulsionGroup1,propulsionGroup2,MLG,settings)
% This function takes the results from ConfigurationAndGeometry.m and
% displays the aircraft geometry in a figure.
%
%%% Reynard de Vries
%%% First created: 20-03-22
%%% Last modified: 13-04-22


%% Prepare figure

% Extract info 
fuselage = aircraft.comps.fuselageGroup.comps.fuselage;
wing = aircraft.comps.wingGroup.comps.wing;

% Create colors for CG markers
MarkerFaceCols = {'g','b','c','m',[1 0.8 0],[0.8 0.2 1.0],'k','r',...
    'g','b','c','m',[1 0.8 0],[0.8 0.2 1.0],'k','r',...
    'g','b','c','m',[1 0.8 0],[0.8 0.2 1.0],'k','r',...
    'g','b','c','m',[1 0.8 0],[0.8 0.2 1.0],'k','r',...
    'g','b','c','m',[1 0.8 0],[0.8 0.2 1.0],'k','r',...
    'g','b','c','m',[1 0.8 0],[0.8 0.2 1.0],'k','r'};
MarkerEdgeCols = {'m',[1 0.8 0],[0.8 0.2 1.0],'k','r','g','b','c',...
    'c','m',[1 0.8 0],[0.8 0.2 1.0],'k','r','g','b',...
    'b','c','m',[1 0.8 0],[0.8 0.2 1.0],'k','r','g',...
    'g','b','c','m',[1 0.8 0],[0.8 0.2 1.0],'k','r',...
    'r','g','b','c','m',[1 0.8 0],[0.8 0.2 1.0],'k',...
    'k','r','g','b','c','m',[1 0.8 0],[0.8 0.2 1.0]};

% Create 3D figure
fig = figure;
fig.Name = 'Aircraft geometry';
hold on; grid on; box on;
xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]');
axis equal; view(3);


%% Add fuselage

% Fuselage surfaces
for i = 1:length(fuselage.plotting.P)
    xx = squeeze(fuselage.plotting.P{i}(1,:,:)) + aircraft.comps.fuselageGroup.RefSystem(1);
    yy = squeeze(fuselage.plotting.P{i}(2,:,:)) + aircraft.comps.fuselageGroup.RefSystem(2);
    zz = squeeze(fuselage.plotting.P{i}(3,:,:)) + aircraft.comps.fuselageGroup.RefSystem(3);
    surf(xx,yy,zz,'facecolor',[0.5 0.5 0.5],'edgecolor','none','facealpha',0.5)
end


%% Add wing

% Wing surfaces
for i = 1:length(wing.plotting.P)
    if i == 2 || i == 4
        surfCol = [0.5 0.5 0.5];
        fAlpha = 0.7;
    else
        surfCol = [0.3 0.3 0.8];
        fAlpha = 0.5;
    end
    xx = squeeze(wing.plotting.P{i}(1,:,:)) + aircraft.comps.wingGroup.RefSystem(1);
    yy = squeeze(wing.plotting.P{i}(2,:,:)) + aircraft.comps.wingGroup.RefSystem(2);
    zz = squeeze(wing.plotting.P{i}(3,:,:)) + aircraft.comps.wingGroup.RefSystem(3);
    surf(xx,yy,zz,'facecolor',surfCol,'edgecolor','none','facealpha',fAlpha)
end

% Wing tip covers
fill3(wing.plotting.P{1}(1,:,3) + aircraft.comps.wingGroup.RefSystem(1),...
    wing.plotting.P{1}(2,:,3) + aircraft.comps.wingGroup.RefSystem(2),...
    wing.plotting.P{1}(3,:,3) + aircraft.comps.wingGroup.RefSystem(3),...
    [0.3 0.3 0.8],'edgecolor','none','facealpha',0.5)
fill3(wing.plotting.P{3}(1,:,3) + aircraft.comps.wingGroup.RefSystem(1),...
    wing.plotting.P{3}(2,:,3) + aircraft.comps.wingGroup.RefSystem(2),...
    wing.plotting.P{3}(3,:,3) + aircraft.comps.wingGroup.RefSystem(3),...
    [0.3 0.3 0.8],'edgecolor','none','facealpha',0.5)


%% Add tail

% Horizontal tail surfaces
for i = 1:length(tail.HT.plotting.P)
    if i == 2 || i == 4
        surfCol = [0.5 0.5 0.5];
        fAlpha = 0.7;
    else
        surfCol = [0.3 0.3 0.8];
        fAlpha = 0.5;
    end
    xx = squeeze(tail.HT.plotting.P{i}(1,:,:)) + aircraft.comps.tailGroup.RefSystem(1);
    yy = squeeze(tail.HT.plotting.P{i}(2,:,:)) + aircraft.comps.tailGroup.RefSystem(2);
    zz = squeeze(tail.HT.plotting.P{i}(3,:,:)) + aircraft.comps.tailGroup.RefSystem(3);
    surf(xx,yy,zz,'facecolor',surfCol,'edgecolor','none','facealpha',fAlpha)
end

% HT tip covers
fill3(tail.HT.plotting.P{1}(1,:,3) + aircraft.comps.tailGroup.RefSystem(1),...
    tail.HT.plotting.P{1}(2,:,3) + aircraft.comps.tailGroup.RefSystem(2),...
    tail.HT.plotting.P{1}(3,:,3) + aircraft.comps.tailGroup.RefSystem(3),...
    [0.3 0.3 0.8],'edgecolor','none','facealpha',0.5)
fill3(tail.HT.plotting.P{3}(1,:,3) + aircraft.comps.tailGroup.RefSystem(1),...
    tail.HT.plotting.P{3}(2,:,3) + aircraft.comps.tailGroup.RefSystem(2),...
    tail.HT.plotting.P{3}(3,:,3) + aircraft.comps.tailGroup.RefSystem(3),...
    [0.3 0.3 0.8],'edgecolor','none','facealpha',0.5)

% Vertical tail surfaces
if isfield(tail.VT,'plotting')
    for i = 1:length(tail.VT.plotting.P)
        if i == 2
            surfCol = [0.5 0.5 0.5];
            fAlpha = 0.7;
        else
            surfCol = [0.3 0.3 0.8];
            fAlpha = 0.5;
        end
        xx = squeeze(tail.VT.plotting.P{i}(1,:,:)) + aircraft.comps.tailGroup.RefSystem(1);
        yy = squeeze(tail.VT.plotting.P{i}(2,:,:)) + aircraft.comps.tailGroup.RefSystem(2);
        zz = squeeze(tail.VT.plotting.P{i}(3,:,:)) + aircraft.comps.tailGroup.RefSystem(3);
        surf(xx,yy,zz,'facecolor',surfCol,'edgecolor','none','facealpha',fAlpha)
    end
    
    % VT tip covers
    fill3(tail.VT.plotting.P{1}(1,:,3) + aircraft.comps.tailGroup.RefSystem(1),...
        tail.VT.plotting.P{1}(2,:,3) + aircraft.comps.tailGroup.RefSystem(2),...
        tail.VT.plotting.P{1}(3,:,3) + aircraft.comps.tailGroup.RefSystem(3),...
        [0.3 0.3 0.8],'edgecolor','none','facealpha',0.5)
end


%% Add propulsion system
% Include additional cases if they become available

% Primary propulsion system
if strcmp(propulsionGroup1.geom.type,'LEDP')
    
    % Add propeller disks 
    for i = 1:propulsionGroup1.geom.N
        fill3(propulsionGroup1.comps.propellers{i}.plotting.P(1,:) + aircraft.comps.wingGroup.RefSystem(1),...
            propulsionGroup1.comps.propellers{i}.plotting.P(2,:) + aircraft.comps.wingGroup.RefSystem(2),...
            propulsionGroup1.comps.propellers{i}.plotting.P(3,:) + aircraft.comps.wingGroup.RefSystem(3),...
            [0.5 0.5 0.5],'facealpha',0.2,'edgecolor',[0.8 0.8 0.8]);
    end
    
    % Add motors
    for i = 1:propulsionGroup1.geom.N
        xx = squeeze(propulsionGroup1.comps.motors{i}.plotting.P(1,:,:)) + aircraft.comps.wingGroup.RefSystem(1);
        yy = squeeze(propulsionGroup1.comps.motors{i}.plotting.P(2,:,:)) + aircraft.comps.wingGroup.RefSystem(2);
        zz = squeeze(propulsionGroup1.comps.motors{i}.plotting.P(3,:,:)) + aircraft.comps.wingGroup.RefSystem(3);
        surf(xx,yy,zz,'facecolor',[0.2 0.2 0.8],'edgecolor','none','facealpha',0.5)
        fill3(propulsionGroup1.comps.motors{i}.plotting.P(1,:,1) + aircraft.comps.wingGroup.RefSystem(1),...
              propulsionGroup1.comps.motors{i}.plotting.P(2,:,1) + aircraft.comps.wingGroup.RefSystem(2),...
              propulsionGroup1.comps.motors{i}.plotting.P(3,:,1) + aircraft.comps.wingGroup.RefSystem(3),...
              [0.2 0.2 0.8],'facealpha',0.5,'edgecolor','b');
        fill3(propulsionGroup1.comps.motors{i}.plotting.P(1,:,2) + aircraft.comps.wingGroup.RefSystem(1),...
              propulsionGroup1.comps.motors{i}.plotting.P(2,:,2) + aircraft.comps.wingGroup.RefSystem(2),...
              propulsionGroup1.comps.motors{i}.plotting.P(3,:,2) + aircraft.comps.wingGroup.RefSystem(3),...
              [0.2 0.2 0.8],'facealpha',0.5,'edgecolor','b');          
    end
    
    % Add nacelles 
    if propulsionGroup1.geom.hasNacelles == 1
        for i = 1:propulsionGroup1.geom.N
            xx = squeeze(propulsionGroup1.comps.nacelles{i}.plotting.P(1,:,:)) + aircraft.comps.wingGroup.RefSystem(1);
            yy = squeeze(propulsionGroup1.comps.nacelles{i}.plotting.P(2,:,:)) + aircraft.comps.wingGroup.RefSystem(2);
            zz = squeeze(propulsionGroup1.comps.nacelles{i}.plotting.P(3,:,:)) + aircraft.comps.wingGroup.RefSystem(3);
            surf(xx,yy,zz,'facecolor',[0.8 0.2 0.2],'edgecolor','none','facealpha',0.3)
        end
    end 

% For fuselage-mounted case
elseif strcmp(propulsionGroup1.geom.type,'fus')

    % Add propeller disks
    for i = 1:propulsionGroup1.geom.N
        if isfield(propulsionGroup1.comps.propellers{i},'plotting')
            fill3(propulsionGroup1.comps.propellers{i}.plotting.P(1,:),...
                propulsionGroup1.comps.propellers{i}.plotting.P(2,:),...
                propulsionGroup1.comps.propellers{i}.plotting.P(3,:),...
                [0.5 0.5 0.5],'facealpha',0.2,'edgecolor',[0.8 0.8 0.8]);
        end
    end
    
    % Add motors
    for i = 1:propulsionGroup1.geom.N
        if isfield(propulsionGroup1.comps.motors{i},'plotting')
            xx = squeeze(propulsionGroup1.comps.motors{i}.plotting.P(1,:,:));
            yy = squeeze(propulsionGroup1.comps.motors{i}.plotting.P(2,:,:));
            zz = squeeze(propulsionGroup1.comps.motors{i}.plotting.P(3,:,:));
            surf(xx,yy,zz,'facecolor',[0.2 0.2 0.8],'edgecolor','none','facealpha',0.5)
            fill3(propulsionGroup1.comps.motors{i}.plotting.P(1,:,1),...
                propulsionGroup1.comps.motors{i}.plotting.P(2,:,1),...
                propulsionGroup1.comps.motors{i}.plotting.P(3,:,1),...
                [0.2 0.2 0.8],'facealpha',0.5,'edgecolor','b');
            fill3(propulsionGroup1.comps.motors{i}.plotting.P(1,:,2),...
                propulsionGroup1.comps.motors{i}.plotting.P(2,:,2),...
                propulsionGroup1.comps.motors{i}.plotting.P(3,:,2),...
                [0.2 0.2 0.8],'facealpha',0.5,'edgecolor','b');
        end
    end
    
    % Add nacelles 
    if propulsionGroup1.geom.hasNacelles == 1
        for i = 1:propulsionGroup1.geom.N
            if isfield(propulsionGroup1.comps.nacelles{i},'plotting')
                xx = squeeze(propulsionGroup1.comps.nacelles{i}.plotting.P(1,:,:));
                yy = squeeze(propulsionGroup1.comps.nacelles{i}.plotting.P(2,:,:));
                zz = squeeze(propulsionGroup1.comps.nacelles{i}.plotting.P(3,:,:));
                surf(xx,yy,zz,'facecolor',[0.8 0.2 0.2],'edgecolor','none','facealpha',0.3)
            end
        end
    end 
end

% Secondary propulsion system
if strcmp(propulsionGroup2.geom.type,'LEDP')
    
    % Add propeller disks 
    for i = 1:propulsionGroup2.geom.N
        fill3(propulsionGroup2.comps.propellers{i}.plotting.P(1,:) + aircraft.comps.wingGroup.RefSystem(1),...
            propulsionGroup2.comps.propellers{i}.plotting.P(2,:) + aircraft.comps.wingGroup.RefSystem(2),...
            propulsionGroup2.comps.propellers{i}.plotting.P(3,:) + aircraft.comps.wingGroup.RefSystem(3),...
            [0.5 0.5 0.5],'facealpha',0.2,'edgecolor',[0.8 0.8 0.8]);
    end
    
    % Add motors 
    for i = 1:propulsionGroup2.geom.N
        xx = squeeze(propulsionGroup2.comps.motors{i}.plotting.P(1,:,:)) + aircraft.comps.wingGroup.RefSystem(1);
        yy = squeeze(propulsionGroup2.comps.motors{i}.plotting.P(2,:,:)) + aircraft.comps.wingGroup.RefSystem(2);
        zz = squeeze(propulsionGroup2.comps.motors{i}.plotting.P(3,:,:)) + aircraft.comps.wingGroup.RefSystem(3);
        surf(xx,yy,zz,'facecolor',[0.2 0.2 0.8],'edgecolor','none','facealpha',0.5)
        fill3(propulsionGroup2.comps.motors{i}.plotting.P(1,:,1) + aircraft.comps.wingGroup.RefSystem(1),...
              propulsionGroup2.comps.motors{i}.plotting.P(2,:,1) + aircraft.comps.wingGroup.RefSystem(2),...
              propulsionGroup2.comps.motors{i}.plotting.P(3,:,1) + aircraft.comps.wingGroup.RefSystem(3),...
              [0.2 0.2 0.8],'facealpha',0.5,'edgecolor','b');
        fill3(propulsionGroup2.comps.motors{i}.plotting.P(1,:,2) + aircraft.comps.wingGroup.RefSystem(1),...
              propulsionGroup2.comps.motors{i}.plotting.P(2,:,2) + aircraft.comps.wingGroup.RefSystem(2),...
              propulsionGroup2.comps.motors{i}.plotting.P(3,:,2) + aircraft.comps.wingGroup.RefSystem(3),...
              [0.2 0.2 0.8],'facealpha',0.5,'edgecolor','b');          
    end
    
    % Add nacelles
    if propulsionGroup2.geom.hasNacelles == 1
        for i = 1:propulsionGroup2.geom.N
            xx = squeeze(propulsionGroup2.comps.nacelles{i}.plotting.P(1,:,:)) + aircraft.comps.wingGroup.RefSystem(1);
            yy = squeeze(propulsionGroup2.comps.nacelles{i}.plotting.P(2,:,:)) + aircraft.comps.wingGroup.RefSystem(2);
            zz = squeeze(propulsionGroup2.comps.nacelles{i}.plotting.P(3,:,:)) + aircraft.comps.wingGroup.RefSystem(3);
            surf(xx,yy,zz,'facecolor',[0.8 0.2 0.2],'edgecolor','none','facealpha',0.3)
        end
    end 

% For fuselage-mounted case
elseif strcmp(propulsionGroup2.geom.type,'fus')

    % Add propeller disks
    for i = 1:propulsionGroup2.geom.N
        if isfield(propulsionGroup2.comps.propellers{i},'plotting')
            fill3(propulsionGroup2.comps.propellers{i}.plotting.P(1,:),...
                propulsionGroup2.comps.propellers{i}.plotting.P(2,:),...
                propulsionGroup2.comps.propellers{i}.plotting.P(3,:),...
                [0.5 0.5 0.5],'facealpha',0.2,'edgecolor',[0.8 0.8 0.8]);
        end
    end
    
    % Add motors
    for i = 1:propulsionGroup2.geom.N
        if isfield(propulsionGroup2.comps.motors{i},'plotting')
            xx = squeeze(propulsionGroup2.comps.motors{i}.plotting.P(1,:,:));
            yy = squeeze(propulsionGroup2.comps.motors{i}.plotting.P(2,:,:));
            zz = squeeze(propulsionGroup2.comps.motors{i}.plotting.P(3,:,:));
            surf(xx,yy,zz,'facecolor',[0.2 0.2 0.8],'edgecolor','none','facealpha',0.5)
            fill3(propulsionGroup2.comps.motors{i}.plotting.P(1,:,1),...
                propulsionGroup2.comps.motors{i}.plotting.P(2,:,1),...
                propulsionGroup2.comps.motors{i}.plotting.P(3,:,1),...
                [0.2 0.2 0.8],'facealpha',0.5,'edgecolor','b');
            fill3(propulsionGroup2.comps.motors{i}.plotting.P(1,:,2),...
                propulsionGroup2.comps.motors{i}.plotting.P(2,:,2),...
                propulsionGroup2.comps.motors{i}.plotting.P(3,:,2),...
                [0.2 0.2 0.8],'facealpha',0.5,'edgecolor','b');
        end
    end
    
    % Add nacelles 
    if propulsionGroup2.geom.hasNacelles == 1
        for i = 1:propulsionGroup2.geom.N
            if isfield(propulsionGroup2.comps.nacelles{i},'plotting')
                xx = squeeze(propulsionGroup2.comps.nacelles{i}.plotting.P(1,:,:));
                yy = squeeze(propulsionGroup2.comps.nacelles{i}.plotting.P(2,:,:));
                zz = squeeze(propulsionGroup2.comps.nacelles{i}.plotting.P(3,:,:));
                surf(xx,yy,zz,'facecolor',[0.8 0.2 0.2],'edgecolor','none','facealpha',0.3)
            end
        end
    end 
end


%% Add MLG 

% Add nacelles
if MLG.geom.hasNacelle == 1
    for i = 1:2
        xx = squeeze(MLG.plotting.P{i}(1,:,:)) + aircraft.comps.wingGroup.RefSystem(1);
        yy = squeeze(MLG.plotting.P{i}(2,:,:)) + aircraft.comps.wingGroup.RefSystem(2);
        zz = squeeze(MLG.plotting.P{i}(3,:,:)) + aircraft.comps.wingGroup.RefSystem(3);
        surf(xx,yy,zz,'facecolor',[0.8 0.2 0.2],'edgecolor','none','facealpha',0.3)
    end
end


%% CG markers
if settings.showCGs == 1
    idxCol = 1;
    
    % Scale markers according to mass of each element
    smin = 3;
    smax = 8;
    TOM = aircraft.weights.m;
    
    % Aerodynamic center, wing
    hCGs(1) = plot3((wing.geom.MAC.x_c025 + aircraft.comps.wingGroup.RefSystem(1))*[1 1 1],...
        (wing.geom.MAC.y_c025 + aircraft.comps.wingGroup.RefSystem(2))*[-1 0 1],...
        (wing.geom.MAC.z_c025 + aircraft.comps.wingGroup.RefSystem(3))*[1 1 1],...
        'x','markersize',8,'markeredgecolor','r');
    stringCGs{1} = 'Wing 25% MAC';
    
    % Aircraft CG
    hCGs(end+1) = plot3(aircraft.weights.coords(1),...
        aircraft.weights.coords(2),...
        aircraft.weights.coords(3),...
        'o','markersize',smin + smax*aircraft.weights.m/TOM,...
        'markerfacecolor',MarkerFaceCols{idxCol},...
        'markeredgecolor',MarkerEdgeCols{idxCol});
    stringCGs{end+1} = 'aircraft';
    idxCol = idxCol + 1;
    
    % Loop over L1 components
    L1names = fieldnames(aircraft.comps);
    for i = 1:length(L1names)
        
        % Plot Level-1 CGs.
        hCGs(end+1) = plot3(aircraft.comps.(L1names{i}).weights.coords(1),...
            aircraft.comps.(L1names{i}).weights.coords(2),...
            aircraft.comps.(L1names{i}).weights.coords(3),...
            'o','markersize',smin + smax*aircraft.comps.(L1names{i}).weights.m/TOM,...
            'markerfacecolor',MarkerFaceCols{idxCol},...
            'markeredgecolor',MarkerEdgeCols{idxCol});
        stringCGs{end+1} = L1names{i};
        idxCol = idxCol + 1;
        
        % Loop over Level-2 components
        % (Have to add an exception for PropulsionGroups, since they
        % don't have marker settings from input file. They also need a
        % larger marker)
        L2names = fieldnames(aircraft.comps.(L1names{i}).comps);
        for j = 1:length(L2names)
            hCGs(end+1) = plot3(aircraft.comps.(L1names{i}).comps.(L2names{j}).weights.coords(1),...
                aircraft.comps.(L1names{i}).comps.(L2names{j}).weights.coords(2),...
                aircraft.comps.(L1names{i}).comps.(L2names{j}).weights.coords(3),...
                'o','markersize',smin + smax*aircraft.comps.(L1names{i}).comps.(L2names{j}).weights.m/TOM,...
                'markerfacecolor',MarkerFaceCols{idxCol},...
                'markeredgecolor',MarkerEdgeCols{idxCol});
            stringCGs{end+1} = L2names{j};
            idxCol = idxCol + 1;
            
            % Loop over Level-3 components, if applicable
            if isfield(aircraft.comps.(L1names{i}).comps.(L2names{j}),'comps')
                L3names = fieldnames(aircraft.comps.(L1names{i}).comps.(L2names{j}).comps);
                for k = 1:length(L3names)
                    if iscell(aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}))
                        for m = 1:length(aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}))
                            hh = plot3(aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}){m}.weights.coords(1),...
                                aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}){m}.weights.coords(2),...
                                aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}){m}.weights.coords(3),...
                                'o','markersize',smin + ...
                                smax*aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}){m}.weights.m/TOM,...
                                'markerfacecolor',MarkerFaceCols{idxCol},'markeredgecolor',MarkerEdgeCols{idxCol});
                            if m == length(aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}))
                                hCGs(end+1) = hh;
                                stringCGs{end+1} = L3names{k};
                                idxCol = idxCol + 1;
                            end
                        end
                    else
                        hCGs(end+1) = plot3(aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}).weights.coords(1),...
                            aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}).weights.coords(2),...
                            aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}).weights.coords(3),...
                            'o','markersize',smin + ...
                            smax*aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}).weights.m/TOM,...
                            'markerfacecolor',MarkerFaceCols{idxCol},'markeredgecolor',MarkerEdgeCols{idxCol});
                        stringCGs{end+1} = L3names{k};
                        idxCol = idxCol + 1;
                    end
                end
            end
        end
    end
    
    % Add legend showing CG labels
    legend(hCGs,stringCGs);
end


%% Lighting effects
if settings.useLighting == 1
    lighting gouraud
    light('Position',[-1 -1 0],'Style','local')
end



