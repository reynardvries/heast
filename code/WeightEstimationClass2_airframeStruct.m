function [M] = WeightEstimationClass2_airframeStruct(M,TOM0,a,m,p,f,s,c,aircraft,C2)
% For input/output see WeightEstimationClass2.m.
%
%%% Reynard de Vries
%%% First created: 29-03-22
%%% Last modified: 10-02-23

% Extract fields from "C2" structure (for shorter writing only...)
fNames = fieldnames(C2);
for i = 1:length(fNames)
    eval([fNames{i} ' = C2.(fNames{i});'])
end

% Wing group (note that several parameters come from Class-I input)
% (Torenbeek S. 8.4.1b, p.280)
if battery.geom.hasBattery == 1     % Battery in wing
    MZFW = TOM0 - M.f - M.bat;
else                                % Only fuel in wing
    MZFW = TOM0 - M.f;
end
if wing.geom.hasSpoilers == 1
    k_spoilers = 0.02;
else
    k_spoilers = 0;
end
if MLG.geom.wingMounted == 1
    k_MLG = 0;
else
    k_MLG = -0.05;
end
if wing.geom.isBracedWing == 1
    k_bracedWing = -0.3;
else
    k_bracedWing = 0;
end
switch a.category
    case 'TransportTurboprop'   % Transport category aircraft with TOM > 5670 kg
        k_category = 6.67e-3;
    case 'TransportTurbofan'
        k_category = 6.67e-3;
    case 'TransportMultiEngine'
        k_category = 6.67e-3;
    case 'LightSingleEngine'    % Light aircraft with TOM < 5670 kg
        k_category = 4.90e-3;
    case 'LightMultiEngine'
        k_category = 4.90e-3;
    case 'JetTrainer'
        error('Category not defined in wing weight estimation')
    case 'BusinessJet'
        error('Category not defined in wing weight estimation')
end
k_wing = wing.geom.FF*(1 + k_spoilers + k_MLG + k_bracedWing + ...
    (wing.geom.FF_bendingRelief - 1));
bs = (aircraft.Sw*a.AR)^0.5/cosd(a.Lambda);
tr = a.tc*(aircraft.Sw/a.AR)^0.5*2/(1+a.TR);
M.airframeStruct.wing = k_wing*...
    MZFW*k_category*bs^0.75*(1+(1.905/bs)^0.5)*...
    a.nUlt^0.55*((bs/tr)/(MZFW/aircraft.Sw))^0.3;

% Tail group (empirical data from Torenbeek Fig. 8.5)
% Modified 10-02-23: dive speed in EAS, not in TAS!
xx = [0.214 0.26 0.324 0.388 0.453 0.516 0.58 0.64];
yy = [10 12.85 16.8 20.78 24.5 27.23 28.89 29.55];
V_dive = m.cr.v*tail.geom.diveSpeedRatio*sqrt(m.cr.rho/c.rho_SL);
% V_dive = m.cr.v*tail.geom.diveSpeedRatio % Old
normalizedAreaHT = (tail.HT.geom.S^0.2 * V_dive/1000)/(cosd(tail.HT.geom.sweep_c050))^0.5;
normalizedAreaVT = (tail.VT.geom.S^0.2 * V_dive/1000)/(cosd(tail.VT.geom.sweep_c050))^0.5;
normalizedMassHT = interp1(xx,yy,normalizedAreaHT,'linear','extrap');
normalizedMassVT = interp1(xx,yy,normalizedAreaVT,'linear','extrap');
VTspanFraction = max([0 min([1 tail.zHT_over_bVT])]);
if tail.geom.variableHT == 1
    k_HT = 1.1;
else
    k_HT = 1;
end
k_VT = 1 + 0.15*tail.HT.geom.S/tail.VT.geom.S*VTspanFraction; % = 1 for conventional tail
M.airframeStruct.HT = normalizedMassHT*k_HT*tail.HT.geom.S*tail.geom.FF_HT;
if strcmp(C2.tail.config,'V-tail')
    M.airframeStruct.VT = 0;
else
    M.airframeStruct.VT = normalizedMassVT*k_VT*tail.VT.geom.S*tail.geom.FF_VT;
end

% Fuselage group
if fuselage.geom.isPressurized == 1
    k_pres = 0.08;
else
    k_pres = 0.0;
end
if fuselage.geom.FusMountedEngines == 1
    k_fusEngines = 0.04;
else
    k_fusEngines = 0.0;
end
if MLG.geom.wingMounted == 0
    k_fusMLG = 0.07;
else
    k_fusMLG = 0.0;
end
k_fus_tot = 1 + k_pres + k_fusEngines + k_fusMLG;
M.airframeStruct.fuselage = 0.23*(V_dive*fuselage.geom.l_tailArm/fuselage.geom.D_fus/2)^0.5*...
    fuselage.geom.S_fus^1.2*k_fus_tot*fuselage.geom.FF;

% Landing gear group (coefficients from Torenbeek table 8.6)
switch a.category
    case 'JetTrainer'
        A_NLG = 5.4;    B_NLG = 0.049;  C_NLG = 0;      D_NLG = 0;
        A_MLG = 15;     B_MLG = 0.033;  C_MLG = 0.021;  D_MLG = 0;
    case 'BusinessJet'
        A_NLG = 5.4;    B_NLG = 0.049;  C_NLG = 0;      D_NLG = 0;
        A_MLG = 15;     B_MLG = 0.033;  C_MLG = 0.021;  D_MLG = 0;
    otherwise
        if MLG.geom.isRetractable == 1
            if MLG.geom.isTailDragger == 1
                A_NLG = 2.3;    B_NLG = 0;      C_NLG = 0.0031; D_NLG = 0;
            else
                A_NLG = 9.1;    B_NLG = 0.082;  C_NLG = 0;      D_NLG = 2.97e-6;
            end
            A_MLG = 18.1;   B_MLG = 0.131;  C_MLG = 0.019;      D_MLG = 2.23e-5;
        else
            if MLG.geom.isTailDragger == 1
                A_NLG = 4.1;    B_NLG = 0;      C_NLG = 0.0024; D_NLG = 0;
            else
                A_NLG = 11.3;   B_NLG = 0;      C_NLG = 0.0024; D_NLG = 0;
            end
            A_MLG = 9.1;    B_MLG = 0.082;  C_MLG = 0.019;      D_MLG = 0;
        end
end
zWing = 0.5 + wing.geom.z_over_Dfus; % = 0 for low wing, = 1 for high wing;
k_LG = 1 + 0.08*max([0 min([1 zWing])]);
M.airframeStruct.NLG = NLG.geom.FF*...
    (A_NLG + B_NLG*TOM0^0.75 + C_NLG*TOM0 + D_NLG*TOM0^1.5);
M.airframeStruct.MLG = MLG.geom.FF*k_LG*...
    (A_MLG + B_MLG*TOM0^0.75 + C_MLG*TOM0 + D_MLG*TOM0^1.5);

% Control surface group (Note: actual TOM based, not ref TOM)
switch a.category
    case 'LightSingleEngine'
        k_sc = 0.177;
    case 'LightMultiEngine'
        k_sc = 0.177;
    otherwise
        if surfaceControls.geom.hasPoweredControls == 1
            k_sc = 0.492;
        else
            k_sc = 0.338;
        end
end
if surfaceControls.geom.hasLEDevices == 1
    k_LEHLD = 0.2;
else
    k_LEHLD = 0;
end
if surfaceControls.geom.hasSpoilers == 1
    k_spoilers = 0.15;
else
    k_spoilers = 0;
end
k_tot_surfaceControls = 1 + k_LEHLD + k_spoilers;
M.airframeStruct.surfaceControls = surfaceControls.geom.FF*...
    k_tot_surfaceControls*k_sc*...
    TOM0^(2/3);

% Engine section/nacelle group (N.B. using "turboprop" correlation also
% for turbofans due to GT power-based calcuations. Probably optimistic!
if propulsionGroup1.geom.hasNacelles == 1
    P_per_engine = aircraft.Pdes.GTM/p.N1/745.7;    % [hp]
    switch a.category
        case 'LightSingleEngine'
            M.airframeStruct.nacelles1 = propulsionGroup1.geom.FF_nac*p.N1*1.134*P_per_engine^0.5;
        case 'LightMultiEngine'
            if propulsionGroup1.geom.hasHorOppCyls == 1
                M.airframeStruct.nacelles1 = propulsionGroup1.geom.FF_nac*p.N1*0.145*P_per_engine;
            else
                M.airframeStruct.nacelles1 = propulsionGroup1.geom.FF_nac*p.N1*0.0204*P_per_engine^1.25;
            end
        otherwise
            if propulsionGroup1.geom.hasMLGInNacelle
                k_MLG_nac = 0.018/0.0635;
            else
                k_MLG_nac = 0;
            end
            if propulsionGroup1.geom.hasThrustReverser == 0 && ~strcmp(a.category,'TransportTurboprop') 
                k_thrustRev = -0.1;
            else
                k_thrustRev = 0;
            end
            k_nac = 1 + k_MLG_nac + k_thrustRev;
            M.airframeStruct.nacelles1 = propulsionGroup1.geom.FF_nac*p.N1*0.0635*k_nac*P_per_engine;
    end
else
    M.airframeStruct.nacelles1 = 0;
end
if propulsionGroup2.geom.hasNacelles == 1           % Apply TP nacelle relation to EMs
    P_per_engine = aircraft.Pdes.EM2M/p.N2/745.7;   % [hp]
    M.airframeStruct.nacelles2 = propulsionGroup2.geom.FF_nac*...
        p.N2*0.0635*P_per_engine; % No MLG/thrust reverser correction
else
    M.airframeStruct.nacelles2 = 0;
end

% Total airframe structure mass
fNames = fieldnames(M.airframeStruct);
M.airframeStructMass = 0;
for i = 1:length(fNames)
    M.airframeStructMass = M.airframeStructMass + M.airframeStruct.(fNames{i});
end

