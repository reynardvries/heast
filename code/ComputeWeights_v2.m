function [M,MA,DOHmiss,DOHtot] = ComputeWeights_v2(a,m,p,f,s,c,aircraft,MA,C2,M,iterC2)
%%% Description
% This function is based on the original ComputeWeights.m but has been
% modified to allow a selection between Class "1.5" and Class-II analysis.
% The same battery and fuel-weight estimation procedures are used, but the
% remaining components are now computed in dedicated subroutines.
% Note: TOM is updated in a loop outside this function!
%
% Input:
%   - a,m,p,f,s,c: structures containing aircraft and program data (see
%       main input file)
%   - aircraft: structure containing:
%       - Pdes: installed power of the powerplant components on the
%           aircraft, as obtained from WPdes in the WP-WS diagram [W]
%       - TOM: initial guess of the take-off mass of the aircraft [kg]
%       - Sw: wing area [m2]
%       - Sw_ref, Pdes_ref: characteristics of reference aircraft
%       - PL: payload mass [kg]    
%   - MA: structure generated in MissionAnalysis.m
%   - C2: Class-II input settings from Input_ClassII.m
%   - M: structure containing masses estimated in previous iteration (only
%       used for Class-II analysis in iterations beyond the first; in other
%       cases insert empty variable M = []).
%   - iterLoop: iteration number of outer mission-analysis loop. For
%       Class-II analysis, in the first iteration, the geometrical size of
%       some components is unknown, so we give a first estimation of
%       component weights using percentages of a typical aircraft. These
%       initial values are then used in ConfigurationAndGeometry.m to
%       estimate the CG location and tail size.
%
% Output:
%   - M: updated structure containing masses in [kg] of:
%       - for CLASS-I ANALYSIS:
%           - f: fuel mass
%           - bat: installed battery mass
%           - bat_miss: battery mass required for nominal mission
%           - bat_E: battery mass required for total mission energy(incl. div.)
%           - bat_P: battery mass required to meet power requirements
%           - EM1: primary electrical machines (total) mass
%           - EM2: secondary electrical machines (total) mass
%           - GT: gas turbines (total) mass
%           - w: wing mass
%           - OEM: operative empty mass EXCLUDING wing and powertrain
%           - TOM: total take-off mass
%           - PL: payload (same as input)
%       - for CLASS-II ANALYSIS: see WeightEstimationClass2_v.m
%   - MA: update MA structure, containing the corrected mission energy
%       profiles.
%   - DOHmiss: degree-of-hybridization of nominal mission, i.e. ratio
%       between battery energy spent during nominal mission and the total
%       energy spent during the nominal mission [-]
%   - DOHtot: degree-of-hybridization of complete mission, i.e. ratio
%       between battery energy spent during complete mission and the total
%       energy spent during the complete (= incl. diversion) mission [-]
%
%
%%% Reynard de Vries
%%% TU Delft
%%% Date created: 04-04-18
%%% Last modified: 14-04-22


%% Compute battery weight
            
% Shift battery energy consumption line to get a
% continuous curve along the missions
MA.Dde.Ebat = MA.Dde.Ebat + (MA.Loiter.Ebat(1) - MA.Dde.Ebat(end));
MA.Dcr.Ebat = MA.Dcr.Ebat + (MA.Dde.Ebat(1) - MA.Dcr.Ebat(end));
MA.Dcl.Ebat = MA.Dcl.Ebat + (MA.Dcr.Ebat(1) - MA.Dcl.Ebat(end));
MA.de.Ebat = MA.de.Ebat + (MA.Dcl.Ebat(1) - MA.de.Ebat(end));
MA.cr.Ebat = MA.cr.Ebat + (MA.de.Ebat(1) - MA.cr.Ebat(end));
MA.cl.Ebat = MA.cl.Ebat + (MA.cr.Ebat(1) - MA.cl.Ebat(end));

% Append arrays
EbatAll = [MA.cl.Ebat MA.cr.Ebat MA.de.Ebat ...
           MA.Dcl.Ebat MA.Dcr.Ebat MA.Dde.Ebat MA.Loiter.Ebat];

% Add start-up and shutdown            
EbatAll = [(EbatAll(1) + MA.EF.E_bat_TakeOff) EbatAll];
EbatAll = [(EbatAll(1) + MA.EF.E_bat_TaxiOut) EbatAll];
EbatAll = [EbatAll (EbatAll(end) - MA.EF.E_bat_Landing)];
EbatAll = [EbatAll (EbatAll(end) - MA.EF.E_bat_TaxiIn)];

% Compute difference between maximum energy level and minimum energy level
% along mission, which should be equal to (1-SOCmargin)*100% of the total
% battery capacity
DeltaEBat = max(EbatAll) - min(EbatAll);

% Calculate what the minimum energy at any point along the mission would be
% if the minimum value is such that the minSOC is not exceeded 
EMin = p.minSOC/(1-p.minSOC)*DeltaEBat;

% Compute battery mass required to provide maximum energy along mission
M_bat_E = (DeltaEBat + EMin)/p.SE.bat;

% Compute battery mass required to provide maximum power
M_bat_P = aircraft.Pdes.bat/p.SP.bat;

% Select maximum
[M_bat,idxMax] = max([M_bat_E M_bat_P]);

% Compute energy consumed during nominal mission, for output
EbatMiss = [MA.cl.Ebat MA.cr.Ebat MA.de.Ebat];
DeltaEbatMiss = max(EbatMiss) - min(EbatMiss);
DeltaEbatMiss = DeltaEbatMiss + MA.EF.E_bat_TakeOff + MA.EF.E_bat_TaxiOut +...
                                MA.EF.E_bat_Landing + MA.EF.E_bat_TaxiIn;

% Offset all mission segments such that EMin is not exceeded, and that
% there is energy left for landing/taxiing in (for function output). If
% power-limited, set to power-limited values
names = fieldnames(MA);
names(contains(names,'EF')) = [];
Ncon = size(names,1);
E_bat_tot = M_bat*p.SE.bat;
if idxMax == 1 % Energy-limited
    for i = 1:Ncon
        MA.(names{i}).Ebat = MA.(names{i}).Ebat - min(EbatAll) + EMin;
    end
elseif idxMax == 2 % Power-limited
    for i = 1:Ncon
        MA.(names{i}).Ebat = MA.(names{i}).Ebat - max(EbatAll) + E_bat_tot;
    end
end
                            

%% Estimate fuel mass

% Check if powertrain is fully electric
if strcmp(p.config,'e-1') || strcmp(p.config,'e-2') ...
                          || strcmp(p.config,'dual-e')
    electric = 1;
else
    electric = 0;
end

% Fuel mass [kg]
if electric == 1
    M_f_miss = 0;
    M_f_tot = 0;
    Ef_miss = 0;
    Ef_tot = 0;
else
    
    % Shift fuel energy consumption line during descent to get a
    % continuous curve along the missions
    MA.Dde.Ef = MA.Dde.Ef + (MA.Loiter.Ef(1) - MA.Dde.Ef(end));
    MA.Dcr.Ef = MA.Dcr.Ef + (MA.Dde.Ef(1) - MA.Dcr.Ef(end));
    MA.Dcl.Ef = MA.Dcl.Ef + (MA.Dcr.Ef(1) - MA.Dcl.Ef(end));
    MA.de.Ef = MA.de.Ef + (MA.Dcl.Ef(1) - MA.de.Ef(end));
    MA.cr.Ef = MA.cr.Ef + (MA.de.Ef(1) - MA.cr.Ef(end));
    MA.cl.Ef = MA.cl.Ef + (MA.cr.Ef(1) - MA.cl.Ef(end));
    
    % Separate mission fuel from total fuel, for PREE computation. Mission
    % fuel excludes "taxi in" fuel fraction, where the e.g. 5% contingency
    % fuel is typically included. 
    Ef_miss = MA.cl.Ef(1)-MA.de.Ef(end) + MA.EF.E_f_TaxiOut ...
                    + MA.EF.E_f_TakeOff + MA.EF.E_f_Landing;
    Ef_tot = Ef_miss + (MA.Dcl.Ef(1) - MA.Loiter.Ef(end)) + MA.EF.E_f_TaxiIn;
    M_f_tot = Ef_tot/p.SE.f;
    M_f_miss = Ef_miss/p.SE.f;
end

% Old
%{
% Estimate DOH of nominal mission and total mission (to save in output MA
% structure). Note: DOH based on battery energy USED, not on the energy on
% board (which may be higher if the batteries are sized by power + SOC 
% limit). Includes energy for taxi/TO/landing
DOHmiss = DeltaEbatMiss/(DeltaEbatMiss + Ef_miss);
DOHtot = (max(EbatTot)-min(EbatTot))/...
         (max(EbatTot)-min(EbatTot) + Ef_tot);
%}

% Estimate DOH of nominal mission, based on USED energy, and DOH based on 
% total installed energy.
DOHmiss = DeltaEbatMiss/(DeltaEbatMiss + Ef_miss);
DOHtot = E_bat_tot/(E_bat_tot + Ef_tot);

% Add fuel/battery/payload weight to output structure
M.f = M_f_tot;
M.f_miss = M_f_miss;
M.bat = M_bat;
M.bat_miss = DeltaEbatMiss/p.SE.bat;
M.bat_E = M_bat_E;
M.bat_P = M_bat_P;
M.PL = aircraft.PL;


%% Estimate component weights

% For input file
s.WeightEstimationClass = 2;

% Select type of weight estimation
% Class 1.5 (NB: OEM *EXCLUDES* WING AND POWERTRAIN)
if s.WeightEstimationClass == 1
    M = WeightEstimationClass1(a,m,p,f,s,c,aircraft,M);
   
% Class 2, first iteration (NB: OEM *INCLUDES* WING AND POWERTRAIN)
elseif s.WeightEstimationClass == 2 && iterC2 == 1
    
    % Get first estimate of TOM using Class-I sizing
    M = WeightEstimationClass1(a,m,p,f,s,c,aircraft,M);
    M.OEM = M.OEM + M.EM1 + M.EM2 + M.GT + M.w; 

    % Initial guesses for component weights, as a fraction of OEM 
    % [CHANGE INITIAL GUESSES HERE IF DESIRED]
    w0.airframeStruct.wing = 0.163;
    w0.airframeStruct.HT = 0.004; % Too low
    w0.airframeStruct.VT = 0.003; % Too low
    w0.airframeStruct.fuselage = 0.124;
    w0.airframeStruct.NLG = 0.014;
    w0.airframeStruct.MLG = 0.078;
    w0.airframeStruct.surfaceControls = 0.012;
    w0.airframeStruct.nacelles1 = 0.000;
    w0.airframeStruct.nacelles2 = 0.015;
    w0.propGroup.GT = 0.044;
    w0.propGroup.gearbox = 0.000;
    w0.propGroup.accessories = 0.0085;
    w0.propGroup.oilSystem = 0.003;
    w0.propGroup.fuelSystem = 0.004;
    w0.propGroup.propellers1 = 0.000;
    w0.propGroup.propellers2 = 0.049;
    w0.propGroup.EM1 = 0.019;
    w0.propGroup.EM2 = 0.124;
    w0.propGroup.PMAD = 0.040;
    w0.propGroup.coolingEM1 = 0.013;
    w0.propGroup.coolingEM2 = 0.029;
    w0.propGroup.coolingPMAD = 0.0362;
    w0.propGroup.coolingBatteries = 0.007;
    w0.servAndEquip.APU = 0.024;
    w0.servAndEquip.InstrAndAvionics = 0.061;
    w0.servAndEquip.hydrPneuSystems = 0.010;    % NB different name for non-transport AC categories
    w0.servAndEquip.elecSystem = 0.020;         % NB different name for non-transport AC categories
    w0.servAndEquip.furnishing = 0.045;
    w0.servAndEquip.AircoAntiIce = 0.018;
    w0.servAndEquip.misc = 0.004;
    w0.operItems.CrewProv = 0.010;
    w0.operItems.CabinSupplies = 0.007;
    w0.operItems.WaterAndChem = 0.002;
    w0.operItems.safeEquip = 0.009;
    w0.operItems.resFuel = 0.001;
    
    % Compute cumulative sum weight to make sure we end up with exactly
    % 100% OEM later
    fNames1 = fieldnames(w0);
    cumulativeWeight = 0;
    for i = 1:length(fNames1)
        fNames2 = fieldnames(w0.(fNames1{i}));
        for j = 1:length(fNames2)
            cumulativeWeight = cumulativeWeight + w0.(fNames1{i}).(fNames2{j});
        end
    end
    
    % Compute remaining components as a percentage of initial guess of OEM
    for i = 1:length(fNames1)
        fNames2 = fieldnames(w0.(fNames1{i}));
        compSum = 0;
        for j = 1:length(fNames2)
            M.(fNames1{i}).(fNames2{j}) = w0.(fNames1{i}).(fNames2{j})*M.OEM/cumulativeWeight;
            compSum = compSum + M.(fNames1{i}).(fNames2{j});
        end
        M.([fNames1{i} 'Mass']) = compSum;
    end
    
% Class 2, remaining iterations
else
    M = WeightEstimationClass2_v2(a,m,p,f,s,c,aircraft,M,C2);
end





