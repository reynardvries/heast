%%% Description
% This script assings the aerodynamic properties per constraint/mission
% segment. This used to be done in CheckInput.m, but now has to be updated
% inside the Class-II loop in case the aerodynamic properties are updated.
% 
%%% Reynard de Vries
%%% Date created: 10-04-22
%%% Last modified: 10-04-22

% Cruise
if isfield(m,'cr')
    a.cr.CD0 = a.CD0_clean;
    a.cr.e = a.e_clean;
    a.cr.CLmax = a.CLmax_clean;
    a.cr.CL_minD = a.CL_minD_clean;
end

% Landing (approach speed)
if isfield(m,'L')
    a.L.CD0 = a.CD0_clean + a.dCD0_L + a.dCD0_LG;
    a.L.e = a.e_clean + a.de_L;
    a.L.CLmax = a.CLmax_clean + a.dCLmax_L;
    a.L.CL_minD = a.CL_minD_clean + a.dCL_minD_L;
end

% Landing distance
if isfield(m,'sL')
    a.sL.CD0 = a.CD0_clean + a.dCD0_L + a.dCD0_LG;
    a.sL.e = a.e_clean + a.de_L;
    a.sL.CLmax = a.CLmax_clean + a.dCLmax_L;
    a.sL.CL_minD = a.CL_minD_clean + a.dCL_minD_L;
end

% Take off
if isfield(m,'TO')
    a.TO.CD0 = a.CD0_clean + a.dCD0_TO + a.dCD0_LG;
    a.TO.e = a.e_clean + a.de_TO;
    a.TO.CLmax = a.CLmax_clean + a.dCLmax_TO;
    a.TO.CL_minD = a.CL_minD_clean + a.dCL_minD_TO;
end

% OEI Balked landing (CS25)
if isfield(m,'bL')
    a.bL.CD0 = a.CD0_clean + a.dCD0_L;
    a.bL.e = a.e_clean + a.de_L;
    a.bL.CLmax = a.CLmax_clean + a.dCLmax_L;
    a.bL.CL_minD = a.CL_minD_clean + a.dCL_minD_L;
end

% OEI Ceiling
if isfield(m,'cI')
    a.cI.CD0 = a.CD0_clean;
    a.cI.e = a.e_clean;
    a.cI.CLmax = a.CLmax_clean;
    a.cI.CL_minD = a.CL_minD_clean;
end

% OEI second segment climb (CS25)
if isfield(m,'ss')
    a.ss.CD0 = a.CD0_clean + a.dCD0_TO;
    a.ss.e = a.e_clean + a.de_TO;
    a.ss.CLmax = a.CLmax_clean + a.dCLmax_TO;
    a.ss.CL_minD = a.CL_minD_clean + a.dCL_minD_TO;
end

% AEO Balked landing (CS23)
if isfield(m,'bLAEO')
    a.bLAEO.CD0 = a.CD0_clean + a.dCD0_L + a.dCD0_LG;
    a.bLAEO.e = a.e_clean + a.de_L;
    a.bLAEO.CLmax = a.CLmax_clean + a.dCLmax_L;
    a.bLAEO.CL_minD = a.CL_minD_clean + a.dCL_minD_L;
end

% AEO climb gradient at take-off
if isfield(m,'ToClAEO')
    a.ToClAEO.CD0 = a.CD0_clean + a.dCD0_TO + a.dCD0_LG;
    a.ToClAEO.e = a.e_clean + a.de_TO;
    a.ToClAEO.CLmax = a.CLmax_clean + a.dCLmax_TO;
    a.ToClAEO.CL_minD = a.CL_minD_clean + a.dCL_minD_TO;
end

% OEI climb gradient at take-off, LG extended
if isfield(m,'ToClEx')
    a.ToClEx.CD0 = a.CD0_clean + a.dCD0_TO + a.dCD0_LG;
    a.ToClEx.e = a.e_clean + a.de_TO;
    a.ToClEx.CLmax = a.CLmax_clean + a.dCLmax_TO;
    a.ToClEx.CL_minD = a.CL_minD_clean + a.dCL_minD_TO;
end

% OEI climb gradient at take-off, LG retracted
if isfield(m,'ToClRe')
    a.ToClRe.CD0 = a.CD0_clean + a.dCD0_TO;
    a.ToClRe.e = a.e_clean + a.de_TO;
    a.ToClRe.CLmax = a.CLmax_clean + a.dCLmax_TO;
    a.ToClRe.CL_minD = a.CL_minD_clean + a.dCL_minD_TO;
end

% En-route OEI climb gradient
if isfield(m,'erOEI')
    a.erOEI.CD0 = a.CD0_clean;
    a.erOEI.e = a.e_clean;
    a.erOEI.CLmax = a.CLmax_clean;
    a.erOEI.CL_minD = a.CL_minD_clean;
end

% Discontinued approach
if isfield(m,'da')
    a.da.CD0 = a.CD0_clean + a.dCD0_L;
    a.da.e = a.e_clean + a.de_L;
    a.da.CLmax = a.CLmax_clean + a.dCLmax_L;
    a.da.CL_minD = a.CL_minD_clean + a.dCL_minD_L;
end

% AEO Ceiling
if isfield(m,'ce')
    a.ce.CD0 = a.CD0_clean;
    a.ce.e = a.e_clean;
    a.ce.CLmax = a.CLmax_clean;
    a.ce.CL_minD = a.CL_minD_clean;
end

% Specified performance: ROC @ SL, AEO (assuming flaps retracted)
if isfield(m,'rocAEO')
    a.rocAEO.CD0 = a.CD0_clean;
    a.rocAEO.e = a.e_clean;
    a.rocAEO.CLmax = a.CLmax_clean;
    a.rocAEO.CL_minD = a.CL_minD_clean;
end

% Specified performance: ROC @ SL, OEI (assuming flaps retracted)
if isfield(m,'rocOEI')
    a.rocOEI.CD0 = a.CD0_clean;
    a.rocOEI.e = a.e_clean;
    a.rocOEI.CLmax = a.CLmax_clean;
    a.rocOEI.CL_minD = a.CL_minD_clean;
end

% Diversion cruise
if isfield(m,'Dcr')
    a.Dcr.CD0 = a.CD0_clean;
    a.Dcr.e = a.e_clean;
    a.Dcr.CLmax = a.CLmax_clean;
    a.Dcr.CL_minD = a.CL_minD_clean;
end
