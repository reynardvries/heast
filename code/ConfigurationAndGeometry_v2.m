function aircraft = ConfigurationAndGeometry_v2(p,C2,aircraft)
%%% Description
% This function computes the geometry of the aircraft and creates a
% simplified 3D model for visual/geometrical verification that everything
% is correct. This function must be called after a Class-1/1.5 analysis,
% and is used for the Class-II aero analysis. The various components are
% gatherd in a structure tree. Currently, the following components are
% included:
%
% LEVEL 0   LEVEL 1             LEVEL 2             LEVEL 3
% _________________________________________________________________________
% aircraft  fuselageGroup [#]   fuselage [#]
%                               payload [#]
%                               fuel**
%                               propulsionGroup1*   propellers***
%                                                   motors***
%                                                   nacelles*** 
%                               servAndEquip
%                               OperItems
%                               PMAD
%                               NLG
%                               MLG**
%           _______________________________________________________________
%           WingGroup [#]       wing [#]
%                               battery**
%                               propulsionGroup2*   propellers***
%                                                   motors***
%                                                   nacelles***
%                               MLG**
%           _______________________________________________________________
%           tailGroup [#]       horizontalTail 
%                               verticalTail
% _________________________________________________________________________
% *These components can each be placed in the wing or fuselage group,
%   depending on the type of propulsion system selected
% ** These components can be placed in the wing or fuselage groups, or both
% *** These components contain multiple instances (cell array)
% [#] These components cannot be omitted
%
% To-do list:
% - For battery & fuel components: allows "option 3" where fuel and/or
%   battery are split between fuselage and wing. Create cell arrays for
%   individual fuel tanks/battery packs, similar to propulsionGroup
%   approach.
% - Neglect components if mass = 0
% - Propulsion groups: allows modelling without propeller ("hasProp"), so
%   that e.g. a serial configuration can have a GT placed in the back of
%   the fuselage.
% - Add nose- and main-landing-gear groups. Manual placement, with the
%   option to show constraint planes in the 3D figure.
% - Improve fuselage cross-section calculation, taking into account floor &
%   shoulder height.
%
%%% Reynard de Vries
%%% First created: 18-03-22
%%% Last modified: 25-04-22


%% Check input

% Extract fields from "C2" structure (for shorter writing only...)
fNames = fieldnames(C2);
for i = 1:length(fNames)
    eval([fNames{i} ' = C2.(fNames{i});'])
end

% Verify whether propulsionGroup1 has been properly specified
% Powertrains that have no primary propulsion system
if strcmp(p.config,'turboelectric') || strcmp(p.config,'serial') || ...
   strcmp(p.config,'e-2') 
    if all([~strcmp(propulsionGroup1.geom.type,'none'),~strcmp(propulsionGroup1.geom.type,'fus')])
        error(['The ' p.config ' architecture cannot have a primary propulsion ' ...
               'group. Check propulsionGroup1.geom.type of the class-II input.'])
    end    

% Powertrains that have a primary propulsion system    
else
    if strcmp(propulsionGroup1.geom.type,'none') 
        error(['The ' p.config ' architecture requires a primary propulsion ' ...
               'group. Check propulsionGroup1.geom.type of the class-II input.'])
    end 
end

% Verify whether propulsionGroup2 has been properly specified
% Powertrains that have no secondary propulsion system
if strcmp(p.config,'conventional') || strcmp(p.config,'parallel') || ...
   strcmp(p.config,'e-1') 
    if ~strcmp(propulsionGroup2.geom.type,'none')
        error(['The ' p.config ' architecture cannot have a secondary propulsion ' ...
               'group. Check propulsionGroup2.geom.type of the class-II input.'])
    end    

% Powertrains that have a secondary propulsion system    
else
    if strcmp(propulsionGroup2.geom.type,'none')
        error(['The ' p.config ' architecture requires a secondary propulsion ' ...
               'group. Check propulsionGroup2.geom.type of the class-II input.'])
    end 
end

                                
%% Fuselage group

% Calculate fuselage dimensions, overwrite fuselage.geom field
[fuselage.geom,fuselage.plotting.P] = CreateFuselageGeometry(...
                                fuselage.geom,fuselage.settings);

% Add components to fuselage-group tree: fuselage structure
fuselageGroup.comps.fuselage = fuselage;

% Add components to fuselage-group tree: payload
fuselageGroup.comps.payload = payload;

% Add components to fuselage-group tree: propulsion group(s). Currently
% only compatible with 'fus', where all components of the primary/secondary
% system are grouped into one CG box
if strcmp(propulsionGroup1.geom.type,'fus')
    [propulsionGroup1] = CreatePropulsionGroup(propulsionGroup1,[],fuselage);
    fuselageGroup.comps.propulsionGroup1 = propulsionGroup1;
end
if strcmp(propulsionGroup2.geom.type,'fus')
    [propulsionGroup2] = CreatePropulsionGroup(propulsionGroup2,[],fuselage);
    fuselageGroup.comps.propulsionGroup2 = propulsionGroup2;   
end

% Add components to fuselage-group tree: fuel
% [Update later on with more detailed fuel split model]
if fuelSystem.geom.hasFuel == 2
    fuselageGroup.comps.fuelSystem = fuelSystem; 
end

% Add components to fuselage-group tree: battery
% [Update later on with more detailed bat split model]
if battery.geom.hasBattery == 2
    fuselageGroup.comps.battery = battery; 
end

% Add components to fuselage-group tree: landing gear
fuselageGroup.comps.NLG = NLG;
if MLG.geom.wingMounted == 0
    fuselageGroup.comps.MLG = MLG;
end

% Add components to fuselage-group tree: services, equipment, operational
% items, and PMAD
fuselageGroup.comps.servAndEquip = servAndEquip;
fuselageGroup.comps.operItems = operItems;
fuselageGroup.comps.PMAD = PMAD;

% Preallocate arrays to store component CG info to compute overall group CG
FGnames = fieldnames(fuselageGroup.comps);
masses = NaN(1,length(FGnames));
coords = NaN(3,length(FGnames));

% Calculate local CG locations in local reference frame (note: propulsion
% group mass/CG's computed based on level-3 components in tail convergence
% loop below)
for i = 1:length(FGnames)
    if ~strcmp(FGnames{i},'propulsionGroup1') && ~strcmp(FGnames{i},'propulsionGroup2')
        fuselageGroup.comps.(FGnames{i}).weights.coords_loc(1,1) = ...
            fuselageGroup.comps.(FGnames{i}).weights.x_over_l*...
            fuselageGroup.comps.fuselage.geom.l_fus;
        fuselageGroup.comps.(FGnames{i}).weights.coords_loc(2,1) = ...
            fuselageGroup.comps.(FGnames{i}).weights.y_over_D*...
            fuselageGroup.comps.fuselage.geom.D_fus;
        fuselageGroup.comps.(FGnames{i}).weights.coords_loc(3,1) = ...
            fuselageGroup.comps.(FGnames{i}).weights.z_over_D*...
            fuselageGroup.comps.fuselage.geom.D_fus;
    end
    masses(1,i) = fuselageGroup.comps.(FGnames{i}).weights.m;
    coords(:,i) = fuselageGroup.comps.(FGnames{i}).weights.coords_loc;
end

% Compute group CG & mass
fuselageGroup.weights.m = sum(masses);
fuselageGroup.weights.coords_loc = sum(masses.*coords,2)/sum(masses);


%% Wing group

% Create wing geometry
[wing.geom,wing.plotting.P] = CreateLiftingSurfaceGeometryMirrored(wing.geom,wing.settings);

% Add components to wing-group tree: wing structure
wingGroup.comps.wing = wing;

% Add components to wing-group tree: propulsion group(s)
if strcmp(propulsionGroup1.geom.type,'LEDP')
    [propulsionGroup1] = CreatePropulsionGroup(propulsionGroup1,wing,fuselage);
    wingGroup.comps.propulsionGroup1 = propulsionGroup1;
end
if strcmp(propulsionGroup2.geom.type,'LEDP')
    [propulsionGroup2] = CreatePropulsionGroup(propulsionGroup2,wing,fuselage);
    wingGroup.comps.propulsionGroup2 = propulsionGroup2;
end

% Add components to wing-group tree: fuel
% UPDATE later on with more detailed fuel split model
if fuelSystem.geom.hasFuel == 1
    wingGroup.comps.fuelSystem = fuelSystem; 
end

% Add components to wing-group tree: battery
% UPDATE later on with more detailed bat split model
if battery.geom.hasBattery == 1
    wingGroup.comps.battery = battery; 
end

% Add components to wing-group tree: main landing gear
if MLG.geom.wingMounted == 1
    [MLG] = CreateMLGNacelle(MLG,wing);
    wingGroup.comps.MLG = MLG;
end

% Preallocate arrays to store component CG info to compute overall group CG
WGnames = fieldnames(wingGroup.comps);
masses = NaN(1,length(WGnames));
coords = NaN(3,length(WGnames));

% Calculate local CG locations in local reference frame. Propulsion group
% CG is already calculated in CreatePropulsionGroup.m. Add additional cases
% for new components here if necessary, depending on what they use as
% reference lengths
for i = 1:length(WGnames)
    if strcmp(WGnames{i},'wing') || strcmp(WGnames{i},'battery') ||...
       strcmp(WGnames{i},'fuelSystem') || strcmp(WGnames{i},'MLG')
        wingGroup.comps.(WGnames{i}).weights.coords_loc(1,1) = ...
            wingGroup.comps.(WGnames{i}).weights.x_over_c*...
            wingGroup.comps.wing.geom.root.c;
        wingGroup.comps.(WGnames{i}).weights.coords_loc(2,1) = ...
            wingGroup.comps.(WGnames{i}).weights.y_over_b*...
            wingGroup.comps.wing.geom.b;
        wingGroup.comps.(WGnames{i}).weights.coords_loc(3,1) = ...
            wingGroup.comps.(WGnames{i}).weights.z_over_c*...
            wingGroup.comps.wing.geom.root.c;
    end
    masses(1,i) = wingGroup.comps.(WGnames{i}).weights.m;
    coords(:,i) = wingGroup.comps.(WGnames{i}).weights.coords_loc;
end

% Compute group CG & mass
wingGroup.weights.m = sum(masses);
wingGroup.weights.coords_loc = sum(masses.*coords,2)/sum(masses);


%% CG analysis: tail sizing & wing positioning

% Determine initial tail size based on assumed areas (this function
% performs similar steps as previous sections, but has been grouped in a
% separate function because it's called multiple times in the loop below
if strcmp(tail.config,'conventional') || strcmp(tail.config,'V-tail')
    [~,tail] = CreateTailGroup(tail);
else
    error('Other tail configurations have not been implemented yet')
end

% Gather structures in overall aircraft structure tree [ADD HERE]. Tail
% group is added/updated inside the loop
aircraft.comps.fuselageGroup = fuselageGroup;
aircraft.comps.wingGroup = wingGroup;

% Initial guess for aircraft CG location
coords0 = [settings.xCG_over_lFus_0*fuselage.geom.l_fus, 0 0]';

% Origin of local reference systems in global reference system [m]
aircraft.comps.fuselageGroup.RefSystem = [0 0 0]';

% Iterate to converge on tail size and CG location
err = 1;
iter = 0;
while err > settings.errmax
    
    %%% 1. Position main wing
    
    % Axial wing position (Currently neglecting tail contribution)
    % [To be improved with tail contribution & improved CG estimation
    % through the loading (potato) diagram]
    xLEMAC = coords0(1) - (tail.AC_wing - tail.SM)*wing.geom.MAC.c + 0;
    xLEroot = xLEMAC - wing.geom.MAC.x_LE;
    
    % Position origin of wing in global ref system
    aircraft.comps.wingGroup.RefSystem = [xLEroot, 0, wing.geom.z_over_Dfus*fuselage.geom.D_fus]';    
    
    %%% 2. Compute tail size
    
    % Distinguish between tail configs (NB: when adding additional configs,
    % we can probably re-use part of this script outside the if-statement)
    if strcmp(tail.config,'conventional') || strcmp(tail.config,'V-tail')
        
        % Calculate tail position in global reference frame (root of HT is
        % origin of tail group system)
        TailRefSystem = [tail.xHT_over_lFus*fuselage.geom.l_fus;
                         0;
                         tail.zHT_over_DFus*fuselage.geom.D_fus];
                     
        % Select tail sizing method
        if strcmp(tail.SizingMethod,'volume')
            
            % Distance between wing AC and tail AC (assumed at 25% MAC for tail;
            % for wing it's specified as input, which is normally also 25%)
            l_HT = (tail.HT.geom.MAC.x_c025 + TailRefSystem(1)) - ...
                (wing.geom.MAC.x_LE + tail.AC_wing*wing.geom.MAC.c_projected + ...
                aircraft.comps.wingGroup.RefSystem(1));

            % For VT, in the case of a V-tail, use same location as HT
            if strcmp(tail.config,'conventional')  
                l_VT = (tail.VT.geom.MAC.x_c025 + TailRefSystem(1)) - ...
                    (wing.geom.MAC.x_LE + tail.AC_wing*wing.geom.MAC.c_projected + ...
                    aircraft.comps.wingGroup.RefSystem(1));
            elseif strcmp(tail.config,'V-tail')
                l_VT = (tail.HT.geom.MAC.x_c025 + TailRefSystem(1)) - ...
                    (wing.geom.MAC.x_LE + tail.AC_wing*wing.geom.MAC.c_projected + ...
                    aircraft.comps.wingGroup.RefSystem(1));
            end

%             % Tail surface areas Method 1: wing-based (as per handbooks)
%             % (using MAC chord and not projected chord)
%             tail.HT.geom.S = tail.HTSizeFactor*tail.HTVolCoeff*wing.geom.MAC.c*wing.geom.S/l_HT;
%             tail.VT.geom.S = tail.VTSizeFactor*tail.VTVolCoeff*wing.geom.b*wing.geom.S/l_VT;
            
            % Tail surface areas Method 2: using wing span and
            % fuselage (side) reference area
            tail.HT.geom.S = tail.HTSizeFactor*tail.HTVolCoeff*wing.geom.MAC.c*wing.geom.S/l_HT;
            tail.VT.geom.S = tail.VTSizeFactor*tail.VTVolCoeff*wing.geom.b*...
                             (fuselage.geom.l_fus*fuselage.geom.D_fus)/l_VT;
                         
        % Other tail sizing methods (x-plot) not implemented yet
        else
            error('Currently only the volume-coefficient method is implemented for tail sizing')
        end
            
        % Update tail size
        [tailGroup,tail] = CreateTailGroup(tail);
        
        % Add to overarching aircraft struct
        aircraft.comps.tailGroup = tailGroup;
        aircraft.comps.tailGroup.RefSystem = TailRefSystem;
        
    else
        error('Other tail configurations have not been implemented yet')
    end

    %%% 3. Compute LG position [TO BE ADDED]

    %%% 4. Compute CG location
    
    % Compute global CG locations of all components
    L1names = fieldnames(aircraft.comps);
    masses = NaN(1,length(L1names));
    coords = NaN(3,length(L1names));
    for i = 1:length(L1names)
        
        % Level-1 components
        aircraft.comps.(L1names{i}).weights.coords = ...
            aircraft.comps.(L1names{i}).weights.coords_loc + ...
            aircraft.comps.(L1names{i}).RefSystem;
        
        % Loop over Level-2 components
        L2names = fieldnames(aircraft.comps.(L1names{i}).comps);
        for j = 1:length(L2names)
            aircraft.comps.(L1names{i}).comps.(L2names{j}).weights.coords = ...
                aircraft.comps.(L1names{i}).comps.(L2names{j}).weights.coords_loc + ...
                aircraft.comps.(L1names{i}).RefSystem;
            
            % Loop over Level-3 components, if applicable
            if isfield(aircraft.comps.(L1names{i}).comps.(L2names{j}),'comps')
                L3names = fieldnames(aircraft.comps.(L1names{i}).comps.(L2names{j}).comps);
                for k = 1:length(L3names) % L3 propulsion components are in cell arrays
                    if iscell(aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}))
                        for m = 1:length(aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}))
                            aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}){m}.weights.coords = ...
                                aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}){m}.weights.coords_loc + ...
                                aircraft.comps.(L1names{i}).RefSystem;
                        end
                    else
                        aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}).weights.coords = ...
                            aircraft.comps.(L1names{i}).comps.(L2names{j}).comps.(L3names{k}).weights.coords_loc + ...
                            aircraft.comps.(L1names{i}).RefSystem;
                    end
                end
            end

        end
        
        % Store L1 masses & CG location in arrays
        masses(1,i) = aircraft.comps.(L1names{i}).weights.m;
        coords(:,i) = aircraft.comps.(L1names{i}).weights.coords;
    end
    
    % Compute total aircraft CG
    coords1 = sum(masses.*coords,2)/sum(masses);
    
    % Check whether aircraft CG location has converged (add tail size to error)
    err = norm((coords1 - coords0))/wing.geom.MAC.c;
    
    % For checking convergence
    %{
    display([iter err])
    aircraft.weights.coords = coords1;
    [fig] = PlotAircraftGeometry(aircraft,fuselage,wing,tail,...
                       propulsionGroup1,propulsionGroup2,settings);
    keyboard
    %}
    
    % Update CG coordinates to next iteration
    coords0 = coords1;
    
    % Add max iter count
    iter = iter + 1;
    if iter > settings.itermax
        error('Tail sizing & CG estimation did not converge')
    end
end

% Set aircraft CG location 
aircraft.weights.m = sum(masses); % Should be equal to TOM
aircraft.weights.coords = coords1;


%% Organize output

% Show in figure 
if settings.plotAircraft == 1
    [fig] = PlotAircraftGeometry(aircraft,tail,...
        propulsionGroup1,propulsionGroup2,MLG,settings);
end






