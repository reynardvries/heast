function [WSdes_ref,WPdes_ref] = ComputeReferencePowerLoading(a,m,p,s,c,f)
%%% Description
%
% This function generates the wing loading - power loading diagram for the
% reference aircraft, for which no aero-propulsive effects or
% hybrid-electric powertrain is considered. This function is essentially a
% wrapper for the WP_WS_diagram script.
%
%%% Reynard de Vries
%%% TU Delft
%%% Date created: 09-06-20
%%% Last modified: 09-06-20

% Change input parameters to values of reference aircraft
a.AR = a.ref.AR;
p.N1 = a.ref.N;
p.N2 = a.ref.N; % To avoid indeterminate TC's in NaN-checks
p.N_conv = a.ref.N;
p.N = a.ref.N; % To avoid indeterminate TC's in NaN-checks
p.DP = 0;
p.config = 'conventional';
p.AeroPropModel = 'none';
p.geom.b_conv = a.ref.b_conv;

% Power-control parameters
fnames = fieldnames(m);
for i = 1:length(fnames)
    m.(fnames{i}).phi = 0;
    m.(fnames{i}).Phi = 0;
    p.(fnames{i}).chi = 0;
    
    % No over-powering for refernce aircraft
    m.(fnames{i}).xi = min([m.(fnames{i}).xi 1]);
end

% Switch off plotting and printing
s.printMessages = 0;
s.plotWPWS = 0;

% Call WP_WS_diagram to compute design wing and power loading
WP_WS_diagram;

% Store output
WSdes_ref = WSdes;
WPdes_ref = WPdes;



