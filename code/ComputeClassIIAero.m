function [a,err,s] = ComputeClassIIAero(a,m,p,f,s,C2,aircraft,MA)
%%% Description
% This function computes the coefficients of the drag polar using handbook
% methods, with the geometrical information computed in the Class-II 
% geometrical sizing. Updated CD0, e, and CL_minD values are substituted 
% into the "a" structure for the next iteration. The relative error between 
% the initial values and update values is also computed for convergence.
% Two options are given to compute CD0: Obert's method, where a single C_f
% is used for all components (and drag per component is proportional to
% wetted area), or Raymer's method, where the C_f, form factor, and
% interference factor are applied for each component. CL_minD is currently
% not computed.
%
% Reynard de Vries
% First version: 10-04-22
% Last modified: 22-04-22


%% Compute CLminD, e, wetted area

% CL_minD [Not updated in current approach]
CL_minD = a.CL_minD_clean;

% Oswald factor, using equation from Obert Fig. 40.36
e = 1/(1.02 + 0.0075*pi*a.AR) + a.e_penalty;
% % e = 1.78*(1-0.045*(a.AR/2)^0.68)-0.64 + a.e_penalty; % Raymer; should this use AR/2?

% % TEMP for winglets
% e = 1/(1.02 + 0.0075*pi*a.AR*1.1) + a.e_penalty;
% e = e*1.1

% Calculate wetted area of main components
S_ref = aircraft.Sw;
S_wet_fus = aircraft.comps.fuselageGroup.comps.fuselage.geom.S_wet;
S_wet_wing = aircraft.comps.wingGroup.comps.wing.geom.S_wet;
S_wet_HT = aircraft.comps.tailGroup.comps.HT.geom.S_wet;
S_wet_VT = aircraft.comps.tailGroup.comps.VT.geom.S_wet;

% Calculate wetted area of MLG nacelle, which may be part of the
% fuselage or wing groups. Is equal to zero if MLg.geom.hasNacelle = 0
if isfield(aircraft.comps.fuselageGroup.comps,'MLG')
	S_wet_nacMLG = aircraft.comps.fuselageGroup.comps.MLG.geom.S_wet;
elseif isfield(aircraft.comps.wingGroup.comps,'MLG')
	S_wet_nacMLG = aircraft.comps.wingGroup.comps.MLG.geom.S_wet;
else
	S_wet_nacMLG = 0;
end

% Calculate wetted area of propulsion groups, which may be part of the
% fuselage or wing groups
if isfield(aircraft.comps.fuselageGroup.comps,'propulsionGroup1')
	S_wet_PG1 = aircraft.comps.fuselageGroup.comps.propulsionGroup1.geom.S_wet;
elseif isfield(aircraft.comps.wingGroup.comps,'propulsionGroup1')
	S_wet_PG1 = aircraft.comps.wingGroup.comps.propulsionGroup1.geom.S_wet;
else
	S_wet_PG1 = 0;
end
if isfield(aircraft.comps.fuselageGroup.comps,'propulsionGroup2')
	S_wet_PG2 = aircraft.comps.fuselageGroup.comps.propulsionGroup2.geom.S_wet;
elseif isfield(aircraft.comps.wingGroup.comps,'propulsionGroup2')
	S_wet_PG2 = aircraft.comps.wingGroup.comps.propulsionGroup2.geom.S_wet;
else
	S_wet_PG2 = 0;
end

% Total wetted area
S_wet = (S_wet_fus + S_wet_wing + S_wet_HT + S_wet_VT + ...
    S_wet_PG1 + S_wet_PG2 + S_wet_nacMLG);
     

%% Compute CD0 due to cooling of electrical systems
% Note that this is simplified approach; should be done pointwise along the
% mission, since heat load varies, and moreover the system has a determined
% drag and not drag-coefficient, so it actually doesn't make sense to
% compute a "CD0" in cruise conditions and then apply it everywhere - would
% lead to low drag values in low speed flight.

% Amount of heat rejected in SIZING condition (conservative? no need to
% size HEX to cool this load?) [W]
if ~isnan(aircraft.Pdes.EM1M); P_cooling_EM1 = aircraft.Pdes.EM1M*(1-p.eta_EM1);
else; P_cooling_EM1 = 0; end
if ~isnan(aircraft.Pdes.EM2M); P_cooling_EM2 = aircraft.Pdes.EM2M*(1-p.eta_EM2);
else; P_cooling_EM2 = 0; end
if ~isnan(aircraft.Pdes.PM); P_cooling_PM = aircraft.Pdes.PM*(1-p.eta_PM);
else; P_cooling_PM = 0; end
if ~isnan(aircraft.Pdes.bat); P_cooling_bat = aircraft.Pdes.bat*(1-p.eta_bat);
else; P_cooling_bat = 0; end
P_cooling_tot = P_cooling_EM1 + P_cooling_EM2 + P_cooling_PM + P_cooling_bat;

% Drag due to cooling [N]
D_cooling = a.cooling_drag*P_cooling_tot;

% Drag coefficient due to cooling (optimistic - in low speed, high power,
% the drag *coefficient* would be higher)
CD0_cooling = D_cooling/(0.5*m.cr.rho*m.cr.v^2*aircraft.Sw);


%% Compute overall CD0

% 1. Compute CD0 using Obert's method where an equivalent aircraft-level C_f 
% is applied to all wetted areas
if strcmp(C2.settings.CD0method,'Obert')
    
    % Compute total wetted area incl. miscellaneous contribution
    S_wet_misc = S_wet*a.S_wet_misc; % Equivalent wetted area for misc drag
    S_wet_tot = S_wet + S_wet_misc;  % Equivalent total wetted area
    
    % Calculate average friction coefficient using data from Obert Fig. 40.17 (wetted area in m2)
    % To be improved using Raymer's component-based method
    xx = [92.90    185.81   325.16   464.52   696.77   929.03   1161.29  ...
        1393.55  1625.80  1858.06  2322.58  2787.09  3251.61];
    yy = [0.003670 0.003510 0.003310 0.003170 0.002995 0.002870 0.002795 ...
        0.002740 0.002700 0.002665 0.002625 0.002610 0.002600];
    Cf_avg = interp1(xx,yy,S_wet,'linear','extrap');
    
    % CD0 (actually minimum drag coefficients)
    CD0 = Cf_avg*S_wet_tot/S_ref;
    
    % Individual CD0 contributions (used later for plotting)
    CD0_fus = CD0*S_wet_fus/S_wet_tot;
    CD0_wing = CD0*S_wet_wing/S_wet_tot;
    CD0_HT = CD0*S_wet_HT/S_wet_tot;
    CD0_VT = CD0*S_wet_VT/S_wet_tot;
    CD0_nacelles1 = CD0*S_wet_PG1/S_wet_tot;
    CD0_nacelles2 = CD0*S_wet_PG2/S_wet_tot;
    CD0_nacellesMLG = CD0*S_wet_nacMLG/S_wet_tot;
    CD0_misc = CD0*S_wet_misc/S_wet_tot;
    
    % Add cooling contribution to actual CD0
    CD0 = CD0 + CD0_cooling;
    
% 2. Compute CD0 using Raymer's component breakdown method. Assuming cruise
% conditions
elseif strcmp(C2.settings.CD0method,'Raymer')
       
    % Viscosity in cruise conditions from Sutherland's law
    [T,~,~,~] = atmosisa(m.cr.h);
    mu = f.mu(T);
    
    % 2.1. Fuselage 
    % k-constant for cutoff reynolds number (Raymer Table 12.5)
    switch C2.fuselage.aero.finish
        case 'smoothPaint';     k = 0.634e-5;
        case 'camouflagePaint'; k = 1.015e-5;
        case 'productionMetal'; k = 0.405e-5;
        case 'smoothMetal';     k = 0.152e-5;
        case 'smoothComposite'; k = 0.052e-5;
        otherwise; error ('Wrong surface finish specified')
    end
    l = aircraft.comps.fuselageGroup.comps.fuselage.geom.l_fus;             % Characteristic length
    D = aircraft.comps.fuselageGroup.comps.fuselage.geom.D_fus;             % Fuselage diameter
    u = deg2rad(aircraft.comps.fuselageGroup.comps.fuselage.geom.upsweepAngle); % Tailcone upsweep [rad]
    Re = l*m.cr.rho*m.cr.v/mu;                                              % Cruise Re
    Re_cutoff = 38.21*(l/k)^1.053;                                          % Cut-off Re for turbulent flow
    Cf_lam = 1.328/sqrt(Re);                                                % Friction coefficient
    Cf_turb = 0.455/((log10(min([Re Re_cutoff])))^2.58*(1 + 0.144*m.cr.M^2)^0.65);
    Cf = C2.fuselage.aero.laminarFraction*Cf_lam + (1-C2.fuselage.aero.laminarFraction)*Cf_turb;
    FF = 0.9 + 5/(l/D)^1.5 + (l/D)/400;                                     % Form factor
    CD0_fus_0 = C2.fuselage.aero.Q*FF*Cf*S_wet_fus/S_ref;                   % Parasite drag excl. upsweep
    CD0_fus_u = 3.83*u^2.5*(pi/4*D^2)/S_ref;                                % Contribution of tailcone upsweep
    CD0_fus = CD0_fus_0 + CD0_fus_u;                                        % Total fuselage parasite drag

    % 2.2. Wing 
    switch C2.wing.aero.finish
        case 'smoothPaint';     k = 0.634e-5;
        case 'camouflagePaint'; k = 1.015e-5;
        case 'productionMetal'; k = 0.405e-5;
        case 'smoothMetal';     k = 0.152e-5;
        case 'smoothComposite'; k = 0.052e-5;
        otherwise; error ('Wrong surface finish specified')
    end
    l = aircraft.comps.wingGroup.comps.wing.geom.MAC.c;                     % Characteristic length
    tc = (aircraft.comps.wingGroup.comps.wing.geom.root.tc + ...            % Average thickness-to-chord ratio
          aircraft.comps.wingGroup.comps.wing.geom.tip.tc)*0.5; 
    xc = (aircraft.comps.wingGroup.comps.wing.geom.root.xc_max_t + ...      % Average location of max t/c
          aircraft.comps.wingGroup.comps.wing.geom.tip.xc_max_t)*0.5;       
    Lambda_t_max = aircraft.comps.wingGroup.comps.wing.geom.sweep_t_max;    % Sweep angle at max t/c
    Re = l*m.cr.rho*m.cr.v/mu;                                              % Cruise Re
    Re_cutoff = 38.21*(l/k)^1.053;                                          % Cut-off Re for turbulent flow
    Cf_lam = 1.328/sqrt(Re);                                                % Friction coefficient
    Cf_turb = 0.455/((log10(min([Re Re_cutoff])))^2.58*(1 + 0.144*m.cr.M^2)^0.65);
    Cf = C2.wing.aero.laminarFraction*Cf_lam + (1-C2.wing.aero.laminarFraction)*Cf_turb;
    FF = (1 + 0.6/xc*tc + 100*tc^4)*1.34*m.cr.M^0.18*(cosd(Lambda_t_max))^0.28; % Form factor
    CD0_wing = C2.wing.aero.Q*FF*Cf*S_wet_wing/S_ref;                        % Total wing parasite drag

%     % TEMP
%     CD0_wing = CD0_wing*1.15

    % 2.3. Horizontal tail (incl. 10% for rudder gap)
    switch C2.tail.HT.aero.finish
        case 'smoothPaint';     k = 0.634e-5;
        case 'camouflagePaint'; k = 1.015e-5;
        case 'productionMetal'; k = 0.405e-5;
        case 'smoothMetal';     k = 0.152e-5;
        case 'smoothComposite'; k = 0.052e-5;
        otherwise; error ('Wrong surface finish specified')
    end
    l = aircraft.comps.tailGroup.comps.HT.geom.MAC.c;                       % Characteristic length
    tc = (aircraft.comps.tailGroup.comps.HT.geom.root.tc + ...              % Average thickness-to-chord ratio
          aircraft.comps.tailGroup.comps.HT.geom.tip.tc)*0.5; 
    xc = (aircraft.comps.tailGroup.comps.HT.geom.root.xc_max_t + ...        % Average location of max t/c
          aircraft.comps.tailGroup.comps.HT.geom.tip.xc_max_t)*0.5;       
    Lambda_t_max = aircraft.comps.tailGroup.comps.HT.geom.sweep_t_max;      % Sweep angle at max t/c
    Re = l*m.cr.rho*m.cr.v/mu;                                              % Cruise Re
    Re_cutoff = 38.21*(l/k)^1.053;                                          % Cut-off Re for turbulent flow
    Cf_lam = 1.328/sqrt(Re);                                                % Friction coefficient
    Cf_turb = 0.455/((log10(min([Re Re_cutoff])))^2.58*(1 + 0.144*m.cr.M^2)^0.65);
    Cf = C2.tail.HT.aero.laminarFraction*Cf_lam + (1-C2.tail.HT.aero.laminarFraction)*Cf_turb;
    FF = (1 + 0.6/xc*tc + 100*tc^4)*1.34*m.cr.M^0.18*(cosd(Lambda_t_max))^0.28; % Form factor
    CD0_HT = C2.tail.HT.aero.Q*FF*Cf*S_wet_HT/S_ref*1.1;                    % Total HT parasite drag
    
    % 2.4. Vertical tail (incl. 10% for elevator gap)
    if strcmp(C2.tail.config,'V-tail')
        CD0_VT = 0;
    else
        switch C2.tail.HT.aero.finish
            case 'smoothPaint';     k = 0.634e-5;
            case 'camouflagePaint'; k = 1.015e-5;
            case 'productionMetal'; k = 0.405e-5;
            case 'smoothMetal';     k = 0.152e-5;
            case 'smoothComposite'; k = 0.052e-5;
            otherwise; error ('Wrong surface finish specified')
        end
        l = aircraft.comps.tailGroup.comps.VT.geom.MAC.c;                       % Characteristic length
        tc = (aircraft.comps.tailGroup.comps.VT.geom.root.tc + ...              % Average thickness-to-chord ratio
            aircraft.comps.tailGroup.comps.VT.geom.tip.tc)*0.5;
        xc = (aircraft.comps.tailGroup.comps.VT.geom.root.xc_max_t + ...        % Average location of max t/c
            aircraft.comps.tailGroup.comps.VT.geom.tip.xc_max_t)*0.5;
        Lambda_t_max = aircraft.comps.tailGroup.comps.VT.geom.sweep_t_max;      % Sweep angle at max t/c
        Re = l*m.cr.rho*m.cr.v/mu;                                              % Cruise Re
        Re_cutoff = 38.21*(l/k)^1.053;                                          % Cut-off Re for turbulent flow
        Cf_lam = 1.328/sqrt(Re);                                                % Friction coefficient
        Cf_turb = 0.455/((log10(min([Re Re_cutoff])))^2.58*(1 + 0.144*m.cr.M^2)^0.65);
        Cf = C2.tail.VT.aero.laminarFraction*Cf_lam + (1-C2.tail.VT.aero.laminarFraction)*Cf_turb;
        FF = (1 + 0.6/xc*tc + 100*tc^4)*1.34*m.cr.M^0.18*(cosd(Lambda_t_max))^0.28; % Form factor
        CD0_VT = C2.tail.VT.aero.Q*FF*Cf*S_wet_VT/S_ref*1.1;                    % Total HT parasite drag
    end

    % 2.5. Nacelles, primary propulsion system (using most inboard nacelle 
    % for reference length and diameter)
    if C2.propulsionGroup1.geom.hasNacelles == 1
        switch C2.propulsionGroup1.aero.finish
            case 'smoothPaint';     k = 0.634e-5;
            case 'camouflagePaint'; k = 1.015e-5;
            case 'productionMetal'; k = 0.405e-5;
            case 'smoothMetal';     k = 0.152e-5;
            case 'smoothComposite'; k = 0.052e-5;
            otherwise; error ('Wrong surface finish specified')
        end
        if isfield(aircraft.comps.fuselageGroup.comps,'propulsionGroup1')
            l = aircraft.comps.fuselageGroup.comps.propulsionGroup1.comps.nacelles{1}.geom.l_nac;
            D = aircraft.comps.fuselageGroup.comps.propulsionGroup1.comps.nacelles{1}.geom.D_nac;
        elseif isfield(aircraft.comps.wingGroup.comps,'propulsionGroup1')
            l = aircraft.comps.wingGroup.comps.propulsionGroup1.comps.nacelles{1}.geom.l_nac;
            D = aircraft.comps.wingGroup.comps.propulsionGroup1.comps.nacelles{1}.geom.D_nac;
        else
            l = 1; % CD0 will be zero anyway since S_wet = 0
            D = 1;
        end
        Re = l*m.cr.rho*m.cr.v/mu;                                            
        Re_cutoff = 38.21*(l/k)^1.053;                                       
        Cf_lam = 1.328/sqrt(Re);                                             
        Cf_turb = 0.455/((log10(min([Re Re_cutoff])))^2.58*(1 + 0.144*m.cr.M^2)^0.65);
        Cf = C2.propulsionGroup1.aero.laminarFraction*Cf_lam + ...
             (1-C2.propulsionGroup1.aero.laminarFraction)*Cf_turb;
        FF = 1 + 0.35/(l/D);
        CD0_nacelles1 = C2.propulsionGroup1.aero.Q*FF*Cf*S_wet_PG1/S_ref;      
    else
        CD0_nacelles1 = 0;
    end
    
    % 2.6. Nacelles, secondary propulsion system
    if C2.propulsionGroup2.geom.hasNacelles == 1
        switch C2.propulsionGroup2.aero.finish
            case 'smoothPaint';     k = 0.634e-5;
            case 'camouflagePaint'; k = 1.015e-5;
            case 'productionMetal'; k = 0.405e-5;
            case 'smoothMetal';     k = 0.152e-5;
            case 'smoothComposite'; k = 0.052e-5;
            otherwise; error ('Wrong surface finish specified')
        end
        if isfield(aircraft.comps.fuselageGroup.comps,'propulsionGroup2')
            l = aircraft.comps.fuselageGroup.comps.propulsionGroup2.comps.nacelles{1}.geom.l_nac;
            D = aircraft.comps.fuselageGroup.comps.propulsionGroup2.comps.nacelles{1}.geom.D_nac;
        elseif isfield(aircraft.comps.wingGroup.comps,'propulsionGroup2')
            l = aircraft.comps.wingGroup.comps.propulsionGroup2.comps.nacelles{1}.geom.l_nac;
            D = aircraft.comps.wingGroup.comps.propulsionGroup2.comps.nacelles{1}.geom.D_nac;
        else
            l = 1; % CD0 will be zero anyway since S_wet = 0
            D = 1;
        end
        Re = l*m.cr.rho*m.cr.v/mu;                                            
        Re_cutoff = 38.21*(l/k)^1.053;                                       
        Cf_lam = 1.328/sqrt(Re);                                             
        Cf_turb = 0.455/((log10(min([Re Re_cutoff])))^2.58*(1 + 0.144*m.cr.M^2)^0.65);
        Cf = C2.propulsionGroup2.aero.laminarFraction*Cf_lam + ...
             (1-C2.propulsionGroup2.aero.laminarFraction)*Cf_turb;
        FF = 1 + 0.35/(l/D);
        CD0_nacelles2 = C2.propulsionGroup2.aero.Q*FF*Cf*S_wet_PG2/S_ref;      
    else
        CD0_nacelles2 = 0;
    end
    
    % 2.7. Nacelles, main landing gear
    if C2.MLG.geom.hasNacelle == 1
        switch C2.MLG.aero.finish
            case 'smoothPaint';     k = 0.634e-5;
            case 'camouflagePaint'; k = 1.015e-5;
            case 'productionMetal'; k = 0.405e-5;
            case 'smoothMetal';     k = 0.152e-5;
            case 'smoothComposite'; k = 0.052e-5;
            otherwise; error ('Wrong surface finish specified')
        end
        l = C2.MLG.geom.l_nac;
        D = 2*C2.MLG.geom.R_nac;
        Re = l*m.cr.rho*m.cr.v/mu;                                            
        Re_cutoff = 38.21*(l/k)^1.053;                                       
        Cf_lam = 1.328/sqrt(Re);                                             
        Cf_turb = 0.455/((log10(min([Re Re_cutoff])))^2.58*(1 + 0.144*m.cr.M^2)^0.65);
        Cf = C2.MLG.aero.laminarFraction*Cf_lam + (1-C2.MLG.aero.laminarFraction)*Cf_turb;
        FF = 1 + 0.35/(l/D);
        CD0_nacellesMLG = C2.MLG.aero.Q*FF*Cf*S_wet_nacMLG/S_ref;      
    else
        CD0_nacellesMLG = 0;
    end

    % 2.8. Misc (leakages and protuberance drag). Note that "misc" drag as
    % defined by Raymer is not included here; only contribution considered
    % applicable is fuselage upsweep, which is included in CD0_fus.
    CD0_excl_misc = CD0_fus + CD0_wing + CD0_HT + CD0_VT + CD0_nacelles1 +...
                   CD0_nacelles2 + CD0_nacellesMLG;
    CD0_misc = CD0_excl_misc*a.S_wet_misc;
    
    % Total parasite drag, including cooling
    CD0 = CD0_excl_misc + CD0_misc + CD0_cooling;
    
% Else error    
else
    error('Wrong CD0 method specified')
end

% % % TEMP: for tech eval
% CD0 = CD0*1.02
% e = e/0.98


%% Compute differences w.r.t. previous estimates and update

% Individual errors
err_CD0 = abs((CD0 - a.CD0_clean)/a.CD0_clean);
if a.CL_minD_clean == 0
    err_CL_minD = abs(CL_minD - a.CL_minD_clean);
else
    err_CL_minD = abs((CL_minD - a.CL_minD_clean)/a.CL_minD_clean);
end
err_e= abs((e - a.e_clean)/a.e_clean);

% Total error
err = mean([err_CD0 err_CL_minD err_e]);

% Overwrite values from input
a.CD0_clean = CD0;
a.CL_minD_clean = CL_minD;
a.e_clean = e;


%% Show results
if C2.settings.plotAero == 1

	% Create figure
    fig = figure;%(s.figStart + size(s.figs,2));
    fig.Name = 'Class-II aerodynamic polar';
%     s.figs(size(s.figs,2)+1) = fig;
    fig.Color = [1 1 1];

	% Subplot 1: breakdown of wetted area contributions
    X = [S_wet_fus S_wet_wing S_wet_HT S_wet_VT S_wet_PG1 S_wet_PG2 S_wet_nacMLG];
    Y = 100*X/S_wet;
    Z = {['Fuselage: ' num2str(Y(1),'%.1f') '%'],...
         ['Wing: ' num2str(Y(2),'%.1f') '%'],...
         ['HT: ' num2str(Y(3),'%.1f') '%'],...
         ['VT: ' num2str(Y(4),'%.1f') '%'],...
         ['Nacelles (PG1): ' num2str(Y(5),'%.1f') '%'],...
         ['Nacelles (PG2): ' num2str(Y(6),'%.1f') '%'],...
         ['Nacelles (MLG): ' num2str(Y(7),'%.1f') '%']};
    explode = [0 0 0 0 0 0 0];
    if sum(X==0)>0
        Z(X==0) = []; explode(X==0) = []; X(X==0) = [];
    end
    subplot(2,2,1)
    pie(X,explode,Z);
    T = title(['Wetted area = ' num2str(S_wet,'%.1f') ' m^2']);
    T.Position(2) = -T.Position(2);

	% Subplot 2: breakdown of drag contributions in cruise
	% NB check we're using the correct parameters here
	CL_cruise = nanmean(MA.cr.aero.CL);
	dCD_cruise = nanmean(MA.cr.aero.CD - MA.cr.aero.CDiso);
	CD_iso_cruise = f.CD(CD0,CL_cruise,CL_minD,a.AR,e);
	CDi_iso_cruise = CD_iso_cruise - CD0;
	CD_cruise = CD_iso_cruise + dCD_cruise;
	bb = [CD0_fus   CD0_wing    CD0_HT  CD0_VT          CD0_nacelles1   CD0_nacelles2   CD0_nacellesMLG ...
          CD0_misc  CD0_cooling CD0     CDi_iso_cruise  dCD_cruise      CD_cruise]*1e4;
    cc = {'C_{D0} (fuselage)','C_{D0} (wing)','C_{D0} (HT)',...
          'C_{D0} (VT)','C_{D0} (nac., PG1)','C_{D0} (nac., PG2)','C_{D0} (nac., MLG)',...
          'C_{D0} (misc)','C_{D0} (cooling)','C_{D0} (total)','C_{Di}','\Delta{C_{D}}','C_{D,tot}'};
    subplot(1,3,3)
    barh(flip(bb)); hold on; grid on;
    ax = gca; ax.YAxis.TickLabels = flip(cc);
    xlabel('Drag coeff. \times 10^4')
    title('Drag breakdown in cruise')
      
	% Subplot 3: L/D vs. CL in cruise conditions
	CLq = linspace(0,2,101);
	CDq_iso = f.CD(CD0,CLq,CL_minD,a.AR,e);
	CDq = CDq_iso + dCD_cruise;
	subplot(2,2,3); hold on; grid on; box on;
	hh(1) = plot(CLq,CLq./CDq_iso,'-b');
	hh(2) = plot(CLq,CLq./CDq,':c');
	hh(3) = plot(CL_cruise,CL_cruise/CD_iso_cruise,'xb');
	hh(4) = plot(CL_cruise,CL_cruise/CD_cruise,'oc');
	xlabel('Total lift coefficient C_L [-]'); ylabel('Lift-to-drag ratio L/D [-]')	
	title(['C_{D0} = ' num2str(CD0,'%.4f') ', C_{L,minD} = ' ...
            num2str(CL_minD,'%.2f') ', e = ' num2str(e,'%.3f')])
    
	% Show which lift coefficients correspond to which altitudes, for the selected cruise
	% speed and wing loading
	h_array = 0:2000:12000;
	[~,a_array,~,rho_array] = atmosisa(h_array);
	v_array = m.cr.M*a_array;
	WS = aircraft.WSdes;
	CL_array = WS./(0.5*rho_array.*v_array.^2);
	CD_iso_array = f.CD(CD0,CL_array,CL_minD,a.AR,e);
	hh(5) = plot(CL_array,CL_array./CD_iso_array,'.k');
        
    % Add legend and altitude values
    strings = cell(1,length(h_array));
    for i = 1:length(h_array)
        strings{i} = [' ' num2str(h_array(i)/1000) ' km'];
    end
    text(CL_array,CL_array./CD_iso_array,strings);
    legend(hh,'Polar, iso.','Polar, incl. {\Delta}''s','Cruise L/D, iso',...
            'Cruise L/D, incl. {\Delta}''s',...
            'Cruise C_L vs. altitude [m]','Location','southeast');
end






