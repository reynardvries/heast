function [geom,P] = CreateLiftingSurfaceGeometryMirrored(geom_in,settings)
%%% Description
% This function is a wrapper around CreateLiftingSurfaceGeometry.m, used to
% model symmetric wings, with one mirrored trapezoidal lifting on each
% side. The output P-structure has the following elements:
%   - P{1}: starboard wing surface
%   - P{2}: starboard wing box
%   - P{3}: port wing surface
%   - P{4}: port wing box
%
%%% Reynard de Vries
%%% First created: 20-03-22
%%% Last modified: 05-04-22


% Prepare input for one semi-wing
geom_in.S = geom_in.S/2;
geom_in.AR = geom_in.AR/2;

% Calculate wing dimensions
[geom,P] = CreateLiftingSurfaceGeometry(geom_in,settings);

% Mirror to create full wing
% Note that by default "b" is defined along the span direction, and not 
% the "true" span in Y direction. Now, the projected aspect ratio is not 
% exactly the input aspect ratio! Because of the cosd(dihedral)
geom.S = geom.S*2;
geom.S_ext = geom.S_ext*2;
geom.S_wet = geom.S_wet*2;
geom.AR = geom.AR*2;
geom.b_spanwise_one_side = geom.b; 
geom.b = 2*geom.b_spanwise_one_side*cosd(geom.dihedral);
P{3} = P{1};
P{4} = P{2};
P{3}(2,:) = -P{3}(2,:);
P{4}(2,:) = -P{4}(2,:);