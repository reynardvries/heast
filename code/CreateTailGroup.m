function [tailGroup,tail] = CreateTailGroup(tail)
%%% Description
% This function estimates the tail geometry for a given area and set of
% input parameters. It performs similar tasks to the wing and fuselage
% groups in ConfigurationAndGeometry.m, but is gathered in a function
% because it's called multiple times in the loop.
%
%%% Reynard de Vries
%%% First created: 22-03-22
%%% Last modified: 22-04-22

% Distinguish configurations
if strcmp(tail.config,'conventional')
    
    % Create initial HT geometry
    [tail.HT.geom,tail.HT.plotting.P] = CreateLiftingSurfaceGeometryMirrored(...
        tail.HT.geom,tail.HT.settings);
    
    % Create initial VT geometry
    [tail.VT.geom,tail.VT.plotting.P] = CreateLiftingSurfaceGeometry(...
        tail.VT.geom,tail.VT.settings);
    
    % Add components to tail-group tree [ADD HERE]
    tailGroup.comps.HT = tail.HT;
    tailGroup.comps.VT = tail.VT;
    
    % Origin of VT in HT reference system
    RefSystemVT = [-tail.xHT_over_cVT*tail.VT.geom.root.c; % Check sign
        0;
        -tail.zHT_over_bVT*tail.VT.geom.b];
    
    % Place VT relative to HT
    for i = 1:length(tail.VT.plotting.P)
        tail.VT.plotting.P{i}(1,:) = tail.VT.plotting.P{i}(1,:) + RefSystemVT(1);
        tail.VT.plotting.P{i}(2,:) = tail.VT.plotting.P{i}(2,:) + RefSystemVT(2);
        tail.VT.plotting.P{i}(3,:) = tail.VT.plotting.P{i}(3,:) + RefSystemVT(3);
    end
    airfoilNames = {'root','tip','MAC'};
    for i = 1:length(airfoilNames)
        tail.VT.geom.(airfoilNames{i}).x_LE = tail.VT.geom.(airfoilNames{i}).x_LE + RefSystemVT(1);
        tail.VT.geom.(airfoilNames{i}).x_c025 = tail.VT.geom.(airfoilNames{i}).x_c025 + RefSystemVT(1);
        tail.VT.geom.(airfoilNames{i}).y_c025 = tail.VT.geom.(airfoilNames{i}).y_c025 + RefSystemVT(2);
        tail.VT.geom.(airfoilNames{i}).z_c025 = tail.VT.geom.(airfoilNames{i}).z_c025 + RefSystemVT(3);
    end
    
    % Preallocate arrays to store component CG info to compute overall group CG
    TGnames = fieldnames(tailGroup.comps);
    masses = NaN(1,length(TGnames));
    coords = NaN(3,length(TGnames));
    
    % Calculate local CG locations in local reference frame. Note: reference
    % lengths currently also from HT-VT fields! [MUST ADAPT IF ADDITIONAL
    % COMPONENTS ARE ADDED TO TAIL GROUP]
    for i = 1:length(TGnames)
        
        % For HT, use HT reference lengths
        if strcmp(TGnames{i},'HT')
            tailGroup.comps.(TGnames{i}).weights.coords_loc(1,1) = ...
                tailGroup.comps.(TGnames{i}).weights.x_over_cHT*...
                tailGroup.comps.(TGnames{i}).geom.root.c;
            tailGroup.comps.(TGnames{i}).weights.coords_loc(2,1) = ...
                tailGroup.comps.(TGnames{i}).weights.y_over_bHT*...
                tailGroup.comps.(TGnames{i}).geom.b;
            tailGroup.comps.(TGnames{i}).weights.coords_loc(3,1) = ...
                tailGroup.comps.(TGnames{i}).weights.z_over_cHT*...
                tailGroup.comps.(TGnames{i}).geom.root.c;
            masses(1,i) = tailGroup.comps.(TGnames{i}).weights.m;
            coords(:,i) = tailGroup.comps.(TGnames{i}).weights.coords_loc;
            
        % For VT, use VT reference lengths, change of coordinate system
        elseif strcmp(TGnames{i},'VT')
            tailGroup.comps.(TGnames{i}).weights.coords_loc(1,1) = ...
                tailGroup.comps.(TGnames{i}).weights.x_over_cVT*...
                tailGroup.comps.(TGnames{i}).geom.root.c + RefSystemVT(1);
            tailGroup.comps.(TGnames{i}).weights.coords_loc(2,1) = ...
                tailGroup.comps.(TGnames{i}).weights.y_over_cVT*...
                tailGroup.comps.(TGnames{i}).geom.root.c + RefSystemVT(2);
            tailGroup.comps.(TGnames{i}).weights.coords_loc(3,1) = ...
                tailGroup.comps.(TGnames{i}).weights.z_over_bVT*...
                tailGroup.comps.(TGnames{i}).geom.b + RefSystemVT(3);
            masses(1,i) = tailGroup.comps.(TGnames{i}).weights.m;
            coords(:,i) = tailGroup.comps.(TGnames{i}).weights.coords_loc;
            
            % Code not yet ready for other components
        else
            error('Code mod required: use which reference lengths?')
        end
    end

elseif strcmpi(tail.config,'V-tail')

    % Compute dihedral angle based on ratio of areas
    tail.HT.geom.dihedral = atand(tail.VT.geom.S/tail.HT.geom.S);

    % Compute V-tail surface area based on horizontal and vertical tail
    % areas, and assign to "HT" element. Add up HT and VT areas, don't use
    % hypothenuse because effectiveness is reduced (see slides AE3211-I)
    tail.HT.geom.S = tail.HT.geom.S + tail.VT.geom.S;

    % Create V-tail geometry in "HT" field
    [tail.HT.geom,tail.HT.plotting.P] = CreateLiftingSurfaceGeometryMirrored(...
        tail.HT.geom,tail.HT.settings);

    % Add components to tail-group tree, setting VT size to zero [ADD HERE]
    tailGroup.comps.HT = tail.HT;
    tailGroup.comps.VT = tail.VT;
    tailGroup.comps.VT.geom.S = 0;
    tailGroup.comps.VT.geom.S_ext = 0;
    tailGroup.comps.VT.geom.S_wet = 0;
    
    % Preallocate arrays to store component CG info to compute overall group CG
    TGnames = fieldnames(tailGroup.comps);
    masses = NaN(1,length(TGnames));
    coords = NaN(3,length(TGnames));
    
    % Calculate local CG locations in local reference frame. Note: reference
    % lengths currently also from HT-VT fields! [MUST ADAPT IF ADDITIONAL
    % COMPONENTS ARE ADDED TO TAIL GROUP]
    for i = 1:length(TGnames)
        
        % For HT, use HT reference lengths
        if strcmp(TGnames{i},'HT')
            tailGroup.comps.(TGnames{i}).weights.coords_loc(1,1) = ...
                tailGroup.comps.(TGnames{i}).weights.x_over_cHT*...
                tailGroup.comps.(TGnames{i}).geom.root.c;
            tailGroup.comps.(TGnames{i}).weights.coords_loc(2,1) = ...
                tailGroup.comps.(TGnames{i}).weights.y_over_bHT*...
                tailGroup.comps.(TGnames{i}).geom.b;
            tailGroup.comps.(TGnames{i}).weights.coords_loc(3,1) = ...
                tailGroup.comps.(TGnames{i}).weights.z_over_cHT*...
                tailGroup.comps.(TGnames{i}).geom.root.c;
            masses(1,i) = tailGroup.comps.(TGnames{i}).weights.m;
            coords(:,i) = tailGroup.comps.(TGnames{i}).weights.coords_loc;
            
        % For VT, use VT reference lengths, change of coordinate system
        elseif strcmp(TGnames{i},'VT')
            tailGroup.comps.(TGnames{i}).weights.coords_loc(1,1) = 0;
            tailGroup.comps.(TGnames{i}).weights.coords_loc(2,1) = 0;
            tailGroup.comps.(TGnames{i}).weights.coords_loc(3,1) = 0;
            masses(1,i) = 0;
            coords(:,i) = tailGroup.comps.(TGnames{i}).weights.coords_loc;

        % Code not yet ready for other components
        else
            error('Code mod required: use which reference lengths?')
        end
    end
    
% Only works with conventional & V-tails for now
else
    error('Other tail configurations have not been implemented yet')
end

% Replace original fields to include shifted reference system (only in case
% shorthand "tail" is used lated on (e.g. in plotting)
tail.HT.weights = tailGroup.comps.HT.weights;
tail.VT.weights = tailGroup.comps.VT.weights;

% Compute group CG & mass
tailGroup.weights.m = sum(masses);
tailGroup.weights.coords_loc = sum(masses.*coords,2)/sum(masses);


